/*******************************************************************************
 * Copyright (c) 2008 - ANSTO/Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * Norman Xiong (nxi@Bragg Institute) - initial API and implementation
 * Tony Lam (nxi@Bragg Institute) - initial API and implementation
 * Majid Ounsy (SOLEIL Synchrotron) - API v2 design and conception
 * Stephane Poirier (SOLEIL Synchrotron) - API v2 design and conception
 * Clement Rodriguez (ALTEN for SOLEIL Synchrotron) - API evolution
 * Gregory VIGUIER (SOLEIL Synchrotron) - API evolution
 * Raphael GIRARDOT (SOLEIL Synchrotron) - API evolution
 ******************************************************************************/
// ****************************************************************************
// Copyright (c) 2008 Australian Nuclear Science and Technology Organisation.
// All rights reserved. This program and the accompanying materials
// are made available under the terms of the Eclipse Public License v1.0
// which accompanies this distribution, and is available at
// http://www.eclipse.org/legal/epl-v10.html
//
// Contributors:
//    Clément Rodriguez - initial API and implementation
// ****************************************************************************
package org.cdma.internal.dictionary.solvers;

/// @cond internal

/**
 * @brief ItemSolver aims to return an IContainer that corresponds to the mapping.
 * 
 *        The ItemSolver class as an unique method which solve. The goal is to return
 *        the IContainer that is defined into the institute's mapping. To do so, it uses
 *        a Context which permits to access the CDMA environment. The IContainer can be
 *        searched in a specific path, or might need a named method execution a to construct it.
 *        <p>
 *        That class is used internally by the LogicalGroup to construct item.
 * 
 * @see org.cdma.dictionary.IPluginMethod
 * @see org.cdma.dictionary.Context
 * 
 * @author rodriguez
 *
 */

import java.util.ArrayList;
import java.util.List;

import org.cdma.IFactory;
import org.cdma.dictionary.Context;
import org.cdma.dictionary.IPluginMethod;
import org.cdma.dictionary.Path;
import org.cdma.dictionary.filter.IFilter;
import org.cdma.interfaces.IAttribute;
import org.cdma.interfaces.IContainer;
import org.cdma.interfaces.IKey;
import org.cdma.internal.dictionary.classloader.PluginMethodManager;
import org.cdma.utils.Utilities.ModelType;
import org.jdom2.Element;

public class ItemSolver {

    private final String view; // Logical view to use
    private IFactory factory; // IFactory instance of the plug-in using this item solver
    private List<Solver> contentSolvers; // List of solvers to process to get IContainer content
    private List<AttributeSolver> attributeSolvers; // List of attribute solvers to process to get IContainer attributes
    private ModelType modelType; // The model type of created item when solved

    private ItemSolver(final String view, IFactory factory, ModelType modelType) {
        this.view = view;
        this.factory = factory;

        // Prepare list of solver
        this.contentSolvers = new ArrayList<>();

        // Prepare list of attribute solvers
        this.attributeSolvers = new ArrayList<>();

        // Set the model type
        this.modelType = modelType;
    }

    public ItemSolver(final String view, IFactory factory, Path path) {
        this(view, factory, ModelType.DataItem);

        // Add the solver to this item solver
        Solver solver = new Solver(view, path);
        this.contentSolvers.add(solver);
    }

    public ItemSolver(final String view, IFactory factory, PluginMethodManager manager, Element elem) {
        this(view, factory, ModelType.DataItem);

        // Initialize internal fields
        init(manager, elem);
    }

    public ItemSolver(final String view, IFactory factory, IKey key) {
        this(view, factory, ModelType.LogicalGroup);

        // Initialize internal fields
        this.contentSolvers.add(new Solver(view, key));
    }

    public List<IContainer> solve(Context context) {
        List<IContainer> valid = new ArrayList<>();
        List<IContainer> found = new ArrayList<>();

        // Get the key from the context
        IKey key = context.getKey();

        // Execute sequentially each solver
        for (Solver solver : contentSolvers) {
            found = solver.solve(context);

            // Update the context with the last found item
            if (found != null && !found.isEmpty()) {
                context.setContainers(found);
            }
        }

        found = context.getContainers();

        // Add attributes defined in the solver
        if (!attributeSolvers.isEmpty()) {
            context.setContainers(new ArrayList<>());
            IAttribute attrib;

            // For each valid container
            for (IContainer container : found) {
                // For each attributes to create
                for (AttributeSolver solver : attributeSolvers) {

                    // Update the context with container to process as a parameter
                    context.setParams(new IContainer[] { container });

                    // Resolve the attribute
                    attrib = solver.solve(context);

                    // Add the attribute to the IContainer
                    if (attrib != null) {
                        container.addOneAttribute(attrib);
                    }
                }
            }
        }

        // Check found items match what is requested by key (filters on key)
        for (IContainer container : found) {
            if (isValidContainer(key, container)) {
                valid.add(container);
            }
        }
        // Update the context with the last found item
        context.setContainers(valid);

        return valid;
    }

    // ---------------------------------------------------------
    // / Private methods
    // ---------------------------------------------------------
    @SuppressWarnings("unchecked")
    private void init(PluginMethodManager manager, Element elem) {
        // Temporary variables
        IPluginMethod method;
        Solver current;

        // List DOM children
        List<?> nodes = elem.getChildren();
        List<?> attrs;

        // For each children of the mapping key item
        for (Element node : (List<Element>) nodes) {
            // If path open the path
            if (node.getName().equals("path")) {
                current = new Solver(view, factory.createPath(node.getText()));
                contentSolvers.add(current);
            }
            // If call on a method
            else if (node.getName().equals("call")) {
                String param = node.getAttributeValue("param");
                String param_args = node.getAttributeValue("param_args");

                method = manager.getPluginMethod(factory.getName(), node.getText());
                current = new Solver(view, method, new String[] { param, param_args });
                contentSolvers.add(current);
            }
            // If attribute
            else if (node.getName().equals("attribute")) {
                // For each children of the mapping attribute
                attrs = node.getChildren();
                String attrName = node.getAttributeValue("name");
                String attrValue = node.getAttributeValue("value");

                AttributeSolver attr = null;
                if (attrValue != null) {
                    attr = new AttributeSolver(attrName, attrValue);
                    ;
                } else {
                    // Create a list of solver for that particular attribute
                    List<Solver> attrSolv = new ArrayList<>();
                    for (Element subNode : (List<Element>) attrs) {
                        // If path open the path
                        if (subNode.getName().equals("path")) {
                            current = new Solver(view, factory.createPath(subNode.getText()));
                            attrSolv.add(current);
                        }
                        // If call on a method
                        else if (subNode.getName().equals("call")) {
                            method = manager.getPluginMethod(factory.getName(), subNode.getText());
                            current = new Solver(view, method);
                            attrSolv.add(current);
                        }
                    }
                    attr = new AttributeSolver(attrName, attrSolv);
                }
                // Store the attribute into a list
                attributeSolvers.add(attr);
            }
        }
    }

    /**
     * Check if the IContainer is conform to the IKey's filters
     * 
     * @param key IKey with filters if any
     * @param container IContainer to check
     * @return true if the IContainer is compliant with the IKey
     */
    private boolean isValidContainer(IKey key, IContainer container) {
        boolean result = true;
        if (container != null) {
            for (IFilter filter : key.getFilterList()) {
                if ((filter != null) && (!filter.matches(container))) {
                    result = false;
                    break;
                }
            }
        }
        return result;
    }

    /**
     * Returns the model type of instantiated object when solve is called
     * 
     * @return the ModelType of item
     */
    public ModelType getModelType() {
        return modelType;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(getClass().getName());
        builder.append("(view:'").append(view).append("', factory: '").append(factory).append("', modelType: '")
                .append(modelType).append("', contentSolvers: '").append(contentSolvers)
                .append("', attributeSolvers: '").append(attributeSolvers).append("')@").append(hashCode());
        return builder.toString();
    }
}

// / @endcond internal

/*******************************************************************************
 * Copyright (c) 2008 - ANSTO/Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * Norman Xiong (nxi@Bragg Institute) - initial API and implementation
 * Tony Lam (nxi@Bragg Institute) - initial API and implementation
 * Majid Ounsy (SOLEIL Synchrotron) - API v2 design and conception
 * Stephane Poirier (SOLEIL Synchrotron) - API v2 design and conception
 * Clement Rodriguez (ALTEN for SOLEIL Synchrotron) - API evolution
 * Gregory VIGUIER (SOLEIL Synchrotron) - API evolution
 * Raphael GIRARDOT (SOLEIL Synchrotron) - API evolution
 ******************************************************************************/
package org.cdma.internal.dictionary.readers;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import org.cdma.Factory;
import org.cdma.IFactory;
import org.cdma.dictionary.Concept;
import org.cdma.exception.UriAccessException;
import org.cdma.internal.dictionary.classloader.PluginMethodManager;
import org.cdma.internal.dictionary.solvers.ItemSolver;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.input.sax.XMLReaders;

import fr.soleil.lib.project.ObjectUtils;

public class DictionaryReader {

    private static volatile PluginMethodManager mMethodMgr;
    private static volatile DataManager mDataManager;
    private String mViewSrc; // View file to read
    private String mConceptSrc; // Concept file to read
    private boolean mLoadSynonyms; // Have synonyms to be reloaded

    public DictionaryReader(String viewFile) {
        mMethodMgr = PluginMethodManager.instantiate();
        mDataManager = DataManager.instantiate();
        mViewSrc = viewFile;
        mConceptSrc = null;
        mLoadSynonyms = false;
    }

    public DataView getView() throws UriAccessException {
        DataView result = mDataManager.getView(mViewSrc);
        if (result == null) {
            init();
            result = mDataManager.getView(mViewSrc);
        }
        return result;
    }

    public DataMapping getMapping(String view, IFactory factory, String mappingFile) throws UriAccessException {
        DataMapping result = mDataManager.getMapping(mappingFile);
        if (result == null) {
            readMappingFile(view, factory, mappingFile);
            result = mDataManager.getMapping(mappingFile);
        }
        return result;
    }

    public DataConcepts getConcepts() throws UriAccessException {
        DataConcepts result = mDataManager.getConcept(mViewSrc);
        if (result == null) {
            init();
            result = mDataManager.getConcept(mViewSrc);
        }
        return result;
    }

    public void init() throws UriAccessException {
        readConceptsFile();
        readViewFile();
        readSynonyms();
    }

    public static String getDictionaryName(String file) throws UriAccessException {
        Element root = saxBuildFile(file);
        return root.getAttributeValue("name");
    }

    // ---------------------------------------------------------------
    // Protected methods
    // ---------------------------------------------------------------
    protected void readConceptsFile() throws UriAccessException {
        // Check the need of reading concepts file
        if (mDataManager.getConcept(mViewSrc) == null) {
            mLoadSynonyms = true;
            try {
                // Get the specific concept file's path if any
                Element startNode = saxBuildFile(mViewSrc);
                if (startNode != null) {
                    String concept = startNode.getAttributeValue("concept");
                    if (concept != null) {
                        mConceptSrc = Factory.getPathConceptDictionaryFolder() + concept;
                    }
                }

                // Read concept files
                Collection<Concept> list = readConceptsFiles();
                mDataManager.registerConcept(mViewSrc, new DataConcepts(list));
            } catch (UriAccessException e) {
                throw new UriAccessException("Error while reading the concept file: " + e.getMessage(), e);
            }
        }
    }

    protected void readMappingFile(String view, IFactory factory, String file) throws UriAccessException {
        DataMapping mapping;
        try {
            Element root = saxBuildFile(file);
            if (root == null) {
                mapping = new DataMapping(ObjectUtils.EMPTY_STRING);
            } else {
                String version = root.getAttributeValue("version");

                mapping = new DataMapping(version);

                List<Element> nodes = root.getChildren("item");
                String keyName;

                // Updating the KeyID / Path map
                for (Element elem : nodes) {
                    // Get the key name
                    keyName = elem.getAttributeValue("key");

                    // Add the corresponding mapping
                    mapping.addSolver(keyName, new ItemSolver(view, factory, mMethodMgr, elem));
                }
            }
            mDataManager.registerMapping(file, mapping);
        } catch (UriAccessException e) {
            throw new UriAccessException("Error while reading the mapping file: " + e.getMessage(), e);
        }
    }

    protected void readViewFile() throws UriAccessException {
        if (mDataManager.getView(mViewSrc) == null) {
            mLoadSynonyms = true;
            try {
                // Read view dictionary
                Element root = saxBuildFile(mViewSrc);
                DataView view;
                if (root != null) {
                    view = readViewFile(root);
                    view.setName(root.getAttributeValue("name"));

                } else {
                    view = new DataView();
                }
                mDataManager.registerView(mViewSrc, view);
            } catch (UriAccessException e) {
                throw new UriAccessException("Error while reading the view file: " + e.getMessage(), e);
            }
        }
    }

    protected void readSynonyms() throws UriAccessException {
        if (mLoadSynonyms) {
            mLoadSynonyms = false;
            try {
                // Read view dictionary
                Element root = saxBuildFile(mViewSrc);
                if (root != null) {
                    String synonymFile = root.getAttributeValue("synonym");
                    if (synonymFile != null && !synonymFile.trim().isEmpty()) {
                        File file = new File(mViewSrc);
                        String fileName = file.getParentFile().getAbsolutePath();
                        readSynonyms(fileName + "/" + synonymFile);
                    }
                }
            } catch (UriAccessException e) {
                throw new UriAccessException("Error while reading the synonyms file: " + e.getMessage(), e);
            }
        }
    }

    // ---------------------------------------------------------------
    // PRIVATE : Reading methods
    // ---------------------------------------------------------------
    private Collection<Concept> readConceptsFiles() throws UriAccessException {
        Collection<Concept> concepts = Collections.newSetFromMap(new ConcurrentHashMap<Concept, Boolean>());

        String commonFile = Factory.getPathCommonConceptDictionary();
        // Read common concept file
        if (commonFile != null) {
            concepts.addAll(readConceptsFile(commonFile));
        }

        // Read specific concept file
        concepts.addAll(readConceptsFile(mConceptSrc));

        return concepts;
    }

    private Collection<Concept> readConceptsFile(String filePath) throws UriAccessException {
        // Check file exists
        Collection<Concept> concepts = Collections.newSetFromMap(new ConcurrentHashMap<Concept, Boolean>());
        Element elem = saxBuildFile(filePath);
        if (elem != null) {
            Concept concept;
            List<?> nodes = elem.getChildren("concept");
            for (Object child : nodes) {
                elem = (Element) child;
                concept = Concept.instantiate(elem);
                if (concept != null) {
                    concepts.add(concept);
                }
            }
        }

        return concepts;
    }

    private DataView readViewFile(Element startNode) throws UriAccessException {
        DataView view = new DataView();

        // List all child nodes
        List<Element> nodes = startNode.getChildren();
        String keyName;

        // For each element of the view (item or group)
        for (Element current : nodes) {
            // Get the key name
            keyName = current.getAttributeValue("key");
            if (keyName != null && !keyName.isEmpty()) {
                // If the element is an item
                if (current.getName().equals("item")) {
                    view.addKey(keyName);
                }
                // If the element is a group of keys
                else if (current.getName().equals("group")) {
                    // Read the sub view
                    DataView subView = readViewFile(current);

                    // Add it to current view
                    view.addView(keyName, subView);
                }
            }
        }
        return view;
    }

    private void readSynonyms(String synonymFile) throws UriAccessException {
        Element root = saxBuildFile(synonymFile);
        if (root != null) {
            String plugID, key, mapping;
            for (Element plugin : root.getChildren("plugin")) {
                plugID = plugin.getAttributeValue("name");

                for (Element synonym : plugin.getChildren("synonym")) {
                    // Get synonym values
                    key = synonym.getAttributeValue("key");
                    mapping = synonym.getAttributeValue("mapping");

                    // Add the synonym
                    addSynonym(key, mapping, plugID);
                }
            }
        }
    }

    private void addSynonym(String key, String mapping, String plugID) throws UriAccessException {
        DataConcepts concepts = getConcepts();
        Concept concept = concepts.getConcept(key);
        if (concept != null) {
            if (!concept.getSynonymList().contains(mapping)) {
                concept.addSynonym(mapping, plugID);
            }
        } else {
            concept = new Concept(key);
            concept.addSynonym(mapping, plugID);
            concepts.addConcept(concept);
        }
    }

    /**
     * Check the given file exists and open the root node
     * 
     * @param filePath XML file to be opened
     * @return Sax element that is the root of the XML file
     * @throws UriAccessException
     */
    static private Element saxBuildFile(String filePath) throws UriAccessException {
        Element result;
        // Check the file path isn't empty
        if (filePath == null || filePath.isEmpty()) {
            result = null;
        } else {
            // Check the file path is a valid XML file
            File dicFile = new File(filePath);
            if (!dicFile.exists()) {
                throw new UriAccessException("the target file does not exist: " + filePath);
            } else if (dicFile.isDirectory()) {
                throw new UriAccessException("the target is a folder, not an XML file: " + filePath);
            }

            // Open the XML file to get its root element
            SAXBuilder xmlFile = new SAXBuilder(XMLReaders.DTDVALIDATING);
            Document document;
            try {
                document = xmlFile.build(dicFile);
            } catch (JDOMException e) {
                throw new UriAccessException("error while parsing file: " + filePath + "\n" + e.getMessage(), e);
            } catch (IOException e) {
                throw new UriAccessException("an I/O error prevent parsing file: " + filePath + "\n" + e.getMessage(),
                        e);
            }
            result = document.getRootElement();
        }
        return result;
    }
}

/*******************************************************************************
 * Copyright (c) 2008 - ANSTO/Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * Norman Xiong (nxi@Bragg Institute) - initial API and implementation
 * Tony Lam (nxi@Bragg Institute) - initial API and implementation
 * Majid Ounsy (SOLEIL Synchrotron) - API v2 design and conception
 * Stephane Poirier (SOLEIL Synchrotron) - API v2 design and conception
 * Clement Rodriguez (ALTEN for SOLEIL Synchrotron) - API evolution
 * Gregory VIGUIER (SOLEIL Synchrotron) - API evolution
 * Raphael GIRARDOT (SOLEIL Synchrotron) - API evolution
 ******************************************************************************/
// ****************************************************************************
// Copyright (c) 2008 Australian Nuclear Science and Technology Organisation.
// All rights reserved. This program and the accompanying materials
// are made available under the terms of the Eclipse Public License v1.0 
// which accompanies this distribution, and is available at
// http://www.eclipse.org/legal/epl-v10.html
// 
// Contributors: 
//    Clément Rodriguez (clement.rodriguez@synchrotron-soleil.fr)
// ****************************************************************************
package org.cdma.arrays;

import org.cdma.exception.InvalidRangeException;
import org.cdma.interfaces.IRange;

public final class DefaultRange implements IRange, Cloneable {
    public static final DefaultRange EMPTY = new DefaultRange();
    public static final DefaultRange VLEN = new DefaultRange(-1);

    // Members
    private long last; // number of elements
    private long first; // first value in range
    private long stride; // stride, must be >= 1
    private final String name; // optional name
    private boolean reduced; // was this ranged reduced or not

    // Constructors
    /**
     * Used for EMPTY
     */
    private DefaultRange() {
        this(null, 0, 0, 1, false);
    }

    /**
     * Create a range starting at zero, with an unit stride of length "length".
     * 
     * @param length number of elements in the NxsRange
     */
    public DefaultRange(int length) {
        this(null, 0, length - 1, 1, false);
    }

    /**
     * Create a range with a specified stride.
     *
     * @param name name of the range
     * @param first first index in range
     * @param last last index in range, inclusive
     * @param stride stride between consecutive elements, must be > 0
     */
//    * @throws InvalidRangeException elements must be nonnegative: 0 <= first <= last, stride > 0
    public DefaultRange(String name, long first, long last, long stride) {
        this(name, first, last, stride, false);
    }

    public DefaultRange(String name, long first, long last, long stride, boolean reduced) {
        this.name = name;
        this.first = first;
        this.last = last;
        this.stride = Math.max(stride, 1);
        this.reduced = reduced;
    }

    public DefaultRange(IRange range) {
        this(range.getName(), range.first(), range.last(), range.stride(), false);
    }

    // / Accessors
    @Override
    public String getName() {
        return name;
    }

    @Override
    public long first() {
        return first;
    }

    @Override
    public long last() {
        return last;
    }

    @Override
    public int length() {
        return (int) ((last - first) / stride) + 1;
    }

    @Override
    public long stride() {
        return stride;
    }

    public void stride(long value) {
        stride = Math.max(value, 1);
    }

    public void last(long value) {
        last = value;
    }

    public void first(long value) {
        first = value;
    }

    // / Methods
    @Override
    public DefaultRange clone() {
        DefaultRange range;
        try {
            range = (DefaultRange) super.clone();
        } catch (CloneNotSupportedException e) {
            // Should not happen as DefaultRange implements Cloneable
            e.printStackTrace();
            range = DefaultRange.EMPTY;
        }
        return range;
    }

    @Override
    public IRange compact() throws InvalidRangeException {
        long first, last, stride;
        String name;

        stride = 1;
        first = this.first / this.stride;
        last = this.last / this.stride;
        name = this.name;

        return new DefaultRange(name, first, last, stride);
    }

    @Override
    public IRange compose(IRange r) throws InvalidRangeException {
        if ((length() == 0) || (r.length() == 0)) {
            return EMPTY;
        }
        if (this.equals(VLEN) || r.equals(VLEN)) {
            return VLEN;
        }

        long first = element(r.first());
        long stride = stride() * r.stride();
        long last = element(r.last());
        return new DefaultRange(name, first, last, stride);
    }

    @Override
    public boolean contains(int i) {
        boolean contains;
        if (i < first()) {
            contains = false;
        } else if (i > last()) {
            contains = false;
        } else if (stride == 1) {
            contains = true;
        } else {
            contains = (i - first) % stride == 0;
        }
        return contains;
    }

    @Override
    public int element(long i) throws InvalidRangeException {
        if (i < 0) {
            throw new InvalidRangeException("i must be >= 0");
        }
        if (i > last) {
            throw new InvalidRangeException("i must be < length");
        }

        return (int) (first + i * stride);
    }

    @Override
    public int getFirstInInterval(int start) {
        long result;
        if (start > last()) {
            result = -1;
        } else if (start <= first) {
            result = first;
        } else if (stride == 1) {
            result = start;
        } else {
            long offset = start - first;
            long incr = offset % stride;
            result = start + incr;
            result = (result > last()) ? -1 : result;
        }
        return (int) result;
    }

    @Override
    public long index(int elem) throws InvalidRangeException {
        if (elem < first) {
            throw new InvalidRangeException("elem must be >= first");
        }
        long result = (elem - first) / stride;
        if (result > last) {
            throw new InvalidRangeException("elem must be <= last = n * stride");
        }
        return (int) result;
    }

    @Override
    public IRange intersect(IRange r) throws InvalidRangeException {
        IRange range;
        if ((length() == 0) || (r.length() == 0)) {
            range = EMPTY;
        } else if (this.equals(VLEN) || r.equals(VLEN)) {
            range = VLEN;
        } else {

            long last = Math.min(this.last(), r.last());
            long stride = stride() * r.stride();

            long useFirst;
            if (stride == 1) {
                useFirst = Math.max(this.first(), r.first());
            } else if (stride() == 1) { // then r has a stride
                if (r.first() >= first()) {
                    useFirst = r.first();
                } else {
                    long incr = (first() - r.first()) / stride;
                    useFirst = r.first() + incr * stride;
                    if (useFirst < first()) {
                        useFirst += stride;
                    }
                }
            } else if (r.stride() == 1) { // then this has a stride
                if (first() >= r.first()) {
                    useFirst = first();
                } else {
                    long incr = (r.first() - first()) / stride;
                    useFirst = first() + incr * stride;
                    if (useFirst < r.first()) {
                        useFirst += stride;
                    }
                }
            } else {
                throw new UnsupportedOperationException("Intersection when both ranges have a stride");
            }
            if (useFirst > last) {
                range = EMPTY;
            } else {
                range = new DefaultRange(name, useFirst, last, stride);
            }
        }
        return range;
    }

    @Override
    public boolean intersects(IRange r) {
        if ((length() == 0) || (r.length() == 0)) {
            return false;
        }
        if (this.equals(VLEN) || r.equals(VLEN)) {
            return true;
        }

        long last = Math.min(this.last(), r.last());
        long stride = stride() * r.stride();

        long useFirst;
        if (stride == 1) {
            useFirst = Math.max(this.first(), r.first());
        } else if (stride() == 1) { // then r has a stride
            if (r.first() >= first()) {
                useFirst = r.first();
            } else {
                long incr = (first() - r.first()) / stride;
                useFirst = r.first() + incr * stride;
                if (useFirst < first()) {
                    useFirst += stride;
                }
            }
        } else if (r.stride() == 1) { // then this has a stride
            if (first() >= r.first()) {
                useFirst = first();
            } else {
                long incr = (r.first() - first()) / stride;
                useFirst = first() + incr * stride;
                if (useFirst < r.first()) {
                    useFirst += stride;
                }
            }
        } else {
            throw new UnsupportedOperationException("Intersection when both ranges have a stride");
        }
        return (useFirst <= last);
    }

    @Override
    public IRange shiftOrigin(int origin) throws InvalidRangeException {
        return new DefaultRange(name, first + origin, last + origin, stride, reduced);
    }

    @Override
    public IRange union(IRange r) throws InvalidRangeException {
        IRange range;
        if (r.stride() != stride) {
            throw new InvalidRangeException("Stride must identical to make a IRange union!");
        } else if (length() == 0) {
            range = r;
        } else if (this.equals(VLEN) || r.equals(VLEN)) {
            range = VLEN;
        } else if (r.length() == 0) {
            range = this;
        } else {
            long first, last;
            String name = this.name;

            // Seek the smallest value
            first = Math.min(this.first, r.first());
            last = Math.max(this.last, r.last());

            range = new DefaultRange(name, first, last, this.stride);
        }
        return range;
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append("name: '").append(getName());
        str.append("', first: ").append(first());
        str.append(", last: ").append(last());
        str.append(", stride: ").append(stride());
        str.append(", length: ").append(length());
        str.append(", reduce: ").append(reduced);
        return str.toString();
    }

    public boolean reduced() {
        return reduced;
    }

    public void reduced(boolean reduce) {
        reduced = reduce;
    }
}

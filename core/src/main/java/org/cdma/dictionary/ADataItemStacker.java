package org.cdma.dictionary;

import java.util.List;

import org.cdma.interfaces.IContainer;
import org.cdma.interfaces.IDataItem;

/**
 * Stacks all found data items to construct some aggregated IDataItem(s).
 *
 * @param <D> The type of {@link IDataItem} produced by this {@link ADataItemSingleStacker}.
 * @author GIRARDOT
 */
public abstract class ADataItemStacker<D extends IDataItem> implements IPluginMethod {

    public ADataItemStacker() {
        super();
    }

    protected void prepareNodes(final List<IContainer> nodes) {
        // nothing do do by default
    }

    /**
     * Checks whether a node can be considered as valid by this {@link ADataItemStacker}, according to given
     * {@link Context}.
     * 
     * @param node The node to check.
     * @param context The {@link Context}.
     * @return A <code>boolean</code>: <code>true</code> for valid.
     */
    protected boolean isValidNode(final IContainer node, final Context context) {
        // by default, always consider a node as valid
        return true;
    }

    /**
     * Builds an aggregated item from a {@link List} of {@link IDataItem}s, according to given {@link Context}.
     * 
     * @param items The {@link IDataItem} {@link List}.
     * @param context The {@link Context}.
     * @return An aggregated item.
     */
    protected abstract D buildDataItem(List<IDataItem> items, final Context context);

}

/*******************************************************************************
 * Copyright (c) 2008 - ANSTO/Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * Norman Xiong (nxi@Bragg Institute) - initial API and implementation
 * Tony Lam (nxi@Bragg Institute) - initial API and implementation
 * Majid Ounsy (SOLEIL Synchrotron) - API v2 design and conception
 * Stephane Poirier (SOLEIL Synchrotron) - API v2 design and conception
 * Clement Rodriguez (ALTEN for SOLEIL Synchrotron) - API evolution
 * Gregory VIGUIER (SOLEIL Synchrotron) - API evolution
 * Raphaël GIRARDOT (SOLEIL Synchrotron) - API evolution
 ******************************************************************************/
package org.cdma.dictionary;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.cdma.exception.CDMAException;
import org.cdma.exception.DataAccessException;
import org.cdma.interfaces.IArray;
import org.cdma.interfaces.IAttribute;
import org.cdma.interfaces.IContainer;
import org.cdma.interfaces.IDataItem;
import org.cdma.utils.Utilities.ModelType;

public abstract class ANumberCorrector<I extends IDataItem, A extends IArray, D> implements IPluginMethod {

    /**
     * Creates a new {@link IDataItem}, with the same name, parent etc. as <code>item</code>, but neither its data nor
     * its attributes.
     * 
     * @param originalItem The {@link IDataItem} for which to prepare a copy
     * @return An {@link IDataItem}
     * @throws CDMAException In case a problem occurred
     */
    protected abstract I prepareItemCopy(IDataItem originalItem) throws CDMAException;

    /**
     * Creates a new {@link IArray} with a given flat value and shape
     * 
     * @param value The flat value
     * @param shape The shape
     * @return An {@link IArray}
     * @throws CDMAException In case a problem occurred
     */
    protected abstract A generateArray(double[] value, int[] shape) throws CDMAException;

    /**
     * Extracts this plugin's parameters from given {@link Context}
     * 
     * @param context The {@link Context} from which to extract parameters
     * @return An {@link Object}
     */
    protected abstract D extractParameters(Context context);

    /**
     * Returns the corrected value of a given value, knowing given parameters
     * 
     * @param value The value to correct
     * @param parameters The parameters
     * @return A <code>double</code>.
     */
    protected abstract double getCorrectedValue(double value, D parameters);

    @Override
    public void execute(Context context) throws CDMAException {
        List<IContainer> inList = context.getContainers();
        List<IContainer> outList = new ArrayList<>();

        IDataItem item;

        D parameters = extractParameters(context);

        for (IContainer container : inList) {
            if (container.getModelType().equals(ModelType.DataItem)) {
                IDataItem originalItem = (IDataItem) container;
                Object inlineOriginalArray = null;
                item = prepareItemCopy(originalItem);
                if (item != originalItem) {
                    Collection<IAttribute> attributes = originalItem.getAttributeList();
                    if ((attributes != null) && (!attributes.isEmpty())) {
                        for (IAttribute attribute : attributes) {
                            if (attribute != null) {
                                item.addOneAttribute(attribute);
                            }
                        }
                    }
                }
                try {
                    inlineOriginalArray = originalItem.getData().getArrayUtils().copyTo1DJavaArray();
                    int[] originalShape = originalItem.getData().getShape();

                    int length = Array.getLength(inlineOriginalArray);
                    double[] finalArray = new double[length];

                    for (int i = 0; i < length; i++) {
                        double singleValue;
                        Object value = Array.get(inlineOriginalArray, i);
                        if (value instanceof Number) {
                            singleValue = ((Number) value).doubleValue();
                        } else {
                            singleValue = Double.valueOf(String.valueOf(value));
                        }
                        finalArray[i] = getCorrectedValue(singleValue, parameters);
                    }
                    A inlineArray = generateArray(finalArray,
                            originalShape == null ? new int[] { length, 1 } : originalShape.clone());
                    item.setCachedData(inlineArray, false);
                } catch (DataAccessException e) {
                    e.printStackTrace();
                }
                outList.add(item);
            }
        }

        // Update context
        context.setContainers(outList);
    }
}

/*******************************************************************************
 * Copyright (c) 2008 - ANSTO/Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * Norman Xiong (nxi@Bragg Institute) - initial API and implementation
 * Tony Lam (nxi@Bragg Institute) - initial API and implementation
 * Majid Ounsy (SOLEIL Synchrotron) - API v2 design and conception
 * Stephane Poirier (SOLEIL Synchrotron) - API v2 design and conception
 * Clement Rodriguez (ALTEN for SOLEIL Synchrotron) - API evolution
 * Gregory VIGUIER (SOLEIL Synchrotron) - API evolution
 * Raphael GIRARDOT (SOLEIL Synchrotron) - API evolution
 ******************************************************************************/
// ****************************************************************************
// Copyright (c) 2008 Australian Nuclear Science and Technology Organisation.
// All rights reserved. This program and the accompanying materials
// are made available under the terms of the Eclipse Public License v1.0
// which accompanies this distribution, and is available at
// http://www.eclipse.org/legal/epl-v10.html
//
// Contributors:
//    Norman Xiong (nxi@Bragg Institute) - initial API and implementation
//    Tony Lam (nxi@Bragg Institute) - initial API and implementation
//    Clement Rodriguez (ALTEN for SOLEIL Synchrotron) - API evolution
// ****************************************************************************
package org.cdma.dictionary;

import java.util.ArrayList;
import java.util.List;

import org.cdma.interfaces.IContainer;
import org.cdma.interfaces.IDataset;
import org.cdma.interfaces.IKey;
import org.cdma.internal.dictionary.solvers.Solver;

public final class Context {
    private IDataset dataset;
    private IContainer caller;
    private IKey key;
    private Object[] params;
    private List<IContainer> containers;
    private final List<Solver> solvers;
    private Concept concept;

    public Context(final IDataset dataset) {
        this(dataset, null, null);
    }

    public Context(final IDataset dataset, final IContainer caller, final IKey key) {
        this.dataset = dataset;
        this.caller = caller;
        this.key = key;
        this.solvers = new ArrayList<>();
        this.params = null;
        this.containers = new ArrayList<>();
        this.concept = null;
    }

    /**
     * Permits to get the IDataset we want to work on.
     */
    public IDataset getDataset() {
        return dataset;
    }

    /**
     * Permits to set the IDataset we want to work on.
     */
    public void setDataset(final IDataset dataset) {
        this.dataset = dataset;
    }

    /**
     * Permits to get the IContainer that instantiated the context.
     */
    public IContainer getCaller() {
        return caller;
    }

    /**
     * Permits to set the IContainer that instantiated the context.
     */
    public void setCaller(final IContainer caller) {
        this.caller = caller;
    }

    /**
     * Permits to get the IKey that lead to this instantiation.
     */
    public IKey getKey() {
        return key;
    }

    /**
     * Permits to set the IKey that lead to this instantiation.
     */
    public void setKey(final IKey key) {
        this.key = key;
    }

    /**
     * Permits to get the Solver list corresponding to the
     * IKey that have been previously executed
     */
    public List<Solver> getSolver() {
        return solvers;
    }

    /**
     * Permits to add a Solver corresponding to the IKey
     */
    public void addSolver(final Solver solver) {
        solvers.add(solver);
    }

    /**
     * Permits to have some parameters that are defined by the instantiating plug-in
     * and that can be useful for the method using this context.
     * 
     * @return array of object
     */
    public Object[] getParams() {
        return params.clone();
    }

    /**
     * Permits to set some parameters that are defined by the instantiating plug-in
     * and that can be useful for the method using this context.
     * 
     * @return array of object
     */
    public void setParams(final Object[] params) {
        if (params != null) {
            this.params = params.clone();
        }
    }

    /**
     * Get the list of found Containers by previously executed solver
     * 
     * @return list of containers
     */
    public List<IContainer> getContainers() {
        return containers;
    }

    /**
     * Clear the list of found Containers
     */
    public void clearContainers() {
        if (containers != null) {
            containers.clear();
        }
    }

    /**
     * Set the list of found containers
     */
    public void setContainers(final List<IContainer> items) {
        this.containers = items;
    }

    /**
     * Add a Container to the list of found Containers
     */
    public void addContainer(final IContainer item) {
        containers.add(item);
    }

    /**
     * Returns the concept that is expected for this context.
     * 
     * @return concept that the system expects to have
     */
    public Concept getConcept() {
        return concept;
    }

    /**
     * Set the concept that is expected by the context user.
     * 
     * @param concept to have at end of the process
     */
    public void setConcept(final Concept concept) {
        this.concept = concept;
    }
}

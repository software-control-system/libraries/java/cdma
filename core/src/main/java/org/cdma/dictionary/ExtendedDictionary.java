/*******************************************************************************
 * Copyright (c) 2008 - ANSTO/Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * Norman Xiong (nxi@Bragg Institute) - initial API and implementation
 * Tony Lam (nxi@Bragg Institute) - initial API and implementation
 * Majid Ounsy (SOLEIL Synchrotron) - API v2 design and conception
 * Stephane Poirier (SOLEIL Synchrotron) - API v2 design and conception
 * Clement Rodriguez (ALTEN for SOLEIL Synchrotron) - API evolution
 * Gregory VIGUIER (SOLEIL Synchrotron) - API evolution
 * Raphael GIRARDOT (SOLEIL Synchrotron) - API evolution
 ******************************************************************************/
// ****************************************************************************
// Copyright (c) 2008 Australian Nuclear Science and Technology Organisation.
// All rights reserved. This program and the accompanying materials
// are made available under the terms of the Eclipse Public License v1.0 
// which accompanies this distribution, and is available at
// http://www.eclipse.org/legal/epl-v10.html
// 
// Contributors: 
//    Norman Xiong (nxi@Bragg Institute) - initial API and implementation
//    Tony Lam (nxi@Bragg Institute) - initial API and implementation
//    Clement Rodriguez (ALTEN for SOLEIL Synchrotron) - API evolution
// ****************************************************************************
package org.cdma.dictionary;

// JAVA imports
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.cdma.Factory;
import org.cdma.IFactory;
import org.cdma.exception.UriAccessException;
import org.cdma.interfaces.IKey;
import org.cdma.internal.IModelObject;
import org.cdma.internal.dictionary.readers.DataConcepts;
import org.cdma.internal.dictionary.readers.DataMapping;
import org.cdma.internal.dictionary.readers.DataView;
import org.cdma.internal.dictionary.readers.DictionaryReader;
import org.cdma.internal.dictionary.solvers.ItemSolver;

/**
 * @brief ExtendedDictionary class is the logical representation of a IDataset.
 * 
 *        It defines how data is logically structured and permits a standardized browsing what
 *        ever the plug-in, the data source format or its structure is. <br/>
 *        The dictionary is compound of two element a key file that defines the representation
 *        of the dataset and a mapping file that associates
 *        Association of objects is the following: <br/>
 *        - IKey and Path for a IDataItem, <br/>
 *        - IKey and ExtendedDictionary for a LogicalGroup.
 */

public final class ExtendedDictionary implements IModelObject, Cloneable {

    private final IFactory factory; // Name of the plug-in's factory that created this object
    private String view; // View matching that dictionary
//    private String   mVersion;     // Version of the read dictionary
    private final String keyFile; // Path to reach the key file (containing the view)
    private final String mapFile; // Path to reach the mapping file
    private IKey[] keyPath;

    private DictionaryReader mReader;

    private Map<IKey, String> keys;
    private Map<String, ItemSolver> maps;

    /**
     * Create an empty dictionary
     * 
     * @param factory
     */
    public ExtendedDictionary(IFactory factory, String view) {
        this.factory = factory;
        this.view = view;
        this.keyFile = null;
        this.mapFile = null;
        this.keyPath = new IKey[] {};
        this.keys = null;
        this.maps = null;
    }

    public ExtendedDictionary(IFactory factory, String keyFile, String mapFile) {
        this.factory = factory;
        this.view = null;
        this.keyFile = keyFile;
        this.mapFile = mapFile;
        this.keyPath = new IKey[] {};
        this.keys = null;
        this.maps = null;
    }

    /**
     * Add an entry of key and item solver.
     * 
     * @param keyName key's name in string
     * @param solver Solver that will be used to resolve the key when asked
     */
    public void addEntry(String keyName, ItemSolver solver) {
        keys.put(factory.createKey(keyName), keyName);
        maps.put(keyName, solver);
    }

    /**
     * Add an entry of key and path.
     * 
     * @param keyName key's name in string
     * @param path where data can be found
     */
    public void addEntry(String keyName, Path path) {
        ItemSolver solver = new ItemSolver(view, factory, path);
        keys.put(factory.createKey(keyName), keyName);
        maps.put(keyName, solver);
    }

    /**
     * Add a concept to the dictionary.
     * 
     * @param concept to be added
     */
    public void addConcept(Concept concept) {
        try {
            mReader.getConcepts().addConcept(concept);
        } catch (UriAccessException e) {
            Factory.getLogger().warn(e.getMessage());
        }
    }

    /**
     * Returns true if the given key is in this dictionary
     * 
     * @param key key object
     * @return true or false
     */
    public boolean containsKey(String keyName) {
        IKey key = factory.createKey(keyName);
        return keys.containsKey(key);
    }

    /**
     * Return all keys referenced in the dictionary.
     * 
     * @return a list of String objects
     */
    public List<IKey> getAllKeys() {
        return new ArrayList<>(keys.keySet());
    }

    /**
     * Get the item solver referenced by the key or null if not found.
     * 
     * @param key key object
     * @return ItemSolver object
     */
    public ItemSolver getItemSolver(IKey key) {
        ItemSolver solver = null;
        if (keys.containsKey(key)) {
            String keyName = keys.get(key);
            if (maps.containsKey(keyName)) {
                solver = maps.get(keyName);
            }
        }
        return solver;
    }

    /**
     * Remove an entry from the dictionary.
     * 
     * @param key key object
     */
    public void removeEntry(String keyName) {
        IKey key = factory.createKey(keyName);
        keys.remove(key);
        maps.remove(keyName);
    }

    @Override
    public ExtendedDictionary clone() throws CloneNotSupportedException {
        ExtendedDictionary dict = new ExtendedDictionary(factory, keyFile, mapFile);
        dict.mReader = mReader;
        dict.keyPath = keyPath.clone();
        dict.view = view;

        return dict;
    }

    /**
     * Get a sub part of this dictionary that corresponds to a key.
     * 
     * @param IKey object
     * @return IExtendedDictionary matching the key
     */
    public ExtendedDictionary getDictionary(IKey key) {
        ExtendedDictionary subDict;
        try {
            subDict = this.clone();

            // Update the key path to reach sub dictionary
            int depth = keyPath.length + 1;
            subDict.keyPath = java.util.Arrays.copyOf(keyPath, depth);
            subDict.keyPath[depth - 1] = key.clone();

            // Calculate the sub data view
            DataView dataview = mReader.getView();
            for (IKey tmp : subDict.keyPath) {
                dataview = dataview.getView(tmp.getName());
            }

            // Get concepts and mappings
            DataConcepts concepts = mReader.getConcepts();
            DataMapping mapping = mReader.getMapping(view, factory, mapFile);

            subDict.link(dataview, mapping, concepts);
        } catch (CloneNotSupportedException e) {
            subDict = null;
            Factory.getLogger().error(e.getMessage());
        } catch (UriAccessException e) {
            subDict = null;
            Factory.getLogger().error(e.getMessage());
        }

        return subDict;
    }

    /**
     * Get the view name matching this dictionary
     * 
     * @return the name of the experimental view
     */
    public String getView() {
        if (view == null) {
            try {
                readEntries();
            } catch (UriAccessException e) {
            }
        }
        return view;
    }

    @Override
    public String getFactoryName() {
        return factory.getName();
    }

    /**
     * Return the path to reach the key dictionary file
     * 
     * @return the path of the dictionary key file
     */
    public String getKeyFilePath() {
        return keyFile;
    }

    /**
     * Return the path to reach the mapping dictionary file
     * 
     * @return the path of the plug-in's dictionary mapping file
     */
    public String getMappingFilePath() {
        return mapFile;
    }

    /**
     * Return the concept object corresponding to the given key
     * 
     * @param key
     * @return the Concept
     */
    public Concept getConcept(IKey key) {
        Concept concept;
        try {
            concept = mReader.getConcepts().getConcept(key.getName(), factory.getName());
        } catch (UriAccessException e) {
            concept = null;
            Factory.getLogger().error(e.getMessage());
        }
        return concept;
    }

    /**
     * Read all keys stored in the XML dictionary file
     * 
     * @throws UriAccessException in case of any problem while reading
     */
    public void readEntries() throws UriAccessException {
        synchronized (ExtendedDictionary.class) {
            // Init the XML files reader: mappings, views, concepts
            mReader = new DictionaryReader(keyFile);
            mReader.init();

            // Get mappings, views, concepts
            DataView dataView = mReader.getView();
            if (view == null) {
                view = mReader.getView().getName();
            }
            DataConcepts concepts = mReader.getConcepts();
            DataMapping mapping = mReader.getMapping(view, factory, mapFile);

            // Link the data view and the concept
            link(dataView, mapping, concepts);
        }
    }

    private void link(DataView dataview, DataMapping mapping, DataConcepts concepts) {
        String keyID;

        // Init keys map
        if (keys == null) {
            keys = new HashMap<>();
        }

        // Get concept IDs from view item
        for (String keyName : dataview.getItemKeys()) {
            Concept concept = concepts.getConcept(keyName);
            if (concept == null) {
                concept = new Concept(keyName);
                concepts.addConcept(concept);
                keyID = keyName;
            } else {
                keyID = concept.getConceptID();
            }

            keys.put(factory.createKey(keyName), keyID);
        }

        // Init mapping map
        if (maps == null) {
            maps = new HashMap<>();
        }

        // Add all sub views to dictionary (i.e: keys and mappings)
        for (Entry<String, DataView> entry : dataview.getSubViews().entrySet()) {
            IKey key = factory.createKey(entry.getKey());
            maps.put(key.getName(), new ItemSolver(view, factory, key));
            keys.put(key, key.getName());
        }

        // Add all items to mapping
        for (Entry<String, ItemSolver> entry : mapping.getSolvers().entrySet()) {
            Concept concept = concepts.getConcept(entry.getKey());
            if (concept == null) {
                keyID = entry.getKey();
            } else {
                keyID = concept.getConceptID();
            }
            maps.put(keyID, entry.getValue());
            if (keyFile == null) {
                keys.put(factory.createKey(keyID), keyID);
            }
        }

        // Check all keys from view are mapped
        StringBuilder configInfo = new StringBuilder();
        for (Entry<IKey, String> entry : keys.entrySet()) {
            if (!maps.containsKey(entry.getValue())) {
                configInfo.append("\n  - Key '");
                configInfo.append(entry.getKey().getName()).append("'");
            }
        }

        // Log not mapped keys
        if (configInfo.length() != 0) {
            StringBuilder header = new StringBuilder();
            header.append("Dictionary unavailable keys\nFor plug-in: '").append(factory.getName()).append("' named ")
                    .append(factory.getPluginLabel());
            header.append("\nIn view file: '").append(keyFile).append("'");
            header.append("\nWith mapping file: '").append(mapFile).append("'");
            header.append("\nFollowing keys haven't been mapped nor have synonyms:");
            Factory.getLogger().trace(header.append(configInfo).toString());
        }

    }
}

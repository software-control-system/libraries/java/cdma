/*******************************************************************************
 * Copyright (c) 2008 - ANSTO/Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * Norman Xiong (nxi@Bragg Institute) - initial API and implementation
 * Tony Lam (nxi@Bragg Institute) - initial API and implementation
 * Majid Ounsy (SOLEIL Synchrotron) - API v2 design and conception
 * Stephane Poirier (SOLEIL Synchrotron) - API v2 design and conception
 * Clement Rodriguez (ALTEN for SOLEIL Synchrotron) - API evolution
 * Gregory VIGUIER (SOLEIL Synchrotron) - API evolution
 * Raphaël GIRARDOT (SOLEIL Synchrotron) - API evolution
 ******************************************************************************/
package org.cdma.dictionary;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cdma.exception.CDMAException;
import org.cdma.interfaces.IContainer;
import org.cdma.interfaces.IDataItem;
import org.cdma.utils.Utilities.ModelType;

/**
 * Stacks all found data items, organizes them in a {@link Map} and constructs aggregated IDataItems.
 *
 * @param <D> The type of {@link IDataItem} produced by this {@link ADataItemMultiStacker}.
 * @param <K> The type key used to organize {@link IDataItem}s in a {@link Map}.
 * @author GIRARDOT
 */
public abstract class ADataItemMultiStacker<D extends IDataItem, K> extends ADataItemStacker<D> {

    public ADataItemMultiStacker() {
        super();
    }

    /**
     * Builds a new {@link IDataItem} {@link Map}.
     * 
     * @return A {@link Map}, vener <code>null</code>.
     */
    protected Map<K, List<IDataItem>> generateItemsMap() {
        return new HashMap<>();
    }

    /**
     * Extracts map key from an {@link IDataItem}.
     * 
     * @param item The {@link IDataItem}.
     * @return A key.
     */
    protected abstract K extractKey(IDataItem item);

    /**
     * Stacks data items in a {@link Map} and constructs aggregated items, according to given {@link Context}.
     * 
     * @param context The {@link Context}.
     * @return An array of aggregated items.
     */
    public IDataItem[] stackDataItems(final Context context) {
        Map<K, List<IDataItem>> itemsMap = generateItemsMap();
        // Get all previously found nodes
        List<IContainer> nodes = context.getContainers();
        prepareNodes(nodes);
        for (IContainer node : nodes) {
            if ((node.getModelType() == ModelType.DataItem) && isValidNode(node, context)) {
                IDataItem item = (IDataItem) node;
                K key = extractKey(item);
                if (key != null) {
                    List<IDataItem> items = itemsMap.get(key);
                    if (items == null) {
                        items = new ArrayList<>();
                        itemsMap.put(key, items);
                    }
                    items.add((IDataItem) node);
                }
            }
        }
        IDataItem[] array = new IDataItem[itemsMap.size()];
        int index = 0;
        for (List<IDataItem> items : itemsMap.values()) {
            if (!items.isEmpty()) {
                array[index] = buildDataItem(items, context);
            }
            index++;
        }
        return array;
    }

    @Override
    public void execute(Context context) throws CDMAException {
        List<IContainer> result = new ArrayList<IContainer>();
        IDataItem[] items = stackDataItems(context);
        if (items != null) {
            for (IDataItem item : items) {
                if (item != null) {
                    result.add(item);
                }
            }
        }
        context.setContainers(result);
    }

}

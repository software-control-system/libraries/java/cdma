/*******************************************************************************
 * Copyright (c) 2008 - ANSTO/Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * Norman Xiong (nxi@Bragg Institute) - initial API and implementation
 * Tony Lam (nxi@Bragg Institute) - initial API and implementation
 * Majid Ounsy (SOLEIL Synchrotron) - API v2 design and conception
 * Stephane Poirier (SOLEIL Synchrotron) - API v2 design and conception
 * Clement Rodriguez (ALTEN for SOLEIL Synchrotron) - API evolution
 * Gregory VIGUIER (SOLEIL Synchrotron) - API evolution
 * Raphaël GIRARDOT (SOLEIL Synchrotron) - API evolution
 ******************************************************************************/
package org.cdma.dictionary;

import java.util.ArrayList;
import java.util.List;

import org.cdma.exception.CDMAException;
import org.cdma.interfaces.IContainer;
import org.cdma.interfaces.IDataItem;
import org.cdma.utils.Utilities.ModelType;

/**
 * Stack all found data items to construct an aggregated IDataItem
 *
 * @param <D> The type of {@link IDataItem} produced by this {@link ADataItemSingleStacker}
 * @author GIRARDOT
 */
public abstract class ADataItemSingleStacker<D extends IDataItem> extends ADataItemStacker<D> {

    public ADataItemSingleStacker() {
        super();
    }

    /**
     * Stacks data items and constructs an aggregated item, according to given {@link Context}.
     * 
     * @param context The {@link Context}.
     * @return An aggregated item.
     */
    public IDataItem stackDataItems(final Context context) {
        IDataItem item = null;
        // Get all previously found nodes
        List<IDataItem> items = new ArrayList<>();
        List<IContainer> nodes = context.getContainers();
        prepareNodes(nodes);
        for (IContainer node : nodes) {
            if ((node.getModelType() == ModelType.DataItem) && isValidNode(node, context)) {
                items.add((IDataItem) node);
            }
        }
        if (!items.isEmpty()) {
            item = buildDataItem(items, context);
        }
        return item;
    }

    @Override
    public void execute(Context context) throws CDMAException {
        List<IContainer> result = new ArrayList<IContainer>();
        IDataItem item = stackDataItems(context);
        if (item != null) {
            result.add(item);
        }
        context.setContainers(result);
    }

}

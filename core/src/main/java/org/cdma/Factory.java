/*******************************************************************************
 * Copyright (c) 2008 - ANSTO/Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * Norman Xiong (nxi@Bragg Institute) - initial API and implementation
 * Tony Lam (nxi@Bragg Institute) - initial API and implementation
 * Majid Ounsy (SOLEIL Synchrotron) - API v2 design and conception
 * Stephane Poirier (SOLEIL Synchrotron) - API v2 design and conception
 * Clement Rodriguez (ALTEN for SOLEIL Synchrotron) - API evolution
 * Gregory VIGUIER (SOLEIL Synchrotron) - API evolution
 * Raphael GIRARDOT (SOLEIL Synchrotron) - API evolution
 ******************************************************************************/
// ****************************************************************************
// Copyright (c) 2008 Australian Nuclear Science and Technology Organisation.
// All rights reserved. This program and the accompanying materials
// are made available under the terms of the Eclipse Public License v1.0
// which accompanies this distribution, and is available at
// http://www.eclipse.org/legal/epl-v10.html
//
// Contributors:
//    Norman Xiong (nxi@Bragg Institute) - initial API and implementation
//    Tony Lam (nxi@Bragg Institute) - initial API and implementation
//    Clement Rodriguez (ALTEN for SOLEIL Synchrotron) - API evolution
// ****************************************************************************
package org.cdma;

import java.io.File;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.cdma.exception.UriAccessException;
import org.cdma.interfaces.IDataset;
import org.cdma.interfaces.IDatasource;
import org.cdma.internal.dictionary.readers.DictionaryReader;
import org.cdma.utils.FactoryManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @brief The Core factory is the entry point of the CDMA API
 * 
 *        The Factory class in common data model is a tools to create CDMA objects. It manages all plug-ins instances.
 *        <p>
 *        According to an URI, it will detect which plug-in is relevant to that data source. It can take an URI as a
 *        parameter to instantiate a plug-in in order to get an access of the targeted data source using CDMA objects.
 *        <p>
 *        Abbreviation: Common Data Model Access -- CDMA
 * 
 * @author XIONG Norman
 * @contributor RODRIGUEZ Clément
 * @version 1.1
 */
public final class Factory {

    // Plugin manager
    private static final FactoryManager MANAGER = new FactoryManager(IFactory.CDMA_VERSION);

    // Dictionary view
    private static final String DICTIONARY_PATH = "CDM_DICTIONARY_PATH";

    // Files' suffix / prefix
    private static final String FILE_CONCEPT_NAME = "concepts";
    private static final String FILE_CONCEPT_CORE = "core_";
    private static final String FILE_VIEW_XML_SUFFIX = "_view.xml";

    private static final String XML = ".xml";

    // Dictionaries
    private static final String PATH_FOLDER_MAPS = "mappings";
    private static final String PATH_FOLDER_VIEWS = "views";
    private static final String PATH_FOLDER_CONCEPTS = "concepts";

    // Global logger for the CDMA
//    private static Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
    private static final Logger LOGGER = LoggerFactory.getLogger(Factory.class.getName());

    /**
     * Create a CDMA Dataset that can read the given URI.
     * 
     * @param uri URI object
     * @return CDMA Dataset
     * @throws Exception
     */
    public static IDataset openDataset(final URI uri) throws Exception {
        return openDataset(uri, false);
    }

    /**
     * Create a CDMA Dataset that can read the given URI and use optionally the Extended Dictionary mechanism.
     * 
     * @param uri URI object
     * @param useProducer only
     * @return CDMA Dataset
     * @throws Exception
     */
    public static IDataset openDataset(final URI uri, final boolean useDictionary) throws Exception {
        IFactory factory = getFactory(uri);
        IDataset dataset = null;
        if (factory != null) {
            IDatasource source = factory.getPluginURIDetector();
            if (!useDictionary || source.isProducer(uri)) {
                dataset = factory.openDataset(uri);
            }
        }
        return dataset;
    }

    /**
     * Returns the logger that will be used by the CDMA.
     * 
     * @return org.slf4j.Logger
     */
    public static Logger getLogger() {
        return LOGGER;
    }

    /**
     * Returns the list of all available views for the dictionary mechanism.
     * 
     * @return views' names
     */
    public static List<String> getAvailableViews() {
        List<String> result = new ArrayList<String>();

        // Get the dictionary folder
        String path = getDictionariesFolder();
        if (path != null) {
            File folder = new File(new StringBuilder(path).append(File.separator).append(PATH_FOLDER_VIEWS).toString());
            if (folder.exists() && folder.isDirectory()) {
                // List folder's files
                File[] files = folder.listFiles();
                String name, fileName;
                for (File file : files) {
                    // Check files name
                    if (file.getName().toLowerCase().endsWith(FILE_VIEW_XML_SUFFIX)) {
                        fileName = file.getPath();
                        try {
                            // Get the view name
                            name = DictionaryReader.getDictionaryName(fileName);
                            result.add(name);
                        } catch (UriAccessException e) {
                            Factory.getLogger().info("Unable to get the view name for: " + fileName, e);
                        }
                    }
                }
            }
        }

        return result;
    }

    /**
     * According to the currently defined experiment, this method will return the path to reach the declarative
     * dictionary. It means the file where is defined what should be found in a IDataset that fits the experiment.
     * It's a descriptive file.
     * 
     * @return the path to the standard declarative file
     */
    public static String getPathKeyDictionary(String view) {
        String file = null;
        String sDict = getDictionariesFolder();

        if ((view != null) && (!view.trim().isEmpty())) {
            file = new StringBuilder(sDict).append(File.separator).append(PATH_FOLDER_VIEWS).append(File.separator)
                    .append(view.toLowerCase()).append(FILE_VIEW_XML_SUFFIX).toString();
        }

        return file;
    }

    /**
     * This method will return the path to reach the <b>common concept dictionary</b>.
     * It means the file where is defined every physical concepts.
     * 
     * @return the path to the common concept file or null if not found
     * @note The file name is as following: getDictionariesFolder() + "concepts.xml"
     * @see Factory.getConceptViewDictionaryPath()
     */
    public static String getPathCommonConceptDictionary() {
        String sDict = getDictionariesFolder();
        String sPath = new StringBuilder(sDict).append(File.separator).append(PATH_FOLDER_CONCEPTS)
                .append(File.separator).append(FILE_CONCEPT_CORE).append(FILE_CONCEPT_NAME).append(XML).toString();

        // Check the concept dictionary corresponding to the view exist
        if (!new File(sPath).exists()) {
            sPath = null;
        }

        return sPath;
    }

    /**
     * According to the given factory this method will return the path to reach the folder containing mapping
     * dictionaries. This file associate entry keys to paths that are plug-in dependent.
     * 
     * @param factory of the plug-in instance from which we want to load the dictionary
     * @return the path to the plug-in's mapping dictionaries folder
     */
    public static String getPathMappingDictionaryFolder(final IFactory factory) {

        return getPathMappingDictionaryFolder(factory.getName());
    }

    /**
     * According to the given factory this method will return the path to reach the folder containing mapping
     * dictionaries. This file associate entry keys to paths that are plug-in dependent.
     * 
     * @param factory of the plug-in instance from which we want to load the dictionary
     * @return the path to the plug-in's mapping dictionaries folder
     */
    public static String getPathMappingDictionaryFolder(final String factoryName) {
        String sDict = getDictionariesFolder();

        return new StringBuilder(sDict).append(File.separator).append(PATH_FOLDER_MAPS).append(File.separator)
                .append(factoryName).append(File.separator).toString();
    }

    /**
     * This method will return the path to reach the <b>specific concept dictionary</b>.
     * It means the file where is defined every specific physical concepts defined by the view.
     * 
     * @return the path to the specific concept folder or null if not found
     */
    public static String getPathConceptDictionaryFolder() {
        String sDict = getDictionariesFolder();
        String sPath = new StringBuilder(sDict).append(File.separator).append(PATH_FOLDER_CONCEPTS)
                .append(File.separator).toString();

        // Check the concept dictionary corresponding to the view exist
        File file = new File(sPath);
        if (!file.exists()) {
            sPath = null;
        }

        return sPath;
    }

    /**
     * Set the folder path where to search for key dictionary files.
     * This folder should contains all dictionaries that the above application needs.
     * 
     * @param path targeting a folder
     */
    public static void setDictionariesFolder(final String path) {
        if (path != null) {
            System.setProperty(DICTIONARY_PATH, path);
        }
    }

    /**
     * Get the folder path where to search for key dictionary files (e.q: view or experiment).
     * This folder should contains all dictionaries that the above application needs.
     * 
     * @return path targeting a folder
     */
    public static String getDictionariesFolder() {
        return System.getProperty(DICTIONARY_PATH, System.getenv(DICTIONARY_PATH));
    }

    /**
     * Return the singleton instance of the plug-ins factory manager
     * 
     * @return IFactoryManager unique instance
     */
    public static FactoryManager getManager() {
        return MANAGER;
    }

//    /**
//     * Return the IFactory of the first available plug-in that was loaded
//     * 
//     * @return first loaded IFactory
//     */
//    @Deprecated
//    public static IFactory getFactory() {
//        return getManager().getFactory();
//    }

    /**
     * Return the plug-in's factory having the given name
     * 
     * @param name of the requested factory
     * @return IFactory instance
     */
    public static IFactory getFactory(final String name) {
        return getManager().getFactory(name);
    }

    /**
     * Return a plug-in IFactory that is the most relevant for the given URI.
     * Try to detect factories according the following:
     * if a plug-in declares itself as the owner of the targeted data source returns its factory
     * else returns the first plug-in that is compatible with given data format
     * no plug-in is compatible returns null
     * 
     * @param uri of the data source
     * @return IFactory instance
     */
    public static IFactory getFactory(final URI uri) {
        List<String> reader = new ArrayList<String>();
        IFactory result = null;

        // Get the list of data source detector
        List<IDatasource> sources = getDatasources();
        // For each check if it can read the given source
        for (IDatasource source : sources) {
            // Can read ?
            boolean canRead = source.isReadable(uri);
            if (canRead) {
                reader.add(source.getFactoryName());

                // Does it have the ownership on the source
                boolean isProd = source.isProducer(uri);
                if (isProd) {
                    result = getFactory(source.getFactoryName());
                    break;
                }
            }
        }

        // No ownership detected, so return the first reader
        if (result == null && reader.size() > 0) {
            result = getFactory(reader.get(0));
        }
        return result;
    }

    /**
     * Returns the list of all available IDataSource implementations.
     * 
     * @return list of found IDataSource
     */
    public static List<IDatasource> getDatasources() {
        List<IDatasource> result = new ArrayList<IDatasource>();
        IDatasource source;

        // Ensure a factory manager has been loaded
        FactoryManager mngr = getManager();

        // Get the registry of factories
        Map<String, IFactory> registry = mngr.getFactoryRegistry();
        for (Entry<String, IFactory> entry : registry.entrySet()) {
            source = entry.getValue().getPluginURIDetector();
            if (source != null) {
                result.add(source);
            }
        }

        return result;
    }

    /**
     * Returns the list of all available IFactory implementations.
     * 
     * @return list of found IFactory
     */
    public static List<IFactory> getFactories() {
        // Ensure a factory manager has been loaded
        FactoryManager mngr = getManager();
        // Get the registry of factories
        Map<String, IFactory> registry = mngr.getFactoryRegistry();
        return new ArrayList<>(registry.values());
    }

    /**
     * Hide default constructor.
     */
    private Factory() {
    }

}

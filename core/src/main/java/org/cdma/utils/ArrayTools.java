/*******************************************************************************
 * Copyright (c) 2008 - ANSTO/Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * Norman Xiong (nxi@Bragg Institute) - initial API and implementation
 * Tony Lam (nxi@Bragg Institute) - initial API and implementation
 * Majid Ounsy (SOLEIL Synchrotron) - API v2 design and conception
 * Stephane Poirier (SOLEIL Synchrotron) - API v2 design and conception
 * Clement Rodriguez (ALTEN for SOLEIL Synchrotron) - API evolution
 * Gregory VIGUIER (SOLEIL Synchrotron) - API evolution
 * Raphael GIRARDOT (SOLEIL Synchrotron) - API evolution
 ******************************************************************************/
// ****************************************************************************
// Copyright (c) 2008 Australian Nuclear Science and Technology Organisation.
// All rights reserved. This program and the accompanying materials
// are made available under the terms of the Eclipse Public License v1.0 
// which accompanies this distribution, and is available at
// http://www.eclipse.org/legal/epl-v10.html
// 
// Contributors: 
//    Clément Rodriguez (clement.rodriguez@synchrotron-soleil.fr)
// ****************************************************************************
package org.cdma.utils;

import java.lang.reflect.Array;
import java.util.Arrays;

import fr.soleil.lib.project.math.ArrayUtils;

public class ArrayTools {

    /**
     * Determine the shape of the given array (raged array aren't considered)
     * 
     * @param data on which we want to analyze shape
     * @return array of integer representing the length of each dimension
     * @note an empty array is returned in case of scalar data
     */
    public static int[] detectShape(final Object data) {
        int[] shape;

        // Check data existence
        if (data == null) {
            shape = ArrayUtils.EMPTY_SHAPE;
        } else {
            // Determine rank of array (by parsing data array class name)
            int iRank = 0;
            Class<?> clazz = data.getClass();
            while (clazz != null && clazz.isArray()) {
                clazz = clazz.getComponentType();
                iRank++;
            }

            // Set dimension rank
            shape = new int[iRank];

            // Fill dimension size array
            Object array = data;
            int length;

            for (int i = 0; i < iRank; i++) {
                length = java.lang.reflect.Array.getLength(array);
                if (length < 1) {
                    Arrays.fill(shape, 0);
                    break;
                }
                array = java.lang.reflect.Array.get(array, 0);
                shape[i] = length;
            }
        }
        return shape;
    }

    /**
     * Increment a counter by 1, according the shape limitation.
     * The most varying dimension will be the one with the higher index.
     * 
     * @param counter to be incremented
     * @param shape limitation of the counter
     */
    public static void incrementCounter(int[] counter, int[] shape) {
        for (int i = counter.length - 1; i >= 0; i--) {
            if (counter[i] + 1 >= shape[i] && i > 0) {
                counter[i] = 0;
            } else {
                counter[i]++;
                break;
            }
        }
    }

    public static Object copyJavaArray(Object array) {
        Object result = array;
        if (result == null) {
            return null;
        } else {
            // Determine rank of array (by parsing data array class name)
            String sClassName = array.getClass().getName();
            int iRank = 0;
            int iIndex = 0;
            char cChar;
            while (iIndex < sClassName.length()) {
                cChar = sClassName.charAt(iIndex);
                iIndex++;
                if (cChar == '[') {
                    iRank++;
                }
            }

            // Set dimension rank
            int[] shape = new int[iRank];

            // Fill dimension size array
            for (int i = 0; i < iRank; i++) {
                shape[i] = java.lang.reflect.Array.getLength(result);
                result = java.lang.reflect.Array.get(result, 0);
            }

            // Define a convenient array (shape and type)
            result = ArrayUtils.newInstance(array.getClass().getComponentType(), shape);
            result = copyJavaArray(array, result);
        }
        return result;
    }

    public static Object copyJavaArray(Object source, Object target) {
        Object item = java.lang.reflect.Array.get(source, 0);
        int length = java.lang.reflect.Array.getLength(source);

        if (item.getClass().isArray()) {
            Object tmpSrc;
            Object tmpTar;
            for (int i = 0; i < length; i++) {
                tmpSrc = java.lang.reflect.Array.get(source, i);
                tmpTar = java.lang.reflect.Array.get(target, i);
                copyJavaArray(tmpSrc, tmpTar);
            }
        } else {
            System.arraycopy(source, 0, target, 0, length);
        }

        return target;
    }

    private static void reshapeArray(int dimIndex, int[] startPositions, int[] dimensions, Object singleDimArray,
            Object multiDimArray, boolean from1ToN) {
        int lStartRaw;
        int lLinearStart;
        if (dimIndex == dimensions.length - 1) {
            lLinearStart = 0;
            for (int k = 1; k < dimensions.length; k++) {
                lStartRaw = 1;
                for (int j = k; j < dimensions.length; j++) {
                    lStartRaw *= dimensions[j];
                }
                lStartRaw *= startPositions[k - 1];
                lLinearStart += lStartRaw;
            }
            if ((singleDimArray != null) && (multiDimArray != null)) {
                if (from1ToN) {
                    System.arraycopy(singleDimArray, lLinearStart, multiDimArray, 0, dimensions[dimensions.length - 1]);
                } else {
                    System.arraycopy(multiDimArray, 0, singleDimArray, lLinearStart, dimensions[dimensions.length - 1]);
                }
            }
        } else {
            Object[] outputAsArray = (Object[]) multiDimArray;
            for (int i = 0; i < dimensions[dimIndex]; i++) {
                Object o = outputAsArray[i];
                reshapeArray(dimIndex + 1, startPositions, dimensions, singleDimArray, o, from1ToN);
                if (startPositions[dimIndex] < dimensions[dimIndex] - 1) {
                    startPositions[dimIndex] = i + 1;
                } else {
                    startPositions[dimIndex] = 0;
                }
            }
        }
    }

    /**
     * Converts a mono-dimensional array into a multi-dimensional array of the same data type
     * (recovered by using {@link #recoverDataType(Object)}).
     * 
     * @param singleDimArray The mono-dimensional array
     * @param multiDimArrayShape The shape of the multi-dimensional array. Example: if you want a {@link String}
     *            <code>[dimZ][dimY][dimX]</code>, <code>multiDimArrayShape</code> should be
     *            <code>{dimZ, dimY, dimX}</code>
     * @return The expected multi-dimensional array. It will return <code>null</code> in any of
     *         these cases:
     *         <ul>
     *         <li><code>singleDimArray</code> is <code>null</code></li>
     *         <li><code>singleDimArray</code> is not a mono-dimensional array</li>
     *         <li><code>multiDimArrayShape</code> is <code>null</code></li>
     *         <li><code>multiDimArrayShape</code> is of length 0</li>
     *         <li><code>multiDimArrayShape</code> contains a negative value</li>
     *         <li><code>multiDimArrayShape</code> is too big to initialize such an array</li>
     *         <li><code>singleDimArray</code> length is not compatible with <code>multiDimArrayShape</code></li>
     *         </ul>
     * @see Array#newInstance(Class, int...)
     * @see #recoverDataType(Object)
     */
    public static Object convertArrayDimensionFrom1ToN(Object singleDimArray, int... multiDimArrayShape) {
        Object result = null;
        if ((singleDimArray != null) && (multiDimArrayShape != null) && (multiDimArrayShape.length > 0)
                && singleDimArray.getClass().isArray() && (!singleDimArray.getClass().getComponentType().isArray())) {
            if (multiDimArrayShape.length == 1) {
                result = singleDimArray;
            } else {
                Class<?> dataType = recoverDataType(singleDimArray);
                if (ArrayUtils.newInstance(dataType, 0).getClass().equals(singleDimArray.getClass())) {
                    int arrayLength = Array.getLength(singleDimArray);
                    int expectedLength = 1;
                    for (int currentLength : multiDimArrayShape) {
                        expectedLength *= currentLength;
                    }
                    if (arrayLength == expectedLength) {
                        try {
                            result = ArrayUtils.newInstance(dataType, multiDimArrayShape);
                        } catch (Exception e) {
                            result = null;
                        }
                        if (result != null) {
                            // java initializes int values with 0 by default
                            int[] startPositions = new int[multiDimArrayShape.length];
                            reshapeArray(0, startPositions, multiDimArrayShape, singleDimArray, result, true);
                        }
                    }
                }
            }
        }
        return result;
    }

    /**
     * Converts a multi-dimensional array into a mono-dimensional array of the same data type
     * (recovered by using {@link #recoverDataType(Object)}).
     * 
     * @param multiDimArray The multi-dimensional array
     * @return The expected multi-dimensional array. It will return <code>null</code> in any of
     *         these cases:
     *         <ul>
     *         <li><code>multiDimArray</code> is <code>null</code></li>
     *         <li><code>multiDimArray</code> is not an array</li>
     *         <li><code>multiDimArray</code> is too big to be converted into a mono-dimensional array</li>
     *         </ul>
     * @see Array#newInstance(Class, int...)
     * @see #recoverDataType(Object)
     */
    public static Object convertArrayDimensionFromNTo1(Object multiDimArray) {
        Object result = null;
        if ((multiDimArray != null) && multiDimArray.getClass().isArray()) {
            int[] multiDimArrayShape = detectShape(multiDimArray);
            if ((multiDimArrayShape != null) && (multiDimArrayShape.length > 0)) {
                if (multiDimArrayShape.length == 1) {
                    result = multiDimArray;
                } else {
                    Class<?> dataType = recoverDataType(multiDimArray);
                    // java initializes int values with 0 by default
                    int[] startPositions = new int[multiDimArrayShape.length];
                    // compare with the smallest multi-dimensional array of the same expected type
                    if (ArrayUtils.newInstance(dataType, startPositions).getClass().equals(multiDimArray.getClass())) {
                        int expectedLength = 1;
                        for (int currentLength : multiDimArrayShape) {
                            expectedLength *= currentLength;
                        }
                        result = ArrayUtils.newInstance(dataType, expectedLength);
                        try {
                            reshapeArray(0, startPositions, multiDimArrayShape, result, multiDimArray, false);
                        } catch (Exception e) {
                            result = null;
                        }
                    }
                }
            }
        }
        return result;
    }

    /**
     * This methods recovers the type of data present in a N dimension array.
     * 
     * @param array The array
     * @return The {@link Class} that represents the data type in the given array. (Example: if <code>array</code> is a
     *         <code>boolean[][]</code>, the result will be {@link Boolean#TYPE}). This method returns <code>null</code>
     *         if <code>array</code> is <code>null</code>.
     */
    public static Class<?> recoverDataType(Object array) {
        Class<?> result = null;
        if (array != null) {
            result = recoverDeepComponentType(array.getClass());
        }
        return result;
    }

    /**
     * This methods recovers the type of data present in a {@link Class} that represents N dimension
     * arrays.
     * 
     * @param arrayClass The {@link Class}
     * @return The {@link Class} that represents the data type in the given array. (Example: if <code>arrayClass</code>
     *         is <code>boolean[][]</code>, the result will be {@link Boolean#TYPE}). This method returns
     *         <code>null</code> if <code>arrayClass</code> is <code>null</code>.
     */
    public static Class<?> recoverDeepComponentType(Class<?> arrayClass) {
        Class<?> result = arrayClass;
        if (arrayClass != null) {
            while (result.isArray()) {
                result = result.getComponentType();
            }
        }
        return result;
    }

    /**
     * Consider an big array as a block of sub arrays, and extract these sub arrays.
     * 
     * @param bigArray The big array
     * @param type The data type inside the array.
     * @param originalSize The size of the full data (generally: the big array size).
     * @param blockSize The number of data in the block.
     * @param offset The potential offset to use for data extraction.
     * @return The sub arrays.
     */
    public static Object[] extractBlockOf1DJavaArrays(Object bigArray, Class<?> type, int originalSize, int blockSize,
            int offset) {
        // Instantiate a new convenient array for storage
        int length = originalSize / blockSize;
        int residual = originalSize % blockSize;
        if (residual > 0) {
            length++;
            residual = originalSize % length;
        }
        int max = blockSize - 1;
        Object[] array = (Object[]) ArrayUtils.newInstance(type, blockSize, length);
        for (int i = 0; i < max; i++) {
            System.arraycopy(bigArray, offset + i * length, array[i], 0, length);
        }
        if (residual == 0) {
            System.arraycopy(bigArray, offset + max * length, array[blockSize - 1], 0, length);
        } else {
            array[max] = ArrayUtils.newInstance(type, residual);
            System.arraycopy(bigArray, offset + (blockSize - 1) * length, array[max], 0, residual);
        }
        return array;

    }
}

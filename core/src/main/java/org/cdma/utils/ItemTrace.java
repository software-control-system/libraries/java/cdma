package org.cdma.utils;

import fr.soleil.lib.project.ObjectUtils;

/**
 * A class that yells how to determine which item to trace.
 * 
 * @author GIRARDOT
 */
public class ItemTrace {
    private String name;
    private boolean start, end;

    /**
     * {@link ItemTrace} constructor.
     * 
     * @param name the referent name.
     * @param start whether referent name is expected at beginning of an item name.
     * @param end whether referent name is expected at end of an item name.
     */
    public ItemTrace(String name, boolean start, boolean end) {
        super();
        this.name = name == null ? ObjectUtils.EMPTY_STRING : name;
        this.start = start;
        this.end = end;
    }

    /**
     * Returns whether the name of an item matches a traceable item name.
     * 
     * @param shortName The item name.
     * @return A <code>boolean</code>.
     */
    public boolean shouldCheck(String shortName) {
        boolean check;
        if (shortName == null) {
            check = false;
        } else if (start) {
            check = shortName.startsWith(name);
        } else if (end) {
            check = shortName.endsWith(name);
        } else {
            check = shortName.equals(name);
        }
        return check;
    }
}

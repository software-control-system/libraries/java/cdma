package org.cdma.utils;

public final class MetricsUtils {

    private static boolean useMetrics = false;
    private static String itemCheckableKey = null;
    private static ItemTrace itemTrace = null;

    private MetricsUtils() {
        // hide constructor
    }

    public static final boolean isUseMetrics() {
        return useMetrics;
    }

    public static final void setUseMetrics(boolean useMetrics) {
        MetricsUtils.useMetrics = useMetrics;
    }

    public static String getItemCheckableKey() {
        return itemCheckableKey;
    }

    public static void setItemCheckableKey(String itemCheckableKey) {
        MetricsUtils.itemCheckableKey = itemCheckableKey;
    }

    public static void setItemTrace(String name, boolean start, boolean end) {
        if (name == null) {
            itemTrace = null;
        } else {
            itemTrace = new ItemTrace(name, start, end);
        }
    }

    public static boolean shouldProfile(String shorName) {
        return (isUseMetrics() && itemTrace.shouldCheck(shorName));
    }

}

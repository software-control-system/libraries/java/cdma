/*******************************************************************************
 * Copyright (c) 2008 - ANSTO/Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * Norman Xiong (nxi@Bragg Institute) - initial API and implementation
 * Tony Lam (nxi@Bragg Institute) - initial API and implementation
 * Majid Ounsy (SOLEIL Synchrotron) - API v2 design and conception
 * Stephane Poirier (SOLEIL Synchrotron) - API v2 design and conception
 * Clement Rodriguez (ALTEN for SOLEIL Synchrotron) - API evolution
 * Gregory VIGUIER (SOLEIL Synchrotron) - API evolution
 * Raphael GIRARDOT (SOLEIL Synchrotron) - API evolution
 ******************************************************************************/
// ****************************************************************************
// Copyright (c) 2008 Australian Nuclear Science and Technology Organisation.
// All rights reserved. This program and the accompanying materials
// are made available under the terms of the Eclipse Public License v1.0 
// which accompanies this distribution, and is available at
// http://www.eclipse.org/legal/epl-v10.html
// 
// Contributors: 
//    Norman Xiong (nxi@Bragg Institute) - initial API and implementation
//    Clément Rodriguez (clement.rodriguez@synchrotron-soleil.fr) - API evolution
// ****************************************************************************
package org.cdma.interfaces;

import org.cdma.internal.IModelObject;

/**
 * @brief The IIndex interface defines a view on a IArray.
 */

/// @cond pluginAPI

/**
 * @note When developing a plug-in consider using the data format engine's implementation.
 *       You should <b>redefine this interface implementation</b>, only in case of <b>very specific needs.</b>
 *       <p>
 */

/// @endcond pluginAPI

/**
 * The IIndex fully describes the IArray's visible part. Whatever the storage is, it defines
 * the shape of the visible part of the storage, its origin, how two consecutive
 * cells should be considered. Manages the stride and the way the parsing will be done.
 * <br>
 * The IIndex can describe the whole array storage or just a portion of it.
 * It can be used to refer to a particular element of an IArray.
 * 
 * @author nxi
 * 
 */
public interface IIndex extends IModelObject, Cloneable {

    /**
     * Get the number of dimensions in the array.
     * 
     * @return integer value
     */
    int getRank();

    /**
     * Get the shape: length of array in each dimension.
     * 
     * @return array of integer
     */
    int[] getShape();

    /**
     * Get the origin: first index of array in each dimension.
     * 
     * @return array of integer
     */
    int[] getOrigin();

    /**
     * Get the total number of elements in the array.
     * 
     * @return long value
     */
    long getSize();

    /**
     * Get the stride: for each dimension number elements to jump in the array between two
     * consecutive element of the same dimension
     * 
     * @return array of integer
     */
    long[] getStride();

    /**
     * Get the current element's index into the 1D backing array.
     * 
     * @return integer value
     */
    long currentElement();

    /**
     * Get the last element's index into the 1D backing array.
     * 
     * @return integer value
     */
    long lastElement();

    /**
     * Set the current element's index.
     * 
     * @param index array of integer
     * @return this, so you can use A.get(i.set(i))
     */
    IIndex set(int... index);

    /**
     * Set current element at dimension dim to value.
     * 
     * @param dim integer value
     * @param value integer value
     * @return this, so you can use A.get(i.set(i))
     */
    IIndex setDim(int dim, int value);

    /**
     * Set current element at dimensions 0,1,...,v.length-1 to v[0],v[1],...,v[v.length-1].
     * 
     * @param dimIndex The dimension index
     * @param v integer values
     * @return this, so you can use A.get(i.set(i))
     */
    IIndex setDims(int... v);

    /**
     * Set the origin on each dimension for this index
     * 
     * @param origin array of integers
     */
    void setOrigin(int... origin);

    /**
     * Set the given shape for this index
     * 
     * @param shape array of integers
     */
    void setShape(int... shape);

    /**
     * Set the stride for this index. The stride is the number of
     * cells between two consecutive cells in the same dimension.
     * 
     * @param stride array of integers
     */
    void setStride(long... stride);

    /**
     * String representation.
     * 
     * @return String object
     */
    String toStringDebug();

    /**
     * Return the current location of the index on each dimension.
     * 
     * @return java array of integer
     */
    int[] getCurrentCounter();

    /**
     * Set the name of one of the indices.
     * 
     * @param dim which index
     * @param indexName name of index
     */
    void setIndexName(int dim, String indexName);

    /**
     * Get the name of one of the indices.
     * 
     * @param dim which index?
     * @return name of index, or null if none.
     */
    String getIndexName(int dim);

    /**
     * Remove all index with length one.
     * 
     * @return the new IIndex
     */
    IIndex reduce();

    /**
     * Eliminate the specified dimension.
     * 
     * @param dim dimension to eliminate: must be of length one, else IllegalArgumentException
     * @return the new index
     */
    IIndex reduce(int dim) throws IllegalArgumentException;

    IIndex clone();

}

/*******************************************************************************
 * Copyright (c) 2008 - ANSTO/Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * Norman Xiong (nxi@Bragg Institute) - initial API and implementation
 * Tony Lam (nxi@Bragg Institute) - initial API and implementation
 * Majid Ounsy (SOLEIL Synchrotron) - API v2 design and conception
 * Stephane Poirier (SOLEIL Synchrotron) - API v2 design and conception
 * Clement Rodriguez (ALTEN for SOLEIL Synchrotron) - API evolution
 * Gregory VIGUIER (SOLEIL Synchrotron) - API evolution
 * Raphael GIRARDOT (SOLEIL Synchrotron) - API evolution
 ******************************************************************************/
package org.cdma.engine.archiving.internal.sql;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;
import java.util.Map.Entry;

import org.cdma.engine.archiving.internal.SqlFieldConstants;
import org.cdma.engine.archiving.internal.attribute.Attribute;
import org.cdma.engine.archiving.internal.attribute.AttributeConnector;
import org.cdma.engine.archiving.internal.attribute.AttributePath;
import org.cdma.engine.archiving.internal.attribute.AttributeProperties;
import org.cdma.engine.sql.internal.SqlConnector;
import org.cdma.engine.sql.navigation.SqlDataset;
import org.cdma.engine.sql.utils.DbUtils;
import org.cdma.engine.sql.utils.DbUtils.BaseType;
import org.cdma.engine.sql.utils.SamplingType;
import org.cdma.engine.sql.utils.SamplingType.SamplingPeriod;
import org.cdma.exception.DataAccessException;

import fr.soleil.lib.project.ObjectUtils;

/**
 * This helper class contains all executed queries in the Soleil's Archiving plug-in.
 * 
 * @author rodriguez
 * 
 */
public class ArchivingQueries {

    /**
     * Prepare a query that permits extracting an attribute's properties.
     * The returned fields' properties are the following in the same order: {@link SqlFieldConstants.ADT_FIELDS}
     * 
     * @param dbName name of the archiving data base (HDB, TDB, ...)
     * @param attribute full name of the attribute
     * @return a SQL query string
     */
    static public String queryAttributeProperties(String dbName, final String attribute) {
        StringBuilder query_attr_aptitude = new StringBuilder();

        // Construct the fields for the query
        query_attr_aptitude.append("SELECT ");
        for (int i = 0; i < SqlFieldConstants.ADT_FIELDS.length; i++) {
            query_attr_aptitude.append(SqlFieldConstants.ADT_TABLE_NAME + "." + SqlFieldConstants.ADT_FIELDS[i]);
            if (i < SqlFieldConstants.ADT_FIELDS.length - 1) {
                query_attr_aptitude.append(", ");
            }
        }

        // Check the db name is specified
        if ((dbName != null) && !dbName.isEmpty()) {
            dbName += ".";
        } else {
            dbName = ObjectUtils.EMPTY_STRING;
        }

        // Add the from section to query
        query_attr_aptitude.append(" FROM " + dbName + SqlFieldConstants.ADT_TABLE_NAME);

        // Add the clause section to the query
        query_attr_aptitude.append(" WHERE " + SqlFieldConstants.ADT_TABLE_NAME + "."
                + SqlFieldConstants.ADT_FIELDS_FULL_NAME + " = '" + attribute + "'");

        return query_attr_aptitude.toString();
    }

    /**
     * Prepare a query that permits to list children group of the given attribute.
     * 
     * @param dbName name of the archiving data base (HDB, TDB, ...)
     * @param attribute Attribute object that define the archived attribute group from which children should be listed
     * @return null if this is a leaf group or a query to find children groups' names
     */
    static public String queryChildGroups(final Attribute attribute) {
        String query = null;
        String dbName = attribute.getDbConnector().getDbName();
        AttributePath path = attribute.getPath();
        String field = path.getNextDbFieldName();

        // Check the db name is specified
        if ((dbName != null) && !dbName.isEmpty()) {
            dbName += ".";
        } else {
            dbName = ObjectUtils.EMPTY_STRING;
        }

        if ((field != null) && (path != null) && (dbName != null)) {
            String select = "SELECT DISTINCT(" + SqlFieldConstants.ADT_TABLE_NAME + "." + field + ")";
            String from = " FROM " + dbName + SqlFieldConstants.ADT_TABLE_NAME;

            // construct the 'where' clause
            StringBuilder where = new StringBuilder();
            int i = 0;
            for (Entry<String, String> entry : path.getSqlCriteria().entrySet()) {
                if (i != 0) {
                    where.append(" AND ");
                } else {
                    where.append(" WHERE ");
                }
                where.append(entry.getKey());
                where.append("='");
                where.append(entry.getValue());
                where.append("'");
                i++;
            }
            query = select + from + where;
        }
        return query;
    }

    /**
     * Prepare a query that permits to list children group of the given attribute.
     * 
     * @param attribute Attribute object describing the archived attribute
     * @param datePattern format expected in the query (i.e 'yyyy-MM-dd HH:mm:ss.SSS')
     * @return SQL string for a preparedStatement
     * @throws ParseException
     * @note 2 parameters are expected (1st start date, 2nd end date) and not set
     */

    static public String queryChildItems(final Attribute attribute, final String datePattern) {
        StringBuilder query = new StringBuilder();
        String result = ObjectUtils.EMPTY_STRING;
        if (attribute != null) {
            AttributeConnector dbCon = attribute.getDbConnector();
            AttributeProperties prop = attribute.getProperties();
            if ((dbCon != null) && (prop != null)) {
                String tableName = prop.getDbTable();
                String dbName = dbCon.getDbName();
                BaseType dbType = dbCon.getDbType();

                // Get the sampling type
                SamplingPeriod sampling = prop.getSampling();
                SamplingType samplingType = DbUtils.getSqlSamplingType(sampling, dbType);
                int factor = prop.getSamplingFactor();

                // Check the db name is specified
                if ((dbName != null) && !dbName.isEmpty()) {
                    dbName += ".";
                } else {
                    dbName = ObjectUtils.EMPTY_STRING;
                }

                // Compute starting and ending dates (if null then '01/01/1970' and 'now')

                // Check if date are expected as numerical
                String time;
                // Case of sampling
                if (sampling != SamplingPeriod.NONE) {
                    time = " dateEchantillon  as " + SqlFieldConstants.ATT_FIELD_TIME + " ";

                } else {
                    time = SqlFieldConstants.ATT_FIELD_TIME;
                }

                // Prepare each part of the query (SELECT, FROM, WHERE, ORDER)
                String select = populateSelectClause(prop, sampling, samplingType, time, dbName, tableName, factor);
                String from = " FROM ( ";
                if (sampling != SamplingPeriod.NONE) {
                    from = from + samplingType.queryAssemblySampledDates(" dateEchantillon ", factor, sampling.name(),
                            tableName) + " ) dates ORDER BY dateEchantillon ";
                } else {
                    from = from + dbName + tableName + ")";

                }

                String where = " WHERE ( " + SqlFieldConstants.ATT_FIELD_TIME + " > ?  AND ("
                        + SqlFieldConstants.ATT_FIELD_TIME + " BETWEEN ? AND ?))  ";

                // Assembly the main query
                query.append(select);
                query.append(from);

                if (sampling == SamplingPeriod.NONE) {
                    query.append(where);
                }

                result = query.toString();

            }
        }
        return result;
    }

    /**
     * Check the database contains the right tables for an archiving database
     * 
     * @param attribute
     * @return
     */
    static public boolean checkDatabaseConformity(final Attribute attribute) {
        boolean result = true;

        if (attribute != null) {
            AttributeConnector dbCon = attribute.getDbConnector();
            SqlDataset dataset = dbCon.getSqlDataset();
            SqlConnector connector = dataset.getSqlConnector();
            try {
                Connection connection = connector.getConnection();
                for (String table : SqlFieldConstants.ARC_TABLES) {
                    if (!ArchivingQueries.existe(connection, table)) {
                        result = false;
                        break;
                    }
                }
            } catch (SQLException e) {
                result = false;
            } catch (DataAccessException e) {
                result = false;
            }

        }
        return result;
    }

    private static boolean existe(final Connection connection, final String nomTable) throws SQLException {
        boolean existe = false;
        if (connection != null) {
            DatabaseMetaData dmd = connection.getMetaData();
            try (ResultSet tables = dmd.getTables(connection.getCatalog(), null, nomTable, null);) {
                existe = tables.next();
            }
        }
        return existe;
    }

    /**
     * Will populate the SELECT SQL clause according to the db type, the sampling policy...
     * 
     * @param prop
     * @param sampling
     * @param select
     * @param samplingType
     * @param tableName
     * @param dbName
     * @param factor
     * @return
     */
    private static String populateSelectClause(final AttributeProperties prop, final SamplingPeriod sampling,
            final SamplingType samplingType, final String field, final String dbName, final String tableName,
            final int factor) {
        String result = "SELECT " + (field != null ? field : ObjectUtils.EMPTY_STRING);

        List<String> fields = prop.getDbFields();

        if (sampling == SamplingPeriod.NONE) {
            for (String name : fields) {
                result += ", " + name;
            }
        } else {
            // Manages CLOB fields
            fields = prop.getDbClobFields();
            String interval = samplingType.getIntervalPattern(factor, sampling.name(), null);
            for (String name : fields) {
                String strtemp = "(select " + name + " from " + dbName + tableName
                        + " ATT where( ATT.TIME BETWEEN dates.dateEchantillon AND dates.dateEchantillon + " + interval
                        + " ) " + samplingType.getLimitToOneRow() + "  ) as " + name;
                // System.out.println(strtemp);
                result += ", " + strtemp;
            }

            // Manages numerical fields
            fields = prop.getDbNumericalFields();
            for (String name : fields) {
                String strtemp = "(select " + name + " from " + dbName + tableName
                        + " ATT where( ATT.TIME BETWEEN dates.dateEchantillon AND dates.dateEchantillon +  " + interval
                        + " ) " + samplingType.getLimitToOneRow() + ") as " + name;
                // System.out.println(strtemp);
                result += ", " + strtemp;
            }
        }
        return result;
    }
}

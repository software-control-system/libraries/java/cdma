/*******************************************************************************
 * Copyright (c) 2008 - ANSTO/Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * Norman Xiong (nxi@Bragg Institute) - initial API and implementation
 * Tony Lam (nxi@Bragg Institute) - initial API and implementation
 * Majid Ounsy (SOLEIL Synchrotron) - API v2 design and conception
 * Stephane Poirier (SOLEIL Synchrotron) - API v2 design and conception
 * Clement Rodriguez (ALTEN for SOLEIL Synchrotron) - API evolution
 * Gregory VIGUIER (SOLEIL Synchrotron) - API evolution
 * Raphael GIRARDOT (SOLEIL Synchrotron) - API evolution
 ******************************************************************************/
package org.cdma.engine.hdf.navigation;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;

import org.cdma.Factory;
import org.cdma.engine.hdf.array.HdfArray;
import org.cdma.engine.hdf.utils.HdfObjectUtils;
import org.cdma.exception.DataAccessException;
import org.cdma.exception.InvalidArrayTypeException;
import org.cdma.interfaces.IArray;
import org.cdma.interfaces.IAttribute;
import org.cdma.utils.ArrayTools;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.math.ArrayUtils;
import fr.soleil.lib.project.math.NumberArrayUtils;
import hdf.hdf5lib.H5;
import hdf.hdf5lib.HDF5Constants;
import hdf.hdf5lib.HDFNativeData;
import hdf.hdf5lib.structs.H5O_info_t;

public class HdfAttribute implements IAttribute {

    private static final String CURRENT_PATH = ".";
    private final String factoryName; // factory name that attribute depends on
    private final String name;
    private boolean dirty;
    private Object value;

    public HdfAttribute(final String factoryName, final String name, final Object value) {
        // This constructor is used when data is supplied by the API user
        this(factoryName, name, value, true);

    }

    protected HdfAttribute(final String factoryName, final String name, final Object value, boolean dirty) {
        // This constructor is used when data is supplied by the API user or read on from files
        this.factoryName = factoryName;
        this.name = name;
        this.value = value;

        if (value.getClass().isArray()) {
            if (value instanceof char[]) {
                this.value = new String[] { new String((char[]) value) };
            }
        } else {
            this.value = ArrayUtils.newInstance(value.getClass(), 1);
            Array.set(this.value, 0, value);
        }
        this.dirty = dirty;
    }

//    protected static Object getData(Attribute attr) {
//        Object data;
//        if (attr == null) {
//            data = null;
//        } else {
//            try {
//                data = attr.getData();
//            } catch (Exception e) {
//                data = null;
//            }
//        }
//        return data;
//    }

    /**
     * Writes an attribute.
     * 
     * @param name the attribute name.
     * @param attributeId The attribute id.
     * @param value The attribute value.
     * @param dataType The attribute deep data type.
     * @param knownTypeId The known data type id if the attribute just was created.
     * @param unsignedIntOrVariableString A <code>boolean</code> that may mean :
     *            <ul>
     *            <li>In case of string data, whether to use variable length strings</li>
     *            <li>In case of integers, whether they are unsigned</li>
     *            </ul>
     * @throws DataAccessException If a problem occurred.
     */
    private static void writeAttribute(String name, long attributeId, Object value, Class<?> dataType,
            boolean unsignedIntOrVariableString, long knownTypeId) throws DataAccessException {
        long nativeTypeId = HdfObjectUtils.getNativeDataTypeForClass(dataType, unsignedIntOrVariableString,
                knownTypeId);
        try {
            if (value instanceof String[]) {
                // XXX Warning!! Not yet supported in current API
                H5.H5AwriteVL(attributeId, nativeTypeId, (String[]) value);
            } else {
                H5.H5Awrite(attributeId, nativeTypeId, value);
            }
        } catch (Exception e) {
            throw HdfObjectUtils.toDataAccessException(e, "Failed to write attribute " + name);
        }
    }

    /**
     * Writes an attribute in a parent identified by its id.
     * 
     * @param parentId The parent id.
     * @param attribute The {@link HdfAttribute} that represents the attribute to write.
     * @throws DataAccessException If a problem occurred.
     */
    public static void writeAttribute(long parentId, HdfAttribute attribute) throws DataAccessException {
        long attributeID = -1, dataTypeId = -1, dataSpaceId = -1;
        boolean closeDataTypeId = false;
        boolean variableLengthStrings = false; // variable length string attributes not yet supported in HDF5 java API.
        String name = null;
        try {
            if ((parentId > -1) && (attribute != null)) {
                Object value = attribute.value;
                name = attribute.name;
                if ((name != null) && (!name.trim().isEmpty()) && (value != null)) {
                    long[] dims = NumberArrayUtils.extractLongArray(ArrayUtils.recoverShape(value));
                    Class<?> dataType = ArrayUtils.recoverDataType(value);
                    if ((dims != null) && (dataType != null) && (dims.length > 0)) {
                        // check existing
                        try {
                            attributeID = H5.H5Aopen(parentId, name, HDF5Constants.H5P_DEFAULT);
                        } catch (Exception e) {
                            attributeID = -1; // attribute does not exist
                        }
                        if (attributeID > -1) {
                            // attribute exists
                            dataTypeId = H5.H5Aget_type(attributeID);
                            closeDataTypeId = true;
                            if (String.class.equals(dataType)) {
                                // This is my guess
                                variableLengthStrings = H5.H5Tis_variable_str(dataTypeId);
                            }
                            if ((value instanceof String[]) && (!variableLengthStrings)) {
                                long size = H5.H5Tget_size(dataTypeId);
                                value = HdfObjectUtils.stringToByte((String[]) value, (int) size);
                            }
                            writeAttribute(name, attributeID, value, dataType, variableLengthStrings, dataTypeId);
                        } else {
                            // attribute should be created
                            dataTypeId = HdfObjectUtils.getDataTypeIdForCreation(dataType, false);
                            dataSpaceId = H5.H5Screate_simple(dims.length, dims, null);
                            if (dataSpaceId > -1) {
                                int size = -1;
                                if (value instanceof String[]) {
                                    closeDataTypeId = true;
                                    if (variableLengthStrings) {
                                        H5.H5Tset_size(dataTypeId, HDF5Constants.H5T_VARIABLE);
                                    } else {
                                        size = HdfObjectUtils.getWritableSize((String[]) value);
                                        H5.H5Tset_size(dataTypeId, size);
                                    }
                                }
                                attributeID = H5.H5Acreate(parentId, name, dataTypeId, dataSpaceId,
                                        HDF5Constants.H5P_DEFAULT, HDF5Constants.H5P_DEFAULT);
                                if ((value instanceof String[]) && (!variableLengthStrings)) {
                                    value = HdfObjectUtils.stringToByte((String[]) value, size);
                                }
                                writeAttribute(name, attributeID, value, dataType, true, dataTypeId);
                            }
                        } // end if (attributeId > -1) ... else
                    } // end if ((dims != null) && (dataType != null) && (dims.length > 0))
                } // end if ((name != null) && (!name.trim().isEmpty()) && (value != null))
            } // end if ((parentId > -1) && (attribute != null))
        } catch (Exception e) {
            throw HdfObjectUtils.toDataAccessException(e, "Failed to write attribute " + name);
        } finally {
            // close dataTypeId, attributeId and dataSpaceId.
            try {
                if (closeDataTypeId && (dataTypeId > -1)) {
                    H5.H5Tclose(dataTypeId);
                }
            } catch (Exception e) {
                Factory.getLogger().error("Failed to close access to data type for attribute " + name, e);
            }
            try {
                if (attributeID > -1) {
                    H5.H5Aclose(attributeID);
                }
            } catch (Exception e) {
                Factory.getLogger().error("Failed to close access to attribute " + name, e);
            }
            try {
                if (dataSpaceId > -1) {
                    H5.H5Sclose(dataSpaceId);
                }
            } catch (Exception e) {
                Factory.getLogger().error("Failed to close access to dataspace for attribute " + name, e);
            }
        }
    }

    /**
     * Removes an attribute from a parent identified by its id.
     * 
     * @param parentId The parent id.
     * @param attribute The {@link HdfAttribute} that represents the attribute to remove.
     * @return A <code>boolean</code>. <code>true</code> if attribute removing was successful.
     * @throws DataAccessException If a problem occurred.
     */
    public static boolean removeAttribute(long parentId, HdfAttribute attribute) throws DataAccessException {
        boolean deleted = false;
        if ((parentId > -1) && (attribute != null)) {
            String name = attribute.getName();
            if ((name != null) && (!name.isEmpty())) {
                try {
                    deleted = H5.H5Adelete(parentId, name) > -1;
                } catch (Exception e) {
                    throw new DataAccessException(e.getMessage(), e);
                }
            }
        }
        return deleted;
    }

    /**
     * Returns whether a {@link Collection} contains an {@link HdfAttribute} of given name and value.
     * 
     * @param name The attribute name.
     * @param value The attribute value.
     * @param attributes The {@link Collection}.
     * @return A <code>boolean</code>.
     */
    public static boolean hasAttribute(String name, Object value, Collection<HdfAttribute> attributes) {
        boolean contains = false;
        if ((attributes != null) && (name != null) && (value != null)) {
            for (HdfAttribute attribute : attributes) {
                if ((attribute != null) && name.equals(attribute.name)
                        && ObjectUtils.sameObject(value, attribute.value)) {
                    contains = true;
                    break;
                }
            }
        }
        return contains;
    }

    /**
     * Recovers an attribute identified by its id.
     * 
     * @param factoryName The factory name, necessary for {@link HdfAttribute} construction.
     * @param attributeId The attribute id.
     * @return An {@link HdfAttribute}
     * @throws DataAccessException If a problem occurred.
     */
    // based on H5File.getAttribute(int, int, int)
    private static HdfAttribute recoverAttribute(String factoryName, long attributeId) throws DataAccessException {
        HdfAttribute attribute = null;
        if (attributeId > -1) {
            long dataTypeId = H5.H5Aget_type(attributeId);
            long spaceId = H5.H5Aget_space(attributeId);
            long lsize = 1;
            long[] dims = HdfObjectUtils.getDimensions(spaceId);
            if (dims != null) {
                for (int j = 0; j < dims.length; j++) {
                    lsize *= dims[j];
                }
            }
            int size = (int) lsize;
            if (size > -1) {
                String attributeName = H5.H5Aget_name(attributeId);
                Object value = null;
                long nativeTypeId = H5.H5Tget_native_type(dataTypeId);
                long encodingType = H5.H5Tget_size(nativeTypeId);
                boolean variableStr, vlen, compound, scalar;
                try {
                    variableStr = H5.H5Tis_variable_str(nativeTypeId);
                } catch (Exception e) {
                    variableStr = false;
                }
                scalar = (dims == null);
                int classType = H5.H5Tget_class(dataTypeId);
                vlen = (classType == HDF5Constants.H5T_VLEN);
                compound = (classType == HDF5Constants.H5T_COMPOUND);
                // TODO maybe use H5Datatype.allocateArray
                value = HdfObjectUtils.createArrayForDataReading(nativeTypeId, encodingType, classType, size,
                        variableStr, vlen, compound, scalar);
                if (vlen || variableStr || compound || (scalar && (classType == HDF5Constants.H5T_ARRAY))) {
                    try {
                        H5.H5AreadVL(attributeId, nativeTypeId, (String[]) value);
                    } catch (Exception e) {
                        Factory.getLogger().warn("Failed to read attribute " + attributeName, e);
                    }
                } else if (value != null) {
                    H5.H5Aread(attributeId, nativeTypeId, value);
                    if (value instanceof byte[]) {
                        byte[] bval = (byte[]) value;
                        if (classType == HDF5Constants.H5T_STRING) {
                            // TODO call Dataset.byteToString
                            value = HdfObjectUtils.byteToString(bval, bval.length / size);
                        } else if (classType == HDF5Constants.H5T_REFERENCE) {
                            value = HDFNativeData.byteToLong(bval);
                        }
                    }
                }
                if ((attributeName != null) && (value != null)) {
                    attribute = new HdfAttribute(factoryName, attributeName, value, false);
                }
            }
        }
        return attribute;
    }

    /**
     * Recovers the attributes of a parent identified by its id.
     * 
     * @param factoryName The factory name, necessary for {@link HdfAttribute} construction.
     * @param parentId The parent id.
     * @return An {@link HdfAttribute} {@link Collection}.
     * @throws DataAccessException If a problem occurred.
     */
    // based on H5File.getAttribute(int, int, int)
    public static Collection<HdfAttribute> readAttributes(String factoryName, long parentId)
            throws DataAccessException {
        Collection<HdfAttribute> attributeList = new ArrayList<>();
        try {
            H5O_info_t info = H5.H5Oget_info(parentId);
            long attributeId = -1;
            for (long i = 0; i < info.num_attrs; i++) {
                // iterate by attribute creation date, top down
                attributeId = H5.H5Aopen_by_idx(parentId, CURRENT_PATH, HDF5Constants.H5_INDEX_NAME,
                        HDF5Constants.H5_ITER_INC, i, HDF5Constants.H5P_DEFAULT, HDF5Constants.H5P_DEFAULT);
//                attributeId = H5.H5Oopen_by_idx(parentId, CURRENT_PATH, HDF5Constants.H5_INDEX_CRT_ORDER,
//                        HDF5Constants.H5_ITER_INC, i, HDF5Constants.H5P_DEFAULT);
                HdfAttribute attribute = recoverAttribute(factoryName, attributeId);
                if (attribute != null) {
                    attributeList.add(attribute);
                }
            }
            return attributeList;
        } catch (Exception e) {
            throw HdfObjectUtils.toDataAccessException(e, "Failed to read attributes");
        }
    }

    @Override
    public String getFactoryName() {
        return factoryName;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Class<?> getType() {
        Class<?> result = null;
        if (value != null && value.getClass().isArray()) {
            result = value.getClass().getComponentType();
        }
        return result;
    }

    @Override
    public boolean isString() {
        boolean result = false;
        if (value != null && value.getClass().isArray()) {
            Class<?> container = value.getClass().getComponentType();
            result = (container.equals(Character.TYPE) || container.equals(String.class));
        }
        return result;
    }

    @Override
    public boolean isArray() {
        return getLength() > 1;
    }

    @Override
    public int getLength() {
        int result = Array.getLength(value);
        return result;
    }

    @Override
    public IArray getValue() {
        IArray result = null;
        try {
            result = new HdfArray(getFactoryName(), value, ArrayTools.detectShape(value));
        } catch (InvalidArrayTypeException e) {
            Factory.getLogger().error("Unable to load attribute value", e);
        }
        return result;
    }

    @Override
    public String getStringValue() {
        String result;
        if (isString()) {
            if (Character.TYPE.equals(getType())) {
                result = new String((char[]) value);
            } else {
                result = getStringValue(0);
            }
        } else {
            result = getNumericValue().toString();
        }
        return result;
    }

    @Override
    public String getStringValue(final int index) {
        if (isString()) {
            return ((String[]) value)[0];
        } else {
            return null;
        }
    }

    @Override
    public Number getNumericValue() {
        Number result = null;
        if (!isString()) {
            result = getNumericValue(0);
        }
        return result;
    }

    @Override
    public Number getNumericValue(final int index) {
        Object localValue;
        localValue = java.lang.reflect.Array.get(value, index);

        if (isString()) {
            localValue = Double.parseDouble((String) localValue);
        }

        return (Number) localValue;
    }

    @Override
    public void setStringValue(final String val) {
        value = new String[] { val };
        dirty = true;
    }

    @Override
    public void setValue(final IArray value) {
        this.value = value.getStorage();
        this.dirty = true;
    }

    public boolean isDirty() {
        return dirty;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        boolean equals;
        if (this == obj) {
            equals = true;
        } else if (obj == null) {
            equals = false;
        } else if (getClass() != obj.getClass()) {
            equals = false;
        } else {
            HdfAttribute other = (HdfAttribute) obj;
            if (name == null) {
                equals = (other.name == null);
            } else {
                equals = name.equals(other.name);
            }
        }
        return equals;
    }

//    @Override
//    public String toString() {
//        StringBuilder builder = new StringBuilder(HdfAttribute.class.getSimpleName());
//        builder.append("{name:'").append(name).append("', value:").append(ArrayUtils.toString(value)).append("}");
//        return builder.toString();
//    }

}

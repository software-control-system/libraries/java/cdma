/*******************************************************************************
 * Copyright (c) 2008 - ANSTO/Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * Norman Xiong (nxi@Bragg Institute) - initial API and implementation
 * Tony Lam (nxi@Bragg Institute) - initial API and implementation
 * Majid Ounsy (SOLEIL Synchrotron) - API v2 design and conception
 * Stephane Poirier (SOLEIL Synchrotron) - API v2 design and conception
 * Clement Rodriguez (ALTEN for SOLEIL Synchrotron) - API evolution
 * Gregory VIGUIER (SOLEIL Synchrotron) - API evolution
 * Raphael GIRARDOT (SOLEIL Synchrotron) - API evolution
 ******************************************************************************/
package org.cdma.engine.hdf.utils;

import org.cdma.interfaces.IContainer;
import org.cdma.interfaces.INode;

import fr.soleil.lib.project.ObjectUtils;

public class HdfNode implements INode {

    public static final String ATTRIBUTE_SEPARATOR_START = "<";
    public static final String ATTRIBUTE_SEPARATOR_START2 = "{";
    protected static final char ATTRIBUTE_SEPARATOR_START_CHAR = '<';
    protected static final char ATTRIBUTE_SEPARATOR_START2_CHAR = '{';
    protected static final String ANY = "*";
    protected static final String REGEXP_ANY = ".*";
    protected static final String CLASS_PREFIX = "NX";
    protected static final String CLASS_END = ">";
    protected static final String CLASS_END2 = "}";
    protected static final char CLASS_END_CHAR = '>';
    protected static final char CLASS_END2_CHAR = '}';

    // Private definitions
    protected final String name;
    private boolean isGroup;

    public HdfNode(final String name) {
        this.name = name;
    }

    public HdfNode(IContainer container) {
        this.name = container.getShortName();
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getNodeName() {
        return getName();
    }

    @Override
    public boolean matchesNode(final INode node) {
        boolean nameMatch;
        nameMatch = node.getNodeName().isEmpty() || this.getNodeName().equalsIgnoreCase(node.getNodeName());

        return (nameMatch);
    }

    @Override
    public boolean matchesPartNode(final INode node) {
        boolean nameMatch = false;
        if (node != null) {
            nameMatch = node.getNodeName().isEmpty() || this.getNodeName().toLowerCase().replace(ANY, REGEXP_ANY)
                    .matches(node.getNodeName().toLowerCase().replace(ANY, REGEXP_ANY));
        }
        return (nameMatch);
    }

    @Override
    public boolean isGroup() {
        return isGroup;
    }

    @Override
    public String toString() {
        return name;
    }


    /**
     * Extracts the name without className, in a node name that may llok like <code>desiredName&lt;className&gt;</code>
     * or <code>desiredName{className}</code>.
     * 
     * @param sNodeName The node name.
     * @return A {@link String}.
     */
    public static String extractName(final String sNodeName) {
        int iPosClassSep = indexOfClassSeparator(sNodeName);
        String tmpNodeName = ObjectUtils.EMPTY_STRING;
        if (iPosClassSep < 0) {
            if ((sNodeName != null) && (!sNodeName.isEmpty())) {
                tmpNodeName = sNodeName.substring(0, sNodeName.length());
            }
        } else {
            tmpNodeName = sNodeName.substring(0, iPosClassSep);
        }
        return tmpNodeName;
    }

    /**
     * Extracts the class name in a node name that looks like <code>desiredName&lt;className&gt;</code> or
     * <code>desiredName{className}</code>.
     * 
     * @param nodeName The node name.
     * @return A {@link String}.
     */
    public static String extractClass(final String nodeName) {
        int iPosClassSep = indexOfClassSeparator(nodeName);
        String tmpClassName = ObjectUtils.EMPTY_STRING;
        if (iPosClassSep > -1) {
            tmpClassName = nodeName.substring(iPosClassSep + 1, nodeName.length() - 1);
        }
        return tmpClassName;
    }

    /**
     * Returns whether a supposed class name really represents a class name.
     * 
     * @param className The name to test.
     * @return A <code>boolean</code>.
     */
    public static boolean isValidClassName(String className) {
        return (className != null) && className.startsWith(CLASS_PREFIX);
    }

    public static boolean hasClass(String nodeName) {
        return isValidClassName(extractClass(nodeName));
    }

    /**
     * Returns the index of the class separator ('<' or '{'), in a node name that looks like
     * <code>desiredName&lt;className&gt;</code> or <code>desiredName{className}</code>.
     * <p>
     * If the node name doesn't match this description, the returned value will be <code>-1</code>.
     * </p>
     * 
     * @param nodeName The node name.
     * @return An <code>int</code>: the index of the class separator. <code>-1</code> If there is no such separator.
     */
    public static int indexOfClassSeparator(String nodeName) {
        int index;
        if ((nodeName == null) || (nodeName.isEmpty())) {
            index = -1;
        } else {
            // find class separator according to class ending separator
            if (nodeName.charAt(nodeName.length() - 1) == CLASS_END_CHAR) {
                index = nodeName.lastIndexOf(ATTRIBUTE_SEPARATOR_START_CHAR);
            } else if (nodeName.charAt(nodeName.length() - 1) == CLASS_END2_CHAR) {
                index = nodeName.lastIndexOf(ATTRIBUTE_SEPARATOR_START2_CHAR);
            } else {
                index = -1;
            }
            if (index > -1) {
                // class separator found: check whether the string after separator really represents a class
                if (index < nodeName.length() - 3) {
                    if ((nodeName.charAt(index + 1) != 'N') || (nodeName.charAt(index + 2) != 'X')) {
                        index = -1;
                    }
                } else {
                    index = -1;
                }
            }
        }
        return index;
    }

}

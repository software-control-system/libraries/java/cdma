/*******************************************************************************
 * Copyright (c) 2008 - ANSTO/Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * Norman Xiong (nxi@Bragg Institute) - initial API and implementation
 * Tony Lam (nxi@Bragg Institute) - initial API and implementation
 * Majid Ounsy (SOLEIL Synchrotron) - API v2 design and conception
 * Stephane Poirier (SOLEIL Synchrotron) - API v2 design and conception
 * Clement Rodriguez (ALTEN for SOLEIL Synchrotron) - API evolution
 * Gregory VIGUIER (SOLEIL Synchrotron) - API evolution
 * Raphael GIRARDOT (SOLEIL Synchrotron) - API evolution
 ******************************************************************************/
package org.cdma.engine.hdf.navigation;

import java.io.File;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.MemoryUsage;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

import org.cdma.Factory;
import org.cdma.engine.hdf.array.HdfArray;
import org.cdma.engine.hdf.array.HdfIndex;
import org.cdma.engine.hdf.utils.HdfObjectUtils;
import org.cdma.engine.hdf.utils.HdfPath;
import org.cdma.exception.DataAccessException;
import org.cdma.exception.DimensionNotSupportedException;
import org.cdma.exception.InvalidArrayTypeException;
import org.cdma.exception.InvalidRangeException;
import org.cdma.exception.NotImplementedException;
import org.cdma.interfaces.IArray;
import org.cdma.interfaces.IAttribute;
import org.cdma.interfaces.IDataItem;
import org.cdma.interfaces.IDimension;
import org.cdma.interfaces.IGroup;
import org.cdma.interfaces.IIndex;
import org.cdma.interfaces.IRange;
import org.cdma.utils.Utilities.ModelType;
import org.slf4j.LoggerFactory;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.date.DateUtil;
import fr.soleil.lib.project.math.ArrayUtils;
import fr.soleil.lib.project.math.MathConst;
import fr.soleil.lib.project.math.NumberArrayUtils;
import hdf.hdf5lib.H5;
import hdf.hdf5lib.HDF5Constants;
import hdf.hdf5lib.HDFNativeData;
import hdf.hdf5lib.exceptions.HDF5LibraryException;
import hdf.hdf5lib.structs.H5L_info_t;

public class HdfDataItem extends AHdfContainer implements IDataItem {

    private static final AtomicLong LOAD_DATA_TIME = new AtomicLong(0);
    private static final String H5D_LAYOUT_ERROR = "H5D_LAYOUT_ERROR";
    private static final String H5D_COMPACT = "H5D_COMPACT";
    private static final String H5D_CONTIGUOUS = "H5D_CONTIGUOUS";
    private static final String H5D_CHUNKED = "H5D_CHUNKED";
    private static final String H5D_VIRTUAL = "H5D_VIRTUAL";
    private static final String H5D_NLAYOUTS = "H5D_NLAYOUTS";
    private static final String UNKNOWN = "UNKNOWN";
    private static final String EXTERNAL_LINK_SEPARATOR = "://";
    private static final String UNIT = "unit";
    private static final String SIGNAL = "signal";
    private static final String DATA_ITEM = "DataItem = ";
    private static final String NEW_LINE = "\n";
    private static final String SHAPE = "shape: ";
    private static final String ATTRIBUTES = "\nAttributes:\n";
    private static final String ATTRIBUTE_START = "- ";

    public static void resetMetrics() {
        LOAD_DATA_TIME.set(0);
    }

    public static void traceMetrics(String applicationId) {
        StringBuilder builder = new StringBuilder(HdfDataItem.class.getSimpleName());
        DateUtil.elapsedTimeToStringBuilder("\nHdfDataItem Cumulative item load data time: ", LOAD_DATA_TIME, builder);
        LoggerFactory.getLogger(applicationId).trace(builder.toString());
    }

    /**
     * Returns the name of a data item (HDF5 dataset) storage layout, knowing the layout type.
     * 
     * @param layoutType The code that identifies the layout type.
     * @return A {@link String}.
     */
    public static String getH5DLayoutName(int layoutType) {
        String layout;
        switch (layoutType) {
            case -1:
                // HDF5Constants.H5D_LAYOUT_ERROR = -1
                layout = H5D_LAYOUT_ERROR;
                break;
            case 0:
                // HDF5Constants.H5D_COMPACT = 0
                layout = H5D_COMPACT;
                break;
            case 1:
                // HDF5Constants.H5D_CONTIGUOUS = 1
                layout = H5D_CONTIGUOUS;
                break;
            case 2:
                // HDF5Constants.H5D_CHUNKED = 2
                layout = H5D_CHUNKED;
                break;
            case 3:
                // HDF5Constants.H5D_VIRTUAL = 3
                layout = H5D_VIRTUAL;
                break;
            case 4:
                // HDF5Constants.H5D_NLAYOUTS = 4
                layout = H5D_NLAYOUTS;
                break;
            default:
                // Unknown layout type
                layout = UNKNOWN;
                break;
        }
        return layout;
    }

    /**
     * Returns whether some data determined by its shape and origin, represents a subpart of some other data determined
     * by its shape.
     * 
     * @param origin The 1st data origin.
     * @param shape The 1st data shape.
     * @param originalShape The 2nd data shape.
     * @return A <code>boolean</code>.
     */
    protected static boolean isSubPart(long[] origin, long[] shape, long... originalShape) {
        boolean subPart = false;
        if ((origin != null) && (shape != null) && (originalShape != null)) {
            for (long pos : origin) {
                if (pos != 0) {
                    subPart = true;
                    break;
                }
            }
            if (!subPart) {
                subPart = !ObjectUtils.sameObject(shape, originalShape);
            }
        }
        return subPart;
    }

    /**
     * Returns the {@link HdfDataItem} identified by a name and a parent id.
     * 
     * @param factoryName The factory name to associate the {@link HdfDataItem} with.
     * @param parent The known parent {@link HdfGroup}.
     * @param parentId The parent id.
     * @param shortName The item short name.
     * @return An {@link HdfDataItem}.
     * @throws DataAccessException If a problem occurred during file reading.
     */
    public static HdfDataItem getDataItem(final String factoryName, final HdfGroup parent, final long parentId,
            final String shortName) throws DataAccessException {
        HdfDataItem item = null;
        if ((parent != null) && (parentId > -1) && (shortName != null) && (!shortName.trim().isEmpty())) {
            long itemID = -1;
            long dataSpaceID = -1;
            long dataTypeID = -1;
            String name = AHdfContainer.nameToStringBuilder(null, parent, shortName).toString();
            try {
                itemID = H5.H5Dopen(parentId, shortName, HDF5Constants.H5P_DEFAULT);
                if (itemID > -1) {
                    dataTypeID = H5.H5Dget_type(itemID);
                    long nativeTypeId = H5.H5Tget_native_type(dataTypeID);
                    Boolean unsigned = null;
                    int classType = H5.H5Tget_class(nativeTypeId);
                    if (classType != HDF5Constants.H5T_FLOAT && classType != HDF5Constants.H5T_STRING
                            && classType != HDF5Constants.H5T_REFERENCE && classType != HDF5Constants.H5T_BITFIELD
                            && classType != HDF5Constants.H5T_OPAQUE) {
                        int signType = H5.H5Tget_sign(nativeTypeId);
                        if (signType == HDF5Constants.H5T_SGN_NONE) {
                            unsigned = Boolean.TRUE;
                        } else {
                            unsigned = Boolean.FALSE;
                        }
                    }
                    dataSpaceID = H5.H5Dget_space(itemID);
                    long[] shape = HdfObjectUtils.getDimensions(dataSpaceID);
                    Collection<HdfAttribute> attributes = HdfAttribute.readAttributes(factoryName, itemID);
                    item = new HdfDataItem(factoryName, parent, shortName, attributes, shape, unsigned, false);
                    H5L_info_t link_info;
                    try {
                        link_info = H5.H5Lget_info(parentId, shortName, HDF5Constants.H5P_DEFAULT);
                    } catch (Throwable err) {
                        link_info = null;
                    }
                    if (link_info != null) {
                        if ((link_info.type == HDF5Constants.H5L_TYPE_SOFT)
                                || (link_info.type == HDF5Constants.H5L_TYPE_EXTERNAL)) {
                            String[] link_value = { null, null };
                            try {
                                H5.H5Lget_value(parentId, shortName, link_value, HDF5Constants.H5P_DEFAULT);
                            } catch (Exception ex) {
                                Arrays.fill(link_value, null);
                            }
                            if (link_value[0] != null) {
                                if (link_info.type == HDF5Constants.H5L_TYPE_SOFT) {
                                    item.linkedPath = link_value[0];
                                } else if (link_info.type == HDF5Constants.H5L_TYPE_EXTERNAL) {
                                    item.linkedPath = link_value[1] + EXTERNAL_LINK_SEPARATOR + link_value[0];
                                }
                            }
                        }
                    }
                    attributes.clear();
                }
            } catch (Exception e) {
                throw HdfObjectUtils.toDataAccessException(e, "Failed to access dataitem " + name);
            } finally {
                close(itemID, -1, dataTypeID, dataSpaceID, null, true, name);
            }
        }
        return item;
    }

    protected static String getPath(HdfDataset dataset) {
        return dataset == null ? null : dataset.getLocation();
    }

    protected static String getPath(HdfGroup group) {
        return group == null ? null : getPath(group.getDataset());
    }

    /**
     * Closes everything necessary for accessing an item.
     * 
     * @param itemID The item id
     * @param dcpl The item creation property
     * @param dataTypeID The data type id
     * @param dataSpaceID the dataspace id
     * @param subSpaceIDs The subspace ids, in case of partial data reading/writing
     * @param closeDataTypeId Whether to close data type.
     * @param name The item name (path).
     */
    public static void close(long itemID, long dcpl, long dataTypeID, long dataSpaceID, long[] subSpaceIDs,
            boolean closeDataTypeId, String name) {
        // close subSpaceIDs, dataTypeID, itemID and dataSpaceID.
        if (subSpaceIDs != null) {
            try {
                if (subSpaceIDs[0] != HDF5Constants.H5S_ALL) {
                    H5.H5Sclose(subSpaceIDs[0]);
                }
            } catch (Exception e) {
                Factory.getLogger().error("Failed to close subspace for item " + name, e);
            }
            try {
                if (subSpaceIDs[1] != HDF5Constants.H5S_ALL) {
                    H5.H5Sclose(subSpaceIDs[1]);
                }
            } catch (Exception e) {
                Factory.getLogger().error("Failed to close dataspace for item " + name, e);
            }
        }
        try {
            if (closeDataTypeId && (dataTypeID > -1)) {
                H5.H5Tclose(dataTypeID);
            }
        } catch (Exception e) {
            Factory.getLogger().error("Failed to close access to data type for item " + name, e);
        }
        try {
            if (itemID > -1) {
                H5.H5Dclose(itemID);
            }
        } catch (Exception e) {
            Factory.getLogger().error("Failed to close access to item " + name, e);
        }
        if (dcpl > -1) {
            try {
                H5.H5Pclose(dcpl);
            } catch (Exception e) {
                Factory.getLogger().error("Failed to close access to creation property for item " + name, e);
            }
        }
        try {
            if (dataSpaceID > -1) {
                H5.H5Sclose(dataSpaceID);
            }
        } catch (Exception e) {
            Factory.getLogger().error("Failed to close access to dataspace for item " + name, e);
        }
    }

    /**
     * Selects a subpart of an item, if necessary.
     * 
     * @param origin The part subpart origin.
     * @param shape The part subpart shape.
     * @param originalShape The item shape.
     * @param itemID THe item id.
     * @param dataSpaceID The item dataspace id.
     * @param subSpaceIDs The subspace ids. Must be an array of length <code>2</code>.
     * @return A <code>boolean</code>: Whether a subpart was selected.
     */
    protected static boolean selectedSubPart(long[] origin, final long[] shape, final long[] originalShape,
            final long itemID, final long dataSpaceID, final long[] subSpaceIDs) {
        boolean subPart = isSubPart(origin, shape, originalShape);
        if (subPart) {
            if (dataSpaceID > -1) {
                subSpaceIDs[1] = dataSpaceID;
            } else {
                subSpaceIDs[1] = H5.H5Dget_space(itemID);
            }
            subSpaceIDs[0] = H5.H5Screate_simple(shape.length, shape, null);
            // We access every element of the subset, 1 by 1, without jumping over elements.
            // https://portal.hdfgroup.org/display/HDF5/Reading+From+or+Writing+To+a+Subset+of+a+Dataset
            H5.H5Sselect_hyperslab(subSpaceIDs[1], HDF5Constants.H5S_SELECT_SET, origin, null, shape, null);
        }
        return subPart;
    }

//    private H5ScalarDS h5Item;
    protected String lastPath;
    protected HdfArray array;
    private Collection<HdfAttribute> attributes;
    protected boolean dirty;
    protected long[] shape;
    private HdfDataItem linkedItem;
    private String linkedPath;
    private Boolean unsigned;
    // fakeIndex is used in case of reshaping with 1 more dimension
    private int fakeIndex;
    private boolean encodingTypeSet;
    private long encodingType;
    private volatile boolean profile;
    private long knownItemID, knownTransferID, knownDataSpaceID;
    private String knownName;
    private boolean shouldCloseDatasetAfterMultiReading;

    // useful to only open dataspace once
    private boolean unsafeReadingEnabled;

    public HdfDataItem(final String factoryName, final String name) {
        this(factoryName, name, null);
    }

    public HdfDataItem(final String factoryName, final String name, Boolean unsigned) {
        this(factoryName, null, name, null, null, unsigned, true);
    }

    protected HdfDataItem(final String factoryName, final HdfGroup parent, final String shortName,
            final Collection<HdfAttribute> attributes, final long[] shape, final Boolean unsigned,
            final boolean dirty) {
        super(factoryName, parent);
        this.attributes = generateAttributeList(attributes);
        this.lastPath = getPath(parent);
        linkedItem = null;
        this.unsigned = unsigned;
        this.shortName = shortName;
        this.shape = shape;
        this.dirty = dirty;
        fakeIndex = -1;
        knownItemID = -1;
        knownTransferID = -1;
        knownDataSpaceID = -1;
        knownName = null;
        shouldCloseDatasetAfterMultiReading = false;
        unsafeReadingEnabled = false;
    }

    private HdfDataItem(final HdfDataItem dataItem) {
        super(dataItem.getFactoryName(), dataItem.parent);
        this.attributes = generateAttributeList(null);
        linkedItem = null;
        this.lastPath = dataItem.lastPath;
        this.shortName = dataItem.shortName;
        try {
            this.array = dataItem.getData();
            dirty = true;
        } catch (DataAccessException e) {
            Factory.getLogger().error(e.getMessage());
        }
        fakeIndex = -1;
        knownItemID = -1;
        knownTransferID = -1;
        knownDataSpaceID = -1;
        shouldCloseDatasetAfterMultiReading = false;
    }

    protected Collection<HdfAttribute> generateAttributeList(Collection<HdfAttribute> ref) {
//        Collection<IAttribute> attributes;
//        if (ref == null) {
//            attributes = new ArrayList<>();
//        } else {
//            attributes = new ArrayList<>(ref);
//        }
        Collection<HdfAttribute> attributes = Collections.newSetFromMap(new ConcurrentHashMap<HdfAttribute, Boolean>());
        if (ref != null) {
            attributes.addAll(ref);
        }
        return attributes;
    }

    // -------------------------------------------------------------------------
    // Code extracted from H5ScalarDS.create
    // -------------------------------------------------------------------------
    // HDF5 requires you to use chunking in order to define extendible datasets.
    // Chunking makes it possible to extend datasets efficiently,
    // without having to reorganize storage excessively.
    // Using default size of 64x...which has good performance
    protected static long[] computeChunks(long... shape) {
        long[] chunks;
        if (shape == null) {
            chunks = null;
        } else {
            chunks = new long[shape.length];
            for (int i = 0; i < shape.length; i++) {
                chunks[i] = Math.min(shape[i], 64);
            }
        }
        return chunks;
    }

    @Override
    public void changeDataset(HdfDataset hdfDataset) {
        if ((parent != null) && (hdfDataset != null)) {
            parent.changeDataset(hdfDataset);
            IDataItem item = parent.getDataItem(shortName);
            if ((item != this) && (item instanceof HdfDataItem)) {
                parent.addDataItem(this);
                if (!dirty) {
                    array = null;
                    attributes.clear();
                    loadAttributes();
                }
            }
        }
    }

    public final void setProfile(boolean profile) {
        this.profile = profile;
    }

    public final boolean isProfile() {
        return profile;
    }

    @Override
    public HdfDataItem clone() {
        HdfDataItem clone;
        clone = (HdfDataItem) super.clone();
        if (clone == this) {
            // should not happen as HdfDataItem implements Cloneable
            clone = new HdfDataItem(this);
        } else {
            clone.attributes = generateAttributeList(attributes);
            clone.dirty = true;
            clone.linkedItem = null;
            if (shape != null) {
                clone.shape = shape.clone();
            }
            try {
                clone.array = getData();
            } catch (DataAccessException e) {
                Factory.getLogger().error(e.getMessage());
            }
        }
        return clone;
    }

    public HdfDataItem getCopyWithFakeDimensionInsertedAt(int position) {
        HdfDataItem reshaped = clone();
        int[] shape = getShape();
        try {
            int[] origin = getData().getIndex().getOrigin();
            int[] newShape = new int[shape.length + 1];
            int[] newOrigin = new int[origin.length + 1];
            if (position == 0) {
                newShape[0] = 1;
                newOrigin[0] = 0;
                System.arraycopy(shape, 0, newShape, 1, shape.length);
                System.arraycopy(origin, 0, newOrigin, 1, origin.length);
            } else if (position == shape.length) {
                newShape[shape.length] = 1;
                newOrigin[shape.length] = 0;
                System.arraycopy(shape, 0, newShape, 0, shape.length);
                System.arraycopy(origin, 0, newOrigin, 0, origin.length);
            } else {
                System.arraycopy(shape, 0, newShape, 0, position);
                System.arraycopy(origin, 0, newOrigin, 0, position);
                newShape[position] = 1;
                newOrigin[position] = 0;
                System.arraycopy(shape, position, newShape, position + 1, shape.length - position);
                System.arraycopy(origin, position, newOrigin, position + 1, origin.length - position);
            }
            reshaped.array = new HdfArray(factoryName, getType(), ArrayUtils.EMPTY_SHAPE, reshaped);
            HdfIndex index = new HdfIndex(this.factoryName, newShape, newOrigin, newShape);
            reshaped.array.setIndex(index);
            reshaped.fakeIndex = position;
        } catch (InvalidArrayTypeException | DataAccessException e) {
            Factory.getLogger().error("Failed to reshape HdfDataItem", e);
        }
        return reshaped;
    }

    @Override
    public ModelType getModelType() {
        return ModelType.DataItem;
    }

    public void linkTo(HdfDataItem dataitem) {
        linkedItem = dataitem;
        dirty = true;
    }

    public void linkToExternal(String filePath, String itemPath) {
        if ((filePath != null) && (itemPath != null)) {
            linkedItem = null;
            linkedPath = filePath + EXTERNAL_LINK_SEPARATOR + itemPath;
            dirty = true;
        }
    }

    @Override
    public void addOneAttribute(final IAttribute attribute) {
        if (attribute instanceof HdfAttribute) {
            dirty = true;
            attributes.add((HdfAttribute) attribute);
        }
    }

    public HdfAttribute getAttribute(final String name, final boolean ignoreCase) {
        HdfAttribute result = null;
        if (name != null) {
            if (attributes.isEmpty()) {
                synchronized (attributes) {
                    if (attributes.isEmpty()) {
                        loadAttributes();
                    }
                }
            }
            for (HdfAttribute attribute : attributes) {
                if (attribute != null) {
                    String attrName = attribute.getName();
                    if ((attrName != null)
                            && ((ignoreCase && name.equalsIgnoreCase(attrName)) || name.equals(attrName))) {
                        result = attribute;
                    }
                }
            }
        }
        return result;
    }

    protected void loadAttributes() {
        // groupID is used to load attributes from a group, in the special case of virtual item
        long itemID = -1, groupID = -1;
        String name = getName();
        HdfDataset dataset = null;
        boolean close = false;
        try {
            dataset = getDataset();
            if (dataset != null) {
                if (!dataset.isOpen()) {
                    dataset.open();
                    close = true;
                }
                try {
                    itemID = H5.H5Dopen(dataset.getH5FileID(), name, HDF5Constants.H5P_DEFAULT);
                } catch (HDF5LibraryException | NullPointerException e) {
                    itemID = -1;
                    try {
                        // virtual item special case
                        groupID = H5.H5Gopen(dataset.getH5FileID(), name, HDF5Constants.H5P_DEFAULT);
                    } catch (HDF5LibraryException | NullPointerException e2) {
                        throw e;
                    }
                }
                Collection<HdfAttribute> attrs = HdfAttribute.readAttributes(factoryName,
                        itemID == -1 ? groupID : itemID);
                attributes.addAll(attrs);
                attrs.clear();
            }
        } catch (DataAccessException | HDF5LibraryException | NullPointerException e) {
            Factory.getLogger().error("Unable to loadAttributes for " + name, e);
        } finally {
            close(itemID, -1, -1, -1, null, false, name);
            try {
                // virtual item special case
                if (groupID > -1) {
                    H5.H5Gclose(groupID);
                }
            } catch (Exception e) {
                Factory.getLogger().error("Failed to close access to " + name, e);
            }

            HdfDataset.closeDataset(dataset, close);
        }
    }

    @Override
    public HdfAttribute getAttribute(final String name) {
        return getAttribute(name, false);
    }

    @Override
    public Collection<IAttribute> getAttributeList() {
        return new ArrayList<>(attributes);
    }

    @Override
    public HdfDataset getDataset() {
        HdfDataset result = null;
        HdfGroup parentGroup = getParentGroup();
        if (parentGroup != null) {
            result = parentGroup.getDataset();
        }
        return result;
    }

//    @Override
//    public String getLocation() {
//        String result = null;
//        IContainer parentGroup = getParentGroup();
//        if (parentGroup != null) {
//            result = parentGroup.getLocation();
//            // result = result + "/" + getName();
//        }
//        return result;
//    }

    @Override
    public HdfGroup getRootGroup() {
        HdfGroup result = null;
        HdfGroup parent = getParentGroup();
        if (parent != null) {
            result = parent.getRootGroup();
        }
        return result;
    }

    @Override
    public boolean hasAttribute(final String name, final String value) {
        return HdfAttribute.hasAttribute(name, value, attributes);
    }

    @Override
    public void setName(final String name) {
        String[] nodes = name.split(HdfPath.PATH_SEPARATOR);
        int depth = nodes.length - 1;

        if (depth >= 0) {
            setShortName(nodes[depth--]);
        }

        IGroup group = getParentGroup();
        while (group != null && !group.isRoot() && depth >= 0) {
            group.setShortName(nodes[depth--]);
        }
    }

    @Override
    public void setParent(final IGroup group) {
        if (group instanceof HdfGroup) {
            parent = (HdfGroup) group;
        }
    }

    @Override
    public long getLastModificationDate() {
        HdfDataset dataset = getDataset();
        return (dataset == null ? 0 : dataset.getLastModificationDate());
    }

    @Override
    public IAttribute findAttributeIgnoreCase(final String name) {
        IAttribute result = null;
        result = getAttribute(name, true);
        return result;
    }

    @Override
    public int findDimensionIndex(final String name) {
        return 0;
    }

//    @Override
//    public IDataItem getASlice(final int dimension, final int value) throws InvalidRangeException {
//        return getSlice(dimension, value);
//    }

    @Override
    public HdfArray getData() throws DataAccessException {
        if (array == null) {
            int[] shape = getShape();
            int[] origin = shape == null ? null : new int[getRank(shape)];
            try {
                array = getData(origin, shape);
            } catch (Exception e) {
                Factory.getLogger().error("Unable to initialize data!", e);
            }
        }
        return array;
    }

    @Override
    public HdfArray getData(final int[] origin, final int[] shape) throws DataAccessException, InvalidRangeException {
        HdfArray array = null;
        IIndex index = null;
        try {
            Class<?> type = getType();
            if (type != null) {
                array = new HdfArray(factoryName, type, ArrayUtils.EMPTY_SHAPE, this);
                if ((shape != null) && (origin != null) && (shape.length != 0) && (origin.length != 0)) {
                    index = new HdfIndex(this.factoryName, shape, origin, shape);
                    array.setIndex(index);
                }
            }
        } catch (InvalidArrayTypeException e) {
            throw new InvalidRangeException(e);
        }

        return array;
    }

    /**
     * Reads data type from hdf file.
     * 
     * @return A {@link Class}.
     */
    protected Class<?> recoverType() {
        // TODO code refactoring with load method
        Class<?> type = null;
        HdfDataset dataset = getDataset();
        if (dataset != null) {
            long itemID = -1, dataTypeID = -1, dataSpaceID = -1;
            int classType = -1;
            boolean close = false;
            String name = getName();
            try {
                if (!dataset.isOpen()) {
                    dataset.open();
                    close = true;
                }
                long fileID = dataset.getH5FileID();
                if (fileID > -1) {
                    itemID = H5.H5Dopen(fileID, name, HDF5Constants.H5P_DEFAULT);
                    if (itemID > -1) {
                        long encodingType = this.encodingType;
                        dataTypeID = H5.H5Dget_type(itemID);
                        classType = H5.H5Tget_class(dataTypeID);
                        dataSpaceID = H5.H5Dget_space(itemID);
                        boolean variableStr = false, vlen = false, compound = false, scalar = false;
                        scalar = (HdfObjectUtils.getDimensions(dataSpaceID) == null);
                        long formerId = -1;
                        try {
                            formerId = dataTypeID;
                            if (dataTypeID > -1) {
                                dataTypeID = H5.H5Tget_native_type(formerId);
                                // Read encodingType only if:
                                // - encodingType is not yet set
                                // - previous encodingType is obviously wrong
                                // This last case seems to happen in case of multi threads,
                                // with previous application badly closed (emergency shutdown).
                                if ((!encodingTypeSet)
                                        || ((encodingType == 0) && ((classType == HDF5Constants.H5T_INTEGER)
                                                || (classType == HDF5Constants.H5T_FLOAT)))) {
                                    encodingType = H5.H5Tget_size(dataTypeID);
                                    this.encodingType = encodingType;
                                    encodingTypeSet = true;
                                }
                                try {
                                    variableStr = H5.H5Tis_variable_str(dataTypeID);
                                } catch (Exception e) {
                                    variableStr = false;
                                }
                            }
                        } catch (Exception e) {
                            Factory.getLogger().error("Failed to recover native type id for item " + name, e);
                        } finally {
                            if ((formerId > -1) && (formerId != dataTypeID)) {
                                try {
                                    H5.H5Tclose(formerId);
                                } catch (Exception e) {
                                    Factory.getLogger().error("Failed to close type id for item " + name, e);
                                }
                            }
                        }
                        vlen = (classType == HDF5Constants.H5T_VLEN);
                        compound = (classType == HDF5Constants.H5T_COMPOUND);
                        type = HdfObjectUtils.getCorrespondingClass(dataTypeID, encodingType, classType, variableStr,
                                vlen, compound, scalar);
                    }
                }
            } catch (Exception e) {
                Factory.getLogger().error("Failed to recover data type for item " + name, e);
            } finally {
                close(itemID, -1, dataTypeID, dataSpaceID, null, true, name);
                HdfDataset.closeDataset(dataset, close);
            }
        }
        return type;
    }

    /**
     * Reads data shape from hdf file.
     * 
     * @return A {@link Class}.
     */
    protected long[] recoverShape() {
        // TODO code refactoring with load method
        long[] shape = null;
        HdfDataset dataset = getDataset();
        if (dataset != null) {
            long itemID = -1, dataTypeID = -1, dataSpaceID = -1;
            boolean close = false;
            String name = getName();
            try {
                if (!dataset.isOpen()) {
                    dataset.open();
                    close = true;
                }
                long fileID = dataset.getH5FileID();
                if (fileID > -1) {
                    itemID = H5.H5Dopen(fileID, name, HDF5Constants.H5P_DEFAULT);
                    if (itemID > -1) {
                        dataTypeID = H5.H5Dget_type(itemID);
                        dataSpaceID = H5.H5Dget_space(itemID);
                        shape = HdfObjectUtils.getDimensions(dataSpaceID);
                    }
                }
            } catch (Exception e) {
                Factory.getLogger().error("Failed to recover shape for item " + name, e);
            } finally {
                close(itemID, -1, dataTypeID, dataSpaceID, null, true, name);
                HdfDataset.closeDataset(dataset, close);
            }
        }
        return shape;
    }

    @Override
    public int prepareForReading(int rank) throws DataAccessException {
        // to optimize reading, we try to open item only once
        int dataRank = Math.max(0, rank);
        long itemID = -1, transferID = -1;
        boolean close = false;
        int dataPerBlock = -1;
        try {
            HdfDataset dataset = getDataset();
            if (dataset != null) {
                if (!dataset.isOpen()) {
                    dataset.open();
                    close = true;
                }
                long fileID = dataset.getH5FileID();
                if (fileID > -1) {
                    // fileID is available: let's try to optimize cache/buffer and open data item (HDF5 dataset)
                    String name = getName();
                    knownName = name;
                    // Creation of data transfer property
                    transferID = H5.H5Pcreate(HDF5Constants.H5P_DATASET_XFER);
                    long fileSize = new File(dataset.getLocation()).length();
                    MemoryMXBean mxBean = ManagementFactory.getMemoryMXBean();
                    MemoryUsage usage = mxBean.getHeapMemoryUsage();
                    long used = usage.getUsed();
                    long max = usage.getMax();
                    // Consider we might have 2 times current used memory, and we may load up to 10 files.
                    long maxSize = (max - 2 * used) / 10;
                    long maxAllowed = Math.max(HdfObjectUtils.DEFAULT_CACHE_SIZE, maxSize);
                    long div = maxAllowed / HdfObjectUtils.DEFAULT_CACHE_SIZE;
                    // Allow cache up to 1000 times default value.
                    long newSize = Math.min(fileSize, HdfObjectUtils.DEFAULT_CACHE_SIZE * Math.min(1000, div));
                    long currentBufferSize = H5.H5Pget_buffer_size(transferID);
                    if (newSize > currentBufferSize) {
                        // Use transferID for setting buffer
                        H5.H5Pset_buffer_size(transferID, newSize);
                    }
                    int[] shape = getShape();
                    if ((shape != null) && (shape.length > 0) && (shape.length >= dataRank)) {
                        long vectorSize = 1, size = 1;
                        vectorSize = 1;
                        for (int i = shape.length - dataRank; i < shape.length; i++) {
                            vectorSize *= shape[i];
                        }
                        size = vectorSize;
                        Class<?> dataType = getType();
                        int mult;
                        if (Boolean.TYPE.equals(dataType) || Boolean.class.equals(dataType)
                                || Byte.TYPE.equals(dataType) || Byte.class.equals(dataType)) {
                            mult = 4;
                        } else if (Short.TYPE.equals(dataType) || Short.class.equals(dataType)) {
                            mult = 8;
                        } else if (Integer.TYPE.equals(dataType) || Integer.class.equals(dataType)
                                || Float.TYPE.equals(dataType) || Float.class.equals(dataType)) {
                            mult = 16;
                        } else if (Long.TYPE.equals(dataType) || Long.class.equals(dataType)
                                || Double.TYPE.equals(dataType) || Double.class.equals(dataType)) {
                            mult = 32;
                        } else {
                            mult = Integer.MAX_VALUE;
                        }
                        if (isUnsigned() && (mult < Integer.MAX_VALUE)) {
                            mult *= 2;
                        }
                        size *= mult;
                        if (shape.length == dataRank) {
                            dataPerBlock = 1;
                        } else {
                            long refCountL = maxSize / size;
                            int refCount;
                            if (refCountL > Integer.MAX_VALUE) {
                                refCount = Integer.MAX_VALUE;
                            } else {
                                refCount = (int) refCountL;
                            }
                            // limit dataPerBlock to last stack dimension
                            dataPerBlock = Math.min(shape[shape.length - (rank + 1)], refCount);
                        }
                        // TODO Maybe readapt hyper vector size according to available memory
                        // According to HDF5 documentation, default hyper vector size is 1024
                        if (vectorSize > 1024) {
                            H5.H5Pset_hyper_vector_size(transferID, vectorSize);
                        }
                    } // end if ((shape != null) && (shape.length > 0) && (shape.length >= dataRank))
                      // TODO Maybe change chunk cache
                    itemID = H5.H5Dopen(fileID, name, HDF5Constants.H5P_DEFAULT);
                } else {
                    transferID = -1;
                }
            } // end if (dataset != null)
        } catch (Exception e) {
            throw HdfObjectUtils.toDataAccessException(e);
        } finally {
            knownItemID = itemID;
            knownTransferID = transferID;
            shouldCloseDatasetAfterMultiReading = close;
        }
        if (dataPerBlock < 1) {
            dataPerBlock = 1;
        }
        return dataPerBlock;
    }

    @Override
    public void finalizeReading() throws DataAccessException {
        long itemID = knownItemID;
        knownItemID = -1;
        close(itemID, -1, -1, -1, null, false, knownName);
        long transferID = knownTransferID;
        knownTransferID = -1;
        HdfDataset.closeDataset(getDataset(), shouldCloseDatasetAfterMultiReading);
        if (transferID > -1) {
            try {
                H5.H5Pclose(transferID);
            } catch (Exception e) {
                Factory.getLogger().error("Failed to close transfer property for " + knownName, e);
            }
        }
        shouldCloseDatasetAfterMultiReading = false;
    }

    @Override
    public boolean isUnsafeReadingEnabled() {
        return unsafeReadingEnabled;
    }

    @Override
    public void setUnsafeReadingEnabled(boolean unsafeReadingEnabled) {
        this.unsafeReadingEnabled = unsafeReadingEnabled;
    }

    public Object load(final long[] origin, final long[] shape) throws DataAccessException {
        long time = -1;
        if (profile) {
            time = System.currentTimeMillis();
        }
        Object result = null;
        try {
            long[] originalShape = this.shape;

            HdfDataset dataset = getDataset();
            long[] start = origin;
            long[] selectedShape = shape;
            if ((start != null) && (selectedShape != null) && (originalShape != null) && (dataset != null)) {
                boolean dimOk;
                if ((selectedShape.length == originalShape.length + 1) && (fakeIndex > -1)
                        && (fakeIndex < selectedShape.length)) {
                    long[] newShape = new long[originalShape.length];
                    if (fakeIndex > 0) {
                        System.arraycopy(selectedShape, 0, newShape, 0, fakeIndex);
                    }
                    if (fakeIndex < newShape.length) {
                        System.arraycopy(selectedShape, fakeIndex + 1, newShape, fakeIndex,
                                newShape.length - fakeIndex);
                    }
                    selectedShape = newShape;
                }
                dimOk = (selectedShape.length == originalShape.length);
                if ((start.length == originalShape.length + 1) && (fakeIndex > -1) && (fakeIndex < start.length)) {
                    long[] newStart = new long[originalShape.length];
                    if (fakeIndex > 0) {
                        System.arraycopy(start, 0, newStart, 0, fakeIndex);
                    }
                    if (fakeIndex < newStart.length) {
                        System.arraycopy(start, fakeIndex + 1, newStart, fakeIndex, newStart.length - fakeIndex);
                    }
                    start = newStart;
                }
                dimOk = dimOk && (start.length == originalShape.length);
                for (int i = 0; i < start.length && dimOk; i++) {
                    long o = start[i], s = selectedShape[i], os = originalShape[i];
                    if ((o < 0) || (s < 0) || (os < 0) || (o + s > os)) {
                        dimOk = false;
                    }
                } // end for (int i = 0; i < origin.length && dimOk; i++)
                if (dimOk) {
                    long lsize = 1;
                    for (long dim : selectedShape) {
                        lsize *= dim;
                    }
                    int size = (int) lsize;
                    boolean closeItem = false;
                    long itemID = knownItemID, dataTypeID = -1, dataSpaceID = knownDataSpaceID;
                    long[] subSpaceIDs = new long[] { HDF5Constants.H5S_ALL, HDF5Constants.H5S_ALL };
                    boolean closeDataTypeId = false;
                    String name = getName();

                    boolean close = false;
                    try {
                        if (!dataset.isOpen()) {
                            dataset.open();
                            close = true;
                        }
                        long fileID = dataset.getH5FileID();
                        if (fileID > -1) {
                            if (itemID < 0) {
                                itemID = H5.H5Dopen(fileID, name, HDF5Constants.H5P_DEFAULT);
                                closeItem = true;
                            }
                            if (itemID > -1) {
                                long encodingType = this.encodingType;
                                dataTypeID = H5.H5Dget_type(itemID);
                                int classType = H5.H5Tget_class(dataTypeID);
                                if (dataSpaceID < 0) {
                                    dataSpaceID = H5.H5Dget_space(itemID);
                                }
                                boolean variableStr = false, vlen = false, compound = false, scalar = false;
                                scalar = (HdfObjectUtils.getDimensions(dataSpaceID) == null);
                                long formerId = -1;
                                try {
                                    formerId = dataTypeID;
                                    if (dataTypeID > -1) {
                                        dataTypeID = H5.H5Tget_native_type(formerId);
                                        if (!encodingTypeSet) {
                                            encodingType = H5.H5Tget_size(dataTypeID);
                                            this.encodingType = encodingType;
                                            encodingTypeSet = true;
                                        }
                                        try {
                                            variableStr = H5.H5Tis_variable_str(dataTypeID);
                                        } catch (Exception e) {
                                            variableStr = false;
                                        }
                                    }
                                } catch (Exception e) {
                                    Factory.getLogger().error("Failed to recover native type id for item " + name, e);
                                } finally {
                                    if ((formerId > -1) && (formerId != dataTypeID)) {
                                        try {
                                            H5.H5Tclose(formerId);
                                        } catch (Exception e) {
                                            Factory.getLogger().error("Failed to close type id for item " + name, e);
                                        }
                                    }
                                }
                                closeDataTypeId = true;
                                vlen = (classType == HDF5Constants.H5T_VLEN);
                                compound = (classType == HDF5Constants.H5T_COMPOUND);
                                if (selectedSubPart(start, selectedShape, originalShape, itemID, dataSpaceID,
                                        subSpaceIDs) && (subSpaceIDs[1] == dataSpaceID)) {
                                    dataSpaceID = -1;
                                }
                                long transferID = knownTransferID;
                                if (transferID < 0) {
                                    transferID = HDF5Constants.H5P_DEFAULT;
                                }
                                // TODO maybe use H5Datatype.allocateArray

                                result = HdfObjectUtils.createArrayForDataReading(dataTypeID, encodingType, classType,
                                        size, variableStr, vlen, compound, scalar);
                                if (variableStr && (result instanceof String[])) {
                                    H5.H5Dread_VLStrings(itemID, dataTypeID, subSpaceIDs[0], subSpaceIDs[1], transferID,
                                            (String[]) result);
                                } else if (result instanceof Object[]) {
                                    H5.H5DreadVL(itemID, dataTypeID, subSpaceIDs[0], subSpaceIDs[1], transferID,
                                            (Object[]) result);
                                } else {
                                    H5.H5Dread(itemID, dataTypeID, subSpaceIDs[0], subSpaceIDs[1], transferID, result);
                                }
                                if (result instanceof byte[]) {
                                    byte[] bval = (byte[]) result;
                                    if (classType == HDF5Constants.H5T_STRING) {
                                        result = HdfObjectUtils.byteToString(bval, bval.length / size);
                                    } else if (classType == HDF5Constants.H5T_REFERENCE) {
                                        result = HDFNativeData.byteToLong(bval);
                                    }
                                }
                            } // end if (itemID > -1)
                        } // end if (fileID > -1)
                    } catch (Exception e) {
                        throw HdfObjectUtils.toDataAccessException(e, "Failed to load data for item " + name);
                    } finally {
                        if (unsafeReadingEnabled) {
                            if ((subSpaceIDs != null) && (subSpaceIDs.length > 1)
                                    && (subSpaceIDs[1] == knownDataSpaceID)) {
                                subSpaceIDs[1] = -1;
                            }
                            close(-1, -1, dataTypeID, -1, subSpaceIDs, closeDataTypeId, name);
                        } else {
                            close(closeItem ? itemID : -1, -1, dataTypeID, dataSpaceID, subSpaceIDs, closeDataTypeId,
                                    name);
                            HdfDataset.closeDataset(dataset, close);
                        }
                    }
                } // end if (dimOk)
            } // end if ((startDims != null) && (selectedShape != null) && (originalShape != null) && (dataset != null))
            else if (dirty) {
                // Special case where dataitem was just created without being stored in a file.
                // Also works with a virtual item.
                HdfArray data = getData();
                if (data != null) {
                    // lock data to avoid stackOverFlowError (data won't try to ask data loading)
                    boolean locked = data.isLocked();
                    if (!locked) {
                        data.lock();
                    }
                    result = data.getStorage();
                    if (locked) {
                        data.unlock();
                    }
                }
            }
        } finally {
            if (profile) {
                LOAD_DATA_TIME.addAndGet(System.currentTimeMillis() - time);
            }
        }
        return result;
    }

    @Override
    public String getDescription() {
        String result = null;
        IAttribute attribute = null;

        result = getAttribute("long_name").getStringValue();
        if (attribute == null) {
            result = getAttribute("description").getStringValue();
        }
        if (attribute == null) {
            result = getAttribute("title").getStringValue();
        }
        if (attribute == null) {
            result = getAttribute("standard_name").getStringValue();
        }
        if (attribute == null) {
            result = getAttribute("name").getStringValue();
        }
        return result;
    }

    @Override
    public List<IDimension> getDimensions(final int index) {
        return new ArrayList<IDimension>();
    }

    @Override
    public List<IDimension> getDimensionList() {
        return new ArrayList<IDimension>();
    }

    @Override
    public String getDimensionsString() {
        return ObjectUtils.EMPTY_STRING;
    }

    @Override
    public int getElementSize() {
        int result = 0;
        if (!encodingTypeSet) {
            HdfDataset dataset = getDataset();
            if (dataset != null) {
                long itemID = -1, dataTypeID = -1, nativeTypeID = -1;
                boolean close = false;
                try {
                    if (!dataset.isOpen()) {
                        dataset.open();
                        close = true;
                    }
                    long fileID = dataset.getH5FileID();
                    if (fileID > -1) {
                        itemID = H5.H5Dopen(fileID, getName(), HDF5Constants.H5P_DEFAULT);
                        if (itemID > -1) {
                            dataTypeID = H5.H5Dget_type(itemID);
                            if (dataTypeID > -1) {
                                nativeTypeID = H5.H5Tget_native_type(dataTypeID);
                                encodingType = H5.H5Tget_size(dataTypeID);
                            }
                        }
                    }
                } catch (Exception e) {
                    Factory.getLogger().error("Failed to read encoding type for item " + getName(), e);
                } finally {
                    if (dataTypeID > -1) {
                        try {
                            H5.H5Tclose(dataTypeID);
                        } catch (Exception e) {
                            Factory.getLogger().error("Failed to close data type id for item " + getName(), e);
                        }
                    }
                    if ((nativeTypeID > -1) && (nativeTypeID != dataTypeID)) {
                        try {
                            H5.H5Tclose(nativeTypeID);
                        } catch (Exception e) {
                            Factory.getLogger().error("Failed to close native type id for item " + getName(), e);
                        }
                    }
                    HdfDataset.closeDataset(dataset, close);
                }
            }
        }
        result = encodingType < 0 ? 0 : (int) encodingType;
        return result;
    }

    @Override
    public String getNameAndDimensions() {
        return null;
    }

    @Override
    public void getNameAndDimensions(final StringBuilder buf, final boolean longName, final boolean length) {
    }

    @Override
    public List<IRange> getRangeList() {
        List<IRange> list = new ArrayList<IRange>();
        try {
            HdfIndex index = getData().getIndex();
            list.addAll(index.getRangeList());
        } catch (DataAccessException e) {
            list = null;
        }
        return list;
    }

    protected int getRank(int... shape) {
        return shape == null ? 0 : shape.length;
    }

    @Override
    public int getRank() {
        return getRank(getShape());
    }

    @Override
    public IDataItem getSection(final List<IRange> section) throws InvalidRangeException {
        HdfDataItem item = null;
        try {
            item = new HdfDataItem(this);
            item.array = item.getData().getArrayUtils().sectionNoReduce(section).getArray();
        } catch (DataAccessException e) {
        }
        return item;
    }

    @Override
    public List<IRange> getSectionRanges() {
        List<IRange> list = new ArrayList<IRange>();
        try {
            HdfIndex index = getData().getIndex();
            list.addAll(index.getRangeList());
        } catch (DataAccessException e) {
            list = null;
        }
        return list;
    }

    @Override
    public int[] getShape() {
        HdfArray array = this.array;
        int[] shape;
        if ((array == null) && (this.shape == null)) {
            this.shape = recoverShape();
        }
        if (array == null) {
            shape = NumberArrayUtils.extractIntArray(this.shape);
        } else {
            shape = array.getShape();
        }
        return shape;
    }

    @Override
    public long getSize() {
        long size = 0;
        long[] dims = shape;
        if (dims != null) {
            for (int i = 0; i < dims.length; i++) {
                if (dims[i] >= 0) {
                    size *= dims[i];
                }
            }
        }
        return size;
    }

    @Override
    public int getSizeToCache() {
        throw new NotImplementedException();
    }

    @Override
    public IDataItem getSlice(final int dim, final int value) throws InvalidRangeException {
        HdfDataItem item = new HdfDataItem(this);
        try {
            item.array = item.getData().getArrayUtils().slice(dim, value).getArray();
        } catch (Exception e) {
            item = null;
        }
        return item;
    }

    @Override
    public Class<?> getType() {
        Class<?> result = null;
        if (array != null) {
            result = array.getElementType();
        }
        if (result == null) {
            result = recoverType();
        }
        return result;
    }

    @Override
    public String getUnitsString() {
        IAttribute attr = getAttribute(UNIT);
        String value = null;
        if (attr != null) {
            value = attr.getStringValue();
        }
        return value;
    }

    @Override
    public boolean hasCachedData() {
        throw new NotImplementedException();
    }

    @Override
    public void invalidateCache() {
        throw new NotImplementedException();
    }

    @Override
    public boolean isCaching() {
        throw new NotImplementedException();
    }

    @Override
    public boolean isMemberOfStructure() {
        throw new NotImplementedException();
    }

    @Override
    public boolean isMetadata() {
        return (getAttribute(SIGNAL) == null);
    }

    @Override
    public boolean isScalar() {
        return (getRank() == 0);
    }

    @Override
    public boolean isUnlimited() {
        return false;
    }

    @Override
    public boolean isUnsigned() {
        boolean result;
        if (unsigned == null) {
            result = false;
        } else {
            result = unsigned.booleanValue();
        }
        return result;
    }

    private Object getItemData() throws DataAccessException {
        return getItemData(HdfObjectUtils.RETRY_COUNT, null);
    }

    private Object getItemData(int count, DataAccessException previousException) throws DataAccessException {
        Object result = null;
        HdfArray array = getData();
        if (count < 0) {
            if (previousException != null) {
                throw previousException;
            }
        } else {
            try {
                long[] shape, origin;
                if (array == null) {
                    shape = this.shape;
                    origin = new long[shape.length];
                } else {
                    shape = NumberArrayUtils.extractLongArray(array.getShape());
                    HdfIndex index = array.getIndex();
                    if (index == null) {
                        origin = new long[shape.length];
                    } else {
                        origin = NumberArrayUtils.extractLongArray(index.getOrigin());
                    }
                }
                result = load(origin, shape);
            } catch (DataAccessException e) {
                if (HdfObjectUtils.prepareRetry()) {
                    result = getItemData(count - 1, e);
                }
            }
        }

        return result;
    }

    @Override
    public byte readScalarByte() throws DataAccessException {
        try {
            Object data = getItemData();
            return data == null ? 0 : Array.getByte(data, 0);
        } catch (Exception e) {
            throw HdfObjectUtils.toDataAccessException(e, "Failed to read byte for item " + getName());
        }
    }

    @Override
    public short readScalarShort() throws DataAccessException {
        try {
            Object data = getItemData();
            return data == null ? 0 : Array.getShort(data, 0);
        } catch (Exception e) {
            throw HdfObjectUtils.toDataAccessException(e, "Failed to read short for item " + getName());
        }
    }

    @Override
    public int readScalarInt() throws DataAccessException {
        try {
            Object data = getItemData();
            return data == null ? 0 : Array.getInt(data, 0);
        } catch (Exception e) {
            throw HdfObjectUtils.toDataAccessException(e, "Failed to read int for item " + getName());
        }
    }

    @Override
    public long readScalarLong() throws DataAccessException {
        try {
            Object data = getItemData();
            return data == null ? 0 : Array.getLong(data, 0);
        } catch (Exception e) {
            throw HdfObjectUtils.toDataAccessException(e, "Failed to read long for item " + getName());
        }
    }

    @Override
    public float readScalarFloat() throws DataAccessException {
        try {
            Object data = getItemData();
            return data == null ? Float.NaN : Array.getFloat(data, 0);
        } catch (Exception e) {
            throw HdfObjectUtils.toDataAccessException(e, "Failed to read float for item " + getName());
        }
    }

    @Override
    public double readScalarDouble() throws DataAccessException {
        try {
            Object data = getItemData();
            return data == null ? MathConst.NAN_FOR_NULL : Array.getDouble(data, 0);
        } catch (Exception e) {
            throw HdfObjectUtils.toDataAccessException(e, "Failed to read double for item " + getName());
        }
    }

    @Override
    public String readScalarString() throws DataAccessException {
        try {
            // Scalar Strings are 1 dimension String arrays
            Object data = getItemData();
            return data == null ? null : ((String[]) data)[0];
        } catch (Exception e) {
            throw HdfObjectUtils.toDataAccessException(e, "Failed to read string for item " + getName());
        }
    }

    @Override
    public boolean removeAttribute(final IAttribute a) {
        boolean removed;
        if (a instanceof HdfAttribute) {
            removed = attributes.remove(a);
        } else {
            removed = false;
        }
        return removed;
    }

    @Override
    public void setCachedData(final IArray cacheData, final boolean isMetadata) throws InvalidArrayTypeException {
        if (cacheData instanceof HdfArray) {
            array = (HdfArray) cacheData;
            dirty = true;
        }
    }

    @Override
    public void setCaching(final boolean caching) {
        throw new NotImplementedException();
    }

    @Override
    public void setDataType(final Class<?> dataType) {
        throw new NotImplementedException();
    }

    @Override
    public void setDimensions(final String dimString) {
        throw new NotImplementedException();
    }

    @Override
    public void setDimension(final IDimension dim, final int ind) throws DimensionNotSupportedException {
        throw new NotImplementedException();
    }

    @Override
    public void setElementSize(final int elementSize) {
        throw new NotImplementedException();
    }

    @Override
    public void setSizeToCache(final int sizeToCache) {
        throw new NotImplementedException();
    }

    @Override
    public void setUnitsString(final String units) {
        throw new NotImplementedException();
    }

    @Override
    public String toString() {
        StringBuilder buffer = new StringBuilder(DATA_ITEM);
        buffer.append(getName());
        return buffer.toString();
    }

    @Override
    public String toStringDebug() {
        StringBuilder strDebug = new StringBuilder();
        strDebug.append(getName());
        if (strDebug.length() > 0) {
            strDebug.append(NEW_LINE);
        }
        try {
            strDebug.append(SHAPE).append(getData().shapeToString()).append(NEW_LINE);
            List<IDimension> dimensions = getDimensionList();
            for (IDimension dim : dimensions) {
                strDebug.append(dim.getCoordinateVariable().toString());
            }

            Collection<HdfAttribute> list = this.attributes;
            if (list.size() > 0) {
                strDebug.append(ATTRIBUTES);
            }
            for (IAttribute a : list) {
                strDebug.append(ATTRIBUTE_START).append(a).append(NEW_LINE);
            }
        } catch (DataAccessException e) {
        }
        return strDebug.toString();
    }

    /**
     * Prepares this {@link HdfDataItem} for being written by parts (if any preparation needed).
     */
    public void startPartialWriteMode() {
        // Nothing to do: no need for partial write mode.
        // This method is only here for backward compatibility with hdf engine.
    }

    /**
     * Sets this {@link HdfDataItem} back to classic data writing (if any preparation needed).
     */
    public void stopPartialWriteMode() {
        // Nothing to do: no need for partial write mode.
        // This method is only here for backward compatibility with hdf engine.
    }

    public void save(long parentId, long fileId, boolean ignoreDirtyTest) throws DataAccessException {
        if ((parentId > -1) && (fileId > -1) && (ignoreDirtyTest || dirty)) {
            long itemID = -1, dcpl = -1, dataTypeID = -1, dataSpaceID = -1;
            long[] subSpaceIDs = new long[] { HDF5Constants.H5S_ALL, HDF5Constants.H5S_ALL };
            boolean closeDataTypeId = false;
            boolean variableLengthStrings = true;
            String name = getName(), shortName = getShortName();
            if (linkedItem != null) {
                linkedPath = linkedItem.getName();
                linkedItem = null;
            }
            try {
                if (linkedPath == null) {
                    // Write data
                    try {
                        itemID = H5.H5Dopen(parentId, getShortName(), HDF5Constants.H5P_DEFAULT);
                    } catch (Exception e) {
                        itemID = -1; // item does not exist
                    }
                    long[] originalShape = this.shape;
                    if (array == null) {
                        array = getData();
                    }
                    HdfArray array = this.array;
                    if (array != null) {
                        // Recover what to write
                        Object toWrite = array.getStorage();
                        Class<?> dataType = array.getElementType();
                        long[] shape = NumberArrayUtils.extractLongArray(array.getShape());
                        long size = -1;
                        if (itemID > -1) {
                            // Item exists
                            dataTypeID = H5.H5Dget_type(itemID);
                            closeDataTypeId = true;
                            if (originalShape == null) {
                                dataSpaceID = H5.H5Dget_space(itemID);
                                originalShape = HdfObjectUtils.getDimensions(dataSpaceID);
                                this.shape = originalShape;
                            }
                            if (String.class.equals(dataType)) {
                                // This is my guess
                                variableLengthStrings = H5.H5Tis_variable_str(dataTypeID);
                            }
                            size = H5.H5Tget_size(dataTypeID);
                            IIndex index = array.getIndex();
                            if (index != null) {
                                long[] origin = NumberArrayUtils.extractLongArray(index.getOrigin());
                                if (origin.length < originalShape.length) {
                                    origin = Arrays.copyOf(origin, originalShape.length);
                                }
                                if (shape.length < originalShape.length) {
                                    long[] tmp = new long[originalShape.length];
                                    int start = originalShape.length - shape.length;
                                    System.arraycopy(shape, 0, tmp, start, shape.length);
                                    for (int i = 0; i < start; i++) {
                                        tmp[i] = 1;
                                    }
                                    shape = tmp;
                                }
                                if (selectedSubPart(origin, shape, originalShape, itemID, dataSpaceID, subSpaceIDs)
                                        && (subSpaceIDs[1] == dataSpaceID)) {
                                    dataSpaceID = -1;
                                }
                            }
                        } else {
                            // Item should be created
                            if (dataType != null) {
                                dataTypeID = HdfObjectUtils.getDataTypeIdForCreation(dataType, unsigned);
                                closeDataTypeId = String.class.equals(dataType);
                                dataSpaceID = H5.H5Screate_simple(shape.length, shape, shape);
                                dcpl = H5.H5Pcreate(HDF5Constants.H5P_DATASET_CREATE);
                                if (!ObjectUtils.sameObject(shape, originalShape)) {
                                    originalShape = shape;
                                    this.shape = originalShape;
                                }
                                if (toWrite instanceof String[]) {
                                    if (variableLengthStrings) {
                                        H5.H5Tset_size(dataTypeID, HDF5Constants.H5T_VARIABLE);
                                    } else {
                                        size = HdfObjectUtils.getWritableSize((String[]) toWrite);
                                        H5.H5Tset_size(dataTypeID, size);
                                    }
                                } else if (closeDataTypeId) {
                                    if (variableLengthStrings) {
                                        H5.H5Tset_size(dataTypeID, HDF5Constants.H5T_VARIABLE);
                                    } else {
                                        // by default, if you create strings without data: 1024 characters per string
                                        H5.H5Tset_size(dataTypeID, 1024);
                                    }
                                }
                                itemID = H5.H5Dcreate(parentId, shortName, dataTypeID, dataSpaceID,
                                        HDF5Constants.H5P_DEFAULT, dcpl, HDF5Constants.H5P_DEFAULT);
                            } // end if (dataType != null)
                        } // end if (itemID > -1) ... else
                        if ((itemID > -1) && (toWrite != null)) {
                            // Do write data
                            long nativeTypeId = HdfObjectUtils.getNativeDataTypeForClass(dataType, unsigned,
                                    dataTypeID);
                            if (variableLengthStrings && (toWrite instanceof String[])) {
                                H5.H5Dwrite_VLStrings(itemID, nativeTypeId, subSpaceIDs[0], subSpaceIDs[1],
                                        HDF5Constants.H5P_DEFAULT, (String[]) toWrite);
                            } else {
                                if (toWrite instanceof String[]) {
                                    toWrite = HdfObjectUtils.stringToByte((String[]) toWrite, (int) size);
                                }
                                H5.H5Dwrite(itemID, nativeTypeId, subSpaceIDs[0], subSpaceIDs[1],
                                        HDF5Constants.H5P_DEFAULT, toWrite);
                            }
                            H5.H5Dflush(itemID);
                            H5.H5Drefresh(itemID);
                        }
                    } // end if (array != null)
                } else {
                    // Write link
                    int index = linkedPath.indexOf(EXTERNAL_LINK_SEPARATOR);
                    if (index > -1) {
                        // external link
                        String file = linkedPath.substring(0, index);
                        String target = linkedPath.substring(index + EXTERNAL_LINK_SEPARATOR.length());
                        H5.H5Lcreate_external(file, target, fileId, getName(), HDF5Constants.H5P_DEFAULT,
                                HDF5Constants.H5P_DEFAULT);
                    } else {
                        // internal link
                        H5.H5Lcreate_soft(linkedPath, fileId, getName(), HDF5Constants.H5P_DEFAULT,
                                HDF5Constants.H5P_DEFAULT);
                    }
                    itemID = H5.H5Dopen(parentId, shortName, HDF5Constants.H5P_DEFAULT);
                }
                if (itemID > -1) {
                    // write attributes
                    for (HdfAttribute attribute : attributes) {
                        HdfAttribute.writeAttribute(itemID, attribute);
                    }
                }
                if (!ignoreDirtyTest) {
                    dirty = false;
                }
            } catch (Exception e) {
                throw HdfObjectUtils.toDataAccessException(e, "Failed to save data for item " + name);
            } finally {
                close(itemID, dcpl, dataTypeID, dataSpaceID, subSpaceIDs, closeDataTypeId, name);
            }
        }
    }

//    @Override
//    public String writeCDL(final String indent, final boolean useFullName, final boolean strict) {
//        throw new NotImplementedException();
//    }

}

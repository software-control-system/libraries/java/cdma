/*******************************************************************************
 * Copyright (c) 2008 - ANSTO/Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * Norman Xiong (nxi@Bragg Institute) - initial API and implementation
 * Tony Lam (nxi@Bragg Institute) - initial API and implementation
 * Majid Ounsy (SOLEIL Synchrotron) - API v2 design and conception
 * Stephane Poirier (SOLEIL Synchrotron) - API v2 design and conception
 * Clement Rodriguez (ALTEN for SOLEIL Synchrotron) - API evolution
 * Gregory VIGUIER (SOLEIL Synchrotron) - API evolution
 * Raphael GIRARDOT (SOLEIL Synchrotron) - API evolution
 ******************************************************************************/
package org.cdma.engine.hdf.navigation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.cdma.Factory;
import org.cdma.dictionary.Path;
import org.cdma.engine.hdf.utils.HdfNode;
import org.cdma.engine.hdf.utils.HdfObjectUtils;
import org.cdma.engine.hdf.utils.HdfPath;
import org.cdma.exception.DataAccessException;
import org.cdma.exception.NoResultException;
import org.cdma.exception.NotImplementedException;
import org.cdma.exception.SignalNotAvailableException;
import org.cdma.interfaces.IAttribute;
import org.cdma.interfaces.IContainer;
import org.cdma.interfaces.IDataItem;
import org.cdma.interfaces.IDimension;
import org.cdma.interfaces.IGroup;
import org.cdma.interfaces.INode;
import org.cdma.utils.Utilities.ModelType;

import hdf.hdf5lib.H5;
import hdf.hdf5lib.HDF5Constants;
import hdf.hdf5lib.exceptions.HDF5LibraryException;

public class HdfGroup extends AHdfContainer implements IGroup {

    private static final String GROUP = "Group ";
    private static final String PARENT = " (parent = ";
    private static final String NOBODY = "Nobody";
    private static final String STRING_END = ")";

    private HdfGroup root;
    private Map<String, HdfGroup> groupMap;
    private Map<String, HdfDataItem> itemMap;
    private Map<String, HdfAttribute> attributeMap;
    private String name;
    private HdfDataset dataset;
    private boolean initiated;

    public HdfGroup(String factoryName, final String name, final HdfGroup parent, final HdfDataset dataset) {
        super(factoryName, parent);
        this.groupMap = new LinkedHashMap<>();
        this.itemMap = new LinkedHashMap<>();
        this.attributeMap = new HashMap<>();
        this.root = null;
        this.initiated = false;
        this.dataset = dataset;
        this.shortName = name;
    }

    private void init() {
        if (!initiated) {
            synchronized (this) {
                if (!initiated) {
                    try {
                        HdfDataset dataset = getDataset();
                        if (dataset != null) {
                            boolean close = false;
                            if (!dataset.isOpen()) {
                                dataset.open();
                                close = true;
                            }
                            try {
                                long parentID = -1;
                                boolean closeParentID = false;
                                long fileID = dataset.getH5FileID();
                                String parentName = null;
                                if (fileID > -1) {
                                    try {
                                        HdfGroup parent = getParentGroup();
                                        if (parent == null) {
                                            parentID = fileID;
                                        } else {
                                            parentName = parent.getName();
                                            try {
                                                parentID = H5.H5Gopen(fileID, parentName, HDF5Constants.H5P_DEFAULT);
                                            } catch (HDF5LibraryException e) {
                                                parentID = -1;
                                            }
                                            closeParentID = true;
                                        }
                                        fillChildren(factoryName, dataset, this, parentID, true, false);
                                    } finally {
                                        if (closeParentID && (parentID > -1)) {
                                            try {
                                                H5.H5Gclose(parentID);
                                            } catch (Exception e) {
                                                Factory.getLogger().error("Failed to close group " + parentName, e);
                                            }
                                        }
                                    }
                                }
                            } finally {
                                HdfDataset.closeDataset(dataset, close);
                            }
                        } // end if (dataset != null)
                    } catch (DataAccessException e) {
                        Factory.getLogger().error("Failed to init group " + getName(), e);
                    }
                }
            }
        }
    }

    protected static String getChildFullName(String groupName, String childName) {
        boolean addSlash;
        if (groupName.charAt(groupName.length() - 1) == '/') {
            addSlash = false;
        } else if ((childName != null) && (!childName.isEmpty()) && (childName.charAt(0) == '/')) {
            addSlash = false;
        } else {
            addSlash = true;
        }
        return addSlash ? groupName + '/' + childName : groupName + childName;
    }

    protected static void fillAttributes(final String factoryName, final HdfGroup group, final long groupId)
            throws DataAccessException {
        if ((group != null) && (groupId > -1)) {
            Collection<HdfAttribute> attributes = HdfAttribute.readAttributes(factoryName, groupId);
            if (attributes != null) {
                for (HdfAttribute attribute : attributes) {
                    if ((attribute != null) && (attribute.getName() != null)) {
                        group.attributeMap.put(attribute.getName(), attribute);
                    }
                }
            }
            attributes.clear();
        }
    }

    /**
     * Returns the group identified by a name, in a parent identified by its id.
     * 
     * @param factoryName The factory name, necessary for {@link HdfGroup} creation.
     * @param dataset The parent {@link HdfDataset}, necessary for {@link HdfGroup} creation.
     * @param parentId The parent id.
     * @param groupName The group name.
     * @return An {@link HdfGroup}. <code>null</code> if no such group.
     * @throws DataAccessException If a problem occurred.
     */
    public static HdfGroup getGroup(final String factoryName, final HdfDataset dataset, long parentId, String groupName)
            throws DataAccessException {
        HdfGroup group;
        long groupId = -1;
        try {
            groupId = H5.H5Gopen(parentId, groupName, HDF5Constants.H5P_DEFAULT);
            if (groupId > -1) {
                group = new HdfGroup(factoryName, groupName, null, dataset);
            } else {
                group = null;
            }
        } catch (Exception e) {
            throw new DataAccessException("Failed to access group " + groupName, e);
        } finally {
            if (groupId > -1) {
                H5.H5Gclose(groupId);
            }
        }
        return group;
    }

    /**
     * Fills an {@link HdfGroup} with its children
     * 
     * @param factoryName The factory name, necessary for constructions.
     * @param dataset The parent {@link HdfDataset}, necessary for constructions.
     * @param group The {@link HdfGroup} to fill.
     * @param parentID The id of the group's parent
     * @param fillAttributes Whether to fill attributes too
     * @param recursive Whether to be recursive (i.e. fill children of children groups and so on).
     */
    public static void fillChildren(final String factoryName, final HdfDataset dataset, final HdfGroup group,
            final long parentID, final boolean fillAttributes, final boolean recursive) throws DataAccessException {
        if ((parentID > -1) && (group != null)) {
            String groupName = group.getShortName();
            if ((groupName != null) && (!groupName.trim().isEmpty())) {
                long groupID = -1;
                try {
                    int count;
                    try {
                        count = (int) H5.H5Gn_members(parentID, groupName);
                    } catch (HDF5LibraryException e) {
                        count = 0;
                    }
                    if (count > 0) {
                        String[] oname = new String[count];
                        int[] otype = new int[count];
                        int[] ltype = new int[count];
                        long[] orefs = new long[count];
                        H5.H5Gget_obj_info_all(parentID, groupName, oname, otype, ltype, orefs,
                                HDF5Constants.H5_INDEX_NAME);
                        groupID = H5.H5Gopen(parentID, groupName, HDF5Constants.H5P_DEFAULT);
                        // Get type of the object and display its name and type.
                        for (int indx = 0; indx < otype.length; indx++) {
                            String childName = oname[indx];
                            long subGroupID = -1;
                            try {
                                switch (H5O_type.get(otype[indx])) {
                                    case H5O_TYPE_GROUP:
                                        HdfGroup subGroup = group.groupMap.get(childName);
                                        if (subGroup == null) {
                                            subGroup = new HdfGroup(factoryName, childName, group, dataset);
                                        }
                                        // subgroup attributes
                                        try {
                                            subGroupID = H5.H5Gopen(groupID, childName, HDF5Constants.H5P_DEFAULT);
                                            fillAttributes(factoryName, subGroup, subGroupID);
                                        } catch (Exception e) {
                                            Factory.getLogger().warn("Failed to access group " + subGroup.getName(), e);
                                        } finally {
                                            if (subGroupID > -1) {
                                                H5.H5Gclose(subGroupID);
                                                subGroupID = -1;
                                            }
                                        }
                                        group.groupMap.put(childName, subGroup);
                                        if (recursive) {
                                            fillChildren(factoryName, dataset, subGroup, groupID, fillAttributes,
                                                    recursive);
                                        }
                                        break;
                                    case H5O_TYPE_DATASET:
                                        HdfDataItem item = group.itemMap.get(childName);
                                        if (item == null) {
                                            item = HdfDataItem.getDataItem(factoryName, group, groupID, childName);
                                            group.itemMap.put(childName, item);
                                        }
                                        break;
                                    case H5O_TYPE_NAMED_DATATYPE:
                                        // TODO Datatype: ignore?
                                        break;
                                    default:
                                        // nothing to do
                                }
                            } catch (Exception e) {
                                Factory.getLogger().warn("Failed to access group " + childName, e);
                            } finally {
                                if (subGroupID > -1) {
                                    try {
                                        H5.H5Gclose(subGroupID);
                                    } catch (Exception e) {
                                        Factory.getLogger().warn("Failed to close group " + childName, e);
                                    }
                                }
                            }
                        } // end for (int indx = 0; indx < otype.length; indx++)
                    }
                    group.initiated = true;
                } catch (Exception e) {
                    if (groupID > -1) {
                        try {
                            H5.H5Gclose(groupID);
                        } catch (Exception e2) {
                            Factory.getLogger().warn("Failed to close group " + group.getName(), e2);
                        }
                    }
                    throw HdfObjectUtils.toDataAccessException(e,
                            "Failed to fill children of group " + group.getName());
                }
            } // end f ((groupName != null) && (!groupName.trim().isEmpty()))
        } // end if ((parentId > -1) && (group != null))
    }

    @Override
    public HdfGroup clone() {
        HdfGroup clone = (HdfGroup) super.clone();
        if (clone == this) {
            clone = new HdfGroup(factoryName, shortName, parent, dataset);
        } else {
            clone.groupMap = new LinkedHashMap<>();
            clone.itemMap = new LinkedHashMap<>();
            clone.attributeMap = new HashMap<>();
            clone.root = null;
            clone.initiated = false;
        }
        return clone;
    }

    @Override
    public ModelType getModelType() {
        return ModelType.Group;
    }

    @Override
    public void addOneAttribute(final IAttribute attribute) {
        if (attribute instanceof HdfAttribute) {
            attributeMap.put(attribute.getName(), (HdfAttribute) attribute);
        }
    }

    @Override
    public IAttribute getAttribute(final String name) {
        init();
        IAttribute result = attributeMap.get(name);
        return result;
    }

    @Override
    public Collection<IAttribute> getAttributeList() {
        init();
        List<IAttribute> result = new ArrayList<>(attributeMap.values());
        return result;
    }

    @Override
    public String getName() {
        if (name == null) {
            name = super.getName();
        }
        return name;
    }

    @Override
    public boolean hasAttribute(final String name, final String value) {
        init();
        return attributeMap.containsKey(name);
    }

    @Override
    public boolean removeAttribute(final IAttribute a) {
        init();
        return attributeMap.remove(a.getName()) != null;
    }

    @Override
    public void setName(final String name) {
        init();
        addStringAttribute("long_name", name);
    }

    @Override
    public void setShortName(final String name) {
        try {
            super.setShortName(name);
            this.name = getParentGroup().getName() + HdfPath.PATH_SEPARATOR + name;
            for (IDataItem item : itemMap.values()) {
                item.setParent(this);
            }

        } catch (Exception e) {
            Factory.getLogger().error(e.getMessage());
        }
    }

    @Override
    public void setParent(final IGroup group) {
        try {
            parent = (HdfGroup) group;
        } catch (Exception e) {
            Factory.getLogger().error(e.getMessage());
        }
    }

    @Override
    public long getLastModificationDate() {
        long result = 0;
        getDataset().getLastModificationDate();
        return result;
    }

    @Override
    public void addDataItem(final IDataItem item) {
        if (item instanceof HdfDataItem) {
            HdfDataItem hdfDataItem = (HdfDataItem) item;
            hdfDataItem.setParent(this);
            itemMap.put(hdfDataItem.getShortName(), hdfDataItem);
        }
    }

    @Override
    public Map<String, String> harvestMetadata(final String mdStandard) throws DataAccessException {
        throw new NotImplementedException();
    }

    @Override
    public HdfGroup getRootGroup() {
        if (root == null) {
            root = getDataset().getRootGroup();
        }
        return root;
    }

    @Override
    public void addOneDimension(final IDimension dimension) {
    }

    @Override
    public void addSubgroup(final IGroup group) {
        if (group instanceof HdfGroup) {
            HdfGroup subGroup = (HdfGroup) group;
            init();
            subGroup.setParent(this);
            groupMap.put(subGroup.getShortName(), subGroup);
        }
    }

    @Override
    public HdfDataItem getDataItem(final String shortName) {
        init();
        HdfDataItem result = null;
        if (shortName != null) {
            result = itemMap.get(shortName);
        }

        return result;
    }

    @Override
    public IDataItem getDataItemWithAttribute(final String name, final String value) {
        init();
        IDataItem resItem = null;
        List<IDataItem> groups = getDataItemList();
        boolean found = false;
        for (Iterator<?> iter = groups.iterator(); iter.hasNext();) {
            resItem = (IDataItem) iter.next();
            if (resItem.hasAttribute(name, value)) {
                groups.clear();
                found = true;
                break;
            }
        }
        if (!found) {
            resItem = null;
        }
        return resItem;
    }

    @Override
    public IDimension getDimension(final String name) {
        return null;
    }

    @Override
    public AHdfContainer getContainer(final String shortName) {
        AHdfContainer container = null;
        init();
        if ((shortName != null) && shortName.isEmpty()) {
            container = this;
        } else {
            HdfGroup resultGroupItem = getGroup(shortName);
            if (resultGroupItem == null) {
                HdfDataItem resultVariableItem = getDataItem(shortName);
                if (resultVariableItem != null) {
                    container = resultVariableItem;
                }
            } else {
                container = resultGroupItem;
            }
        }
        return container;
    }

    @Override
    public HdfGroup getGroup(final String shortName) {
        init();
        HdfGroup result = null;
        if (shortName != null) {
            result = groupMap.get(shortName);
        }
        return result;
    }

    @Override
    public IGroup getGroupWithAttribute(final String attributeName, final String value) {
        // init() is called by getGroupList()
        List<IGroup> groups = getGroupList();
        IAttribute attr;
        IGroup result = null;
        for (IGroup group : groups) {
            attr = group.getAttribute(attributeName);
            if (attr.getStringValue().equals(value)) {
                result = group;
                break;
            }
        }

        return result;
    }

    @Override
    public List<IDataItem> getDataItemList() {
        init();
        List<IDataItem> result;
        result = new ArrayList<IDataItem>(itemMap.values());
        return result;
    }

    @Override
    public int getDataItemCount() {
        init();
        return itemMap.size();
    }

    @Override
    public HdfDataset getDataset() {
        if (dataset == null) {
            if (parent != null) {
                dataset = parent.getDataset();
            }
        }
        return dataset;
    }

    protected void updateFromGroupNoCheck(HdfGroup group) {
        groupMap.clear();
        groupMap.putAll(group.groupMap);
        itemMap.clear();
        itemMap.putAll(group.itemMap);
        attributeMap.clear();
        attributeMap.putAll(group.attributeMap);
        this.initiated = group.initiated;
//        this.h5Group = group.h5Group;
        HdfGroup parent = getParentGroup();
        if (parent != null) {
            parent.groupMap.put(shortName, this);
        }
    }

    @Override
    public void changeDataset(HdfDataset hdfDataset) {
        if ((hdfDataset != null) && (hdfDataset != dataset)) {
            dataset = hdfDataset;
            if (parent != null) {
                parent.changeDataset(hdfDataset);
            }
            if (shortName != null) {
                if (parent == null) {
                    HdfGroup root = dataset.getRootGroup();
                    if ((root != null) && (root != this)) {
                        if (shortName.isEmpty() || HdfPath.PATH_SEPARATOR.equals(shortName)) {
                            updateFromGroupNoCheck(root);
                            dataset.setRoot(this);
                        } else {
                            try {
                                String location = getLocation();
                                if ((location != null) && (!location.isEmpty()) && (location.charAt(0) == '/')) {
                                    location = location.substring(1);
                                }
                                IContainer container = root.findContainerByPath(location);
                                if ((container != this) && (container instanceof HdfGroup)) {
                                    updateFromGroupNoCheck((HdfGroup) container);
                                }
                            } catch (NoResultException e) {
                                Factory.getLogger()
                                        .error("Failed to find group " + getName() + " after changing dataset", e);
                            }
                        }
                    }
                } else {
                    HdfGroup group = parent.getGroup(shortName);
                    if ((group != this) && (group != null)) {
                        updateFromGroupNoCheck(group);
                    }
                }
            }
        }
    }

    @Override
    public List<IDimension> getDimensionList() {
        init();
        List<IDimension> result = new ArrayList<IDimension>();
        return result;
    }

    @Override
    public List<IGroup> getGroupList() {
        init();
        List<IGroup> result;
        result = new ArrayList<IGroup>(groupMap.values());
        return result;
    }

    @Override
    public int getGroupCount() {
        init();
        return groupMap.size();
    }

    public HdfPath getHdfPath() {
        HdfPath result;
        List<HdfNode> parentNodes = getParentNodes();
        result = new HdfPath(parentNodes.toArray(new HdfNode[parentNodes.size()]));
        return result;
    }

    private List<HdfNode> getParentNodes() {
        List<HdfNode> nodes = new ArrayList<HdfNode>();

        HdfNode node = new HdfNode(this);

        if (parent != null) {
            nodes.addAll(parent.getParentNodes());
            nodes.add(node);
        }

        return nodes;
    }

    private List<INode> getChildNodes() {
        init();
        List<INode> nodes = new ArrayList<INode>();

        for (IDataItem item : itemMap.values()) {
            nodes.add(new HdfNode(item));
        }
        for (IGroup item : groupMap.values()) {
            nodes.add(new HdfNode(item));
        }

        return nodes;
    }

    @Override
    public AHdfContainer findContainerByPath(final String path) throws NoResultException {
        // Split path into nodes
        String[] sNodes = HdfPath.splitStringPath(path);
        AHdfContainer node = getRootGroup();

        // Try to open each node
        for (String shortName : sNodes) {
            if (!shortName.isEmpty() && (node instanceof HdfGroup)) {
                node = ((HdfGroup) node).getContainer(shortName);
            }
        }

        return node;
    }

    public List<IContainer> findAllContainerByPath(final INode[] nodes) throws NoResultException {
        List<IContainer> list = new ArrayList<IContainer>();
        IGroup root = getRootGroup();

        // Call recursive method
        int level = 0;
        list = findAllContainer(root, nodes, level);

        return list;
    }

    @Override
    public List<IContainer> findAllContainerByPath(final String path) throws NoResultException {
        // Try to list all nodes matching the path
        // Transform path into a NexusNode array
        INode[] nodes = HdfPath.splitStringToNode(path);

        List<IContainer> result = findAllContainerByPath(nodes);

        return result;
    }

    private List<IContainer> findAllContainer(final IContainer container, final INode[] nodes, final int level) {
        List<IContainer> result = new ArrayList<IContainer>();
        if (container != null) {
            if (container instanceof HdfGroup) {
                HdfGroup group = (HdfGroup) container;
                if (nodes.length > level) {
                    // List current node children
                    List<INode> childs = group.getChildNodes();

                    INode current = nodes[level];

                    for (INode node : childs) {

                        if (node.matchesPartNode(current)) {
                            if (level < nodes.length - 1) {
                                result.addAll(findAllContainer(group.getContainer(node.getName()), nodes, level + 1));
                            }
                            // Create IContainer and add it to result list
                            else {
                                result.add(group.getContainer(node.getName()));
                            }
                        }
                    }
                }
            } else {
                HdfDataItem dataItem = (HdfDataItem) container;
                result.add(dataItem);
            }
        }
        return result;
    }

    @Override
    public boolean removeDataItem(final IDataItem item) {
        return removeDataItem(item.getShortName());

    }

    @Override
    public boolean removeDataItem(final String varName) {
        init();
        boolean result = true;
        itemMap.remove(varName);
        return result;
    }

    @Override
    public boolean removeDimension(final String name) {
        return false;
    }

    @Override
    public boolean removeDimension(final IDimension dimension) {
        return false;
    }

    @Override
    public boolean removeGroup(final IGroup group) {
        return removeGroup(group.getShortName());
    }

    @Override
    public boolean removeGroup(final String name) {
        init();
        groupMap.remove(name);
        return true;
    }

    @Override
    public void updateDataItem(final String key, final IDataItem dataItem) throws SignalNotAvailableException {
        throw new NotImplementedException();
    }

    @Override
    public boolean isRoot() {
        return parent == null;
    }

    @Override
    public boolean isEntry() {
        boolean result = false;
        result = getParentGroup().isRoot();
        return result;
    }

    @Override
    public IContainer findObjectByPath(final Path path) {
        throw new NotImplementedException();
    }

    @Override
    public String toString() {
        StringBuilder buffer = new StringBuilder();
        IGroup parent = getParentGroup();
        buffer.append(GROUP).append(getShortName()).append(PARENT).append(parent == null ? NOBODY : parent.getName())
                .append(STRING_END);
        return buffer.toString();
    }

    /**
     * Saves this group in file.
     * 
     * @throws DataAccessException If a problem occurred.
     */
    public void save() throws DataAccessException {
        HdfDataset dataset = getDataset();
        if ((dataset != null) && dataset.hasWriteAccess()) {
            long fileId = dataset.getH5FileID();
            save(fileId, fileId, false);
        }
    }

    protected void save(long parentId, long fileId, boolean ignoreDirtyTest) throws DataAccessException {
        if (parentId > -1) {
            init();
            long groupID = -1;
            try {
                try {
                    groupID = H5.H5Gopen(parentId, shortName, HDF5Constants.H5P_DEFAULT);
                } catch (Exception e) {
                    groupID = -1; // group does not exist
                }
                if (groupID < 0) {
                    groupID = H5.H5Gcreate(parentId, getName(), HDF5Constants.H5P_DEFAULT, HDF5Constants.H5P_DEFAULT,
                            HDF5Constants.H5P_DEFAULT);
                }
                if (groupID > -1) {
                    // write attributes
                    for (HdfAttribute attribute : attributeMap.values()) {
                        HdfAttribute.writeAttribute(groupID, attribute);
                    }
                    // write items
                    for (HdfDataItem item : itemMap.values()) {
                        item.save(groupID, fileId, ignoreDirtyTest);
                    }
                    // write subgroups
                    for (HdfGroup group : groupMap.values()) {
                        group.save(groupID, fileId, ignoreDirtyTest);
                    }
                }
            } catch (Exception e) {
                throw HdfObjectUtils.toDataAccessException(e, "Failed to save group " + getName());
            } finally {
                if (groupID > -1) {
                    try {
                        H5.H5Gclose(groupID);
                    } catch (Exception e) {
                        Factory.getLogger().error("Failed to close access to group " + getName(), e);
                    }
                }
            }
        }
    }

    // XXX This is hack for nexus plugin interaction.
    // For some reason, it seems for NxsGroup to list children at construction when using hdf engine,
    // whereas it can be avoided (resulting in better performances, cf.DATAREDUC-897) with hdf-swmr.
    /**
     * A method that indicates whether it is preferable for this group to always search for its children.
     * 
     * @return A <code>boolean</code>.
     */
    public boolean shouldAlwaysCheckForChildren() {
        return false;
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    // Code found here :
    // https://bitbucket.hdfgroup.org/projects/HDFFV/repos/hdf5/browse/java/examples/groups/H5Ex_G_Iterate.java
    public static enum H5O_type {

        H5O_TYPE_UNKNOWN(-1), // Unknown object type
        H5O_TYPE_GROUP(0), // Object is a group
        H5O_TYPE_DATASET(1), // Object is a dataset
        H5O_TYPE_NAMED_DATATYPE(2), // Object is a named data type
        H5O_TYPE_NTYPES(3); // Number of different object types

        private static final Map<Integer, H5O_type> LOOKUP = new HashMap<Integer, H5O_type>();

        static {
            for (H5O_type s : EnumSet.allOf(H5O_type.class)) {
                LOOKUP.put(Integer.valueOf(s.getCode()), s);
            }
        }

        private int code;

        H5O_type(int layout_type) {
            this.code = layout_type;
        }

        public int getCode() {
            return this.code;
        }

        public static H5O_type get(int code) {
            return LOOKUP.get(code);
        }
    }

}

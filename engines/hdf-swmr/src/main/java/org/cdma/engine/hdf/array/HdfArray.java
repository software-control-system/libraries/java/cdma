/*******************************************************************************
 * Copyright (c) 2008 - ANSTO/Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * Norman Xiong (nxi@Bragg Institute) - initial API and implementation
 * Tony Lam (nxi@Bragg Institute) - initial API and implementation
 * Majid Ounsy (SOLEIL Synchrotron) - API v2 design and conception
 * Stephane Poirier (SOLEIL Synchrotron) - API v2 design and conception
 * Clement Rodriguez (ALTEN for SOLEIL Synchrotron) - API evolution
 * Gregory VIGUIER (SOLEIL Synchrotron) - API evolution
 * Raphael GIRARDOT (SOLEIL Synchrotron) - API evolution
 ******************************************************************************/
package org.cdma.engine.hdf.array;

import java.lang.ref.WeakReference;
import java.util.concurrent.atomic.AtomicLong;

import org.cdma.Factory;
import org.cdma.arrays.DefaultArrayInline;
import org.cdma.arrays.DefaultIndex;
import org.cdma.engine.hdf.navigation.HdfDataItem;
import org.cdma.exception.InvalidArrayTypeException;
import org.cdma.exception.InvalidRangeException;
import org.cdma.exception.ShapeNotMatchException;
import org.cdma.interfaces.IIndex;
import org.slf4j.LoggerFactory;

import fr.soleil.lib.project.date.DateUtil;
import fr.soleil.lib.project.math.NumberArrayUtils;

public class HdfArray extends DefaultArrayInline {

    private static final AtomicLong ITEM_LOAD_TIME = new AtomicLong(0);
    private static final AtomicLong GET_DATA_TIME = new AtomicLong(0);
    private static final AtomicLong COPY_TIME = new AtomicLong(0);

    public static void resetMetrics() {
        ITEM_LOAD_TIME.set(0);
        GET_DATA_TIME.set(0);
        COPY_TIME.set(0);
        HdfDataItem.resetMetrics();
    }

    public static void traceMetrics(String applicationId) {
        StringBuilder builder = new StringBuilder(HdfArray.class.getSimpleName());
        DateUtil.elapsedTimeToStringBuilder("\nHdfArray Cumulative item load time: ", ITEM_LOAD_TIME, builder);
        DateUtil.elapsedTimeToStringBuilder("\nHdfArray Cumulative get data time: ", GET_DATA_TIME, builder);
        DateUtil.elapsedTimeToStringBuilder("\nHdfArray Cumulative copy time: ", COPY_TIME, builder);
        LoggerFactory.getLogger(applicationId).trace(builder.toString());
        HdfDataItem.traceMetrics(applicationId);
    }

    private HdfDataItem dataItem;
    private volatile boolean profile;

    // Constructors
    public HdfArray(String factoryName, Class<?> clazz, int[] shape) throws InvalidArrayTypeException {
        super(factoryName, clazz, shape.clone());
    }

    public HdfArray(String factoryName, Class<?> clazz, int[] shape, HdfDataItem dataItem)
            throws InvalidArrayTypeException {
        super(factoryName, clazz, shape);
        this.dataItem = dataItem;
    }

    public HdfArray(String factoryName, Object array, int[] shape) throws InvalidArrayTypeException {
        super(factoryName, array, shape);
        this.lock();
    }

    public HdfArray(String factoryName, Object array, int[] shape, int[] origin) throws InvalidArrayTypeException {
        this(factoryName, array, shape);
        setIndex(new HdfIndex(factoryName, shape, origin, shape));
    }

    public HdfArray(HdfArray array) throws InvalidArrayTypeException {
        super(array);
        this.dataItem = array.dataItem;
    }

    @Override
    protected HdfIndex generateIndex(String factory, int... shape) {
        return new HdfIndex(factory, shape);
    }

    @Override
    protected HdfIndex toDefaultIndex(IIndex index) {
        HdfIndex result;
        if (index instanceof HdfIndex) {
            result = (HdfIndex) index;
        } else if (index instanceof DefaultIndex) {
            result = new HdfIndex((DefaultIndex) index);
        } else {
            result = new HdfIndex(index);
        }
        return result;
    }

    @Override
    public HdfIndex getIndex() {
        return (HdfIndex) super.getIndex();
    }

    @Override
    public HdfArrayUtils getArrayUtils() {
        return new HdfArrayUtils(this);
    }

    public void setProfile(boolean trace) {
        this.profile = trace;
        if (dataItem != null) {
            dataItem.setProfile(trace);
        }
    }

    public boolean isProfile() {
        return profile;
    }

    @Override
    public HdfArrayIterator getIterator() {
        return new HdfArrayIterator(this);
    }

    @Override
    public HdfSliceIterator getSliceIterator(int rank) throws ShapeNotMatchException, InvalidRangeException {
        return new HdfSliceIterator(this, rank);
    }

    @Override
    protected WeakReference<Object> loadData() {
        Object data = null;
        WeakReference<Object> result = new WeakReference<>(data);

        if (dataItem != null) {
            DefaultIndex index = getIndex();
            try {
                long time = -1;
                if (profile) {
                    time = System.currentTimeMillis();
                }
                data = dataItem.load(NumberArrayUtils.extractLongArray(index.getProjectionOrigin()),
                        NumberArrayUtils.extractLongArray(index.getProjectionShape()));
                if (profile) {
                    long loadTime = System.currentTimeMillis() - time;
                    ITEM_LOAD_TIME.addAndGet(loadTime);
                }
                result = new WeakReference<>(data);
            } catch (OutOfMemoryError | Exception e) {
                Factory.getLogger().error("Unable to loadData()", e);
            }
        }
        return result;
    }

    @Override
    public HdfArray copy(boolean data) {
        long time = -1;
        if (profile) {
            time = System.currentTimeMillis();
        }
        HdfArray result;
        try {
            result = new HdfArray(this);
        } catch (InvalidArrayTypeException e) {
            result = null;
            Factory.getLogger().error("Unable to copy the HdfArray array: " + this, e);
        }
        if (profile) {
            long copyTime = System.currentTimeMillis() - time;
            COPY_TIME.addAndGet(copyTime);
        }
        return result;
    }

    @Override
    protected Object getData() {
        Object result = null;
        long time = -1;
        if (profile) {
            time = System.currentTimeMillis();
        }
        Object data = super.getData();
        if (isLocked()) {
            result = data;
        } else if ((data == null) || (data instanceof WeakReference<?>)) {
            WeakReference<?> reference = (WeakReference<?>) data;

            // If reference doesn't exists: WeakRef & its data has never been loaded
            if (reference == null) {
                reference = loadData();
            }
            result = reference.get();

            // Reference exists but data is null: GC has cleaned data
            // we have to reload it
            if (result == null) {
                reference = loadData();
                result = reference.get();
            }
        }
        if (profile) {
            long getDataTime = System.currentTimeMillis() - time;
            GET_DATA_TIME.addAndGet(getDataTime);
        }
        return result;
    }
}

/*******************************************************************************
 * Copyright (c) 2008 - ANSTO/Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * Norman Xiong (nxi@Bragg Institute) - initial API and implementation
 * Tony Lam (nxi@Bragg Institute) - initial API and implementation
 * Majid Ounsy (SOLEIL Synchrotron) - API v2 design and conception
 * Stephane Poirier (SOLEIL Synchrotron) - API v2 design and conception
 * Clement Rodriguez (ALTEN for SOLEIL Synchrotron) - API evolution
 * Gregory VIGUIER (SOLEIL Synchrotron) - API evolution
 * Raphael GIRARDOT (SOLEIL Synchrotron) - API evolution
 ******************************************************************************/
package org.cdma.engine.hdf.navigation;

import java.io.File;

import org.cdma.Factory;
import org.cdma.dictionary.LogicalGroup;
import org.cdma.engine.hdf.utils.HdfObjectUtils;
import org.cdma.engine.hdf.utils.HdfPath;
import org.cdma.exception.DataAccessException;
import org.cdma.exception.NotImplementedException;
import org.cdma.exception.WriterException;
import org.cdma.interfaces.IAttribute;
import org.cdma.interfaces.IContainer;
import org.cdma.interfaces.IDataset;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.file.FileUtils;
import hdf.hdf5lib.H5;
import hdf.hdf5lib.HDF5Constants;
import hdf.hdf5lib.exceptions.HDF5LibraryException;

public class HdfDataset implements IDataset, Cloneable {

    public static String AUTO_SWMR_PROPERTY = "AutoSWMR";

    private static final String TO_STRING_PREFIX = "Dataset = ";

    private static final int OPEN_READ = HDF5Constants.H5F_ACC_RDONLY;
    private static final int OPEN_READ_SWMR = HDF5Constants.H5F_ACC_RDONLY | HDF5Constants.H5F_ACC_SWMR_READ;
    private static final int OPEN_WRITE = HDF5Constants.H5F_ACC_RDWR;

    private final String factoryName;
    private String hdfFileName;
    private long h5FileID, fapl;
    private String title;
    private HdfGroup root;
    private int openFlag;
    private boolean swmr;
    private volatile String lastSavedPath;

    public HdfDataset(final String factoryName, final File hdfFile) throws DataAccessException {
        this(factoryName, hdfFile, false);
    }

    public HdfDataset(final String factoryName, final File hdfFile, final boolean withWriteAccess)
            throws DataAccessException {
        this(factoryName, hdfFile, withWriteAccess, canUseSWMR(hdfFile, withWriteAccess));
    }

    public HdfDataset(final String factoryName, final File hdfFile, final boolean withWriteAccess, final boolean swmr)
            throws DataAccessException {
//        long time = System.currentTimeMillis();
        this.h5FileID = -1;
        this.fapl = -1;
        this.factoryName = factoryName;
        this.hdfFileName = hdfFile.getAbsolutePath();
        this.title = hdfFile.getName();
        this.swmr = swmr;
        if (withWriteAccess) {
            openFlag = OPEN_WRITE;
        } else if (swmr) {
            openFlag = OPEN_READ_SWMR;
        } else {
            openFlag = OPEN_READ;
        }
        lastSavedPath = null;
//        System.out.println(
//                DateUtil.elapsedTimeToStringBuilder(new StringBuilder("** From HdfDataset point of view, it took "),
//                        System.currentTimeMillis() - time).append(" to create HdfDataset"));
    }

    private long initHdfFile(String location)
            throws HDF5LibraryException, IllegalArgumentException, NullPointerException {
        long fapl = -1;
        File tmp;
        if (location == null) {
            tmp = null;
        } else {
            tmp = new File(location);
            // If the file does not exist, create and immediately close it.
            if ((openFlag == OPEN_WRITE) && (!tmp.exists())) {
                long fileID = -1;
                try {
                    fapl = H5.H5Pcreate(HDF5Constants.H5P_FILE_ACCESS);
                    H5.H5Pset_libver_bounds(fapl, HDF5Constants.H5F_LIBVER_LATEST, HDF5Constants.H5F_LIBVER_LATEST);
                    fileID = H5.H5Fcreate(location, HDF5Constants.H5F_ACC_TRUNC, HDF5Constants.H5P_DEFAULT, fapl);
                } finally {
                    if (fileID > -1) {
                        try {
                            H5.H5Fclose(fileID);
                        } catch (Exception e) {
                            Factory.getLogger().error("Failed to close " + location, e);
                        }
                    }
                    if (fapl > -1) {
                        try {
                            H5.H5Pclose(fapl);
                        } catch (Exception e) {
                            Factory.getLogger().error("Failed to close creation property for " + location, e);
                        } finally {
                            fapl = -1;
                        }
                    }
                }
            } // end if ((openFlag == OPEN_WRITE) && (!new File(location).exists()))
            else if ((openFlag == OPEN_READ_SWMR) && tmp.exists()) {
                fapl = H5.H5Pcreate(HDF5Constants.H5P_FILE_ACCESS);
                H5.H5Pset_libver_bounds(fapl, HDF5Constants.H5F_LIBVER_LATEST, HDF5Constants.H5F_LIBVER_LATEST);
            } else if ((openFlag == OPEN_READ) && tmp.exists()) {
                fapl = H5.H5Pcreate(HDF5Constants.H5P_FILE_ACCESS);
            }
        }
        // XXX finally, we decided not to change sieve buffer size, as it resulted in worse performances
        if (fapl < 0) {
            fapl = HDF5Constants.H5P_DEFAULT;
//        } else {
//            // Try to change sieve buffer size before file opening (otherwise, changes won't be effective)
//            long fileSize = new File(hdfFileName).length();
//            long currentSieveBufferSize = H5.H5Pget_sieve_buf_size(fapl);
////            StringBuilder builder = new StringBuilder("\n=================\n");
////            builder.append(hdfFileName).append(": ");
////            builder.append("\n- cache size:\t\t\t\t").append(currentSieveBufferSize);
////            builder.append("\n- file size:\t\t\t\t").append(fileSize);
//            MemoryMXBean mxBean = ManagementFactory.getMemoryMXBean();
//            MemoryUsage usage = mxBean.getHeapMemoryUsage();
//            long used = usage.getUsed();
//            long max = usage.getMax();
////            long allocated = usage.getCommitted();
//
//            // Consider we might have 2 times current used memory, and we may load up to 30 files.
//            long maxAllowed = Math.max(HdfObjectUtils.DEFAULT_CACHE_SIZE, (max - 2 * used) / 30);
//            long div = maxAllowed / HdfObjectUtils.DEFAULT_CACHE_SIZE;
////            builder.append("\n- potential available memory size:\t").append(maxAllowed);
////            builder.append("\n- potential available memory div:\t").append(div);
////            builder.append("\n- test size:\t\t\t\t").append(fileSize / DEFAULT_CACHE_SIZE);
//
//            // Allow cache up to 1000 times default value.
//            long newSize = Math.min(fileSize, HdfObjectUtils.DEFAULT_CACHE_SIZE * Math.min(1000, div));
//            if (newSize > currentSieveBufferSize) {
//                H5.H5Pset_sieve_buf_size(fapl, newSize);
//            }
        }
        return fapl;
    }

    @Override
    public String getFactoryName() {
        return factoryName;
    }

    @Override
    public HdfGroup getRootGroup() {
        HdfGroup group;
        if (root == null) {
            synchronized (this) {
                if (root == null) {
                    boolean close = false;
                    try {
                        if (!isOpen()) {
                            open();
                            close = true;
                        }
                        root = HdfGroup.getGroup(factoryName, this, h5FileID, HdfPath.PATH_SEPARATOR);
                    } catch (DataAccessException e) {
                        Factory.getLogger().error(e.getMessage());
                    } finally {
                        closeDataset(this, close);
                    }
                }
                group = root;
            }
        } else {
            group = root;
        }
        return group;
    }

    public void setRoot(HdfGroup root) {
        this.root = root;
    }

    @Override
    public LogicalGroup getLogicalRoot(String view) {
        return new LogicalGroup(view, null, this);
    }

    @Override
    public synchronized void setLocation(final String location) {
        if (!ObjectUtils.sameObject(location, hdfFileName)) {
            boolean wasOpen = isOpen();
            if (wasOpen) {
                try {
                    close();
                } catch (DataAccessException e) {
                    Factory.getLogger().error(e.getMessage());
                }
            }
            hdfFileName = location;
            if (wasOpen) {
                try {
                    open();
                } catch (DataAccessException e) {
                    Factory.getLogger().error(e.getMessage());
                }
            }
        }
    }

    @Override
    public String getLocation() {
        return hdfFileName;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public void setTitle(final String title) {
        this.title = title;
    }

    @Override
    public long getLastModificationDate() {
        return new File(hdfFileName).lastModified();
    }

    @Override
    public synchronized void open() throws DataAccessException {
        try {
            long[] ids = open(hdfFileName, h5FileID, swmr);
            h5FileID = ids[0];
            fapl = ids[1];
        } catch (DataAccessException e) {
            h5FileID = -1;
            throw e;
        }
    }

    protected long[] open(final String hdfFileName, final long knownFileID, final boolean swmr)
            throws DataAccessException {
        long h5FileID = knownFileID;
        long fapl = -1;
        try {
            if ((hdfFileName == null) || (hdfFileName.isEmpty())) {
                h5FileID = -1;
            } else {
                // Do not open the same file access twice
                if (h5FileID < 0) {
                    try {
                        fapl = initHdfFile(hdfFileName);
                    } catch (Exception e) {
                        throw new DataAccessException("Failed to create " + hdfFileName, e);
                    }
                    h5FileID = H5.H5Fopen(hdfFileName, openFlag, fapl);
                    if (swmr && (openFlag == OPEN_WRITE)) {
                        H5.H5Fstart_swmr_write(h5FileID);
                    }
                } // end if (h5FileID < 0)
            } // end if ((hdfFileName == null) || (hdfFileName.isEmpty())) ... else
        } catch (Exception e) {
            if (h5FileID > -1) {
                try {
                    H5.H5Fclose(h5FileID);
                    h5FileID = -1;
                } catch (Exception ce) {
                    Factory.getLogger().error("Failed to close " + hdfFileName, ce);
                }
            }
            if (e instanceof DataAccessException) {
                throw (DataAccessException) e;
            } else {
                throw new DataAccessException("Failed to open " + hdfFileName, e);
            }
        }
        return new long[] { h5FileID, fapl };
    }

    protected static void closeDataset(HdfDataset dataset, boolean close) {
        if (close && (dataset != null)) {
            try {
                dataset.close();
            } catch (DataAccessException e) {
                Factory.getLogger().error("Failed to close " + dataset.getLocation(), e);
            }
        }
    }

    @Override
    public void close() throws DataAccessException {
        try {
            h5FileID = close(h5FileID, fapl, hdfFileName);
        } catch (DataAccessException e) {
            h5FileID = -1;
            throw e;
        } finally {
            fapl = -1;
        }
    }

    protected synchronized long close(final long fileID, final long fapl, final String hdfFileName)
            throws DataAccessException {
        long h5FileID = fileID;
        try {
            // Code extracted from H5File.close()
            if (h5FileID > -1) {
                // Close all open objects associated with this file.
                try {
                    int n = 0, type = -1;
                    long[] oids;
                    n = (int) H5.H5Fget_obj_count(h5FileID, HDF5Constants.H5F_OBJ_ALL);

                    if (n > 0) {
                        oids = new long[n];
                        H5.H5Fget_obj_ids(h5FileID, HDF5Constants.H5F_OBJ_ALL, n, oids);

                        for (int i = 0; i < n; i++) {
                            type = H5.H5Iget_type(oids[i]);

                            if (HDF5Constants.H5I_DATASET == type) {
                                try {
                                    H5.H5Dclose(oids[i]);
                                } catch (Exception ex2) {
                                    Factory.getLogger().debug("Object[{}] H5Dclose failure: ", i, ex2);
                                }
                            } else if (HDF5Constants.H5I_GROUP == type) {
                                try {
                                    H5.H5Gclose(oids[i]);
                                } catch (Exception ex2) {
                                    Factory.getLogger().debug("Object[{}] H5Gclose failure: ", i, ex2);
                                }
                            } else if (HDF5Constants.H5I_DATATYPE == type) {
                                try {
                                    H5.H5Tclose(oids[i]);
                                } catch (Exception ex2) {
                                    Factory.getLogger().debug("Object[{}] H5Tclose failure: ", i, ex2);
                                }
                            } else if (HDF5Constants.H5I_ATTR == type) {
                                try {
                                    H5.H5Aclose(oids[i]);
                                } catch (Exception ex2) {
                                    Factory.getLogger().debug("Object[{}] H5Aclose failure: ", i, ex2);
                                }
                            }
                        } // for (int i = 0; i < n; i++)
                    } // if (n > 0)
                } catch (Exception ex) {
                    Factory.getLogger().debug("close open objects failure: ", ex);
                }

                try {
                    H5.H5Fflush(h5FileID, HDF5Constants.H5F_SCOPE_GLOBAL);
                } catch (Exception ex) {
                    Factory.getLogger().debug("H5Fflush failure: ", ex);
                }

                try {
                    H5.H5Fclose(h5FileID);
                } finally {
                    // Set file ID to -1
                    h5FileID = -1;
                }

            }
        } catch (Exception e) {
            throw HdfObjectUtils.toDataAccessException(e, "Failed to close access to " + hdfFileName);
        } finally {
            if ((fapl > -1) && (fapl != HDF5Constants.H5P_DEFAULT)) {
                try {
                    H5.H5Pclose(fapl);
                } catch (Exception e) {
                    Factory.getLogger().error("Failed to close file access property for " + hdfFileName, e);
                }
            }
        }
        return h5FileID;
    }

    @Override
    public boolean isOpen() {
        return (h5FileID != -1);
    }

    @Override
    public boolean isDead() {
        // not managed
        return false;
    }

    public boolean hasWriteAccess() {
        return openFlag == OPEN_WRITE;
    }

    public long getH5FileID() {
        return h5FileID;
    }

    @Override
    public boolean sync() throws DataAccessException {
        throw new NotImplementedException();
    }

    // As explained in Jira JAVAAPI-441, there are some known limitations with SWMR.
    // - Write part is very limited.
    // - SWMR won't work on network files.
    // For these reasons, we should only use SWMR with local files and read access.
    protected static boolean canUseSWMR(final File hdfFile, final boolean withWriteAccess) {
//        long time = System.currentTimeMillis();
        boolean swmr = ((hdfFile != null) && (!withWriteAccess) && (!FileUtils.isNetworkFile(hdfFile)));
//        System.out.println(
//                DateUtil.elapsedTimeToStringBuilder(new StringBuilder("** It took "), System.currentTimeMillis() - time)
//                        .append(" to test for SWMR compatibility"));
        return swmr;
    }

    public static boolean checkHdfAPI() {
        return true;
    }

    @Override
    public void save() throws WriterException {
        saveTo(hdfFileName);
    }

    @Override
    public synchronized void saveTo(final String location) throws WriterException {
        boolean myLocation = ObjectUtils.sameObject(location, getLocation());
        boolean ignoreDirtyTest = (!myLocation) && (!ObjectUtils.sameObject(location, lastSavedPath));
        long fileID = myLocation ? this.h5FileID : -1;
        long fapl = myLocation ? this.fapl : -1;
        boolean close = (fileID < 0);
        try {
            if (close) {
                long[] ids = open(location, fileID, swmr);
                fileID = ids[0];
                fapl = ids[1];
            }
            if (myLocation) {
                this.h5FileID = fileID;
            }
            HdfGroup root = getRootGroup();
            // recursive save
            root.save(fileID, fileID, ignoreDirtyTest);
        } catch (Exception e) {
            Factory.getLogger().error(e.getMessage());
            throw new WriterException(e);
        } finally {
            if (close && (fileID > -1)) {
                try {
                    fileID = close(fileID, fapl, location);
                } catch (DataAccessException e) {
                    fileID = -1;
                    Factory.getLogger().error(e.getMessage(), e);
                } finally {
                    if (myLocation) {
                        this.h5FileID = fileID;
                        this.fapl = -1;
                    }
                }
            }
            lastSavedPath = location;
        }
    }

    @Override
    public void save(final IContainer container) throws WriterException {
        throw new NotImplementedException();
    }

    @Override
    public void save(final String parentPath, final IAttribute attribute) throws WriterException {
        throw new NotImplementedException();
    }

    @Override
    public String toString() {
        StringBuilder buffer = new StringBuilder(TO_STRING_PREFIX);
        buffer.append(getLocation());
        return buffer.toString();
    }

    @Override
    protected void finalize() throws Throwable {
        try {
            close();
        } catch (Exception e) {
            // just ignore: we only try to clean
        }
        super.finalize();
    }
}

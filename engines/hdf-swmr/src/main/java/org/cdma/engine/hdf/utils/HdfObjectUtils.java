/*******************************************************************************
 * Copyright (c) 2008 - ANSTO/Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * Norman Xiong (nxi@Bragg Institute) - initial API and implementation
 * Tony Lam (nxi@Bragg Institute) - initial API and implementation
 * Majid Ounsy (SOLEIL Synchrotron) - API v2 design and conception
 * Stephane Poirier (SOLEIL Synchrotron) - API v2 design and conception
 * Clement Rodriguez (ALTEN for SOLEIL Synchrotron) - API evolution
 * Gregory VIGUIER (SOLEIL Synchrotron) - API evolution
 * Raphael GIRARDOT (SOLEIL Synchrotron) - API evolution
 ******************************************************************************/
package org.cdma.engine.hdf.utils;

import java.util.Arrays;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

import org.cdma.exception.DataAccessException;

import fr.soleil.lib.project.ObjectUtils;
import hdf.hdf5lib.H5;
import hdf.hdf5lib.HDF5Constants;
import hdf.hdf5lib.exceptions.HDF5LibraryException;

public class HdfObjectUtils {

    public static final int RETRY_COUNT;
    public static final long RETRY_SLEEPING_PERIOD;
    public static final boolean RETRY_WITH_INIT;
    public static final boolean HDF_API_READY;
    // 65536 is default allocated cache (64 KB = 64 * 1024 = 65536 bytes)
    public static final long DEFAULT_CACHE_SIZE = 65536;
    static {
        int count;
        try {
            count = Integer.parseInt(System.getProperty("HDF5Retries"));
            if (count < 0) {
                count = 0;
            }
        } catch (Exception e) {
            count = 0;
        }
        RETRY_COUNT = count;
        long sleep;
        try {
            sleep = Long.parseLong(System.getProperty("HDF5SleepingPeriod"));
            if (sleep < 0) {
                sleep = 0;
            }
        } catch (Exception e) {
            sleep = 0;
        }
        RETRY_SLEEPING_PERIOD = sleep;
        boolean init;
        try {
            init = Boolean.parseBoolean(System.getProperty("HDF5RetryWithInit"));
        } catch (Exception e) {
            init = false;
        }
        RETRY_WITH_INIT = init;
        long test;
        try {
            // HDF5Constant has a static block code which will be executed the first time the class is created
            // This static code loads JHDF and HDF natives.
            test = HDF5Constants.H5E_DATASET;
        } catch (Exception e) {
            // Native API is found but something else happened
            test = -1;
        } catch (UnsatisfiedLinkError e) {
            // Native lib is NOT found
            test = -1;
        }
        HDF_API_READY = (test != -1);
    }

    /**
     * Converts an array of bytes into an array of Strings for a fixed string
     * dataset.
     * <p>
     * A C-string is an array of chars while an Java String is an object. When a
     * string dataset is read into Java application, the data is stored in an
     * array of Java bytes. byteToString() is used to convert the array of bytes
     * into array of Java strings so that applications can display and modify
     * the data content.
     * <p>
     * For example, the content of a two element C string dataset is {"ABC",
     * "abc"}. Java applications will read the data into an byte array of {65,
     * 66, 67, 97, 98, 99). byteToString(bytes, 3) returns an array of Java
     * String of strs[0]="ABC", and strs[1]="abc".
     * <p>
     * If memory data of strings is converted to Java Strings, stringToByte()
     * must be called to convert the memory data back to byte array before data
     * is written to file.
     * 
     * @see #stringToByte(String[], int)
     * 
     * @param bytes the array of bytes to convert.
     * @param length the length of string.
     * 
     * @return the array of Java String.
     */
    // Extracted from Dataset
    public static final String[] byteToString(byte[] bytes, int length) {
        String[] strArray;
        if (bytes == null) {
            strArray = null;
        } else {
            int n = bytes.length / length;
            strArray = new String[n];
            String str = null;
            int idx = 0;
            for (int i = 0; i < n; i++) {
                str = new String(bytes, i * length, length);

                idx = str.indexOf('\0');
                if (idx > 0) {
                    str = str.substring(0, idx);
                }

                // trim only the end
                int end = str.length();
                while ((end > 0) && (str.charAt(end - 1) <= '\u0020')) {
                    end--;
                }

                strArray[i] = (end <= 0) ? ObjectUtils.EMPTY_STRING : str.substring(0, end);
            }
        }

        return strArray;
    }

    /**
     * Converts a string array into an array of bytes for a fixed string
     * dataset.
     * <p>
     * If memory data of strings is converted to Java Strings, stringToByte()
     * must be called to convert the memory data back to byte array before data
     * is written to file.
     * 
     * @see #byteToString(byte[] bytes, int length)
     * 
     * @param strings the array of string.
     * @param length the length of string.
     * 
     * @return the array of bytes.
     */
    // Extracted from Dataset
    public static final byte[] stringToByte(String[] strings, int length) {
        byte[] bytes;
        if (strings == null) {
            bytes = null;
        } else {
            int size = strings.length;
            bytes = new byte[size * length];
            StringBuffer strBuff = new StringBuffer(length);
            for (int i = 0; i < size; i++) {
                // initialize the string with spaces
                strBuff.replace(0, length, " ");
                String str = strings[i];
                if (str != null) {
                    if (str.length() > length) {
                        str = str.substring(0, length);
                    }
                    strBuff.replace(0, length, str);
                }
                strBuff.setLength(length);
                System.arraycopy(strBuff.toString().getBytes(), 0, bytes, length * i, length);
            }
        }
        return bytes;
    }

    /**
     * Computes the writable {@link String} size of a {@link String} array.
     * 
     * @param value The {@link String} array.
     * @return An <code>int</code>.
     */
    public static int getWritableSize(String... value) {
        int size = -1;
        if (value != null) {
            for (String str : value) {
                if (str != null) {
                    size = Math.max(size, str.length());
                }
            }
            if (size > -1) {
                size++;
            }
        }
        return size;
    }

    /**
     * Recovers the dimensions of some data identified by a space id.
     * 
     * @param spaceId The space id.
     * @return A <code>long[]</code>.
     * @throws HDF5LibraryException If a reading problem occurred.
     */
    public static long[] getDimensions(long spaceId) throws HDF5LibraryException {
        long[] dims = null;
        if (spaceId > -1) {
            int rank = H5.H5Sget_simple_extent_ndims(spaceId);
            if (rank > 0) {
                dims = new long[rank];
                H5.H5Sget_simple_extent_dims(spaceId, dims, null);
            } else {
                long nbPoints = H5.H5Sget_simple_extent_npoints(spaceId);
                dims = new long[] { nbPoints };
            }
        }
        return dims;
    }

    /**
     * Returns a {@link DataAccessException} equivalent to a given {@link Exception}.
     * 
     * @param e The {@link Exception}.
     * @param message The error message to display, if a new {@link DataAccessException} should be created.
     * @return A {@link DataAccessException}.
     */
    public static DataAccessException toDataAccessException(Exception e, String message) {
        DataAccessException dae;
        if (e instanceof DataAccessException) {
            dae = (DataAccessException) e;
        } else {
            dae = new DataAccessException(message == null ? e.getMessage() : message, e);
        }
        return dae;
    }

    /**
     * Returns a {@link DataAccessException} equivalent to a given {@link Exception}.
     * 
     * @param e The {@link Exception}.
     * @return A {@link DataAccessException}.
     */
    public static DataAccessException toDataAccessException(Exception e) {
        return toDataAccessException(e, null);
    }

    /**
     * Parses a potentially <code>null</code> {@link Boolean} into a <code>boolean</code>.
     * 
     * @param b The {@link Boolean}.
     * @return A <code>boolean</code>.
     */
    protected static boolean getBoolean(Boolean b) {
        return b == null ? false : b.booleanValue();
    }

    /**
     * Returns the native data type identifier for an attribute or item, knowing its expected stored data type (data
     * type used at creation).
     * 
     * @param dataType The type of data stored in the attribute or item.
     * @param knownTypeId The potentially already known non-native data type id. If not known, use a negative value
     *            (example: <code>-1</code>).
     * @return A <code>long</code>: the expected identifier.
     * @throws DataAccessException If a problem occurred with HDF5 library.
     */
    public static long getNativeDataTypeForClass(Class<?> dataType, long knownTypeId) throws DataAccessException {
        return getNativeDataTypeForClass(dataType, false, knownTypeId);
    }

    /**
     * Returns the native data type identifier for an attribute or item, knowing its expected stored data type (data
     * type used at creation).
     * 
     * @param dataType The type of data stored in the attribute or item.
     * @param unsignedOrVariableLength A {@link Boolean}, that means:
     *            <ul>
     *            <li>If data is integer, whether it is unsigned.</li>
     *            <li>If data is string, whether it is of variable length.</li>
     *            </ul>
     *            <p>
     *            If <code>null</code>, it will be considered as <code>false</code>.
     *            </p>
     * @param knownTypeId The potentially already known non-native data type id. If not known, use a negative value
     *            (example: <code>-1</code>).
     * @return A <code>long</code>: the expected identifier.
     * @throws DataAccessException If a problem occurred with HDF5 library.
     */
    public static long getNativeDataTypeForClass(Class<?> dataType, Boolean unsignedOrVariableLength, long knownTypeId)
            throws DataAccessException {
        return getNativeDataTypeForClass(dataType, getBoolean(unsignedOrVariableLength), knownTypeId);
    }

    /**
     * Returns the native data type identifier for an attribute or item, knowing its expected stored data type (data
     * type used at creation).
     * 
     * @param dataType The type of data stored in the attribute or item.
     * @param unsignedOrVariableLength A <code>boolean</code> that means:
     *            <ul>
     *            <li>If data is integer, whether it is unsigned.</li>
     *            <li>If data is string, whether it is of variable length.</li>
     *            </ul>
     * @param knownTypeId The potentially already known non-native data type id. If not known, use a negative value
     *            (example: <code>-1</code>).
     * @return A <code>long</code>: the expected identifier.
     * @throws DataAccessException If a problem occurred with HDF5 library.
     */
    public static long getNativeDataTypeForClass(Class<?> dataType, boolean unsignedOrVariableLength, long knownTypeId)
            throws DataAccessException {
        // Default is STRING
        long dataTypeId = HDF5Constants.H5T_STRING;
        try {
            if ((Byte.TYPE.equals(dataType)) || Byte.class.equals(dataType)) {
                dataTypeId = unsignedOrVariableLength ? HDF5Constants.H5T_NATIVE_UCHAR : HDF5Constants.H5T_NATIVE_CHAR;
            } else if ((Short.TYPE.equals(dataType)) || Short.class.equals(dataType)) {
                dataTypeId = unsignedOrVariableLength ? HDF5Constants.H5T_NATIVE_USHORT
                        : HDF5Constants.H5T_NATIVE_SHORT;
            } else if ((Integer.TYPE.equals(dataType)) || Integer.class.equals(dataType)) {
                dataTypeId = unsignedOrVariableLength ? HDF5Constants.H5T_NATIVE_UINT : HDF5Constants.H5T_NATIVE_INT;
            } else if ((Long.TYPE.equals(dataType)) || Long.class.equals(dataType)) {
                dataTypeId = unsignedOrVariableLength ? HDF5Constants.H5T_NATIVE_ULONG : HDF5Constants.H5T_NATIVE_LONG;
            } else if ((Float.TYPE.equals(dataType)) || Float.class.equals(dataType)) {
                dataTypeId = HDF5Constants.H5T_NATIVE_FLOAT;
            } else if ((Double.TYPE.equals(dataType)) || Double.class.equals(dataType)) {
                dataTypeId = HDF5Constants.H5T_NATIVE_DOUBLE;
            } else if (String.class.equals(dataType)) {
                if (knownTypeId > -1) {
                    dataTypeId = knownTypeId;
                } else {
                    dataTypeId = H5.H5Tcopy(HDF5Constants.H5T_C_S1);
                    if (unsignedOrVariableLength) {
                        H5.H5Tset_size(dataTypeId, HDF5Constants.H5T_VARIABLE);
                    }
                }
            }
        } catch (Exception e) {
            throw toDataAccessException(e);
        }
        return dataTypeId;
    }

    /**
     * Returns the data type identifier for an attribute or item creation, knowing its expected stored data type.
     * 
     * @param dataType The type of data stored in the attribute or item.
     * @param unsignedOrVariableLength A {@link Boolean}, that means:
     *            <ul>
     *            <li>If data is integer, whether it is unsigned.</li>
     *            <li>If data is string, whether it is of variable length.</li>
     *            </ul>
     *            <p>
     *            If <code>null</code>, it will be considered as <code>false</code>.
     *            </p>
     * @return A <code>long</code>: the expected identifier.
     * @throws DataAccessException If a problem occurred with HDF5 library.
     */
    public static long getDataTypeIdForCreation(Class<?> dataType, Boolean unsignedOrVariableLength)
            throws DataAccessException {
        return getDataTypeIdForCreation(dataType, getBoolean(unsignedOrVariableLength));
    }

    /**
     * Returns the data type identifier for an attribute or item creation, knowing its expected stored data type.
     * 
     * @param dataType The type of data stored in the attribute or item.
     * @param unsignedOrVariableLength A <code>boolean</code> that means:
     *            <ul>
     *            <li>If data is integer, whether it is unsigned.</li>
     *            <li>If data is string, whether it is of variable length.</li>
     *            </ul>
     * @return A <code>long</code>: the expected identifier.
     * @throws DataAccessException If a problem occurred with HDF5 library.
     */
    public static long getDataTypeIdForCreation(Class<?> dataType, boolean unsignedOrVariableLength)
            throws DataAccessException {
        long dataTypeId;
        try {
            // Java uses Big Endian integers:
            // https://stackoverflow.com/questions/362384/does-java-read-integers-in-little-endian-or-big-endian
            if (Byte.TYPE.equals(dataType) || Byte.class.equals(dataType)) {
                dataTypeId = unsignedOrVariableLength ? HDF5Constants.H5T_STD_U8BE : HDF5Constants.H5T_STD_I8BE;
            } else if (Short.TYPE.equals(dataType) || Short.class.equals(dataType)) {
                dataTypeId = unsignedOrVariableLength ? HDF5Constants.H5T_STD_U16BE : HDF5Constants.H5T_STD_I16BE;
            } else if (Integer.TYPE.equals(dataType) || Integer.class.equals(dataType)) {
                dataTypeId = unsignedOrVariableLength ? HDF5Constants.H5T_STD_U32BE : HDF5Constants.H5T_STD_I32BE;
            } else if (Long.TYPE.equals(dataType) || Long.class.equals(dataType)) {
                dataTypeId = unsignedOrVariableLength ? HDF5Constants.H5T_STD_U64BE : HDF5Constants.H5T_STD_I64BE;
            } else if (Float.TYPE.equals(dataType) || Float.class.equals(dataType)) {
                dataTypeId = HDF5Constants.H5T_IEEE_F32BE;
            } else if (Double.TYPE.equals(dataType) || Double.class.equals(dataType)) {
                dataTypeId = HDF5Constants.H5T_IEEE_F64BE;
            } else if (String.class.equals(dataType)) {
                dataTypeId = H5.H5Tcopy(HDF5Constants.H5T_C_S1);
                if (unsignedOrVariableLength) {
                    H5.H5Tset_size(dataTypeId, HDF5Constants.H5T_VARIABLE);
                }
            } else {
                // TODO all other attributes cases
                dataTypeId = -1;
            }
        } catch (Exception e) {
            throw toDataAccessException(e);
        }
        return dataTypeId;
    }

    /**
     * Recovers the {@link Class} to interact with some data.
     * 
     * @param nativeTypeID The native data type ID
     * @param encodingType The data encoding type/length (8bits, 16bits, ...)
     * @param classType The data class type
     * @param variableStr Whether data represents variable length string(s)
     * @param vlen Whether data represents some variable length data
     * @param compound Whether data represents some compound data
     * @param scalar Whether data is scalar
     * @return A {@link Class}. <code>null</code> if no matching class found.
     */
    public static Class<?> getCorrespondingClass(final long nativeTypeID, final long encodingType, int classType,
            final boolean variableStr, final boolean vlen, final boolean compound, final boolean scalar) {
        Class<?> type;
        if (vlen || variableStr || compound || (scalar && (classType == HDF5Constants.H5T_ARRAY))
                || H5.H5Tequal(nativeTypeID, HDF5Constants.H5T_STD_REF_DSETREG)) {
            type = String.class;
        } else if (classType == HDF5Constants.H5T_INTEGER) {
            if (encodingType == 1) {
                type = Byte.TYPE;
            } else if (encodingType == 2) {
                type = Short.TYPE;
            } else if (encodingType == 4) {
                type = Integer.TYPE;
            } else if (encodingType == 8) {
                type = Long.TYPE;
            } else {
                type = null;
            }
        } else if (classType == HDF5Constants.H5T_FLOAT) {
            if (encodingType == 4) {
                type = Float.TYPE;
            } else if (encodingType == 8) {
                type = Double.TYPE;
            } else {
                type = null;
            }
        } else if (classType == HDF5Constants.H5T_STRING) {
            type = String.class;
        } else if (classType == HDF5Constants.H5T_REFERENCE) {
            type = Long.TYPE;
            // XXX This former code is wrong and leads to data reading error
//        } else if ((classType == HDF5Constants.H5T_OPAQUE) || (classType == HDF5Constants.H5T_BITFIELD)) {
//            type = Byte.TYPE;
        } else {
            type = null;
        }
        return type;
    }

    /**
     * Prepares an array for data reading.
     * 
     * @param nativeTypeID The native data type ID
     * @param encodingType The data encoding type/length (8bits, 16bits, ...)
     * @param classType The data class type
     * @param size The calculated data size
     * @param variableStr Whether data represents variable length string(s)
     * @param vlen Whether data represents some variable length data
     * @param compound Whether data represents some compound data
     * @param scalar Whether data is scalar
     * @return An array, ready for receiving read data.
     */
    // based on H5Datatype.allocateArray
    public static Object createArrayForDataReading(final long nativeTypeID, final long encodingType,
            final int classType, final int size, final boolean variableStr, final boolean vlen, final boolean compound,
            final boolean scalar) {
        Object value;
        if (vlen || variableStr || compound || (scalar && (classType == HDF5Constants.H5T_ARRAY))
                || H5.H5Tequal(nativeTypeID, HDF5Constants.H5T_STD_REF_DSETREG)) {
            String[] strs = new String[size];
            Arrays.fill(strs, ObjectUtils.EMPTY_STRING);
            value = strs;
        } else if (classType == HDF5Constants.H5T_INTEGER) {
            if (encodingType == 1) {
                value = new byte[size];
            } else if (encodingType == 2) {
                value = new short[size];
            } else if (encodingType == 4) {
                value = new int[size];
            } else if (encodingType == 8) {
                value = new long[size];
            } else {
                value = null;
            }
        } else if (classType == HDF5Constants.H5T_FLOAT) {
            if (encodingType == 4) {
                float[] val = new float[size];
                Arrays.fill(val, Float.NaN); // TODO maybe remove that line
                value = val;
            } else if (encodingType == 8) {
                double[] val = new double[size];
                Arrays.fill(val, Double.NaN); // TODO maybe remove that line
                value = val;
            } else {
                value = null;
            }
        } else if ((classType == HDF5Constants.H5T_STRING) || (classType == HDF5Constants.H5T_REFERENCE)) {
            value = new byte[size * (int) encodingType];
        } else if ((classType == HDF5Constants.H5T_OPAQUE) || (classType == HDF5Constants.H5T_BITFIELD)) {
            value = new byte[size * (int) encodingType];
        } else {
            // TODO cas HDF5Constants.H5T_ARRAY, HDF5Constants.H5T_ENUM
            value = null;
        }
        return value;
    }

    public static boolean prepareRetry() {
        boolean retry = true;
        if (RETRY_SLEEPING_PERIOD > 0) {
            try {
                Thread.sleep(RETRY_SLEEPING_PERIOD);
            } catch (InterruptedException e1) {
                retry = false;
            }
        }
        return retry;
    }

    public static String getFilterType(long dcplId) throws DataAccessException {
        try {
            String filterTypeStr;
            // Java lib requires a valid filter_name object and cd_values
            int[] flags = { 0 };
            long[] cd_nelmts = { 1 };
            int[] cd_values = { 0 };
            String[] filter_name = { ObjectUtils.EMPTY_STRING };
            int[] filter_config = { 0 };
            int filter_type = -1;

            filter_type = H5.H5Pget_filter(dcplId, 0, flags, cd_nelmts, cd_values, 120, filter_name, filter_config);
            switch (HdfObjectUtils.H5Z_filter.get(filter_type)) {
                case H5Z_FILTER_DEFLATE:
                    filterTypeStr = "H5Z_FILTER_DEFLATE";
                    break;
                case H5Z_FILTER_SHUFFLE:
                    filterTypeStr = "H5Z_FILTER_SHUFFLE";
                    break;
                case H5Z_FILTER_FLETCHER32:
                    filterTypeStr = "H5Z_FILTER_FLETCHER32";
                    break;
                case H5Z_FILTER_SZIP:
                    filterTypeStr = "H5Z_FILTER_SZIP";
                    break;
                case H5Z_FILTER_NBIT:
                    filterTypeStr = "H5Z_FILTER_NBIT";
                    break;
                case H5Z_FILTER_SCALEOFFSET:
                    filterTypeStr = "H5Z_FILTER_SCALEOFFSET";
                    break;
                default:
                    filterTypeStr = "H5Z_FILTER_ERROR";
            }
            return filterTypeStr;
        } catch (Exception e) {
            throw toDataAccessException(e);
        }
    }

    public static long openH5Dataset(long fileId, String datasetName) throws DataAccessException {
        try {
            return H5.H5Dopen(fileId, datasetName, HDF5Constants.H5P_DEFAULT);
        } catch (Exception e) {
            throw toDataAccessException(e);
        }
    }

    public static long getCreatePlist(long datasetId) throws DataAccessException {
        try {
            return H5.H5Dget_create_plist(datasetId);
        } catch (Exception e) {
            throw toDataAccessException(e);
        }
    }

    public static void readDatasetForTests(long datasetId, int[][] dsetData) throws DataAccessException {
        try {
            H5.H5Dread(datasetId, HDF5Constants.H5T_NATIVE_INT, HDF5Constants.H5S_ALL, HDF5Constants.H5S_ALL,
                    HDF5Constants.H5P_DEFAULT, dsetData);
        } catch (Exception e) {
            throw toDataAccessException(e);
        }
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    // Values for the status of space allocation
    public static enum H5Z_filter {
        H5Z_FILTER_ERROR(HDF5Constants.H5Z_FILTER_ERROR), H5Z_FILTER_NONE(HDF5Constants.H5Z_FILTER_NONE),
        H5Z_FILTER_DEFLATE(HDF5Constants.H5Z_FILTER_DEFLATE), H5Z_FILTER_SHUFFLE(HDF5Constants.H5Z_FILTER_SHUFFLE),
        H5Z_FILTER_FLETCHER32(HDF5Constants.H5Z_FILTER_FLETCHER32), H5Z_FILTER_SZIP(HDF5Constants.H5Z_FILTER_SZIP),
        H5Z_FILTER_NBIT(HDF5Constants.H5Z_FILTER_NBIT), H5Z_FILTER_SCALEOFFSET(HDF5Constants.H5Z_FILTER_SCALEOFFSET),
        H5Z_FILTER_RESERVED(HDF5Constants.H5Z_FILTER_RESERVED), H5Z_FILTER_MAX(HDF5Constants.H5Z_FILTER_MAX);
        private static final Map<Integer, H5Z_filter> lookup = new HashMap<Integer, H5Z_filter>();

        static {
            for (H5Z_filter s : EnumSet.allOf(H5Z_filter.class))
                lookup.put(s.getCode(), s);
        }

        private final int code;

        H5Z_filter(int layout_type) {
            this.code = layout_type;
        }

        public int getCode() {
            return this.code;
        }

        public static H5Z_filter get(int code) {
            return lookup.get(code);
        }
    }

}

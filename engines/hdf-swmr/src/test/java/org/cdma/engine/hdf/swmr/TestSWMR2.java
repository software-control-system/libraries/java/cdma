package org.cdma.engine.hdf.swmr;

import javax.swing.JOptionPane;

/*-
 *******************************************************************************
 * Copyright (c) 2011, 2016 Diamond Light Source Ltd.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Matthew Gerring - initial API and implementation and/or initial documentation
 *******************************************************************************/

import hdf.hdf5lib.H5;
import hdf.hdf5lib.HDF5Constants;
import hdf.hdf5lib.exceptions.HDF5Exception;

public class TestSWMR2 {
    private final static String SWING_FILE_TO_READ = "/nfs/ruche/swing-users/20180576/2019/Run1_Session2/arold/arold_00037_HPLCBuffer_2019-02-02_14-22-03_0037.nxs";

    /**
     * open file in SWMR or non-SWMR, read dataset and wait
     * 
     * @param file
     * @param swmr
     * @param sleep
     * @throws HDF5Exception
     * @throws NullPointerException
     */
    private void readSWMR(String file, String name, boolean asc) throws NullPointerException, HDF5Exception {
        long fapl = H5.H5Pcreate(HDF5Constants.H5P_FILE_ACCESS);
        int access = HDF5Constants.H5F_ACC_RDONLY;
        H5.H5Pset_libver_bounds(fapl, HDF5Constants.H5F_LIBVER_LATEST, HDF5Constants.H5F_LIBVER_LATEST);
        access |= HDF5Constants.H5F_ACC_SWMR_READ;
        long fileID = H5.H5Fopen(file, access, fapl);

        long dID = H5.H5Dopen(fileID, "/cdc48Aapo_HPLCBuffer_00037/scan_data/eiger_image", HDF5Constants.H5P_DEFAULT);
        long[] count = new long[] { 1, 1083, 1035 };
        long[] start = new long[] { 0, 0, 0 };
        long fsID = H5.H5Dget_space(dID);

        for (int i = 0; i < 110; i++) {
            int frameIdx = (asc) ? i : 110 - i;
            start[0] = frameIdx;

            H5.H5Sselect_hyperslab(fsID, HDF5Constants.H5S_SELECT_SET, start, null, count, null);

            long[] dims = new long[] { 1083, 1035 };
            long mDsId = H5.H5Screate_simple(2, dims, null);
            int[][] image = null;
            try {
                image = new int[1083][1035];

                H5.H5Dread(dID, HDF5Constants.H5T_NATIVE_UINT32, mDsId, fsID, HDF5Constants.H5P_DEFAULT, image);
                System.out.println("[" + name + "] Image value at (" + frameIdx + ",1,1) = " + image[1][1]);
                System.out.println("[" + name + "] Image value at (" + frameIdx + ",700,800) = " + image[700][800]);
                try {
                    Thread.sleep(250);
                } catch (InterruptedException e) {
                }
            } catch (Exception e) {

                image = null;
                H5.H5Sclose(fsID);
                H5.H5Dclose(dID);
                H5.H5Fclose(fileID);
                H5.H5Pclose(fapl);
                e.printStackTrace();
                System.out.println("!!! Error on Thread " + name);
                System.exit(1);
            }
            image = null;
        }

        H5.H5Sclose(fsID);
        H5.H5Dclose(dID);
        H5.H5Fclose(fileID);
        H5.H5Pclose(fapl);
    }

    private Thread createReadThread(final String file, final String name, final boolean asc) {
        return new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    readSWMR(file, name, asc);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }, name + "");
    }

    public static void main(String[] args) throws HDF5Exception, NullPointerException {
        JOptionPane.showMessageDialog(null, "Ready to start");
        TestSWMR2 instance = new TestSWMR2();
//        Thread ta = instance.createReadThread(SWING_FILE_TO_READ, 0 + "", true);
//        ta.start();
        for (int j = 0; j < 25; j++) {
            Thread ta = instance.createReadThread(SWING_FILE_TO_READ, j + "", (j % 2 == 0));
            ta.start();
        }
        for (int j = 0; j < 20; j++) {
            Thread ta = instance.createReadThread(SWING_FILE_TO_READ, j + " bis", (j % 2 == 0));
            ta.start();
        }
    }

}

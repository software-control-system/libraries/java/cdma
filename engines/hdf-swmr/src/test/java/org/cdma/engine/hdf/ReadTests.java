/*******************************************************************************
 * Copyright (c) 2008 - ANSTO/Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * Norman Xiong (nxi@Bragg Institute) - initial API and implementation
 * Tony Lam (nxi@Bragg Institute) - initial API and implementation
 * Majid Ounsy (SOLEIL Synchrotron) - API v2 design and conception
 * Stephane Poirier (SOLEIL Synchrotron) - API v2 design and conception
 * Clement Rodriguez (ALTEN for SOLEIL Synchrotron) - API evolution
 * Gregory VIGUIER (SOLEIL Synchrotron) - API evolution
 * Raphael GIRARDOT (SOLEIL Synchrotron) - API evolution
 ******************************************************************************/
package org.cdma.engine.hdf;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Random;

import org.cdma.Factory;
import org.cdma.engine.hdf.navigation.HdfDataItem;
import org.cdma.engine.hdf.navigation.HdfDataset;
import org.cdma.engine.hdf.navigation.HdfGroup;
import org.cdma.interfaces.IArray;
import org.cdma.interfaces.IContainer;
import org.cdma.interfaces.IDataItem;
import org.cdma.interfaces.IDataset;
import org.cdma.interfaces.IGroup;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import hdf.hdf5lib.H5;
import hdf.hdf5lib.HDF5Constants;
import hdf.hdf5lib.exceptions.HDF5LibraryException;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ReadTests {

    @Test
    public void aReadFile() throws Exception {
        System.out.println("-------------------");
        System.out.println("- aReadFile start -");
        System.out.println("-------------------");
        IDataset dataSet = new HdfDataset(WriteTests.FACTORY_NAME, WriteTests.FIRST_FILE_TO_WRITE);
        dataSet.open();
        IGroup root = dataSet.getRootGroup();
        if (root != null) {
            // Test ROOT
            Collection<IGroup> groupList = root.getGroupList();
            assertEquals(2, groupList.size());
            assertNull(root.getParentGroup());

            // Test IGroup
            IGroup group1 = root.getGroup(WriteTests.GROUP1);
            assertNotNull(group1);
            assertEquals("Attribute List size", 3, group1.getAttributeList().size());
            assertEquals(WriteTests.GROUP1, group1.getShortName());
            assertEquals(WriteTests.GROUP1_FULL_PATH, group1.getName());
            assertEquals(root, group1.getRootGroup());
            assertEquals(dataSet, group1.getDataset());
            assertTrue(group1.isEntry());
            assertFalse(group1.isRoot());

            // Test IDataItem
            IDataItem data1 = group1.getDataItem(WriteTests.DATA1_BIS);
            assertNotNull(data1);
            IArray iArray = data1.getData();
            assertEquals(int.class, iArray.getElementType());
            Object storage = iArray.getStorage();
            int[] array = (int[]) storage;
            int[] expected = { 0, 1, 2, 3 };
            assertArrayEquals(expected, array);
            assertEquals(int.class, iArray.getElementType());
            assertEquals(root, data1.getRootGroup());
            assertEquals(group1, data1.getParentGroup());
            assertEquals(dataSet, data1.getDataset());
            assertEquals(WriteTests.DATA1_BIS_FULL_PATH, data1.getName());
            assertEquals(WriteTests.DATA1_BIS, data1.getShortName());

            // Test IDataItem
            IDataItem link = group1.getDataItem(WriteTests.TEST_LINK_BIS);
            assertNotNull(link);
            IArray linkiArray = link.getData();
            assertEquals(int.class, linkiArray.getElementType());
            Object linkStorage = linkiArray.getStorage();
            assertEquals(int.class, linkiArray.getElementType());
            int[] linkArray = (int[]) linkStorage;
            int[] expectedInLink = { 0, 1, 2, 3 };
            assertArrayEquals(expectedInLink, linkArray);
            assertEquals(root, link.getRootGroup());
            assertEquals(group1, link.getParentGroup());
            assertEquals(dataSet, link.getDataset());
            assertEquals("/group1/testLinkBis", link.getName());
            assertEquals(WriteTests.TEST_LINK_BIS, link.getShortName());

            // Test Navigation
            IContainer container = root.findContainerByPath(WriteTests.DATA1_FULL_PATH);
            assertNotNull(container);

            List<IContainer> containers = root.findAllContainerByPath(WriteTests.DATA1_FULL_PATH);
            assertNotNull(containers);
            assertTrue(containers.size() == 1);

            containers = root.findAllContainerByPath("/group*/data*");
            assertNotNull(containers);
            assertEquals(3, containers.size());
        }
        dataSet.close();
        System.out.println("-------------------\n");
    }

    protected void testArray(long[] array, String name) throws Exception {
        int length = array.length - 1;
        for (int i = 0; i < length; i++) {
            for (int j = i + 1; j < array.length; j++) {
                Assert.assertNotEquals(name + " should be all different. Found same value at indexes " + i + ", " + j,
                        array[i], array[j]);
            }
        }
    }

    @Test
    public void bReadFileWithConcurrency() throws Exception {
        System.out.println("----------------------------------");
        System.out.println("- bReadFileWithConcurrency start -");
        System.out.println("----------------------------------");
        HdfDataset dataset = new HdfDataset(WriteTests.FACTORY_NAME, WriteTests.WRITESLICES);
        dataset.open();
        HdfGroup root = dataset.getRootGroup();
        assertNotNull("Root group should not be null!", root);
        HdfGroup group1 = root.getGroup(WriteTests.GROUP1);
        assertNotNull("group1 group should not be null!", group1);
        HdfDataItem imageStack = group1.getDataItem(WriteTests.IMAGE_STACK);
        assertNotNull("imageStack group should not be null!", imageStack);
        int threadCound = (new Random().nextInt(10) + 1) * 20;
        final boolean[] threadFinished = new boolean[threadCound];
        final long[] itemIDs = generateLongArray(threadCound), spaceIDs = generateLongArray(threadCound),
                dataTypeIDs = generateLongArray(threadCound), nativeTypeIDs = generateLongArray(
                        threadCound)/*,
                                    start = new long[] { new Random().nextInt(4), 0, 0 }, shape = new long[] { 1, 3, 5 }*/;

        for (int i = 0; i < threadCound; i++) {
            new ReadThread(i, dataset, imageStack, threadFinished, itemIDs, spaceIDs, dataTypeIDs, nativeTypeIDs/*, start,
                                                                                                                shape*/)
                    .start();
        }

        System.out.println("Waiting for " + threadCound + " threads to finish...");
        boolean finished = false;
        while (!finished) {
            Thread.sleep(100);
            finished = true;
            for (boolean b : threadFinished) {
                finished = finished && b;
            }
        }
        dataset.close();
        System.out.println("- itemIDs: " + Arrays.toString(itemIDs));
        System.out.println("- spaceIDs: " + Arrays.toString(spaceIDs));
        System.out.println("- dataTypeIDs: " + Arrays.toString(dataTypeIDs));
        System.out.println("- nativeTypeIDs: " + Arrays.toString(nativeTypeIDs));
        testArray(itemIDs, "itemIDs");
        System.out.println("itemIDs OK");
        testArray(spaceIDs, "spaceIDs");
        System.out.println("spaceIDs OK");
        testArray(dataTypeIDs, "dataTypeIDs");
        System.out.println("dataTypeIDs OK");
        testArray(nativeTypeIDs, "nativeTypeIDs");
        System.out.println("nativeTypeIDs OK");
        System.out.println("----------------------------------\n");
    }

    @Test
    public void cReadFileWithConcurrencyAndSingleItemID() throws Exception {
        System.out.println("-------------------------------------------------");
        System.out.println("- cReadFileWithConcurrencyAndSingleItemID start -");
        System.out.println("-------------------------------------------------");
        HdfDataset dataset = new HdfDataset(WriteTests.FACTORY_NAME, WriteTests.WRITESLICES);
        dataset.open();
        HdfGroup root = dataset.getRootGroup();
        assertNotNull("Root group should not be null!", root);
        HdfGroup group1 = root.getGroup(WriteTests.GROUP1);
        assertNotNull("group1 group should not be null!", group1);
        HdfDataItem imageStack = group1.getDataItem(WriteTests.IMAGE_STACK);
        assertNotNull("imageStack group should not be null!", imageStack);
        int threadCound = (new Random().nextInt(10) + 1) * 20;
        final boolean[] threadFinished = new boolean[threadCound];
        final long[] spaceIDs = generateLongArray(threadCound), dataTypeIDs = generateLongArray(threadCound),
                nativeTypeIDs = generateLongArray(
                        threadCound)/*,
                                    start = new long[] { new Random().nextInt(4), 0, 0 }, shape = new long[] { 1, 3, 5 }*/;
        String name = imageStack.getName();
        long itemID = -1;
        long fileID = dataset.getH5FileID();
        if (fileID > -1) {
            itemID = H5.H5Dopen(fileID, name, HDF5Constants.H5P_DEFAULT);
        }

        for (int i = 0; i < threadCound; i++) {
            new ReadThread2(i, name, threadFinished, itemID, spaceIDs, dataTypeIDs, nativeTypeIDs).start();
        }

        System.out.println("Waiting for " + threadCound + " threads to finish...");
        boolean finished = false;
        while (!finished) {
            Thread.sleep(100);
            finished = true;
            for (boolean b : threadFinished) {
                finished = finished && b;
            }
        }
        HdfDataItem.close(itemID, -1, -1, -1, null, false, name);
        dataset.close();
        System.out.println("- itemID: " + itemID);
        System.out.println("- spaceIDs: " + Arrays.toString(spaceIDs));
        System.out.println("- dataTypeIDs: " + Arrays.toString(dataTypeIDs));
        System.out.println("- nativeTypeIDs: " + Arrays.toString(nativeTypeIDs));
        testArray(spaceIDs, "spaceIDs");
        System.out.println("spaceIDs OK");
        testArray(dataTypeIDs, "dataTypeIDs");
        System.out.println("dataTypeIDs OK");
        testArray(nativeTypeIDs, "nativeTypeIDs");
        System.out.println("nativeTypeIDs OK");
        System.out.println("-------------------------------------------------\n");
    }

    protected static long[] generateLongArray(int count) {
        long[] array = new long[count];
        Arrays.fill(array, -1);
        return array;
    }

    protected static void readDataItem(int index, HdfDataset dataset, String name, long[] itemIDs, long[] spaceIDs,
            long[] dataTypeIDs, long[] nativeTypeIDs) throws HDF5LibraryException, NullPointerException {
        long fileID = dataset.getH5FileID();
        if (fileID > -1) {
            long itemID = H5.H5Dopen(fileID, name, HDF5Constants.H5P_DEFAULT);
            itemIDs[index] = itemID;
            recoverDataIDs(index, itemID, spaceIDs, dataTypeIDs, nativeTypeIDs);
        }
    }

    protected static void recoverDataIDs(int index, long itemID, long[] spaceIDs, long[] dataTypeIDs,
            long[] nativeTypeIDs) throws HDF5LibraryException {
        if (itemID > -1) {
            long dataTypeID = H5.H5Dget_type(itemID);
            dataTypeIDs[index] = dataTypeID;
            long dataSpaceID = H5.H5Dget_space(itemID);
            spaceIDs[index] = dataSpaceID;
            dataTypeID = H5.H5Tget_native_type(dataTypeID);
            nativeTypeIDs[index] = dataTypeID;
        }
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    protected static class ReadThread extends Thread {
        private final int index;
        private final HdfDataset dataset;
        private final HdfDataItem dataItem;
        private final boolean[] finished;
        private final long[] itemIDs, spaceIDs, dataTypeIDs, nativeTypeIDs/*, start, shape*/;

        public ReadThread(int index, HdfDataset dataset, HdfDataItem dataItem, boolean[] finished, long[] itemIDs,
                long[] spaceIDs, long[] dataTypeIDs, long[] nativeTypeIDs/*, long[] start, long[] shape*/) {
            super("Read data thread n°" + (index + 1));
            this.index = index;
            this.dataset = dataset;
            this.dataItem = dataItem;
            this.finished = finished;
            this.itemIDs = itemIDs;
            this.spaceIDs = spaceIDs;
            this.dataTypeIDs = dataTypeIDs;
            this.nativeTypeIDs = nativeTypeIDs;
            /*this.start = start;
            this.shape = shape*/;
        }

        @Override
        public void run() {
            // Sleep between 0 and 100ms
            String name = dataItem.getName();
            try {
                sleep(new Random().nextInt(100));
                readDataItem(index, dataset, name, itemIDs, spaceIDs, dataTypeIDs, nativeTypeIDs);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    H5.H5Tclose(nativeTypeIDs[index]);
                } catch (Exception e) {
                    Factory.getLogger().error("Failed to close native type id for item " + name, e);
                }
                HdfDataItem.close(itemIDs[index], -1, dataTypeIDs[index], spaceIDs[index], null, true, name);
                finished[index] = true;
            }
        }
    }

    protected static class ReadThread2 extends Thread {
        private final int index;
        private final String name;
        private final long itemID;
        private final boolean[] finished;
        private final long[] spaceIDs, dataTypeIDs, nativeTypeIDs;

        public ReadThread2(int index, String name, boolean[] finished, long itemID, long[] spaceIDs, long[] dataTypeIDs,
                long[] nativeTypeIDs) {
            super("Read data thread n°" + (index + 1));
            this.index = index;
            this.name = name;
            this.finished = finished;
            this.itemID = itemID;
            this.spaceIDs = spaceIDs;
            this.dataTypeIDs = dataTypeIDs;
            this.nativeTypeIDs = nativeTypeIDs;
            /*this.start = start;
            this.shape = shape*/;
        }

        @Override
        public void run() {
            // Sleep between 0 and 100ms
            try {
                sleep(new Random().nextInt(100));
                recoverDataIDs(index, itemID, spaceIDs, dataTypeIDs, nativeTypeIDs);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    H5.H5Tclose(nativeTypeIDs[index]);
                } catch (Exception e) {
                    Factory.getLogger().error("Failed to close native type id for item " + name, e);
                }
                HdfDataItem.close(-1, -1, dataTypeIDs[index], spaceIDs[index], null, true, name);
                finished[index] = true;
            }
        }
    }

}

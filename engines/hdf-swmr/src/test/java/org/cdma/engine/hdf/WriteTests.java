/*******************************************************************************
 * Copyright (c) 2008 - ANSTO/Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * Norman Xiong (nxi@Bragg Institute) - initial API and implementation
 * Tony Lam (nxi@Bragg Institute) - initial API and implementation
 * Majid Ounsy (SOLEIL Synchrotron) - API v2 design and conception
 * Stephane Poirier (SOLEIL Synchrotron) - API v2 design and conception
 * Clement Rodriguez (ALTEN for SOLEIL Synchrotron) - API evolution
 * Gregory VIGUIER (SOLEIL Synchrotron) - API evolution
 * Raphael GIRARDOT (SOLEIL Synchrotron) - API evolution
 ******************************************************************************/
package org.cdma.engine.hdf;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.Random;

import org.cdma.engine.hdf.array.HdfArray;
import org.cdma.engine.hdf.navigation.HdfAttribute;
import org.cdma.engine.hdf.navigation.HdfDataItem;
import org.cdma.engine.hdf.navigation.HdfDataset;
import org.cdma.engine.hdf.navigation.HdfGroup;
import org.cdma.exception.InvalidArrayTypeException;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import fr.soleil.lib.project.date.DateUtil;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class WriteTests {

    protected static final String TEMP_PATH = System.getProperty("java.io.tmpdir");
    protected static final String FACTORY_NAME = "HDF";
    protected static final String GROUP1 = "group1";
    protected static final String GROUP2 = "group2";
    protected static final String GROUP3 = "group3";
    protected static final String GROUP1_FULL_PATH = "/group1";
    protected static final String DATA1 = "data1";
    protected static final String DATA1_BIS = "data1Bis";
    protected static final String DATA2 = "data2";
    protected static final String DATA3 = "data3";
    protected static final String DATA1_FULL_PATH = "/group1/data1";
    protected static final String DATA1_BIS_FULL_PATH = "/group1/data1Bis";
    protected static final String STRING_DATA = "stringData";
    protected static final String TEST_LINK = "testLink";
    protected static final String TEST_LINK_BIS = "testLinkBis";
    protected static final String IMAGE_STACK = "imageStack";
    protected static final String ATTR1 = "attr1";
    protected static final String ATTR2 = "attr2";
    protected static final String ATTR100 = "attr100";
    protected static final String LINK_ATTR = "linkAttr";
    protected static final String ITEM_ATTR = "itemAttr";

    public static final File FIRST_FILE_TO_WRITE;
    public static final File SECOND_FILE_TO_WRITE;
    public static final File WRITESLICES;

    static {
        String tempPath = TEMP_PATH;
        if (!tempPath.endsWith(File.separator)) {
            tempPath = TEMP_PATH + File.separator;
        }
        FIRST_FILE_TO_WRITE = new File(tempPath + "testWriteFromScratch.nxs");
        SECOND_FILE_TO_WRITE = new File(tempPath + "testCopyIntoNewFile.nxs");
        WRITESLICES = new File(tempPath + "testWriteSlices.nxs");
    }

    private HdfArray createRandom1DArray(final int arrayLength) {
        HdfArray result = null;

        double[] values = new double[arrayLength];
        for (int i = 0; i < values.length; i++) {
            values[i] = Math.random() * 1000;
        }
        int[] shape = { 1, arrayLength };

        try {
            result = new HdfArray(FACTORY_NAME, values, shape);
        } catch (InvalidArrayTypeException e) {
            e.printStackTrace();
        }
        return result;
    }

    private HdfArray createRandom1DIntArray(final int arrayLength) {
        HdfArray result = null;

        int[] values = new int[arrayLength];
        for (int i = 0; i < values.length; i++) {
            values[i] = new Random().nextInt(arrayLength);
        }
        int[] shape = { 1, arrayLength };

        try {
            result = new HdfArray(FACTORY_NAME, values, shape);
        } catch (InvalidArrayTypeException e) {
            e.printStackTrace();
        }
        return result;
    }

    private HdfArray createRandomString1DArray() {
        HdfArray result = null;

        String[] values = new String[1];
        values[0] = "blééééééé";
        int[] shape = { 1 };

        try {
            result = new HdfArray(FACTORY_NAME, values, shape);
        } catch (InvalidArrayTypeException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return result;
    }

//    private double[] createImages(final int xarrayLength, final int yarrayLength, final int offset) {
//        double[] values = new double[yarrayLength * xarrayLength];
//        int index = 0;
//        for (int i = 0; i < yarrayLength; i++) {
//            for (int j = 0; j < xarrayLength; j++) {
//                values[index] = index++ + offset;
//            }
//        }
//        return values;
//    }
//
//    private double[][][] createImages2D(final int xarrayLength, final int yarrayLength, final int offset) {
//        double[][][] values = new double[1][yarrayLength][xarrayLength];
//        double[][] image = values[0];
//        int index = 0;
//        for (int i = 0; i < yarrayLength; i++) {
//            for (int j = 0; j < xarrayLength; j++) {
//                image[i][j] = index++ + offset;
//            }
//        }
//        return values;
//    }

    @Test
    public void dTestModifyInExistingFile() throws Exception {
        System.out.println("--------------------------------------------------");
        System.out.println("Test: Modify existing file");
        System.out.println(" - Modify group1/data1 values to [0,1,2,3]");
        long time = System.currentTimeMillis();
        HdfDataset dataset = new HdfDataset(FACTORY_NAME, FIRST_FILE_TO_WRITE, true);

        HdfGroup root = dataset.getRootGroup();

        HdfGroup group1 = root.getGroup(GROUP1);

        // Modifiy dataItem values
        HdfDataItem dataItem = group1.getDataItem(DATA1_BIS);
        assertNotNull(dataItem);
        int[] values = { 0, 1, 2, 3 };
        int[] shape = { 1, 4 };
        HdfArray newArray = new HdfArray(FACTORY_NAME, values, shape);
        dataItem.setCachedData(newArray, false);

        assertEquals(int.class, newArray.getElementType());
        // assertArrayEquals(shape, newArray.getShape());

        // Modify attribute
        HdfGroup group2 = root.getGroup(GROUP2);
        HdfDataItem item2 = group2.getDataItem(DATA2);
        HdfAttribute attr1 = item2.getAttribute(ATTR1);
        attr1.setStringValue("attr1modifié");
        dataset.save();

        dataset.close();
        System.out.println(DateUtil.elapsedTimeToStringBuilder(new StringBuilder("End of test: Modify existing file "),
                System.currentTimeMillis() - time));
        System.out.println("--------------------------------------------------");
    }

//
    // @Test
    // public void eTestWriteMultiIntoNewFile() throws Exception {
    // System.out.println("--------------------------------------------------");
    // System.out.println("Test: Copy existing dataset into new file and add new group & dataitem");
    // if (FIRST_FILE_TO_WRITE.exists()) {
    // if (!FIRST_FILE_TO_WRITE.delete()) {
    // System.out.println("Cannot delete file: missing close() ??");
    // System.exit(0);
    // }
    // }
    //
    // HdfDataset dataset = new HdfDataset(FACTORY_NAME, FIRST_FILE_TO_WRITE);
    //
    // // Create group with name group3 under root node and save it
    // HdfGroup root = (HdfGroup) dataset.getRootGroup();
    // IGroup group3 = new HdfGroup(FACTORY_NAME, GROUP3, "/", root, dataset);
    // root.addSubgroup(group3);
    // dataset.save();
    //
    // // Image are 20x10
    // int xLength = 20;
    // int yLength = 10;
    //
    // // We have 3 random images
    // double[] image1 = createImages(xLength, yLength, 0);
    // double[] image2 = createImages(xLength, yLength, 400);
    // double[] image3 = createImages(xLength, yLength, 800);
    //
    // // So shape is:
    // int[] shape = new int[] { 3, yLength, xLength };
    //
    // // We create a DataItem under group3
    // HdfDataItem dataItem = new HdfDataItem(FACTORY_NAME, dataset.getH5File(), group3, "imageStack", shape,
    // double.class);
    //
    // // Now we have to tune the underlying HDF item
    // H5ScalarDS h5Item = dataItem.getH5DataItem();
    // long[] selectedDims = h5Item.getSelectedDims();
    // long[] startDims = h5Item.getStartDims();
    //
    // // We have to give to HDF the non-reduced shape of the slabs we are going to put in the dataitem
    // selectedDims[0] = 1;
    // selectedDims[1] = yLength;
    // selectedDims[2] = xLength;
    //
    // // We have to modify the startDims because HDF cannot guess where to put the slab
    // // First image is at index 0 on the first dimension of our 3 dimension hyperslab
    // startDims[0] = 0; // optional, this is the default value
    // dataItem.getH5DataItem().write(image1);
    //
    // // For the next image, we want start at index 1 on the first dimension of the hyperslab
    // startDims[0] = 1;
    // dataItem.getH5DataItem().write(image2);
    //
    // // For the next image, we want start at index 2 on the first dimension of the hyperslab
    // startDims[0] = 2;
    // dataItem.getH5DataItem().write(image3);
    //
    // dataset.save();
    // dataset.close();
    // }
    @Test
    public void cTestWriteIntoNewFile() throws Exception {
        System.out.println("--------------------------------------------------");
        System.out.println("Test: Copy existing dataset into new file and add new group & dataitem");
        if (SECOND_FILE_TO_WRITE.exists()) {
            if (!SECOND_FILE_TO_WRITE.delete()) {
                System.out.println("Cannot delete file: missing close() ??");
            }
        }
        long time = System.currentTimeMillis();

        HdfDataset dataset = new HdfDataset(FACTORY_NAME, FIRST_FILE_TO_WRITE, true);

        HdfGroup root = dataset.getRootGroup();
        HdfGroup group3 = new HdfGroup(FACTORY_NAME, GROUP3, root, dataset);

        HdfDataItem dataItem = new HdfDataItem(FACTORY_NAME, DATA3);

        HdfArray array = createRandom1DArray(10);
        dataItem.setCachedData(array, false);
        group3.addDataItem(dataItem);
        root.addSubgroup(group3);

        assertEquals(3, root.getGroupList().size());

        dataset.saveTo(SECOND_FILE_TO_WRITE.getAbsolutePath());

        dataset.close();
        System.out.println(DateUtil.elapsedTimeToStringBuilder(
                new StringBuilder("End of test: Copy existing dataset into new file "),
                System.currentTimeMillis() - time));
        System.out.println("--------------------------------------------------");
    }

    @Test
    public void bTestWriteIntoExistingFile() throws Exception {
        System.out.println("--------------------------------------------------");
        System.out.println("Test: Write into a the previous file");
        long time = System.currentTimeMillis();

        // Create a dataset in _Append_ mode
        HdfDataset dataset = new HdfDataset(FACTORY_NAME, FIRST_FILE_TO_WRITE, true);

        HdfGroup root = dataset.getRootGroup();
        HdfGroup group2 = new HdfGroup(FACTORY_NAME, GROUP2, root, dataset);

        HdfDataItem dataItem2 = new HdfDataItem(FACTORY_NAME, DATA2);
        dataItem2.addStringAttribute(ATTR1, "mon attribut");
        dataItem2.addOneAttribute(new HdfAttribute(FACTORY_NAME, ATTR2, 5));

        HdfArray array = createRandom1DArray(10);
        dataItem2.setCachedData(array, false);
        group2.addDataItem(dataItem2);
        root.addSubgroup(group2);

        assertEquals(2, root.getGroupList().size());

        // Test if we can acces to previously written group1
        HdfGroup group1 = root.getGroup(GROUP1);
        assertNotNull(group1);
        group1.addStringAttribute(ATTR100, "mon attribut sauvé ensuite");
        HdfDataItem data1 = group1.getDataItem(DATA1);
        assertNotNull(data1);
        data1.addStringAttribute(ATTR100, "mon attribut sauvé ensuite");

        dataset.save();
        dataset.close();
        System.out.println(DateUtil.elapsedTimeToStringBuilder(
                new StringBuilder("End of test: Write into the previous file "), System.currentTimeMillis() - time));
        System.out.println("--------------------------------------------------");
    }

    @Test
    public void eWriteSlideData() throws Exception {
        long time = System.currentTimeMillis();

        if (WRITESLICES.exists()) {
            if (!WRITESLICES.delete()) {
                System.out.println("Cannot delete file: missing close() ??");
            }
        }
        HdfDataset dataset = new HdfDataset(FACTORY_NAME, WRITESLICES, true);
        HdfGroup root = dataset.getRootGroup();

        // Test Sub Group
        HdfGroup group = new HdfGroup(FACTORY_NAME, GROUP1, root, dataset);
        root.addSubgroup(group);

        // Creation de l'item qui va porter la pile.
        HdfDataItem dataitem = new HdfDataItem(FACTORY_NAME, IMAGE_STACK);
        group.addDataItem(dataitem);

        // Je construit mes données pour le test
        // image 1 (3 lignes de 4 valeurs)
        int[] image1 = { 1, 2, 3, 4, 5, 1, 2, 3, 4, 5, 1, 2, 3, 4, 5 };
        // image 2 (3 lignes de 4 valeurs)
        int[] image2 = { 6, 7, 8, 9, 10, 6, 7, 8, 9, 10, 6, 7, 8, 9, 10 };
        // image 3 (3 lignes de 4 valeurs)
        int[] image3 = { 11, 12, 13, 14, 15, 11, 12, 13, 14, 15, 11, 12, 13, 14, 15 };
        // image 4 n'existe pas encore

        // Preparation d'un tableau virtuel vide dont la taille est celle de la pile
        // -> array de 4x3x5
        int[] fullShape = new int[] { 4, 3, 5 };
        HdfArray emptyArray = new HdfArray(FACTORY_NAME, Integer.TYPE, fullShape);
        // Il faut le sauver pour preparer HDF5 à un item de cette taille.
        dataitem.setCachedData(emptyArray, false);
        dataset.save();

        // Preparation des HDf Array en décalant l'origine à chaque fois
        int[] imageShape = new int[] { 3, 5 };
        HdfArray image1Array = new HdfArray(FACTORY_NAME, image1, imageShape);
        int[] image2Origin = new int[] { 1, 0, 0 };
        HdfArray image2Array = new HdfArray(FACTORY_NAME, image2, imageShape, image2Origin);
        int[] image3Origin = new int[] { 2, 0, 0 };
        HdfArray image3Array = new HdfArray(FACTORY_NAME, image3, imageShape, image3Origin);

        // Ensuite on sauve image par image
        dataitem.setCachedData(image1Array, false);
        dataset.save();

        dataitem.setCachedData(image2Array, false);
        dataset.save();

        dataitem.setCachedData(image3Array, false);
        dataset.save();
        System.out.println(DateUtil.elapsedTimeToStringBuilder(new StringBuilder("End write slices "),
                System.currentTimeMillis() - time));
    }

    @Test
    public void aTestWriteFromScratch() throws Exception {
        System.out.println("--------------------------------------------------");
        System.out.println("Test: Write into a new file");
        long time = System.currentTimeMillis();

        if (FIRST_FILE_TO_WRITE.exists()) {
            if (!FIRST_FILE_TO_WRITE.delete()) {
                System.out.println("Cannot delete file: missing close() ??");
            }
        }
        long time2 = System.currentTimeMillis();
        System.out.println(DateUtil.elapsedTimeToStringBuilder(new StringBuilder("** It took "), time2 - time)
                .append(" to check file"));
        HdfDataset dataset = new HdfDataset(FACTORY_NAME, FIRST_FILE_TO_WRITE, true);
        System.out.println(DateUtil
                .elapsedTimeToStringBuilder(new StringBuilder("** It took "), System.currentTimeMillis() - time2)
                .append(" to create dataset"));

        // Test Root Group
        HdfGroup root = dataset.getRootGroup();
        assertNotNull(root);
        assertTrue(root.isRoot());

        // Test Sub Group
        HdfGroup group1 = new HdfGroup(FACTORY_NAME, GROUP1, root, dataset);
        root.addSubgroup(group1);
        group1.addStringAttribute(ATTR1, "mon attribut");
        group1.addOneAttribute(new HdfAttribute(FACTORY_NAME, ATTR2, 5));
        assertEquals("Attribute List size", 2, group1.getAttributeList().size());
        assertEquals(GROUP1, group1.getShortName());
        assertEquals(GROUP1_FULL_PATH, group1.getName());
        assertEquals(root, group1.getRootGroup());
        assertEquals(dataset, group1.getDataset());
        assertTrue(group1.isEntry());
        assertFalse(group1.isRoot());

        // Test Data Item
        HdfDataItem dataItem = new HdfDataItem(FACTORY_NAME, DATA1);
        dataItem.addStringAttribute(ITEM_ATTR, "1");
        // XXX DEBUG
        group1.addDataItem(dataItem);
        HdfArray array = createRandom1DArray(10);
        dataItem.setCachedData(array, false);
        assertEquals(double.class, array.getElementType());
        assertEquals(root, dataItem.getRootGroup());
        assertEquals(group1, dataItem.getParentGroup());
        assertEquals(dataset, dataItem.getDataset());
        assertEquals(DATA1_FULL_PATH, dataItem.getName());
        assertEquals(DATA1, dataItem.getShortName());

        // Test Data Item bis
        HdfDataItem dataItemBis = new HdfDataItem(FACTORY_NAME, DATA1_BIS);
        dataItemBis.addStringAttribute(ITEM_ATTR, "1");
        // XXX DEBUG
        group1.addDataItem(dataItemBis);
        HdfArray arrayBis = createRandom1DIntArray(4);
        dataItemBis.setCachedData(arrayBis, false);
        assertEquals(int.class, arrayBis.getElementType());
        assertEquals(root, dataItemBis.getRootGroup());
        assertEquals(group1, dataItemBis.getParentGroup());
        assertEquals(dataset, dataItemBis.getDataset());
        assertEquals(DATA1_BIS_FULL_PATH, dataItemBis.getName());
        assertEquals(DATA1_BIS, dataItemBis.getShortName());

        // Test String Data Item
        HdfDataItem stringDataItem = new HdfDataItem(FACTORY_NAME, STRING_DATA);
        group1.addDataItem(stringDataItem);
        HdfArray stringArray = createRandomString1DArray();
        stringDataItem.setCachedData(stringArray, false);

        time2 = System.currentTimeMillis();
        dataset.save();
        System.out.println(DateUtil
                .elapsedTimeToStringBuilder(new StringBuilder("** It took "), System.currentTimeMillis() - time2)
                .append(" at 1st save"));

        HdfDataItem linkdataItem = new HdfDataItem(FACTORY_NAME, TEST_LINK);
        linkdataItem.addStringAttribute(LINK_ATTR, "1");
        group1.addDataItem(linkdataItem);
        linkdataItem.linkTo(dataItem);

        HdfDataItem linkdataItemBis = new HdfDataItem(FACTORY_NAME, TEST_LINK_BIS);
        linkdataItemBis.addStringAttribute(LINK_ATTR, "1");
        group1.addDataItem(linkdataItemBis);
        linkdataItemBis.linkTo(dataItemBis);

        dataset.save();

        System.out.println(DateUtil.elapsedTimeToStringBuilder(new StringBuilder("End of test: Write into a new file "),
                System.currentTimeMillis() - time));
        System.out.println("--------------------------------------------------");
    }

}

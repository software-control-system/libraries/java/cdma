package org.cdma.engine.hdf.basic;

import java.io.File;

import org.cdma.engine.hdf.array.HdfArray;
import org.cdma.engine.hdf.navigation.HdfDataItem;
import org.cdma.engine.hdf.navigation.HdfDataset;
import org.cdma.engine.hdf.navigation.HdfGroup;
import org.junit.Ignore;

@Ignore
public class WriteTmp {

    public static void main(String[] args) throws Exception {
        HdfDataset dataset = new HdfDataset("HDF", new File("D:/GIRARDOT/Desktop/test.h5"), true);
        dataset.open();
        HdfGroup group = dataset.getRootGroup();
        HdfDataItem item = new HdfDataItem("HDF", "data");
        int[] shape = new int[] { 2, 3, 4 };
        int length = 1;
        for (int dim : shape) {
            length *= dim;
        }
        double[] value = new double[length];
        for (int i = 0; i < length; i++) {
            value[i] = i;
        }
        HdfArray array = new HdfArray("HDF", value, shape);
        item.setCachedData(array, false);
        group.addDataItem(item);
        dataset.save();
        dataset.close();
    }

}

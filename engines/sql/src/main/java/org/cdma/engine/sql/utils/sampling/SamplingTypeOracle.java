/*******************************************************************************
 * Copyright (c) 2008 - ANSTO/Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * Norman Xiong (nxi@Bragg Institute) - initial API and implementation
 * Tony Lam (nxi@Bragg Institute) - initial API and implementation
 * Majid Ounsy (SOLEIL Synchrotron) - API v2 design and conception
 * Stephane Poirier (SOLEIL Synchrotron) - API v2 design and conception
 * Clement Rodriguez (ALTEN for SOLEIL Synchrotron) - API evolution
 * Gregory VIGUIER (SOLEIL Synchrotron) - API evolution
 * Raphael GIRARDOT (SOLEIL Synchrotron) - API evolution
 ******************************************************************************/
package org.cdma.engine.sql.utils.sampling;

import java.text.SimpleDateFormat;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import org.cdma.engine.sql.utils.SamplingType;

public enum SamplingTypeOracle implements SamplingType {
    MONTH("YYYY-"), DAY("YYYY-MM-"), HOUR("YYYY-MM-DD "), MINUTE("YYYY-MM-DD HH24:"), SECOND(
            "YYYY-MM-DD HH24:MI:"), FRACTIONAL("YYYY-MM-DD HH24:MI:SS."), NONE("YYYY-MM-DD HH24:MI:SS.FF");

    private String mSampling;
    static private LinkedHashMap<String, String> mCorrespondance;

    static {
        synchronized (SamplingTypeMySQL.class) {
            mCorrespondance = new LinkedHashMap<String, String>();
            mCorrespondance.put("yyyy", "YYYY");
            mCorrespondance.put("MM", "MM");
            mCorrespondance.put("dd", "DD");
            mCorrespondance.put("HH", "HH24");
            mCorrespondance.put("mm", "MI");
            mCorrespondance.put("ss", "SS");
            mCorrespondance.put("SSS", "FF");
        }

    }

    private SamplingTypeOracle(final String sampling) {
        mSampling = sampling;
    }

    @Override
    public String getPattern(final SamplingPeriod period) {
        String result = SamplingTypeOracle.valueOf(period.name()).mSampling;

        for (Entry<String, String> entry : mCorrespondance.entrySet()) {
            result = result.replace(entry.getValue(), entry.getKey());
        }

        return result;
    }

    @Override
    public String getSQLRepresentation() {
        return mSampling;
    }

    @Override
    public String getSQLRepresentation(final SimpleDateFormat format) {
        String result = format.toPattern();

        for (Entry<String, String> entry : mCorrespondance.entrySet()) {
            result = result.replace(entry.getKey(), entry.getValue());
        }

        return result;
    }

    @Override
    public SamplingType getType(final SamplingPeriod time) {
        SamplingType result = SamplingTypeOracle.valueOf(time.name());
        return result;
    }

    @Override
    public String getPatternPeriodUnit(final SamplingPeriod period) {
        String result = getSamplingPeriodUnit(period);

        for (Entry<String, String> entry : mCorrespondance.entrySet()) {
            result = result.replace(entry.getValue(), entry.getKey());
        }

        return result;
    }

    @Override
    public String getSamplingPeriodUnit(final SamplingPeriod period) {
        String result;
        if (period == null) {
            result = null;
        } else {
            switch (period) {
                case FRACTION:
                    result = "FF";
                    break;
                case SECOND:
                    result = "SS";
                    break;
                case MINUTE:
                    result = "MI";
                    break;
                case HOUR:
                    result = "HH24";
                    break;
                case DAY:
                    result = "DD";
                    break;
                case MONTH:
                    result = "MM";
                    break;
                default:
                    result = null;
                    break;
            }
        }
        return result;
    }

    @Override
    public String getLimitToOneRow() {

        return " AND ROWNUM =1 ";
    }

    @Override
    public String getIntervalPattern(final int factor, final String unityTime, final String multiplier) {
        String intervalPattern = " (INTERVAL '" + factor + "' " + unityTime + ") ";
        if (multiplier != null) {
            intervalPattern = intervalPattern + " * " + multiplier + " ";
        }

        return intervalPattern;
    }

    @Override
    public String queryAssemblySampledDates(final String string, final int factor, final String name,
            final String tableName) {
        String query = "SELECT    ( ? )  input    FROM DUAL ";
        String multiplier = "  (level - 1)";
        String intervalMultiplier = this.getIntervalPattern(factor, name, multiplier);
        String interval = this.getIntervalPattern(factor, name, null);
        String result = "SELECT ( input + " + intervalMultiplier + ") dateEchantillon  FROM ( " + query + " )"
                + " CONNECT BY ( input + " + intervalMultiplier + ") BETWEEN  ? and ( ? - " + interval + ")";

        return result;
    }
}

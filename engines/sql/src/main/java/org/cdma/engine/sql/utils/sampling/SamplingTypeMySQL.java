/*******************************************************************************
 * Copyright (c) 2008 - ANSTO/Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * Norman Xiong (nxi@Bragg Institute) - initial API and implementation
 * Tony Lam (nxi@Bragg Institute) - initial API and implementation
 * Majid Ounsy (SOLEIL Synchrotron) - API v2 design and conception
 * Stephane Poirier (SOLEIL Synchrotron) - API v2 design and conception
 * Clement Rodriguez (ALTEN for SOLEIL Synchrotron) - API evolution
 * Gregory VIGUIER (SOLEIL Synchrotron) - API evolution
 * Raphael GIRARDOT (SOLEIL Synchrotron) - API evolution
 ******************************************************************************/
package org.cdma.engine.sql.utils.sampling;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.cdma.engine.sql.utils.SamplingType;

public enum SamplingTypeMySQL implements SamplingType {

    MONTH("%Y-"), DAY("%Y-%m-"), HOUR("%Y-%m-%d "), MINUTE("%Y-%m-%d %H:"), SECOND("%Y-%m-%d %H:%i:"), FRACTIONAL(
            "%Y-%m-%d %H:%i:%s."), NONE("%Y-%m-%d %H:%i:%s.%f");

    private String mSampling;

    private static final Map<String, String> MATCHING;
    static {
        synchronized (SamplingTypeMySQL.class) {
            MATCHING = new HashMap<String, String>();
            MATCHING.put("yyyy", "%Y");
            MATCHING.put("MM", "%m");
            MATCHING.put("dd", "%d");
            MATCHING.put("HH", "%H");
            MATCHING.put("mm", "%i");
            MATCHING.put("ss", "%s");
            MATCHING.put("SSS", "%f");
        }
    }

    private SamplingTypeMySQL(final String sampling) {
        mSampling = sampling;
    }

    @Override
    public String getPattern(final SamplingPeriod period) {
        String result = SamplingTypeMySQL.valueOf(period.name()).mSampling;

        for (Entry<String, String> entry : MATCHING.entrySet()) {
            result = result.replace(entry.getValue(), entry.getKey());
        }

        return result;
    }

    @Override
    public SamplingType getType(final SamplingPeriod time) {
        SamplingType result = SamplingTypeMySQL.valueOf(time.name());
        return result;
    }

    @Override
    public String getSQLRepresentation(final SimpleDateFormat format) {
        String result = format.toPattern();

        for (Entry<String, String> entry : MATCHING.entrySet()) {
            result = result.replace(entry.getKey(), entry.getValue());
        }

        return result;
    }

    @Override
    public String getSQLRepresentation() {
        return mSampling;
    }

    @Override
    public String getPatternPeriodUnit(final SamplingPeriod period) {
        String result = getSamplingPeriodUnit(period);

        for (Entry<String, String> entry : MATCHING.entrySet()) {
            result = result.replace(entry.getValue(), entry.getKey());
        }

        return result;
    }

    @Override
    public String getSamplingPeriodUnit(final SamplingPeriod period) {
        String result;
        if (period == null) {
            result = null;
        } else {
            switch (period) {
                case FRACTION:
                    result = "%f";
                    break;
                case SECOND:
                    result = "%s";
                case MINUTE:
                    result = "%i";
                    break;
                case HOUR:
                    result = "%H";
                    break;
                case DAY:
                    result = "%d";
                    break;
                case MONTH:
                    result = "%m";
                    break;
                default:
                    result = null;
                    break;
            }
        }
        return result;
    }

    @Override
    public String getLimitToOneRow() {
        return "  LIMIT 1 ";
    }

    @Override
    public String getIntervalPattern(final int factor, final String unityTime, final String multiplier) {
        String intervalPattern = " INTERVAL " + factor + " ";
        if (multiplier != null) {
            intervalPattern = intervalPattern + " * " + multiplier + " ";
        }

        intervalPattern = intervalPattern + unityTime + " ";
        return intervalPattern;
    }

    @Override
    public String queryAssemblySampledDates(final String string, final int factor, final String name,
            final String tableName) {
        String query = "SELECT   @i :=  ? ";
        String multiplier = "  @i ";
        String intervalMultiplier = this.getIntervalPattern(factor, name, multiplier);
        String interval = this.getIntervalPattern(factor, name, null);
        String result = "SELECT CAST( @i := @i + " + intervalMultiplier + " as DATETIME) dateEchantillon  FROM "
                + tableName + " , ( " + query + " ) as I" + " WHERE CAST( @input as DATETIME)  BETWEEN  ? and ( ? - "
                + interval + ")";

        return result;
    }
}

/*******************************************************************************
 * Copyright (c) 2008 - ANSTO/Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * Norman Xiong (nxi@Bragg Institute) - initial API and implementation
 * Tony Lam (nxi@Bragg Institute) - initial API and implementation
 * Majid Ounsy (SOLEIL Synchrotron) - API v2 design and conception
 * Stephane Poirier (SOLEIL Synchrotron) - API v2 design and conception
 * Clement Rodriguez (ALTEN for SOLEIL Synchrotron) - API evolution
 * Gregory VIGUIER (SOLEIL Synchrotron) - API evolution
 * Raphael GIRARDOT (SOLEIL Synchrotron) - API evolution
 ******************************************************************************/
package org.cdma.engine.hdf;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.Random;

import org.cdma.engine.hdf.array.HdfArray;
import org.cdma.engine.hdf.navigation.HdfAttribute;
import org.cdma.engine.hdf.navigation.HdfDataItem;
import org.cdma.engine.hdf.navigation.HdfDataset;
import org.cdma.engine.hdf.navigation.HdfGroup;
import org.cdma.exception.InvalidArrayTypeException;
import org.cdma.interfaces.IArray;
import org.cdma.interfaces.IAttribute;
import org.cdma.interfaces.IDataItem;
import org.cdma.interfaces.IGroup;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import fr.soleil.lib.project.date.DateUtil;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class WriteTests {

    public static final File FIRST_FILE_TO_WRITE = new File("C:/temp/testWriteFromScratch.nxs");
    public static final File SECOND_FILE_TO_WRITE = new File("C:/temp/testCopyIntoNewFile.nxs");
    public static final File WRITESLICES = new File("C:/temp/testWriteSlices.nxs");

    private static final String FACTORY_NAME = "HDF";

    private HdfArray createRandom1DArray(final int arrayLength) {
        HdfArray result = null;

        double[] values = new double[arrayLength];
        for (int i = 0; i < values.length; i++) {
            values[i] = Math.random() * 1000;
        }
        int[] shape = { 1, arrayLength };

        try {
            result = new HdfArray(FACTORY_NAME, values, shape);
        } catch (InvalidArrayTypeException e) {
            e.printStackTrace();
        }
        return result;
    }

    private HdfArray createRandom1DIntArray(final int arrayLength) {
        HdfArray result = null;

        int[] values = new int[arrayLength];
        for (int i = 0; i < values.length; i++) {
            values[i] = new Random().nextInt(arrayLength);
        }
        int[] shape = { 1, arrayLength };

        try {
            result = new HdfArray(FACTORY_NAME, values, shape);
        } catch (InvalidArrayTypeException e) {
            e.printStackTrace();
        }
        return result;
    }

    private HdfArray createRandomString1DArray() {
        HdfArray result = null;

        String[] values = new String[1];
        values[0] = "blééééééé";
        int[] shape = { 1 };

        try {
            result = new HdfArray(FACTORY_NAME, values, shape);
        } catch (InvalidArrayTypeException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return result;
    }

//    private double[] createImages(final int xarrayLength, final int yarrayLength, final int offset) {
//        double[] values = new double[yarrayLength * xarrayLength];
//        int index = 0;
//        for (int i = 0; i < yarrayLength; i++) {
//            for (int j = 0; j < xarrayLength; j++) {
//                values[index] = index++ + offset;
//            }
//        }
//        return values;
//    }
//
//    private double[][][] createImages2D(final int xarrayLength, final int yarrayLength, final int offset) {
//        double[][][] values = new double[1][yarrayLength][xarrayLength];
//        double[][] image = values[0];
//        int index = 0;
//        for (int i = 0; i < yarrayLength; i++) {
//            for (int j = 0; j < xarrayLength; j++) {
//                image[i][j] = index++ + offset;
//            }
//        }
//        return values;
//    }

    @Test
    public void dTestModifyInExistingFile() throws Exception {
        System.out.println("--------------------------------------------------");
        System.out.println("Test: Modify existing file");
        System.out.println(" - Modify group1/data1 values to [0,1,2,3]");
        long time = System.currentTimeMillis();
        HdfDataset dataset = new HdfDataset(FACTORY_NAME, FIRST_FILE_TO_WRITE, true);

        HdfGroup root = dataset.getRootGroup();

        HdfGroup group1 = root.getGroup("group1");

        // Modifiy dataItem values
        IDataItem dataItem = group1.getDataItem("data1Bis");
        assertNotNull(dataItem);
        int[] values = { 0, 1, 2, 3 };
        int[] shape = { 1, 4 };
        IArray newArray = new HdfArray(FACTORY_NAME, values, shape);
        dataItem.setCachedData(newArray, false);

        assertEquals(int.class, newArray.getElementType());
        // assertArrayEquals(shape, newArray.getShape());

        // Modify attribute
        HdfGroup group2 = root.getGroup("group2");
        IDataItem item2 = group2.getDataItem("data2");
        IAttribute attr1 = item2.getAttribute("attr1");
        attr1.setStringValue("attr1modifié");
        dataset.save();

        dataset.close();
        System.out.println(DateUtil.elapsedTimeToStringBuilder(new StringBuilder("End of test: Modify existing file "),
                System.currentTimeMillis() - time));
        System.out.println("--------------------------------------------------");
    }

//
    // @Test
    // public void eTestWriteMultiIntoNewFile() throws Exception {
    // System.out.println("--------------------------------------------------");
    // System.out.println("Test: Copy existing dataset into new file and add new group & dataitem");
    // if (FIRST_FILE_TO_WRITE.exists()) {
    // if (!FIRST_FILE_TO_WRITE.delete()) {
    // System.out.println("Cannot delete file: missing close() ??");
    // System.exit(0);
    // }
    // }
    //
    // HdfDataset dataset = new HdfDataset(FACTORY_NAME, FIRST_FILE_TO_WRITE);
    //
    // // Create group with name group3 under root node and save it
    // HdfGroup root = (HdfGroup) dataset.getRootGroup();
    // IGroup group3 = new HdfGroup(FACTORY_NAME, "group3", "/", root, dataset);
    // root.addSubgroup(group3);
    // dataset.save();
    //
    // // Image are 20x10
    // int xLength = 20;
    // int yLength = 10;
    //
    // // We have 3 random images
    // double[] image1 = createImages(xLength, yLength, 0);
    // double[] image2 = createImages(xLength, yLength, 400);
    // double[] image3 = createImages(xLength, yLength, 800);
    //
    // // So shape is:
    // int[] shape = new int[] { 3, yLength, xLength };
    //
    // // We create a DataItem under group3
    // HdfDataItem dataItem = new HdfDataItem(FACTORY_NAME, dataset.getH5File(), group3, "imageStack", shape,
    // double.class);
    //
    // // Now we have to tune the underlying HDF item
    // H5ScalarDS h5Item = dataItem.getH5DataItem();
    // long[] selectedDims = h5Item.getSelectedDims();
    // long[] startDims = h5Item.getStartDims();
    //
    // // We have to give to HDF the non-reduced shape of the slabs we are going to put in the dataitem
    // selectedDims[0] = 1;
    // selectedDims[1] = yLength;
    // selectedDims[2] = xLength;
    //
    // // We have to modify the startDims because HDF cannot guess where to put the slab
    // // First image is at index 0 on the first dimension of our 3 dimension hyperslab
    // startDims[0] = 0; // optional, this is the default value
    // dataItem.getH5DataItem().write(image1);
    //
    // // For the next image, we want start at index 1 on the first dimension of the hyperslab
    // startDims[0] = 1;
    // dataItem.getH5DataItem().write(image2);
    //
    // // For the next image, we want start at index 2 on the first dimension of the hyperslab
    // startDims[0] = 2;
    // dataItem.getH5DataItem().write(image3);
    //
    // dataset.save();
    // dataset.close();
    // }
    @Test
    public void cTestWriteIntoNewFile() throws Exception {
        System.out.println("--------------------------------------------------");
        System.out.println("Test: Copy existing dataset into new file and add new group & dataitem");
        if (SECOND_FILE_TO_WRITE.exists()) {
            if (!SECOND_FILE_TO_WRITE.delete()) {
                System.out.println("Cannot delete file: missing close() ??");
            }
        }
        long time = System.currentTimeMillis();

        HdfDataset dataset = new HdfDataset(FACTORY_NAME, FIRST_FILE_TO_WRITE, true);

        HdfGroup root = dataset.getRootGroup();
        IGroup group3 = new HdfGroup(FACTORY_NAME, "group3", /*"/",*/ root, dataset);

        HdfDataItem dataItem = new HdfDataItem(FACTORY_NAME, "data3");

        HdfArray array = createRandom1DArray(10);
        dataItem.setCachedData(array, false);
        group3.addDataItem(dataItem);
        root.addSubgroup(group3);

        assertEquals(3, root.getGroupList().size());

        dataset.saveTo(SECOND_FILE_TO_WRITE.getAbsolutePath());

        dataset.close();
        System.out.println(DateUtil.elapsedTimeToStringBuilder(
                new StringBuilder("End of test: Copy existing dataset into new file "),
                System.currentTimeMillis() - time));
        System.out.println("--------------------------------------------------");
    }

    @Test
    public void bTestWriteIntoExistingFile() throws Exception {
        System.out.println("--------------------------------------------------");
        System.out.println("Test: Write into a the previous file");
        long time = System.currentTimeMillis();

        // Create a dataset in _Append_ mode
        HdfDataset dataset = new HdfDataset(FACTORY_NAME, FIRST_FILE_TO_WRITE, true);

        HdfGroup root = dataset.getRootGroup();
        IGroup group2 = new HdfGroup(FACTORY_NAME, "group2", /*"/",*/ root, dataset);

        HdfDataItem dataItem = new HdfDataItem(FACTORY_NAME, "data2");
        dataItem.addStringAttribute("attr1", "mon attribut");
        dataItem.addOneAttribute(new HdfAttribute(FACTORY_NAME, "attr2", 5));

        HdfArray array = createRandom1DArray(10);
        dataItem.setCachedData(array, false);
        group2.addDataItem(dataItem);
        root.addSubgroup(group2);

        assertEquals(2, root.getGroupList().size());

        // Test if we can acces to previously written group1
        HdfGroup group1 = root.getGroup("group1");
        assertNotNull(group1);
        group1.addStringAttribute("attr100", "mon attribut sauvé ensuite");
        HdfDataItem data1 = group1.getDataItem("data1");
        assertNotNull(data1);
        data1.addStringAttribute("attr100", "mon attribut sauvé ensuite");

        dataset.save();
        dataset.close();
        System.out.println(DateUtil.elapsedTimeToStringBuilder(
                new StringBuilder("End of test: Write into the previous file "), System.currentTimeMillis() - time));
        System.out.println("--------------------------------------------------");
    }

    @Test
    public void eWriteSlideData() throws Exception {
        long time = System.currentTimeMillis();

        if (WRITESLICES.exists()) {
            if (!WRITESLICES.delete()) {
                System.out.println("Cannot delete file: missing close() ??");
            }
        }
        HdfDataset dataset = new HdfDataset(FACTORY_NAME, WRITESLICES, true);
        HdfGroup root = dataset.getRootGroup();

        // Test Sub Group
        IGroup group = new HdfGroup(FACTORY_NAME, "group1", /*"/",*/ root, dataset);
        root.addSubgroup(group);

        // Creation de l'item qui va porter la pile.
        HdfDataItem dataitem = new HdfDataItem(FACTORY_NAME, "imageStack");
        dataitem.startPartialWriteMode();
        group.addDataItem(dataitem);

        // Je construit mes données pour le test
        // image 1 (3 lignes de 4 valeurs)
        int[] image1 = { 1, 2, 3, 4, 5, 1, 2, 3, 4, 5, 1, 2, 3, 4, 5 };
        // image 2 (3 lignes de 4 valeurs)
        int[] image2 = { 6, 7, 8, 9, 10, 6, 7, 8, 9, 10, 6, 7, 8, 9, 10 };
        // image 3 (3 lignes de 4 valeurs)
        int[] image3 = { 11, 12, 13, 14, 15, 11, 12, 13, 14, 15, 11, 12, 13, 14, 15 };
        // image 4 n'existe pas encore

        // Preparation d'un tableau virtuel vide dont la taille est celle de la pile
        // -> array de 4x3x5
        int[] fullShape = new int[] { 4, 3, 5 };
        HdfArray emptyArray = new HdfArray(FACTORY_NAME, Integer.TYPE, fullShape);
        // Il faut le sauver pour preparer HDF5 à un item de cette taille.
        dataitem.setCachedData(emptyArray, false);
        dataset.save();

        // Preparation des HDf Array en décalant l'origine à chaque fois
        int[] imageShape = new int[] { 3, 5 };
        HdfArray image1Array = new HdfArray(FACTORY_NAME, image1, imageShape);
        int[] image2Origin = new int[] { 1, 0, 0 };
        HdfArray image2Array = new HdfArray(FACTORY_NAME, image2, imageShape, image2Origin);
        int[] image3Origin = new int[] { 2, 0, 0 };
        HdfArray image3Array = new HdfArray(FACTORY_NAME, image3, imageShape, image3Origin);

        // Ensuite on sauve image par image
        dataitem.setCachedData(image1Array, false);
        dataset.save();

        dataitem.setCachedData(image2Array, false);
        dataset.save();

        dataitem.setCachedData(image3Array, false);
        dataset.save();
        System.out.println(DateUtil.elapsedTimeToStringBuilder(new StringBuilder("End write slices "),
                System.currentTimeMillis() - time));
    }

    @Test
    public void aTestWriteFromScratch() throws Exception {
        System.out.println("--------------------------------------------------");
        System.out.println("Test: Write into a new file");
        long time = System.currentTimeMillis();

        if (FIRST_FILE_TO_WRITE.exists()) {
            if (!FIRST_FILE_TO_WRITE.delete()) {
                System.out.println("Cannot delete file: missing close() ??");
            }
        }
        HdfDataset dataset = new HdfDataset(FACTORY_NAME, FIRST_FILE_TO_WRITE, true);

        // Test Root Group
        HdfGroup root = dataset.getRootGroup();
        assertNotNull(root);
        assertTrue(root.isRoot());

        // Test Sub Group
        IGroup group = new HdfGroup(FACTORY_NAME, "group1", /*"/",*/ root, dataset);
        root.addSubgroup(group);
        group.addStringAttribute("attr1", "mon attribut");
        group.addOneAttribute(new HdfAttribute(FACTORY_NAME, "attr2", 5));
        assertEquals("Attribute List size", 2, group.getAttributeList().size());
        assertEquals("group1", group.getShortName());
        assertEquals("/group1", group.getName());
        assertEquals(root, group.getRootGroup());
        assertEquals(dataset, group.getDataset());
        assertTrue(group.isEntry());
        assertFalse(group.isRoot());

        // Test Data Item
        HdfDataItem dataItem = new HdfDataItem(FACTORY_NAME, "data1");
        dataItem.addStringAttribute("itemAttr", "1");
        // XXX DEBUG
        group.addDataItem(dataItem);
        HdfArray array = createRandom1DArray(10);
        dataItem.setCachedData(array, false);
        assertEquals(double.class, array.getElementType());
        assertEquals(root, dataItem.getRootGroup());
        assertEquals(group, dataItem.getParentGroup());
        assertEquals(dataset, dataItem.getDataset());
        assertEquals("/group1/data1", dataItem.getName());
        assertEquals("data1", dataItem.getShortName());

        // Test Data Item bis
        HdfDataItem dataItemBis = new HdfDataItem(FACTORY_NAME, "data1Bis");
        dataItemBis.addStringAttribute("itemAttr", "1");
        // XXX DEBUG
        group.addDataItem(dataItemBis);
        HdfArray arrayBis = createRandom1DIntArray(4);
        dataItemBis.setCachedData(arrayBis, false);
        assertEquals(int.class, arrayBis.getElementType());
        assertEquals(root, dataItemBis.getRootGroup());
        assertEquals(group, dataItemBis.getParentGroup());
        assertEquals(dataset, dataItemBis.getDataset());
        assertEquals("/group1/data1Bis", dataItemBis.getName());
        assertEquals("data1Bis", dataItemBis.getShortName());

        // Test String Data Item
        HdfDataItem stringDataItem = new HdfDataItem(FACTORY_NAME, "stringData");
        group.addDataItem(stringDataItem);
        HdfArray stringArray = createRandomString1DArray();
        stringDataItem.setCachedData(stringArray, false);

        dataset.save();

        HdfDataItem linkdataItem = new HdfDataItem(FACTORY_NAME, "testLink");
        linkdataItem.addStringAttribute("linkAttr", "1");
        group.addDataItem(linkdataItem);
        linkdataItem.linkTo(dataItem);

        HdfDataItem linkdataItemBis = new HdfDataItem(FACTORY_NAME, "testLinkBis");
        linkdataItemBis.addStringAttribute("linkAttr", "1");
        group.addDataItem(linkdataItemBis);
        linkdataItemBis.linkTo(dataItemBis);

        dataset.save();

        System.out.println(DateUtil.elapsedTimeToStringBuilder(new StringBuilder("End of test: Write into a new file "),
                System.currentTimeMillis() - time));
        System.out.println("--------------------------------------------------");
    }

}

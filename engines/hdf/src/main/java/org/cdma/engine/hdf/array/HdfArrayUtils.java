package org.cdma.engine.hdf.array;

import java.util.List;

import org.cdma.arrays.DefaultArrayUtils;
import org.cdma.exception.InvalidRangeException;
import org.cdma.exception.ShapeNotMatchException;
import org.cdma.interfaces.IArray;
import org.cdma.interfaces.IRange;
import org.cdma.utils.IArrayUtils;

public class HdfArrayUtils extends DefaultArrayUtils {

    public HdfArrayUtils(HdfArray array) {
        super(array);
    }

    @Override
    public HdfArray getArray() {
        return (HdfArray) super.getArray();
    }

    @Override
    public HdfArrayUtils flip(int dim) {
        return (HdfArrayUtils) super.flip(dim);
    }

    @Override
    public HdfArrayUtils permute(int[] dims) {
        return (HdfArrayUtils) super.permute(dims);
    }

    @Override
    public HdfArrayUtils transpose(int dim1, int dim2) {
        return (HdfArrayUtils) super.transpose(dim1, dim2);
    }

    @Override
    protected HdfIndex generateIndex(String factoryName, int[] shape, int[] origin) {
        return new HdfIndex(factoryName, shape, origin, shape);
    }

    @Override
    public IArrayUtils createArrayUtils(IArray array) {
        IArrayUtils arrayUtils;
        if (array instanceof HdfArray) {
            arrayUtils = new HdfArrayUtils((HdfArray) array);
        } else {
            arrayUtils = super.createArrayUtils(array);
        }
        return arrayUtils;
    }

    @Override
    public HdfArrayUtils reduce() {
        return (HdfArrayUtils) super.reduce();
    }

    @Override
    public HdfArrayUtils reduce(int dim) {
        return (HdfArrayUtils) super.reduce(dim);
    }

    @Override
    public HdfArrayUtils slice(int dim, int value) {
        return (HdfArrayUtils) super.slice(dim, value);
    }

    @Override
    public HdfArrayUtils reduceTo(int rank) {
        return (HdfArrayUtils) super.reduceTo(rank);
    }

    @Override
    public HdfArrayUtils reshape(int[] shape) throws ShapeNotMatchException {
        return (HdfArrayUtils) super.reshape(shape);
    }

    @Override
    public HdfArrayUtils section(int[] origin, int[] shape) throws InvalidRangeException {
        return (HdfArrayUtils) super.section(origin, shape);
    }

    @Override
    public HdfArrayUtils section(int[] origin, int[] shape, long[] stride) throws InvalidRangeException {
        return (HdfArrayUtils) super.section(origin, shape, stride);
    }

    @Override
    public HdfArrayUtils sectionNoReduce(int[] origin, int[] shape, long[] stride) throws InvalidRangeException {
        return (HdfArrayUtils) super.sectionNoReduce(origin, shape, stride);
    }

    @Override
    public HdfArrayUtils sectionNoReduce(List<IRange> ranges) throws InvalidRangeException {
        return (HdfArrayUtils) super.sectionNoReduce(ranges);
    }

    @Override
    public HdfArrayUtils eltAnd(IArray booleanMap) throws ShapeNotMatchException {
        return (HdfArrayUtils) super.eltAnd(booleanMap);
    }

    @Override
    public HdfArrayUtils enclosedIntegrateDimension(int dimension, boolean isVariance) throws ShapeNotMatchException {
        return (HdfArrayUtils) super.enclosedIntegrateDimension(dimension, isVariance);
    }

    @Override
    public HdfArrayUtils integrateDimension(int dimension, boolean isVariance) throws ShapeNotMatchException {
        return (HdfArrayUtils) super.integrateDimension(dimension, isVariance);
    }
}

/*******************************************************************************
 * Copyright (c) 2008 - ANSTO/Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * Norman Xiong (nxi@Bragg Institute) - initial API and implementation
 * Tony Lam (nxi@Bragg Institute) - initial API and implementation
 * Majid Ounsy (SOLEIL Synchrotron) - API v2 design and conception
 * Stephane Poirier (SOLEIL Synchrotron) - API v2 design and conception
 * Clement Rodriguez (ALTEN for SOLEIL Synchrotron) - API evolution
 * Gregory VIGUIER (SOLEIL Synchrotron) - API evolution
 * Raphael GIRARDOT (SOLEIL Synchrotron) - API evolution
 ******************************************************************************/
package org.cdma.engine.hdf.navigation;

import java.io.File;

import javax.swing.tree.DefaultMutableTreeNode;

import org.cdma.Factory;
import org.cdma.dictionary.LogicalGroup;
import org.cdma.engine.hdf.utils.HdfObjectUtils;
import org.cdma.exception.DataAccessException;
import org.cdma.exception.NotImplementedException;
import org.cdma.exception.WriterException;
import org.cdma.interfaces.IAttribute;
import org.cdma.interfaces.IContainer;
import org.cdma.interfaces.IDataset;

import fr.soleil.lib.project.ObjectUtils;
import ncsa.hdf.object.FileFormat;
import ncsa.hdf.object.h5.H5File;
import ncsa.hdf.object.h5.H5Group;

public class HdfDataset implements IDataset, Cloneable {

    private static final String TO_STRING_PREFIX = "Dataset = ";

    private final String factoryName;
    private String hdfFileName;
    private H5File h5File;
    private String title;
    private HdfGroup root;
    private final int openFlag;
    private boolean dead;

    public HdfDataset(final String factoryName, final File hdfFile) throws DataAccessException {
        this(factoryName, hdfFile, false);
    }

    public HdfDataset(final String factoryName, final File hdfFile, final boolean withWriteAccess)
            throws DataAccessException {
        this.factoryName = factoryName;
        this.hdfFileName = hdfFile.getAbsolutePath();
        this.title = hdfFile.getName();
        // XXX Exception should be treated and/or logged in upper levels
//        try {
        if (withWriteAccess) {
            if (hdfFile.exists()) {
                openFlag = H5File.WRITE;
            } else {
                openFlag = H5File.CREATE;
            }
        } else {
            openFlag = H5File.READ;
        }
        dead = !HdfObjectUtils.HDF_API_READY;
        initHdfFile();
//        } catch (Exception e) {
//            Factory.getLogger().severe(e.getMessage());
//            throw e;
//        }
    }

    public int getOpenFlag() {
        return openFlag;
    }

    private void initHdfFile() throws DataAccessException {
        if (hdfFileName != null) {
            try {
                h5File = new H5File(hdfFileName, openFlag);
                h5File.setMaxMembers(-1);
                h5File.open();
            } catch (Exception e) {
                throw HdfObjectUtils.toDataAccessException(e);
            }
        }
    }

    @Override
    public String getFactoryName() {
        return factoryName;
    }

    @Override
    public HdfGroup getRootGroup() {
        HdfGroup group;
        if (root == null) {
            if (h5File == null) {
                group = root;
            } else {
                boolean openOk;
                try {
                    openOk = true;
                } catch (Exception e) {
                    Factory.getLogger().warn(e.getMessage());
//                    e.printStackTrace();
                    openOk = false;
                }
                if (openOk) {
                    DefaultMutableTreeNode theRoot = (DefaultMutableTreeNode) h5File.getRootNode();
                    if (theRoot != null) {
                        Object userObject = theRoot.getUserObject();
                        if (userObject instanceof H5Group) {
                            H5Group rootObject = (H5Group) userObject;
                            root = new HdfGroup(factoryName, rootObject, (HdfGroup) null, this);
                        }
                    }
                    group = root;
                } else {
                    group = null;
                }
            }
        } else {
            group = root;
        }
        return group;
    }

    public void setRoot(HdfGroup root) {
        this.root = root;
    }

    @Override
    public LogicalGroup getLogicalRoot(String view) {
        return new LogicalGroup(view, null, this);
    }

    @Override
    public void setLocation(final String location) {
        if (!ObjectUtils.sameObject(hdfFileName, location)) {
            hdfFileName = location;
            try {
                close();
            } catch (DataAccessException e) {
                Factory.getLogger().warn(e.getMessage(), e);
            } finally {
                h5File = null;
            }
            try {
                open();
            } catch (DataAccessException e) {
                Factory.getLogger().error(e.getMessage());
            }
        }
    }

    @Override
    public String getLocation() {
        return hdfFileName;
    }

    public H5File getH5File() {
        return h5File;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public void setTitle(final String title) {
        this.title = title;
    }

    @Override
    public long getLastModificationDate() {
        return new File(hdfFileName).lastModified();
    }

    @Override
    public void open() throws DataAccessException {
        try {
            if (h5File == null) {
                initHdfFile();
                // open() is done in initHdfFile()
            } else {
                h5File.open();
            }
        } catch (Exception e) {
            if (e instanceof DataAccessException) {
                throw (DataAccessException) e;
            } else {
                throw new DataAccessException("Failed to open " + hdfFileName, e);
            }
        }
    }

    @Override
    public void close() throws DataAccessException {
        try {
            if (h5File != null) {
                try {
                    h5File.close();
                } finally {
                    if (root == null) {
                        // Studying PROBLEM-1682, tests proved that HdfDataset should be considered as dead when
                        // closed with no root. Otherwise:
                        // - If always dead when closed, ImageReducer won't work.
                        // - If not dead when closed, QuickExafsViewer won't work.
                        // But if dead when closed and null root, both applications will work.
                        dead = true;
                        h5File = null;
                    }
                }
            }
        } catch (Exception e) {
            if (e instanceof DataAccessException) {
                throw (DataAccessException) e;
            } else {
                throw new DataAccessException(e);
            }
        }
    }

    @Override
    public boolean isOpen() {
        // Always consider as closed when file id is < 0
        return (h5File != null) && (h5File.getFID() > -1);
    }

    @Override
    public boolean isDead() {
        return dead;
    }

    @Override
    public boolean sync() throws DataAccessException {
        throw new NotImplementedException();
    }

    public static boolean checkHdfAPI() {
        return true;
    }

    @Override
    public void save() throws WriterException {
        HdfGroup root = getRootGroup();
        // recursive save
        try {
            root.save(h5File, null);
        } catch (Exception e) {
            throw new WriterException(e);
        }
    }

    @Override
    public void saveTo(final String location) throws WriterException {
        try {
            File newFile = new File(location);
            if (newFile.exists()) {
                newFile.delete();
            }

            H5File fileToWrite = new H5File(location, FileFormat.CREATE);
            fileToWrite.open();

            HdfGroup root = getRootGroup();
            // recursive save
            root.save(fileToWrite, null);
            fileToWrite.close();
        } catch (Exception e) {
            Factory.getLogger().error(e.getMessage());
            throw new WriterException(e);
        }
    }

    @Override
    public void save(final IContainer container) throws WriterException {
        throw new NotImplementedException();
    }

    @Override
    public void save(final String parentPath, final IAttribute attribute) throws WriterException {
        throw new NotImplementedException();
    }

    @Override
    public String toString() {
        StringBuilder buffer = new StringBuilder(TO_STRING_PREFIX);
        buffer.append(getLocation());
        return buffer.toString();
    }

    @Override
    protected void finalize() throws Throwable {
        try {
            close();
        } catch (Exception e) {
            // just ignore: we only try to clean
        }
        super.finalize();
    }
}

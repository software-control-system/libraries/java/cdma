package org.cdma.engine.hdf.array;

import org.cdma.arrays.DefaultArrayIterator;

public class HdfArrayIterator extends DefaultArrayIterator {

    public HdfArrayIterator(HdfArray array, HdfIndex index) {
        super(array, index);
    }

    public HdfArrayIterator(HdfArray array) {
        super(array);
    }

    public HdfArrayIterator(HdfIndex index) {
        super(index);
    }

}

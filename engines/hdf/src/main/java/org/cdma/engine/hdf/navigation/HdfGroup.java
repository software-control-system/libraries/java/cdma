/*******************************************************************************
 * Copyright (c) 2008 - ANSTO/Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * Norman Xiong (nxi@Bragg Institute) - initial API and implementation
 * Tony Lam (nxi@Bragg Institute) - initial API and implementation
 * Majid Ounsy (SOLEIL Synchrotron) - API v2 design and conception
 * Stephane Poirier (SOLEIL Synchrotron) - API v2 design and conception
 * Clement Rodriguez (ALTEN for SOLEIL Synchrotron) - API evolution
 * Gregory VIGUIER (SOLEIL Synchrotron) - API evolution
 * Raphael GIRARDOT (SOLEIL Synchrotron) - API evolution
 ******************************************************************************/
package org.cdma.engine.hdf.navigation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.swing.tree.DefaultMutableTreeNode;

import org.cdma.Factory;
import org.cdma.dictionary.Path;
import org.cdma.engine.hdf.utils.HdfNode;
import org.cdma.engine.hdf.utils.HdfObjectUtils;
import org.cdma.engine.hdf.utils.HdfPath;
import org.cdma.exception.DataAccessException;
import org.cdma.exception.NoResultException;
import org.cdma.exception.NotImplementedException;
import org.cdma.exception.SignalNotAvailableException;
import org.cdma.interfaces.IAttribute;
import org.cdma.interfaces.IContainer;
import org.cdma.interfaces.IDataItem;
import org.cdma.interfaces.IDimension;
import org.cdma.interfaces.IGroup;
import org.cdma.interfaces.INode;
import org.cdma.utils.Utilities.ModelType;

import ncsa.hdf.hdf5lib.exceptions.HDF5Exception;
import ncsa.hdf.object.Attribute;
import ncsa.hdf.object.Group;
import ncsa.hdf.object.HObject;
import ncsa.hdf.object.Metadata;
import ncsa.hdf.object.h5.H5CompoundDS;
import ncsa.hdf.object.h5.H5File;
import ncsa.hdf.object.h5.H5Group;
import ncsa.hdf.object.h5.H5ScalarDS;

public class HdfGroup extends AHdfContainer implements IGroup {

    private static final String GROUP = "Group ";
    private static final String PARENT = " (parent = ";
    private static final String NOBODY = "Nobody";
    private static final String STRING_END = ")";

    private HdfGroup root;
    private Map<String, HdfGroup> groupMap;
    private Map<String, HdfDataItem> itemMap;
    private Map<String, HdfAttribute> attributeMap;
    private String originalName;
    private String name;
    private HdfDataset dataset;
    private boolean initiated;
    private H5Group h5Group;
    // Add H5CompoundDS management
    private H5CompoundDS h5Compound;

    private HdfGroup(final String factoryName, final HdfGroup parent, final HdfDataset dataset, String shortName) {
        super(factoryName, parent);
        this.groupMap = new LinkedHashMap<>();
        this.itemMap = new LinkedHashMap<>();
        this.attributeMap = new HashMap<>();
        this.root = null;
        this.initiated = false;
        this.dataset = dataset;
        this.shortName = shortName;
        this.originalName = getName();
    }

    public HdfGroup(final String factoryName, final String name, final HdfGroup parent, final HdfDataset dataset) {
        this(factoryName, parent, dataset, name);
        this.h5Group = new H5Group(dataset.getH5File(), name, originalName, null);
    }

    public HdfGroup(final String factoryName, final H5Group hdfGroup, final HdfGroup parent, final HdfDataset dataset) {
        this(factoryName, parent, dataset, hdfGroup == null ? null : hdfGroup.getName());
        this.h5Group = hdfGroup;
    }

    /**
     * Add a constructor take in account H5CompoundDS
     */
    public HdfGroup(final String factoryName, final H5CompoundDS hdfCompound, final HdfGroup parent,
            final HdfDataset dataset) {
        this(factoryName, parent, dataset, hdfCompound == null ? null : hdfCompound.getName());
        this.h5Compound = hdfCompound;
    }

    private void init() {
        if ((!initiated) && ((h5Group != null) || (h5Compound != null))) {
            // Take H5CompoundDS in account
            if (h5Group == null) {
                if (h5Compound != null) {
                    h5Compound.init();
                    String[] memberName = h5Compound == null ? null : h5Compound.getMemberNames();
                    if (memberName != null) {
                        for (int indexMember = 0; indexMember < memberName.length; indexMember++) {
                            HdfDataItem dataItem = new HdfDataItem(factoryName, this, h5Compound, indexMember);
                            itemMap.put(memberName[indexMember], dataItem);
                        }
                    }
                }
            } else {
                List<HObject> members = h5Group.getMemberList();
                if (members != null) {
                    for (HObject hObject : members) {
                        if (hObject instanceof H5ScalarDS) {
                            H5ScalarDS scalarDS = (H5ScalarDS) hObject;
                            HdfDataItem dataItem = new HdfDataItem(factoryName, this, scalarDS);
                            itemMap.put(scalarDS.getName(), dataItem);
                        } else if (hObject instanceof H5CompoundDS) {
                            // Take H5CompoundDS in account
                            H5CompoundDS compoundDS = (H5CompoundDS) hObject;
                            HdfGroup hdfGroup = new HdfGroup(factoryName, compoundDS, this, dataset);
                            groupMap.put(compoundDS.getName(), hdfGroup);
                        } else if (hObject instanceof H5Group) {
                            H5Group group = (H5Group) hObject;
                            HdfGroup hdfGroup = new HdfGroup(factoryName, group, this, dataset);
                            groupMap.put(group.getName(), hdfGroup);
                        }
                    }
                    try {
                        @SuppressWarnings("unchecked")
                        List<Metadata> metadata = h5Group.getMetadata();
                        for (Metadata meta : metadata) {
                            if (meta instanceof Attribute) {
                                Attribute attribute = (Attribute) meta;
                                HdfAttribute hdfAttr = new HdfAttribute(factoryName, attribute);
                                attributeMap.put(hdfAttr.getName(), hdfAttr);
                            }
                        }
                    } catch (HDF5Exception e) {
                        Factory.getLogger().error(e.getMessage());
                    }
                } // end if (members != null)
            } // end if (h5Group == null) ... else
            initiated = true;
        } // end if ((!initiated) && ((h5Group != null) || (h5Compound != null)))
    }

    @Override
    public HdfGroup clone() {
        HdfGroup clone = (HdfGroup) super.clone();
        if (clone == this) {
            clone = new HdfGroup(factoryName, shortName, parent, dataset);
        } else {
            // XXX don't set h5Group to null --> NullPointerException
//            clone.h5Group = null;
            clone.groupMap = new LinkedHashMap<>();
            clone.itemMap = new LinkedHashMap<>();
            clone.attributeMap = new HashMap<>();
            clone.root = null;
            clone.initiated = false;
        }
        return clone;
    }

    @Override
    public ModelType getModelType() {
        return ModelType.Group;
    }

    @Override
    public void addOneAttribute(final IAttribute attribute) {
        if (attribute instanceof HdfAttribute) {
            attributeMap.put(attribute.getName(), (HdfAttribute) attribute);
        }
    }

    @Override
    public IAttribute getAttribute(final String name) {
        init();
        IAttribute result = attributeMap.get(name);
        return result;
    }

    @Override
    public Collection<IAttribute> getAttributeList() {
        init();
        List<IAttribute> result = new ArrayList<>(attributeMap.values());
        return result;
    }

    @Override
    public String getName() {
        if (name == null) {
            name = super.getName();
        }
        return name;
    }

    @Override
    public boolean hasAttribute(final String name, final String value) {
        init();
        return attributeMap.containsKey(name);
    }

    @Override
    public boolean removeAttribute(final IAttribute a) {
        init();
        return attributeMap.remove(a.getName()) != null;
    }

    @Override
    public void setName(final String name) {
        init();
        addStringAttribute("long_name", name);
    }

    @Override
    public void setShortName(final String name) {
        try {
            super.setShortName(name);
            this.name = getParentGroup().getName() + HdfPath.PATH_SEPARATOR + name;
            for (IDataItem item : itemMap.values()) {
                item.setParent(this);
            }
        } catch (Exception e) {
            Factory.getLogger().error(e.getMessage());
        }
    }

    @Override
    public void setParent(final IGroup group) {
        try {
            parent = (HdfGroup) group;
        } catch (Exception e) {
            Factory.getLogger().error(e.getMessage());
        }
    }

    @Override
    public long getLastModificationDate() {
        long result = 0;
        getDataset().getLastModificationDate();
        return result;
    }

    @Override
    public void addDataItem(final IDataItem item) {
        if (item instanceof HdfDataItem) {
            HdfDataItem hdfDataItem = (HdfDataItem) item;
            hdfDataItem.setParent(this);
            itemMap.put(hdfDataItem.getShortName(), hdfDataItem);
        }
    }

    @Override
    public Map<String, String> harvestMetadata(final String mdStandard) throws DataAccessException {
        throw new NotImplementedException();
    }

    @Override
    public HdfGroup getRootGroup() {
        if (root == null) {
            root = getDataset().getRootGroup();
        }
        return root;
    }

    @Override
    public void addOneDimension(final IDimension dimension) {
    }

    @Override
    public void addSubgroup(final IGroup group) {
        if (group instanceof HdfGroup) {
            HdfGroup subGroup = (HdfGroup) group;
            init();
            subGroup.setParent(this);
            groupMap.put(subGroup.getShortName(), subGroup);
        }
    }

    @Override
    public HdfDataItem getDataItem(final String shortName) {
        init();
        HdfDataItem result = null;
        if (shortName != null) {
            result = itemMap.get(shortName);
        }
        return result;
    }

    @Override
    public IDataItem getDataItemWithAttribute(final String name, final String value) {
        init();
        IDataItem resItem = null;
        List<IDataItem> groups = getDataItemList();
        boolean found = false;
        for (Iterator<?> iter = groups.iterator(); iter.hasNext();) {
            resItem = (IDataItem) iter.next();
            if (resItem.hasAttribute(name, value)) {
                groups.clear();
                found = true;
                break;
            }
        }
        if (!found) {
            resItem = null;
        }
        return resItem;
    }

    @Override
    public IDimension getDimension(final String name) {
        return null;
    }

    @Override
    public AHdfContainer getContainer(final String shortName) {
        AHdfContainer container = null;
        init();
        if ((shortName != null) && shortName.isEmpty()) {
            container = this;
        } else {
            HdfGroup resultGroupItem = getGroup(shortName);
            if (resultGroupItem == null) {
                HdfDataItem resultVariableItem = getDataItem(shortName);
                if (resultVariableItem != null) {
                    container = resultVariableItem;
                }
            } else {
                container = resultGroupItem;
            }
        }
        return container;
    }

    @Override
    public HdfGroup getGroup(final String shortName) {
        init();
        HdfGroup result = null;
        if (shortName != null) {
            result = groupMap.get(shortName);
        }
        return result;
    }

    @Override
    public IGroup getGroupWithAttribute(final String attributeName, final String value) {
        // init() is called by getGroupList()
        List<IGroup> groups = getGroupList();
        IAttribute attr;
        IGroup result = null;
        for (IGroup group : groups) {
            attr = group.getAttribute(attributeName);
            if (attr.getStringValue().equals(value)) {
                result = group;
                break;
            }
        }

        return result;
    }

    @Override
    public List<IDataItem> getDataItemList() {
        init();
        List<IDataItem> result;
        result = new ArrayList<IDataItem>(itemMap.values());
        return result;
    }

    @Override
    public int getDataItemCount() {
        init();
        return itemMap.size();
    }

    @Override
    public HdfDataset getDataset() {
        if (dataset == null) {
            if (parent != null) {
                dataset = parent.getDataset();
            }
        }
        return dataset;
    }

    protected void updateFromGroupNoCheck(HdfGroup group) {
        groupMap.clear();
        groupMap.putAll(group.groupMap);
        itemMap.clear();
        itemMap.putAll(group.itemMap);
        attributeMap.clear();
        attributeMap.putAll(group.attributeMap);
        this.initiated = group.initiated;
        this.h5Group = group.h5Group;
        HdfGroup parent = getParentGroup();
        if (parent != null) {
            parent.groupMap.put(shortName, this);
        }
    }

    @Override
    public void changeDataset(HdfDataset hdfDataset) {
        if ((hdfDataset != null) && (hdfDataset != dataset)) {
            dataset = hdfDataset;
            if (parent != null) {
                parent.changeDataset(hdfDataset);
            }
            if (shortName != null) {
                if (parent == null) {
                    HdfGroup root = dataset.getRootGroup();
                    if ((root != null) && (root != this)) {
                        if (shortName.isEmpty() || HdfPath.PATH_SEPARATOR.equals(shortName)) {
                            updateFromGroupNoCheck(root);
                            dataset.setRoot(this);
                        } else {
                            try {
                                String location = getLocation();
                                if ((location != null) && (!location.isEmpty()) && (location.charAt(0) == '/')) {
                                    location = location.substring(1);
                                }
                                IContainer container = root.findContainerByPath(location);
                                if ((container != this) && (container instanceof HdfGroup)) {
                                    updateFromGroupNoCheck((HdfGroup) container);
                                }
                            } catch (NoResultException e) {
                                Factory.getLogger()
                                        .error("Failed to find group " + getName() + " after changing dataset", e);
                            }
                        }
                    }
                } else {
                    HdfGroup group = parent.getGroup(shortName);
                    if ((group != this) && (group != null)) {
                        updateFromGroupNoCheck(group);
                    }
                }
            }
        }
    }

    @Override
    public List<IDimension> getDimensionList() {
        init();
        List<IDimension> result = new ArrayList<IDimension>();
        return result;
    }

//    @Override
//    public IGroup findGroup(final String shortName) {
//        init();
//        IGroup result = null;
//        result = getGroup(shortName);
//        return result;
//    }

    @Override
    public List<IGroup> getGroupList() {
        init();
        List<IGroup> result;
        result = new ArrayList<IGroup>(groupMap.values());
        return result;
    }

    @Override
    public int getGroupCount() {
        init();
        return groupMap.size();
    }

    public HdfPath getHdfPath() {
        HdfPath result;
        List<HdfNode> parentNodes = getParentNodes();
        result = new HdfPath(parentNodes.toArray(new HdfNode[parentNodes.size()]));
        return result;
    }

    private List<HdfNode> getParentNodes() {
        List<HdfNode> nodes = new ArrayList<HdfNode>();

        HdfNode node = new HdfNode(this);

        if (parent != null) {
            nodes.addAll(parent.getParentNodes());
            nodes.add(node);
        }

        return nodes;
    }

    private List<INode> getChildNodes() {
        init();
        List<INode> nodes = new ArrayList<INode>();

        for (IDataItem item : itemMap.values()) {
            nodes.add(new HdfNode(item));
        }
        for (IGroup item : groupMap.values()) {
            nodes.add(new HdfNode(item));
        }

        return nodes;
    }

    @Override
    public AHdfContainer findContainerByPath(final String path) throws NoResultException {
        // Split path into nodes
        String[] sNodes = HdfPath.splitStringPath(path);
        AHdfContainer node = getRootGroup();

        // Try to open each node
        for (String shortName : sNodes) {
            if (!shortName.isEmpty() && (node instanceof HdfGroup)) {
                node = ((HdfGroup) node).getContainer(shortName);
            }
        }

        return node;
    }

    public List<IContainer> findAllContainerByPath(final INode[] nodes) throws NoResultException {
        List<IContainer> list = new ArrayList<IContainer>();
        IGroup root = getRootGroup();

        // Call recursive method
        int level = 0;
        list = findAllContainer(root, nodes, level);

        return list;
    }

    @Override
    public List<IContainer> findAllContainerByPath(final String path) throws NoResultException {
        // Try to list all nodes matching the path
        // Transform path into a NexusNode array
        INode[] nodes = HdfPath.splitStringToNode(path);

        List<IContainer> result = findAllContainerByPath(nodes);

        return result;
    }

    private List<IContainer> findAllContainer(final IContainer container, final INode[] nodes, final int level) {
        List<IContainer> result = new ArrayList<IContainer>();
        if (container != null) {
            if (container instanceof HdfGroup) {
                HdfGroup group = (HdfGroup) container;
                if (nodes.length > level) {
                    // List current node children
                    List<INode> childs = group.getChildNodes();
                    INode current = nodes[level];
                    for (INode node : childs) {
                        if (node.matchesPartNode(current)) {
                            if (level < nodes.length - 1) {
                                result.addAll(findAllContainer(group.getContainer(node.getName()), nodes, level + 1));
                            }
                            // Create IContainer and add it to result list
                            else {
                                result.add(group.getContainer(node.getName()));
                            }
                        }
                    }
                }
            } else {
                HdfDataItem dataItem = (HdfDataItem) container;
                result.add(dataItem);
            }
        }
        return result;
    }

    @Override
    public boolean removeDataItem(final IDataItem item) {
        return removeDataItem(item.getShortName());

    }

    @Override
    public boolean removeDataItem(final String varName) {
        init();
        boolean result = true;
        itemMap.remove(varName);
        return result;
    }

    @Override
    public boolean removeDimension(final String name) {
        return false;
    }

    @Override
    public boolean removeDimension(final IDimension dimension) {
        return false;
    }

    @Override
    public boolean removeGroup(final IGroup group) {
        return removeGroup(group.getShortName());
    }

    @Override
    public boolean removeGroup(final String name) {
        init();
        groupMap.remove(name);
        return true;
    }

    @Override
    public void updateDataItem(final String key, final IDataItem dataItem) throws SignalNotAvailableException {
        throw new NotImplementedException();
    }

    @Override
    public boolean isRoot() {
        return parent == null;
    }

    @Override
    public boolean isEntry() {
        boolean result = false;
        result = getParentGroup().isRoot();
        return result;
    }

//    @Override
//    @Deprecated
//    public void setDictionary(final org.cdma.interfaces.IDictionary dictionary) {
//        throw new NotImplementedException();
//    }
//
//    @Override
//    @Deprecated
//    public org.cdma.interfaces.IDictionary findDictionary() {
//        throw new NotImplementedException();
//    }
//
//    @Override
//    @Deprecated
//    public List<IContainer> findAllContainers(final IKey key) throws NoResultException {
//        throw new NotImplementedException();
//    }
//
//    @Override
//    @Deprecated
//    public List<IContainer> findAllOccurrences(final IKey key) throws NoResultException {
//        throw new NotImplementedException();
//    }

    @Override
    public IContainer findObjectByPath(final Path path) {
        throw new NotImplementedException();
    }

//    @Override
//    @Deprecated
//    public IContainer findContainer(final String shortName) {
//        throw new NotImplementedException();
//    }
//
//    @Override
//    @Deprecated
//    public IGroup findGroup(final IKey key) {
//        throw new NotImplementedException();
//    }
//
//    @Override
//    public IDataItem findDataItemWithAttribute(final IKey key, final String name, final String value)
//            throws NoResultException {
//        List<IContainer> found = findAllOccurrences(key);
//        IDataItem result = null;
//        for (IContainer item : found) {
//            if (item.getModelType().equals(ModelType.DataItem) && item.hasAttribute(name, value)) {
//                result = (IDataItem) item;
//                break;
//            }
//
//        }
//        return result;
//    }
//
//    @Override
//    @Deprecated
//    public IGroup findGroupWithAttribute(final IKey key, final String name, final String value) {
//        throw new NotImplementedException();
//    }
//
//    @Override
//    @Deprecated
//    public IDataItem findDataItem(final String shortName) {
//        throw new NotImplementedException();
//    }
//
//    @Override
//    @Deprecated
//    public IDataItem findDataItem(final IKey key) {
//        throw new NotImplementedException();
//    }

    @Override
    public String toString() {
        StringBuilder buffer = new StringBuilder();
        IGroup parent = getParentGroup();
        buffer.append(GROUP).append(getShortName()).append(PARENT).append(parent == null ? NOBODY : parent.getName())
                .append(STRING_END);
        return buffer.toString();
    }

    public void save(final H5File fileToWrite, final Group parent) throws DataAccessException {
        try {
            init();
            Group theGroup = null;

            boolean copyToNewFile = !(fileToWrite.getAbsolutePath().equals(dataset.getH5File().getAbsolutePath()));
            boolean isRoot = isRoot();
            if (isRoot) {
                DefaultMutableTreeNode theRoot = (DefaultMutableTreeNode) fileToWrite.getRootNode();
                H5Group rootObject = (H5Group) theRoot.getUserObject();
                theGroup = rootObject;
            } else {
                // New file or new group
                boolean isNew = fileToWrite.get(originalName) == null;

                if (isNew || copyToNewFile) {
                    theGroup = fileToWrite.createGroup(getShortName(), parent);
                }
                // Group has been renamed
                else if (this.originalName != null && !this.originalName.equals(name)) {
                    theGroup = (Group) fileToWrite.get(originalName);
                    theGroup.setName(shortName);
                    this.originalName = this.name;
                } else {
                    theGroup = (Group) fileToWrite.get(name);
                }
            }

            Collection<IAttribute> attribute = getAttributeList();
            for (IAttribute iAttribute : attribute) {
                HdfAttribute attr = (HdfAttribute) iAttribute;
                attr.save(theGroup, copyToNewFile);
            }

            List<IDataItem> dataItems = getDataItemList();
            for (IDataItem dataItem : dataItems) {
                HdfDataItem hdfDataItem = (HdfDataItem) dataItem;
                hdfDataItem.save(fileToWrite, theGroup);
            }

            List<IGroup> groups = getGroupList();
            for (IGroup iGroup : groups) {
                HdfGroup hdfGroup = (HdfGroup) iGroup;
                hdfGroup.save(fileToWrite, theGroup);
            }
        } catch (Exception e) {
            throw HdfObjectUtils.toDataAccessException(e);
        }
    }

    // XXX This is hack for nexus plugin interaction.
    // For some reason, it seems for NxsGroup to list children at construction when using hdf engine,
    // whereas it can be avoided (resulting in better performances, cf.DATAREDUC-897) with hdf-swmr.
    /**
     * A method that indicates whether it is preferable for this group to always search for its children.
     * 
     * @return A <code>boolean</code>.
     */
    public boolean shouldAlwaysCheckForChildren() {
        return true;
    }

}

package org.cdma.engine.hdf.navigation;

import org.cdma.engine.hdf.utils.HdfPath;
import org.cdma.interfaces.IContainer;

public abstract class AHdfContainer implements IContainer {

    protected String shortName;
    protected final String factoryName; // Name of the factory plugin that instantiate
    protected HdfGroup parent;

    public AHdfContainer(String factoryName, HdfGroup parent) {
        super();
        this.factoryName = factoryName;
        this.parent = parent;
    }

    public abstract void changeDataset(HdfDataset hdfDataset);

    @Override
    public abstract HdfGroup getRootGroup();

    @Override
    public HdfGroup getParentGroup() {
        return parent;
    }

    @Override
    public String getFactoryName() {
        return factoryName;
    }

    @Override
    public String getLocation() {
        return getName();
    }

    @Override
    public String getShortName() {
        return shortName;
    }

    @Override
    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    @Override
    public void addStringAttribute(final String name, final String value) {
        addOneAttribute(new HdfAttribute(factoryName, name, value));
    }

    public static StringBuilder nameToStringBuilder(StringBuilder builder, final HdfGroup parent,
            final String shortName) {
        if (builder == null) {
            builder = new StringBuilder();
        }
        if ((parent == null) || parent.isRoot()) {
            if (HdfPath.PATH_SEPARATOR.equals(shortName)) {
                builder.append(shortName);
            } else {
                builder.append(HdfPath.PATH_SEPARATOR).append(shortName);
            }
        } else {
            parent.nameToStringBuilder(builder).append(HdfPath.PATH_SEPARATOR).append(shortName);
        }
        return builder;
    }

    public StringBuilder nameToStringBuilder(StringBuilder builder) {
        return nameToStringBuilder(builder, parent, shortName);
    }

    @Override
    public String getName() {
        return nameToStringBuilder(null).toString();
    }

    @Override
    public AHdfContainer clone() {
        AHdfContainer clone;
        try {
            clone = (AHdfContainer) super.clone();
        } catch (CloneNotSupportedException e) {
            // Will not happen as AHdfContainer implements IContainer, which extends Cloneable
            clone = this;
        }
        return clone;
    }

}

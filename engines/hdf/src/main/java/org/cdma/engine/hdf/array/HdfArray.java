/*******************************************************************************
 * Copyright (c) 2008 - ANSTO/Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * Norman Xiong (nxi@Bragg Institute) - initial API and implementation
 * Tony Lam (nxi@Bragg Institute) - initial API and implementation
 * Majid Ounsy (SOLEIL Synchrotron) - API v2 design and conception
 * Stephane Poirier (SOLEIL Synchrotron) - API v2 design and conception
 * Clement Rodriguez (ALTEN for SOLEIL Synchrotron) - API evolution
 * Gregory VIGUIER (SOLEIL Synchrotron) - API evolution
 * Raphael GIRARDOT (SOLEIL Synchrotron) - API evolution
 ******************************************************************************/
package org.cdma.engine.hdf.array;

import java.lang.ref.WeakReference;
import java.util.concurrent.atomic.AtomicLong;

import org.cdma.Factory;
import org.cdma.arrays.DefaultArrayInline;
import org.cdma.arrays.DefaultIndex;
import org.cdma.engine.hdf.navigation.HdfDataItem;
import org.cdma.exception.InvalidArrayTypeException;
import org.cdma.exception.InvalidRangeException;
import org.cdma.exception.ShapeNotMatchException;
import org.cdma.interfaces.IIndex;
import org.slf4j.LoggerFactory;

import fr.soleil.lib.project.date.DateUtil;

public class HdfArray extends DefaultArrayInline {

    private static final AtomicLong ITEM_LOAD_TIME = new AtomicLong(0);

    public static void resetMetrics() {
        ITEM_LOAD_TIME.set(0);
        HdfDataItem.resetMetrics();
    }

    public static void traceMetrics(String applicationId) {
        StringBuilder builder = new StringBuilder(HdfArray.class.getSimpleName());
        builder.append("\nCumulative item load time: ");
        DateUtil.elapsedTimeToStringBuilder(builder, ITEM_LOAD_TIME.get()).append(" (").append(ITEM_LOAD_TIME.get())
                .append("ms)");
        LoggerFactory.getLogger(applicationId).trace(builder.toString());
        HdfDataItem.traceMetrics(applicationId);
    }

    private HdfDataItem dataItem;
    private volatile boolean profile;

    // Constructors
    public HdfArray(String factoryName, Class<?> clazz, int[] iShape) throws InvalidArrayTypeException {
        super(factoryName, clazz, iShape.clone());
    }

    public HdfArray(String factoryName, Class<?> clazz, int[] iShape, HdfDataItem dataItem)
            throws InvalidArrayTypeException {
        super(factoryName, clazz, iShape);
        this.dataItem = dataItem;
    }

    public HdfArray(String factoryName, Object array, int[] iShape) throws InvalidArrayTypeException {
        super(factoryName, array, iShape);
        this.lock();
    }

    public HdfArray(String factoryName, Object array, int[] iShape, int[] origin) throws InvalidArrayTypeException {
        this(factoryName, array, iShape);
        setIndex(new HdfIndex(factoryName, iShape, origin, iShape));
    }

    public HdfArray(HdfArray array) throws InvalidArrayTypeException {
        super(array);
        this.dataItem = array.dataItem;
    }

    @Override
    public HdfIndex getIndex() {
        return (HdfIndex) super.getIndex();
    }

    @Override
    public HdfArrayUtils getArrayUtils() {
        return new HdfArrayUtils(this);
    }

    public void setProfile(boolean trace) {
        this.profile = trace;
        if (dataItem != null) {
            dataItem.setProfile(trace);
        }
    }

    public boolean isProfile() {
        return profile;
    }

    @Override
    protected DefaultIndex generateIndex(String factory, int... shape) {
        return new HdfIndex(factory, shape);
    }

    @Override
    protected HdfIndex toDefaultIndex(IIndex index) {
        HdfIndex result;
        if (index instanceof HdfIndex) {
            result = (HdfIndex) index;
        } else {
            result = new HdfIndex(index);
        }
        return result;
    }

    @Override
    public HdfArrayIterator getIterator() {
        return new HdfArrayIterator(this);
    }

    @Override
    public HdfSliceIterator getSliceIterator(int rank) throws ShapeNotMatchException, InvalidRangeException {
        return new HdfSliceIterator(this, rank);
    }

    @Override
    protected WeakReference<Object> loadData() {
        Object data = null;
        WeakReference<Object> result = new WeakReference<Object>(data);

        if (dataItem != null) {
            DefaultIndex index = getIndex();
            try {
                long time = System.currentTimeMillis();
                data = dataItem.load(index.getProjectionOrigin(), index.getProjectionShape());
                if (profile) {
                    ITEM_LOAD_TIME.addAndGet(System.currentTimeMillis() - time);
                }
                result = new WeakReference<Object>(data);
            } catch (OutOfMemoryError | Exception e) {
                Factory.getLogger().error("Unable to loadData()", e);
            }
        }
        return result;
    }

    @Override
    public HdfArray copy(boolean data) {
        HdfArray result;
        try {
            result = new HdfArray(this);
        } catch (InvalidArrayTypeException e) {
            result = null;
            Factory.getLogger().error("Unable to copy the HdfArray array: " + this, e);
        }
        return result;
    }

    @Override
    protected Object getData() {
        Object result = null;
        Object data = super.getData();
        if (isLocked()) {
            result = data;
        } else if ((data == null) || (data instanceof WeakReference<?>)) {
            WeakReference<?> reference = (WeakReference<?>) data;

            // If reference doesn't exists: WeakRef & its data has never been loaded
            if (reference == null) {
                reference = loadData();
            }
            result = reference.get();

            // Reference exists but data is null: GC has cleaned data
            // we have to reload it
            if (result == null) {
                reference = loadData();
                result = reference.get();
            }
        }
        return result;
    }
}

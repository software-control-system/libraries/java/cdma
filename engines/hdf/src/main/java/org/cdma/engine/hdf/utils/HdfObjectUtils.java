/*******************************************************************************
 * Copyright (c) 2008 - ANSTO/Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * Norman Xiong (nxi@Bragg Institute) - initial API and implementation
 * Tony Lam (nxi@Bragg Institute) - initial API and implementation
 * Majid Ounsy (SOLEIL Synchrotron) - API v2 design and conception
 * Stephane Poirier (SOLEIL Synchrotron) - API v2 design and conception
 * Clement Rodriguez (ALTEN for SOLEIL Synchrotron) - API evolution
 * Gregory VIGUIER (SOLEIL Synchrotron) - API evolution
 * Raphael GIRARDOT (SOLEIL Synchrotron) - API evolution
 ******************************************************************************/
package org.cdma.engine.hdf.utils;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cdma.Factory;
import org.cdma.engine.hdf.navigation.HdfAttribute;
import org.cdma.exception.DataAccessException;
import org.cdma.interfaces.IAttribute;

import fr.soleil.lib.project.ObjectUtils;
import ncsa.hdf.hdf5lib.H5;
import ncsa.hdf.hdf5lib.HDF5Constants;
import ncsa.hdf.object.Attribute;
import ncsa.hdf.object.Datatype;
import ncsa.hdf.object.HObject;
import ncsa.hdf.object.h5.H5Datatype;

public class HdfObjectUtils {

    public static final int RETRY_COUNT;
    public static final long RETRY_SLEEPING_PERIOD;
    public static final boolean RETRY_WITH_INIT;
    public static final boolean HDF_API_READY;
    static {
        int count;
        try {
            count = Integer.parseInt(System.getProperty("HDF5Retries"));
            if (count < 0) {
                count = 0;
            }
        } catch (Exception e) {
            count = 0;
        }
        RETRY_COUNT = count;
        long sleep;
        try {
            sleep = Long.parseLong(System.getProperty("HDF5SleepingPeriod"));
            if (sleep < 0) {
                sleep = 0;
            }
        } catch (Exception e) {
            sleep = 0;
        }
        RETRY_SLEEPING_PERIOD = sleep;
        boolean init;
        try {
            init = Boolean.parseBoolean(System.getProperty("HDF5RetryWithInit"));
        } catch (Exception e) {
            init = false;
        }
        RETRY_WITH_INIT = init;
        int test;
        try {
            // HDF5Constant has a static block code which will be executed the first time the class is created
            // This static code loads JHDF and HDF natives.
            test = HDF5Constants.H5E_DATASET;
        } catch (Exception e) {
            // Native API is found but something else happened
            test = -1;
        } catch (UnsatisfiedLinkError e) {
            // Native lib is NOT found
            test = -1;
        }
        HDF_API_READY = (test != -1);
    }

    @SuppressWarnings("unchecked")
    public static Attribute getAttribute(HObject object, String name) {
        Attribute result = null;
        if (name != null && !name.trim().isEmpty()) {
            List<Attribute> attributes;
            try {
                attributes = object.getMetadata();

                for (Attribute attribute : attributes) {
                    if (name.equals(attribute.getName())) {
                        result = attribute;
                        break;
                    }
                }
            } catch (Exception e) {
                Factory.getLogger().error(e.getMessage());
            }
        }
        return result;
    }

    public static List<IAttribute> getAttributeList(String factoryName, HObject object) {
        List<IAttribute> result = new ArrayList<IAttribute>();
        List<?> attributes;
        if (object != null) {
            try {
                attributes = object.getMetadata();
                for (Object attribute : attributes) {
                    IAttribute attr = new HdfAttribute(factoryName, (Attribute) attribute);
                    result.add(attr);
                }
            } catch (Exception e) {
                Factory.getLogger().error(e.getMessage());
            }
        }
        return result;
    }

    public static boolean hasAttribute(HObject object, String name, String value) {
        boolean result = false;
        if ((name != null) && (value != null)) {
            Attribute attribute = HdfObjectUtils.getAttribute(object, name);
            if (attribute != null) {
                Object attrValue = attribute.getValue();
                if (value.indexOf('*') > -1) {
                    // Regular expression compatibility
                    String regex = value.replace("*", ".*");
                    result = ((attrValue instanceof String) && ((String) attrValue).matches(regex));
                } else {
                    result = value.equals(attrValue);
                }
            }
        }
        return result;
    }

    public static boolean removeAttribute(HObject object, IAttribute attributeToRemove) {
        boolean result = true;

        if (attributeToRemove == null) {
            return false;
        }

        try {
            object.removeMetadata(HdfObjectUtils.getAttribute(object, attributeToRemove.getName()));
        } catch (Exception e) {
            result = false;
        }

        return result;
    }

    public static int[] convertLongToInt(long[] input) {
        int[] output;
        if (input == null) {
            output = null;
        } else {
            output = new int[input.length];
            for (int i = 0; i < input.length; i++) {
                output[i] = (int) input[i];
            }
        }
        return output;
    }

    public static long[] convertIntToLong(int[] input) {
        long[] output;
        if (input == null) {
            output = null;
        } else {
            output = new long[input.length];
            for (int i = 0; i < input.length; i++) {
                output[i] = input[i];
            }
        }
        return output;
    }

    public static void addStringAttribute(HObject object, String name, String value) {
        long[] dims = { 1 };
        try {
            getMetadataList(object).add(new Attribute(name, new H5Datatype(Datatype.CLASS_STRING), dims, value));
        } catch (Exception e) {
            Factory.getLogger().warn(e.getMessage());
        }
    }

    public static void addOneAttribute(HObject object, IAttribute attribute) {
        if (attribute != null) {

            Class<?> type = attribute.getType();
            int datatype = getHdfDataTypeForClass(type);

            long[] dims = { 1 };
            try {
                getMetadataList(object).add(new Attribute(attribute.getName(), new H5Datatype(datatype), dims,
                        attribute.getValue().getStorage()));
            } catch (Exception e) {
                Factory.getLogger().error("Unable to copy addOneAttribute", e);
            }
        }

    }

    /**
     * Returns a {@link DataAccessException} equivalent to a given {@link Exception}.
     * 
     * @param e The {@link Exception}.
     * @param message The error message to display, if a new {@link DataAccessException} should be created.
     * @return A {@link DataAccessException}.
     */
    public static DataAccessException toDataAccessException(Exception e, String message) {
        DataAccessException dae;
        if (e instanceof DataAccessException) {
            dae = (DataAccessException) e;
        } else {
            dae = new DataAccessException(message == null ? e.getMessage() : message, e);
        }
        return dae;
    }

    /**
     * Returns a {@link DataAccessException} equivalent to a given {@link Exception}.
     * 
     * @param e The {@link Exception}.
     * @return A {@link DataAccessException}.
     */
    public static DataAccessException toDataAccessException(Exception e) {
        return toDataAccessException(e, null);
    }

    public static int getHdfDataTypeForClass(Class<?> type) {
        // Default is STRING
        int datatype = Datatype.CLASS_STRING;

        if ((Byte.TYPE.equals(type)) || Byte.class.equals(type)) {
            datatype = Datatype.CLASS_CHAR;
        } else if ((Short.TYPE.equals(type)) || Short.class.equals(type)) {
            datatype = Datatype.CLASS_INTEGER;
        } else if ((Integer.TYPE.equals(type)) || Integer.class.equals(type)) {
            datatype = Datatype.CLASS_INTEGER;
        } else if ((Long.TYPE.equals(type)) || Long.class.equals(type)) {
            datatype = Datatype.CLASS_FLOAT;
        } else if ((Float.TYPE.equals(type)) || Float.class.equals(type)) {
            datatype = Datatype.CLASS_FLOAT;
        }
        return datatype;
    }

    public static int getNativeHdfDataTypeForClass(Class<?> type) {
        return getNativeHdfDataTypeForClass(type, null);
    }

    public static int getNativeHdfDataTypeForClass(Class<?> type, Boolean unsigned) {
        // Default is STRING
        int datatype = HDF5Constants.H5T_STRING;
        boolean isUnsigned = (unsigned == null ? false : unsigned.booleanValue());

        if ((Byte.TYPE.equals(type)) || Byte.class.equals(type)) {
            datatype = isUnsigned ? HDF5Constants.H5T_NATIVE_UCHAR : HDF5Constants.H5T_NATIVE_CHAR;
        } else if ((Short.TYPE.equals(type)) || Short.class.equals(type)) {
            datatype = isUnsigned ? HDF5Constants.H5T_NATIVE_USHORT : HDF5Constants.H5T_NATIVE_SHORT;
        } else if ((Integer.TYPE.equals(type)) || Integer.class.equals(type)) {
            datatype = isUnsigned ? HDF5Constants.H5T_NATIVE_UINT : HDF5Constants.H5T_NATIVE_INT;
        } else if ((Long.TYPE.equals(type)) || Long.class.equals(type)) {
            datatype = isUnsigned ? HDF5Constants.H5T_NATIVE_ULONG : HDF5Constants.H5T_NATIVE_LONG;
        } else if ((Float.TYPE.equals(type)) || Float.class.equals(type)) {
            datatype = HDF5Constants.H5T_NATIVE_FLOAT;
        } else if ((Double.TYPE.equals(type)) || Double.class.equals(type)) {
            datatype = HDF5Constants.H5T_NATIVE_DOUBLE;
        }
        return datatype;
    }

    @SuppressWarnings("unchecked")
    public static List<Attribute> getMetadataList(HObject object) {
        List<Attribute> h5AttributeList = new ArrayList<Attribute>();
        try {
            h5AttributeList = object.getMetadata();
        } catch (Exception e) {
            Factory.getLogger().error("Unable to copy getMetadataList", e);
        }
        return h5AttributeList;
    }

    public static String getFilterType(long dcplId) throws DataAccessException {
        try {
            String filterTypeStr;
            // Java lib requires a valid filter_name object and cd_values
            int[] flags = { 0 };
            long[] cd_nelmts = { 1 };
            int[] cd_values = { 0 };
            String[] filter_name = { ObjectUtils.EMPTY_STRING };
            int[] filter_config = { 0 };
            int filter_type = -1;

            filter_type = H5.H5Pget_filter((int) dcplId, 0, flags, cd_nelmts, cd_values, 120, filter_name,
                    filter_config);
            switch (HdfObjectUtils.H5Z_filter.get(filter_type)) {
                case H5Z_FILTER_DEFLATE:
                    filterTypeStr = "H5Z_FILTER_DEFLATE";
                    break;
                case H5Z_FILTER_SHUFFLE:
                    filterTypeStr = "H5Z_FILTER_SHUFFLE";
                    break;
                case H5Z_FILTER_FLETCHER32:
                    filterTypeStr = "H5Z_FILTER_FLETCHER32";
                    break;
                case H5Z_FILTER_SZIP:
                    filterTypeStr = "H5Z_FILTER_SZIP";
                    break;
                case H5Z_FILTER_NBIT:
                    filterTypeStr = "H5Z_FILTER_NBIT";
                    break;
                case H5Z_FILTER_SCALEOFFSET:
                    filterTypeStr = "H5Z_FILTER_SCALEOFFSET";
                    break;
                default:
                    filterTypeStr = "H5Z_FILTER_ERROR";
            }
            return filterTypeStr;
        } catch (Exception e) {
            throw toDataAccessException(e);
        }
    }

    public static long openH5Dataset(long fileId, String datasetName) throws DataAccessException {
        try {
            return H5.H5Dopen((int) fileId, datasetName, HDF5Constants.H5P_DEFAULT);
        } catch (Exception e) {
            throw toDataAccessException(e);
        }
    }

    public static long getCreatePlist(long datasetId) throws DataAccessException {
        try {
            return H5.H5Dget_create_plist((int) datasetId);
        } catch (Exception e) {
            throw toDataAccessException(e);
        }
    }

    public static void readDatasetForTests(long datasetId, int[][] dsetData) throws DataAccessException {
        try {
            H5.H5Dread((int) datasetId, HDF5Constants.H5T_NATIVE_INT, HDF5Constants.H5S_ALL, HDF5Constants.H5S_ALL,
                    HDF5Constants.H5P_DEFAULT, dsetData);
        } catch (Exception e) {
            throw toDataAccessException(e);
        }
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    // Values for the status of space allocation
    public static enum H5Z_filter {
        H5Z_FILTER_ERROR(HDF5Constants.H5Z_FILTER_ERROR), H5Z_FILTER_NONE(
                HDF5Constants.H5Z_FILTER_NONE), H5Z_FILTER_DEFLATE(
                        HDF5Constants.H5Z_FILTER_DEFLATE), H5Z_FILTER_SHUFFLE(
                                HDF5Constants.H5Z_FILTER_SHUFFLE), H5Z_FILTER_FLETCHER32(
                                        HDF5Constants.H5Z_FILTER_FLETCHER32), H5Z_FILTER_SZIP(
                                                HDF5Constants.H5Z_FILTER_SZIP), H5Z_FILTER_NBIT(
                                                        HDF5Constants.H5Z_FILTER_NBIT), H5Z_FILTER_SCALEOFFSET(
                                                                HDF5Constants.H5Z_FILTER_SCALEOFFSET), H5Z_FILTER_RESERVED(
                                                                        HDF5Constants.H5Z_FILTER_RESERVED), H5Z_FILTER_MAX(
                                                                                HDF5Constants.H5Z_FILTER_MAX);

        private static final Map<Integer, H5Z_filter> lookup = new HashMap<Integer, H5Z_filter>();

        static {
            for (H5Z_filter s : EnumSet.allOf(H5Z_filter.class))
                lookup.put(s.getCode(), s);
        }

        private final int code;

        H5Z_filter(int layout_type) {
            this.code = layout_type;
        }

        public int getCode() {
            return this.code;
        }

        public static H5Z_filter get(int code) {
            return lookup.get(code);
        }
    }

}

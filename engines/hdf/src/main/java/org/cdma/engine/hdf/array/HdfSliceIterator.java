package org.cdma.engine.hdf.array;

import org.cdma.arrays.DefaultIndex;
import org.cdma.arrays.DefaultSliceIterator;
import org.cdma.exception.InvalidRangeException;
import org.cdma.interfaces.IIndex;

public class HdfSliceIterator extends DefaultSliceIterator {

    public HdfSliceIterator(HdfArray array, int dim) throws InvalidRangeException {
        super(array, dim);
    }

    @Override
    protected HdfArrayIterator generateArrayIterator(IIndex index) {
        HdfIndex hdfIndex;
        if (index instanceof HdfIndex) {
            hdfIndex = (HdfIndex) index;
        } else if (index instanceof DefaultIndex) {
            hdfIndex = new HdfIndex((DefaultIndex) index);
        } else {
            hdfIndex = new HdfIndex(index);
        }
        return new HdfArrayIterator(hdfIndex);
    }

    @Override
    public HdfArray getArrayNext() throws InvalidRangeException {
        return (HdfArray) super.getArrayNext();
    }

}

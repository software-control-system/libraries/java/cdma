/*******************************************************************************
 * Copyright (c) 2008 - ANSTO/Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * Norman Xiong (nxi@Bragg Institute) - initial API and implementation
 * Tony Lam (nxi@Bragg Institute) - initial API and implementation
 * Majid Ounsy (SOLEIL Synchrotron) - API v2 design and conception
 * Stephane Poirier (SOLEIL Synchrotron) - API v2 design and conception
 * Clement Rodriguez (ALTEN for SOLEIL Synchrotron) - API evolution
 * Gregory VIGUIER (SOLEIL Synchrotron) - API evolution
 * Raphael GIRARDOT (SOLEIL Synchrotron) - API evolution
 ******************************************************************************/
package org.cdma.engine.hdf.navigation;

import java.io.File;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

import org.cdma.Factory;
import org.cdma.engine.hdf.array.HdfArray;
import org.cdma.engine.hdf.array.HdfIndex;
import org.cdma.engine.hdf.utils.HdfObjectUtils;
import org.cdma.engine.hdf.utils.HdfPath;
import org.cdma.exception.DataAccessException;
import org.cdma.exception.DimensionNotSupportedException;
import org.cdma.exception.InvalidArrayTypeException;
import org.cdma.exception.InvalidRangeException;
import org.cdma.exception.NotImplementedException;
import org.cdma.interfaces.IArray;
import org.cdma.interfaces.IAttribute;
import org.cdma.interfaces.IDataItem;
import org.cdma.interfaces.IDimension;
import org.cdma.interfaces.IGroup;
import org.cdma.interfaces.IIndex;
import org.cdma.interfaces.IRange;
import org.cdma.utils.Utilities.ModelType;
import org.slf4j.LoggerFactory;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.date.DateUtil;
import fr.soleil.lib.project.math.ArrayUtils;
import fr.soleil.lib.project.math.MathConst;
import ncsa.hdf.hdf5lib.HDF5Constants;
import ncsa.hdf.hdf5lib.exceptions.HDF5Exception;
import ncsa.hdf.object.Attribute;
import ncsa.hdf.object.Dataset;
import ncsa.hdf.object.Datatype;
import ncsa.hdf.object.Group;
import ncsa.hdf.object.HObject;
import ncsa.hdf.object.h5.H5CompoundDS;
import ncsa.hdf.object.h5.H5Datatype;
import ncsa.hdf.object.h5.H5File;
import ncsa.hdf.object.h5.H5ScalarDS;

public class HdfDataItem extends AHdfContainer implements IDataItem {

    private static final AtomicLong SYNCHRONIZED_ACCESS_TIME = new AtomicLong(0);
    private static final AtomicLong CONDITION_TIME = new AtomicLong(0);
    private static final AtomicLong SELECTED_DIMS_TIME = new AtomicLong(0);
    private static final AtomicLong SELECTED_DIMS_CHECK_TIME = new AtomicLong(0);
    private static final AtomicLong START_DIMS_TIME = new AtomicLong(0);
    private static final AtomicLong START_DIMS_CHECK_TIME = new AtomicLong(0);
    private static final AtomicLong CHANGE_CLEAR_TIME = new AtomicLong(0);
    private static final AtomicLong GET_DATA_TIME = new AtomicLong(0);
    private static final AtomicLong CLEAR_TIME = new AtomicLong(0);

    public static void resetMetrics() {
        SYNCHRONIZED_ACCESS_TIME.set(0);
        CONDITION_TIME.set(0);
        SELECTED_DIMS_TIME.set(0);
        SELECTED_DIMS_CHECK_TIME.set(0);
        START_DIMS_TIME.set(0);
        START_DIMS_CHECK_TIME.set(0);
        CHANGE_CLEAR_TIME.set(0);
        GET_DATA_TIME.set(0);
        CLEAR_TIME.set(0);
    }

    public static void traceMetrics(String applicationId) {
        StringBuilder builder = new StringBuilder(HdfDataItem.class.getSimpleName());
        builder.append("\nCumulative synchronized lock time: ");
        DateUtil.elapsedTimeToStringBuilder(builder, SYNCHRONIZED_ACCESS_TIME.get()).append(" (")
                .append(SYNCHRONIZED_ACCESS_TIME.get()).append("ms)");
        builder.append("\nCumulative if time: ");
        DateUtil.elapsedTimeToStringBuilder(builder, CONDITION_TIME.get()).append(" (").append(CONDITION_TIME.get())
                .append("ms)");
        builder.append("\nCumulative selected dimensions obtaining time: ");
        DateUtil.elapsedTimeToStringBuilder(builder, SELECTED_DIMS_TIME.get()).append(" (")
                .append(SELECTED_DIMS_TIME.get()).append("ms)");
        builder.append("\nCumulative selected dimensions checking time: ");
        DateUtil.elapsedTimeToStringBuilder(builder, SELECTED_DIMS_CHECK_TIME.get()).append(" (")
                .append(SELECTED_DIMS_CHECK_TIME.get()).append("ms)");
        builder.append("\nCumulative start dimensions obtaining time: ");
        DateUtil.elapsedTimeToStringBuilder(builder, START_DIMS_TIME.get()).append(" (").append(START_DIMS_TIME.get())
                .append("ms)");
        builder.append("\nCumulative start dimensions checking time: ");
        DateUtil.elapsedTimeToStringBuilder(builder, START_DIMS_CHECK_TIME.get()).append(" (")
                .append(START_DIMS_CHECK_TIME.get()).append("ms)");
        builder.append("\nCumulative clear item on change time: ");
        DateUtil.elapsedTimeToStringBuilder(builder, CHANGE_CLEAR_TIME.get()).append(" (")
                .append(CHANGE_CLEAR_TIME.get()).append("ms)");
        builder.append("\nCumulative item getData time: ");
        DateUtil.elapsedTimeToStringBuilder(builder, GET_DATA_TIME.get()).append(" (").append(GET_DATA_TIME.get())
                .append("ms)");
        builder.append("\nCumulative clear item time: ");
        DateUtil.elapsedTimeToStringBuilder(builder, CLEAR_TIME.get()).append(" (").append(CLEAR_TIME.get())
                .append("ms)");
        LoggerFactory.getLogger(applicationId).trace(builder.toString());
    }

    private static final String UNIT = "unit";
    private static final String SIGNAL = "signal";
    private static final String DATA_ITEM = "DataItem = ";
    private static final String NEW_LINE = "\n";
    private static final String SHAPE = "shape: ";
    private static final String ATTRIBUTES = "\nAttributes:\n";
    private static final String ATTRIBUTE_START = "- ";

    private Dataset h5Item;
    protected String lastPath;
    protected HdfArray array;
    private Collection<HdfAttribute> attributes;
    protected boolean dirty;
    private HdfDataItem linkedItem;
    private boolean partialWriteMode;
    private Boolean unsigned;
    // fakeIndex is used in case of reshaping with 1 more dimension
    private int fakeIndex = -1;
    private volatile boolean profile;
    // In case of H5CompoundDS by default manage first column index
    private Integer memberIndex;

    public HdfDataItem(final String factoryName, final HdfGroup parent, final H5ScalarDS item) {
        this(factoryName, parent, item, null, null);
    }

    public HdfDataItem(final String factoryName, final HdfGroup parent, final Dataset item, Integer memberIndex) {
        this(factoryName, parent, item, null, memberIndex);
    }

    public HdfDataItem(final String factoryName, final String name) {
        this(factoryName, name, null);
    }

    public HdfDataItem(final String factoryName, final String name, Boolean unsigned) {
        this(factoryName, null, null, unsigned, null);
        this.shortName = name;
    }

    protected HdfDataItem(final String factoryName, final HdfGroup parent, final Dataset item, final Boolean unsigned,
            Integer memberIndex) {
        super(factoryName, parent);
        this.attributes = generateAttributeList(null);
        this.h5Item = item;
        this.lastPath = getPath(parent);
        linkedItem = null;
        this.partialWriteMode = false;
        this.unsigned = unsigned;
        // Manage H5CompoundDS column index
        this.memberIndex = memberIndex;
        if (this.h5Item == null) {
            this.dirty = true;
            this.shortName = ObjectUtils.EMPTY_STRING;
        } else {
            this.dirty = false;
            // Manage either H5CompoundDS or H5Scalar
            initH5Item(h5Item);
            this.shortName = h5Item.getName();
            loadAttributes();
        }
    }

    private HdfDataItem(final HdfDataItem dataItem) {
        super(dataItem.getFactoryName(), dataItem.parent);
        this.attributes = generateAttributeList(null);
        dirty = false;
        linkedItem = null;
        partialWriteMode = false;
        this.lastPath = dataItem.lastPath;
        this.h5Item = dataItem.getH5DataItem();
        this.shortName = dataItem.shortName;
        // Take H5CompoundDS in account
        initH5Item(h5Item);
        try {
            this.array = dataItem.getData();
            dirty = true;
        } catch (DataAccessException e) {
            Factory.getLogger().error(e.getMessage());
        }
    }

    /**
     * This method can manage either H5CompoundDS or H5ScalarDS
     */
    private void initH5Item(Dataset item) {
        // Take Compound in account
        if (item != null) {
            item.init();
        }
    }

    protected Collection<HdfAttribute> generateAttributeList(Collection<HdfAttribute> ref) {
        Collection<HdfAttribute> attributes = Collections.newSetFromMap(new ConcurrentHashMap<HdfAttribute, Boolean>());
        if (ref != null) {
            attributes.addAll(ref);
        }
        return attributes;
    }

    protected static String getPath(H5File file) {
        return file == null ? null : file.getAbsolutePath();
    }

    protected static String getPath(HdfDataset dataset) {
        return dataset == null ? null : getPath(dataset.getH5File());
    }

    protected static String getPath(HdfGroup group) {
        return group == null ? null : getPath(group.getDataset());
    }

    @Override
    public void changeDataset(HdfDataset hdfDataset) {
        if ((parent != null) && (hdfDataset != null)) {
            HdfDataItem hdfDataItem = null;
            parent.changeDataset(hdfDataset);
            IDataItem item = parent.getDataItem(shortName);
            if ((item != this) && (item instanceof HdfDataItem)) {
                if (hdfDataItem == null) {
                    hdfDataItem = (HdfDataItem) item;
                }
                h5Item = hdfDataItem.h5Item;
                parent.addDataItem(this);
                if (!dirty) {
                    array = null;
                    attributes.clear();
                    loadAttributes();
                }
            }
        }
    }

    public final void setProfile(boolean profile) {
        this.profile = profile;
    }

    public final boolean isProfile() {
        return profile;
    }

    @Override
    public HdfDataItem clone() {
        HdfDataItem clone;
        clone = (HdfDataItem) super.clone();
        if (clone == this) {
            // should not happen as HdfDataItem implements Cloneable
            clone = new HdfDataItem(this);
        } else {
            clone.attributes = generateAttributeList(attributes);
            clone.dirty = false;
            clone.linkedItem = null;
            clone.partialWriteMode = false;
            // Take H5CompoundDS in account
            initH5Item(clone.h5Item);
            try {
                clone.array = getData();
                clone.dirty = true;
            } catch (DataAccessException e) {
                Factory.getLogger().error(e.getMessage());
            }
        }
        return clone;
    }

    public HdfDataItem getCopyWithFakeDimensionInsertedAt(int position) {
        HdfDataItem reshaped = clone();
        int[] shape = getShape();
        try {
            int[] origin = getData().getIndex().getOrigin();
            int[] newShape = new int[shape.length + 1];
            int[] newOrigin = new int[origin.length + 1];
            if (position == 0) {
                newShape[0] = 1;
                newOrigin[0] = 0;
                System.arraycopy(shape, 0, newShape, 1, shape.length);
                System.arraycopy(origin, 0, newOrigin, 1, origin.length);
            } else if (position == shape.length) {
                newShape[shape.length] = 1;
                newOrigin[shape.length] = 0;
                System.arraycopy(shape, 0, newShape, 0, shape.length);
                System.arraycopy(origin, 0, newOrigin, 0, origin.length);
            } else {
                System.arraycopy(shape, 0, newShape, 0, position);
                System.arraycopy(origin, 0, newOrigin, 0, position);
                newShape[position] = 1;
                newOrigin[position] = 0;
                System.arraycopy(shape, position, newShape, position + 1, shape.length - position);
                System.arraycopy(origin, position, newOrigin, position + 1, origin.length - position);
            }
            reshaped.array = new HdfArray(factoryName, getType(), ArrayUtils.EMPTY_SHAPE, reshaped);
            HdfIndex index = new HdfIndex(this.factoryName, newShape, newOrigin, newShape);
            reshaped.array.setIndex(index);
            reshaped.fakeIndex = position;
        } catch (InvalidArrayTypeException | DataAccessException e) {
            Factory.getLogger().error("Failed to reshape HdfDataItem", e);
        }
        return reshaped;
    }

    @Override
    public ModelType getModelType() {
        return ModelType.DataItem;
    }

    /**
     * Return common super class for H5CompoundDS and H5Scalar
     */
    public Dataset getH5DataItem() {
        return h5Item;
    }

    public void linkTo(HdfDataItem dataitem) {
        linkedItem = dataitem;
    }

    @Override
    public void addOneAttribute(final IAttribute attribute) {
        if (attribute instanceof HdfAttribute) {
            attributes.add((HdfAttribute) attribute);
        }
    }

    @Override
    public void addStringAttribute(final String name, final String value) {
        IAttribute attr = new HdfAttribute(factoryName, name, value);
        addOneAttribute(attr);
    }

    public HdfAttribute getAttribute(final String name, final boolean ignoreCase) {
        HdfAttribute result = null;
        if (name != null) {
            for (HdfAttribute attribute : attributes) {
                if (attribute != null) {
                    String attrName = attribute.getName();
                    if ((attrName != null)
                            && ((ignoreCase && name.equalsIgnoreCase(attrName)) || name.equals(attrName))) {
                        result = attribute;
                    }
                }
            }
        }
        return result;
    }

    protected void loadAttributes() {
        Collection<Attribute> hdfAttributes;
        try {
            if (h5Item != null) {
                hdfAttributes = readAttributesWithRetries(HdfObjectUtils.RETRY_COUNT, null);
                if (hdfAttributes != null) {
                    for (Object attribute : hdfAttributes) {
                        HdfAttribute attr = new HdfAttribute(factoryName, (Attribute) attribute);
                        attributes.add(attr);
                    }
                }
            }
        } catch (HDF5Exception e) {
            Factory.getLogger().error("Unable to loadAttributes for " + getName(), e);
        }
    }

    private Collection<Attribute> readAttributesWithRetries(int count, HDF5Exception previousException)
            throws HDF5Exception {
        Collection<Attribute> attributes = null;
        if (count < 0) {
            if (previousException != null) {
                throw previousException;
            }
        } else {
            try {
                // Call a method that manage H5CompoundDS or H5Scalar
                attributes = getItemAttribute();
            } catch (HDF5Exception e) {
                if (prepareRetry()) {
                    attributes = readAttributesWithRetries(count - 1, e);
                } else {
                    throw e;
                }
            }
        }
        return attributes;
    }

    /**
     * This method can manage H5CompoundDS or H5Scalar
     */
    private Collection<Attribute> getItemAttribute() throws HDF5Exception {
        Collection<Attribute> attributes;
        if (h5Item instanceof H5ScalarDS) {
            @SuppressWarnings("unchecked")
            List<Attribute> meta = ((H5ScalarDS) h5Item).getMetadata();
            attributes = meta;
        } else if (h5Item instanceof H5CompoundDS) {
            if (memberIndex == null) {
                memberIndex = Integer.valueOf(0);
            }
            attributes = ((H5CompoundDS) h5Item).getMetadata(memberIndex);
        } else {
            attributes = null;
        }
        return attributes;
    }

    private boolean prepareRetry() {
        boolean retry = true;
        if (HdfObjectUtils.RETRY_SLEEPING_PERIOD > 0) {
            try {
                Thread.sleep(HdfObjectUtils.RETRY_SLEEPING_PERIOD);
            } catch (InterruptedException e1) {
                retry = false;
            }
        }
        if (retry) {
            if (HdfObjectUtils.RETRY_WITH_INIT) {
                // Take H5CompoundDS in account
                initH5Item(h5Item);
            }
        }
        return retry;
    }

    @Override
    public HdfAttribute getAttribute(final String name) {
        return getAttribute(name, false);
    }

    @Override
    public Collection<IAttribute> getAttributeList() {
        return new ArrayList<>(attributes);
    }

    @Override
    public HdfDataset getDataset() {
        HdfDataset result = null;
        HdfGroup parentGroup = getParentGroup();
        if (parentGroup != null) {
            result = parentGroup.getDataset();
        }
        return result;
    }

    @Override
    public HdfGroup getRootGroup() {
        HdfGroup result = null;
        HdfGroup parent = getParentGroup();
        if (parent != null) {
            result = parent.getRootGroup();
        }
        return result;
    }

    @Override
    public boolean hasAttribute(final String name, final String value) {
        return HdfObjectUtils.hasAttribute(h5Item, name, value);
    }

    @Override
    public void setName(final String name) {
        String[] nodes = name.split(HdfPath.PATH_SEPARATOR);
        int depth = nodes.length - 1;

        if (depth >= 0) {
            setShortName(nodes[depth--]);
        }

        IGroup group = getParentGroup();
        while (group != null && !group.isRoot() && depth >= 0) {
            group.setShortName(nodes[depth--]);
        }
    }

    @Override
    public void setParent(final IGroup group) {
        if (group instanceof HdfGroup) {
            HdfGroup hdfGroup = (HdfGroup) group;
            try {
                parent = hdfGroup;
                if (h5Item != null) {
                    // Warning!!! It is mandatory to end path with "/", otherwise h5Item won't read data
                    StringBuilder builder = hdfGroup.nameToStringBuilder(null);
                    if ((builder.length() == 0) || (builder.charAt(builder.length() - 1) != '/')) {
                        builder.append(HdfPath.PATH_SEPARATOR);
                    }
                    h5Item.setPath(builder.toString());
                }
                // Check virtual item particular case
                if (attributes.isEmpty() && (h5Item == null) && (parent != null)) {
                    AHdfContainer container = parent.getContainer(shortName);
                    if (container instanceof HdfGroup) {
                        Collection<IAttribute> attrs = container.getAttributeList();
                        if ((attrs != null) && (!attrs.isEmpty())) {
                            for (IAttribute attribute : attrs) {
                                if (attribute instanceof HdfAttribute) {
                                    attributes.add((HdfAttribute) attribute);
                                }
                            }
                        }
                    }
                }
            } catch (Exception e) {
                Factory.getLogger().error("Unable to setParent", e);
            }
        }
    }

    @Override
    public long getLastModificationDate() {
        long result = 0;
        if (h5Item != null) {
            String fileName = h5Item.getFile();
            File currentFile = new File(fileName);
            if (currentFile != null && currentFile.exists()) {
                result = currentFile.lastModified();
            }
        }
        return result;
    }

    @Override
    public IAttribute findAttributeIgnoreCase(final String name) {
        IAttribute result = null;
        result = getAttribute(name, true);
        return result;
    }

    @Override
    public int findDimensionIndex(final String name) {
        return 0;
    }

    @Override
    public HdfArray getData() throws DataAccessException {
        if (array == null) {
            int[] shape = getShape();
            int[] origin = new int[getRank()];
            try {
                array = getData(origin, shape);
            } catch (Exception e) {
                Factory.getLogger().error("Unable to initialize data!", e);
            }
        }
        return array;
    }

    @Override
    public HdfArray getData(final int[] origin, final int[] shape) throws DataAccessException, InvalidRangeException {
        HdfArray array = null;
        IIndex index = null;

        try {
            Class<?> type = getType();
            if (type != null) {
                array = new HdfArray(factoryName, type, ArrayUtils.EMPTY_SHAPE, this);
                index = new HdfIndex(this.factoryName, shape, origin, shape);
                array.setIndex(index);
            }
        } catch (InvalidArrayTypeException e) {
            throw new InvalidRangeException(e);
        }

        return array;
    }

    public Object load(final int[] origin, final int[] shape) throws OutOfMemoryError, Exception {
        Object result = null;
        Object tempResult = null;
        boolean viewHasChanged = false;
        long time;
        time = System.currentTimeMillis();
        synchronized (h5Item) {
            long lockTime;
            if (profile) {
                lockTime = System.currentTimeMillis() - time;
                time = System.currentTimeMillis();
            } else {
                lockTime = 0;
            }
            if (shape.length == origin.length || origin.length == getShape().length) {
                // Set Selected dimensions
                long ifTime;
                if (profile) {
                    ifTime = System.currentTimeMillis() - time;
                    time = System.currentTimeMillis();
                } else {
                    ifTime = 0;
                }
                long[] sDims = h5Item.getSelectedDims();
                long selectedDimsTime;
                if (profile) {
                    selectedDimsTime = System.currentTimeMillis() - time;
                    time = System.currentTimeMillis();
                } else {
                    selectedDimsTime = 0;
                }
                for (int i = 0; i < sDims.length; i++) {
                    sDims[i] = 1;
                }
                if (!Arrays.equals(shape, HdfObjectUtils.convertLongToInt(sDims))) {
                    viewHasChanged = true;
                    int j = 0;
                    for (int i = 0; i < shape.length; i++) {
                        if (i == fakeIndex) {
                            continue;
                        }
                        sDims[j++] = shape[i];
                    }
                }
                long selectedDimCheckTime;
                if (profile) {
                    selectedDimCheckTime = System.currentTimeMillis() - time;
                    time = System.currentTimeMillis();
                } else {
                    selectedDimCheckTime = 0;
                }
                // Set origin
                long[] startDims = h5Item.getStartDims();
                long getStartDimsTime;
                if (profile) {
                    getStartDimsTime = System.currentTimeMillis() - time;
                    time = System.currentTimeMillis();
                } else {
                    getStartDimsTime = 0;
                }
                if (!Arrays.equals(origin, HdfObjectUtils.convertLongToInt(startDims))) {
                    viewHasChanged = true;
                    int j = 0;
                    for (int i = 0; i < origin.length; i++) {
                        if (i == fakeIndex) {
                            continue;
                        }
                        startDims[j++] = origin[i];
                    }
                }
                long startDimsFillTime;
                if (profile) {
                    startDimsFillTime = System.currentTimeMillis() - time;
                    time = System.currentTimeMillis();
                } else {
                    startDimsFillTime = 0;
                }
                if (viewHasChanged) {
                    // Call clear method that manage H5CompoundDS or H5Scalar
                    clear();
                }
                long changeClearTime;
                if (profile) {
                    changeClearTime = System.currentTimeMillis() - time;
                    time = System.currentTimeMillis();
                } else {
                    changeClearTime = 0;
                }
                // Get data that manage H5CompoundDS or H5Scalar
                tempResult = getH5Data();
                long getDataTime;
                if (profile) {
                    getDataTime = System.currentTimeMillis() - time;
                    time = System.currentTimeMillis();
                } else {
                    getDataTime = 0;
                }
                // Clear method that manage H5CompoundDS or H5Scalar
                clear();
                long clearTime;
                if (profile) {
                    clearTime = System.currentTimeMillis() - time;
                } else {
                    clearTime = 0;
                }
                result = tempResult;
                if (profile) {
                    CONDITION_TIME.addAndGet(ifTime);
                    SELECTED_DIMS_TIME.addAndGet(selectedDimsTime);
                    SELECTED_DIMS_CHECK_TIME.addAndGet(selectedDimCheckTime);
                    START_DIMS_TIME.addAndGet(getStartDimsTime);
                    START_DIMS_CHECK_TIME.addAndGet(startDimsFillTime);
                    CHANGE_CLEAR_TIME.addAndGet(changeClearTime);
                    GET_DATA_TIME.addAndGet(getDataTime);
                    CLEAR_TIME.addAndGet(clearTime);
                }
            }
            if (profile) {
                SYNCHRONIZED_ACCESS_TIME.addAndGet(lockTime);
            }
        }
        return result;
    }

    /**
     * Get data from H5CompoundDS or H5Scalar
     */
    private Object getH5Data() {
        Object data = null;
        try {
            Dataset h5Item = this.h5Item;
            if (h5Item != null) {
                // Read the data
                if (h5Item instanceof H5ScalarDS) {
                    data = h5Item.getData();
                } else if (h5Item instanceof H5CompoundDS) {
                    Object dataList = h5Item.getData();
                    if (dataList instanceof List<?>) {
                        if (memberIndex == null) {
                            memberIndex = Integer.valueOf(0);
                        }
                        data = ((List<?>) dataList).get(memberIndex);
                    }
                }
            }
        } catch (Exception e) {
            Factory.getLogger().error(e.getMessage());
        }
        return data;
    }

    /**
     * Clear data for H5CompoundDS or H5Scalar
     */
    private void clear() {
        if (h5Item != null) {
            h5Item.clear();
        }
    }

    @Override
    public String getDescription() {
        String result = null;
        IAttribute attribute = null;

        result = getAttribute("long_name").getStringValue();
        if (attribute == null) {
            result = getAttribute("description").getStringValue();
        }
        if (attribute == null) {
            result = getAttribute("title").getStringValue();
        }
        if (attribute == null) {
            result = getAttribute("standard_name").getStringValue();
        }
        if (attribute == null) {
            result = getAttribute("name").getStringValue();
        }
        return result;
    }

    @Override
    public List<IDimension> getDimensions(final int index) {
        return new ArrayList<IDimension>();
    }

    @Override
    public List<IDimension> getDimensionList() {
        return new ArrayList<IDimension>();
    }

    @Override
    public String getDimensionsString() {
        return ObjectUtils.EMPTY_STRING;
    }

    @Override
    public int getElementSize() {
        int result = 0;
        // get DataType for H5CompoundDS or H5Scalar
        Datatype type = getDatatype();
        if (type != null) {
            result = type.getDatatypeSize();
        }
        return result;
    }

    @Override
    public String getNameAndDimensions() {
        return null;
    }

    @Override
    public void getNameAndDimensions(final StringBuilder buf, final boolean longName, final boolean length) {
    }

    @Override
    public List<IRange> getRangeList() {
        List<IRange> list = new ArrayList<IRange>();
        try {
            HdfIndex index = getData().getIndex();
            list.addAll(index.getRangeList());
        } catch (DataAccessException e) {
            list = null;
        }
        return list;
    }

    @Override
    public int getRank() {
        int result = 0;
        if (h5Item != null) {
            result = h5Item.getRank();
            if ((h5Item instanceof H5CompoundDS) && (result == 0)) {
                result = 1;
            }
        }
        return result;
    }

    @Override
    public IDataItem getSection(final List<IRange> section) throws InvalidRangeException {
        HdfDataItem item = null;
        try {
            item = new HdfDataItem(this);
            item.array = item.getData().getArrayUtils().sectionNoReduce(section).getArray();
        } catch (DataAccessException e) {
        }
        return item;
    }

    @Override
    public List<IRange> getSectionRanges() {
        List<IRange> list = new ArrayList<IRange>();
        try {
            HdfIndex index = getData().getIndex();
            list.addAll(index.getRangeList());
        } catch (DataAccessException e) {
            list = null;
        }
        return list;
    }

    protected long[] getDims() {
        return h5Item == null ? null : h5Item.getDims();
    }

    @Override
    public int[] getShape() {
        int[] result = null;
        if (array == null) {
            if (h5Item != null) {
                // Take H5CompoundDS into account
                result = HdfObjectUtils.convertLongToInt(getDims());
            }
        } else {
            result = array.getShape();
        }
        return result;
    }

    @Override
    public long getSize() {
        long size = 0;
        if (h5Item != null) {
            // Take H5CompoundDS into account
            long[] dims = getDims();
            if (dims != null) {
                for (long dim : dims) {
                    if (dim >= 0) {
                        size *= dim;
                    }
                }
            }
        }
        return size;
    }

    @Override
    public int getSizeToCache() {
        throw new NotImplementedException();
    }

    @Override
    public IDataItem getSlice(final int dim, final int value) throws InvalidRangeException {
        HdfDataItem item = new HdfDataItem(this);
        try {
            item.array = item.getData().getArrayUtils().slice(dim, value).getArray();
        } catch (Exception e) {
            item = null;
        }
        return item;
    }

    /**
     * This method return Datatype from Take H5CompoundDS or H5Scalar
     */
    private Datatype getDatatype() {
        // Datatype can be determined by IArray
        Datatype datatype;
        if (h5Item instanceof H5ScalarDS) {
            datatype = ((H5ScalarDS) h5Item).getDatatype();
        } else if (h5Item instanceof H5CompoundDS) {
            if (memberIndex == null) {
                memberIndex = Integer.valueOf(0);
            }
            Datatype[] memberTypes = ((H5CompoundDS) h5Item).getMemberTypes();
            datatype = memberTypes[memberIndex];
        } else {
            datatype = null;
        }
        return datatype;
    }

    private Class<? extends Number> getIntegerType(Datatype dType, Class<? extends Number> defaultType) {
        Class<? extends Number> result = null;
        if (dType != null) {
            // Ignore dType.getDatatypeSign(), as unsigned conversion must be done outside of engine.
            // Otherwise, it will provoke reading failure, see PROBLEM-2132.
            switch (dType.getDatatypeSize()) {
                case 1:
                    result = Byte.TYPE;
                    break;
                case 2:
                    result = Short.TYPE;
                    break;
                case 4:
                    result = Integer.TYPE;
                    break;
                case 8:
                    result = Long.TYPE;
                    break;
                default:
                    result = defaultType;
                    break;
            }
        }
        return result;
    }

    private Class<?> getFloatType(Datatype dType) {
        Class<?> result = null;
        if (dType != null) {
            switch (dType.getDatatypeSize()) {
                case 8:
                    result = Double.TYPE;
                    break;
                case 4:
                default:
                    result = Float.TYPE;
                    break;
            }
        }
        return result;
    }

    @Override
    public Class<?> getType() {
        Class<?> result = null;
        if (h5Item != null) {
            // Take H5CompoundDS into account
            Datatype dType = getDatatype();
            if (dType != null) {
                int datatype = dType.getDatatypeClass();
                switch (datatype) {
                    case Datatype.CLASS_BITFIELD:
                        result = Boolean.TYPE;
                        break;
                    case Datatype.CLASS_CHAR:
                        result = Byte.TYPE;
                        break;
                    case Datatype.CLASS_FLOAT:
                        result = getFloatType(dType);
                        break;
                    case Datatype.CLASS_INTEGER:
                        result = getIntegerType(dType, null);
                        break;
                    case Datatype.CLASS_STRING:
                        result = String.class;
                        break;
                    case Datatype.CLASS_ARRAY:
                        // Manage H5CompoundDS
                        // Read the first data only
                        try {
                            int[] origin = new int[] { 0 };
                            int[] shape = new int[] { 0 };
                            Object h5Data = getData(origin, shape);
                            Class<?> recoverDataType = ArrayUtils.recoverDataType(h5Data);
                            if (recoverDataType.isAssignableFrom(Double.TYPE)
                                    || recoverDataType.isAssignableFrom(Float.TYPE)) {
                                result = getFloatType(dType);
                            } else if (recoverDataType.isAssignableFrom(Long.TYPE)) {
                                result = getIntegerType(dType, Long.TYPE);
                            } else if (recoverDataType.isAssignableFrom(Integer.TYPE)) {
                                result = getIntegerType(dType, Integer.TYPE);
                            } else if (recoverDataType.isAssignableFrom(Short.TYPE)) {
                                result = getIntegerType(dType, Short.TYPE);
                            } else if (recoverDataType.isAssignableFrom(Byte.TYPE)) {
                                result = getIntegerType(dType, Byte.TYPE);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        } else if (array != null) {
            result = array.getElementType();
        }
        return result;
    }

    @Override
    public String getUnitsString() {
        IAttribute attr = getAttribute(UNIT);
        String value = null;
        if (attr != null) {
            value = attr.getStringValue();
        }
        return value;
    }

    @Override
    public boolean hasCachedData() {
        throw new NotImplementedException();
    }

    @Override
    public void invalidateCache() {
        throw new NotImplementedException();
    }

    @Override
    public boolean isCaching() {
        throw new NotImplementedException();
    }

    @Override
    public boolean isMemberOfStructure() {
        throw new NotImplementedException();
    }

    @Override
    public boolean isMetadata() {
        return (getAttribute(SIGNAL) == null);
    }

    @Override
    public boolean isScalar() {
        return (getRank() == 0);
    }

    @Override
    public boolean isUnlimited() {
        return false;
    }

    @Override
    public boolean isUnsigned() {
        boolean result;
        if (unsigned == null) {
            result = false;
            if (h5Item != null) {
                // Get Datatype for H5CompoundDS or H5Scalar
                Datatype type = getDatatype();
                if (type != null) {
                    result = type.isUnsigned();
                }
            }
        } else {
            result = unsigned.booleanValue();
        }
        return result;
    }

    private Object getItemData() throws DataAccessException {
        Object result;
        if (h5Item == null) {
            // Special case where dataitem was just created without being stored in a file.
            // Also works with a virtual item.
            HdfArray data = getData();
            result = data == null ? null : data.getStorage();
        } else {
            result = getItemData(HdfObjectUtils.RETRY_COUNT, null);
        }
        return result;
    }

    private Object getItemData(int count, Exception previousException) throws DataAccessException {
        Object result = null;
        if (count < 0) {
            if (previousException != null) {
                throw HdfObjectUtils.toDataAccessException(previousException);
            }
        } else {
            try {
                // result = h5Item.getData();
                // Get data for H5CompoundDS or H5Scalar
                result = getH5Data();
            } catch (Exception e) {
                if (prepareRetry()) {
                    result = getItemData(count - 1, e);
                }
            }
        }
        return result;
    }

    @Override
    public byte readScalarByte() throws DataAccessException {
        try {
            Object data = getItemData();
            return data == null ? 0 : Array.getByte(data, 0);
        } catch (DataAccessException e) {
            throw e;
        } catch (Exception e) {
            throw new DataAccessException(e);
        }
    }

    @Override
    public short readScalarShort() throws DataAccessException {
        try {
            Object data = getItemData();
            return data == null ? 0 : Array.getShort(data, 0);
        } catch (DataAccessException e) {
            throw e;
        } catch (Exception e) {
            throw new DataAccessException(e);
        }
    }

    @Override
    public int readScalarInt() throws DataAccessException {
        try {
            Object data = getItemData();
            return data == null ? 0 : Array.getInt(data, 0);
        } catch (DataAccessException e) {
            throw e;
        } catch (Exception e) {
            throw new DataAccessException(e);
        }
    }

    @Override
    public long readScalarLong() throws DataAccessException {
        try {
            Object data = getItemData();
            return data == null ? 0 : Array.getLong(data, 0);
        } catch (DataAccessException e) {
            throw e;
        } catch (Exception e) {
            throw new DataAccessException(e);
        }
    }

    @Override
    public float readScalarFloat() throws DataAccessException {
        try {
            Object data = getItemData();
            return data == null ? Float.NaN : Array.getFloat(data, 0);
        } catch (DataAccessException e) {
            throw e;
        } catch (Exception e) {
            throw new DataAccessException(e);
        }
    }

    @Override
    public double readScalarDouble() throws DataAccessException {
        try {
            Object data = getItemData();
            return data == null ? MathConst.NAN_FOR_NULL : Array.getDouble(data, 0);
        } catch (DataAccessException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String readScalarString() throws DataAccessException {
        try {
            // Scalar Strings are 1 dimension String arrays
            Object data = getItemData();
            return data == null ? null : ((String[]) data)[0];
        } catch (Exception e) {
            throw new DataAccessException(e);
        }
    }

    @Override
    public int prepareForReading(int rank) {
        // Only 1 data can be read in a single block
        return 1;
    }

    @Override
    public boolean isUnsafeReadingEnabled() {
        // No unsafe reading allowed
        return false;
    }

    @Override
    public void setUnsafeReadingEnabled(boolean unsafeReadingEnabled) {
        // Not managed: no unsafe reading allowed
    }

    @Override
    public void finalizeReading() {
        // Nothing to do
    }

    @Override
    public boolean removeAttribute(final IAttribute a) {
        return HdfObjectUtils.removeAttribute(h5Item, a);
    }

    @Override
    public void setCachedData(final IArray cacheData, final boolean isMetadata) throws InvalidArrayTypeException {
        if (cacheData instanceof HdfArray) {
            array = (HdfArray) cacheData;
            dirty = true;
        }
    }

    @Override
    public void setCaching(final boolean caching) {
        throw new NotImplementedException();
    }

    @Override
    public void setDataType(final Class<?> dataType) {
        throw new NotImplementedException();
    }

    @Override
    public void setDimensions(final String dimString) {
        throw new NotImplementedException();
    }

    @Override
    public void setDimension(final IDimension dim, final int ind) throws DimensionNotSupportedException {
        throw new NotImplementedException();
    }

    @Override
    public void setElementSize(final int elementSize) {
        throw new NotImplementedException();
    }

    @Override
    public void setSizeToCache(final int sizeToCache) {
        throw new NotImplementedException();
    }

    @Override
    public void setUnitsString(final String units) {
        throw new NotImplementedException();
    }

    @Override
    public String toString() {
        StringBuilder buffer = new StringBuilder(DATA_ITEM);
        buffer.append(getName());
        return buffer.toString();
    }

    @Override
    public String toStringDebug() {
        StringBuilder strDebug = new StringBuilder();
        strDebug.append(getName());
        if (strDebug.length() > 0) {
            strDebug.append(NEW_LINE);
        }
        try {
            strDebug.append(SHAPE).append(getData().shapeToString()).append(NEW_LINE);
            List<IDimension> dimensions = getDimensionList();
            for (IDimension dim : dimensions) {
                strDebug.append(dim.getCoordinateVariable().toString());
            }

            Collection<HdfAttribute> list = this.attributes;
            if (list.size() > 0) {
                strDebug.append(ATTRIBUTES);
            }
            for (HdfAttribute a : list) {
                strDebug.append(ATTRIBUTE_START).append(a).append(NEW_LINE);
            }
        } catch (DataAccessException e) {
        }
        return strDebug.toString();
    }

    /**
     * Prepares this {@link HdfDataItem} for being written by parts (if any preparation needed).
     */
    public void startPartialWriteMode() {
        this.partialWriteMode = true;
    }

    /**
     * Sets this {@link HdfDataItem} back to classic data writing (if any preparation needed).
     */
    public void stopPartialWriteMode() {
        this.partialWriteMode = false;
    }

    public void save(final H5File fileToWrite, final Group parentInFile) throws DataAccessException {
        boolean saveInDifferentFile = false;
        if (parentInFile == null) {
            throw new DataAccessException("Can not save a dataitem with no parent");
        }
        // Save if it's dirty
        boolean doSave = dirty;

        saveInDifferentFile = !ObjectUtils.sameObject(lastPath, getPath(fileToWrite));

        // But save also if when we are going to write in a new file
        if (!dirty) {
            // If this dataset exists in a file ( = !dirty)
            // then we only save it if the destination file is a different file
            doSave = saveInDifferentFile;
        }
        if (doSave) {
            if (linkedItem == null) {
                try {
                    // Default Value
                    long[] shape = { 0, 0 };
                    if (array == null) {
                        array = getData();
                    }
                    // shape can be determined by IArray
                    if (array != null) {
                        shape = HdfObjectUtils.convertIntToLong(array.getShape());
                    } else if (h5Item != null) {
                        // shape can be determined by h5Item
                        // Take H5CompoundDS in account
                        shape = getDims();
                    }
                    // Datatype can be determined by IArray
                    Datatype datatype = null;
                    if (array == null) {
                        if (h5Item == null) {
                            int type_id = HdfObjectUtils.getNativeHdfDataTypeForClass(Integer.class, unsigned);
                            datatype = new H5Datatype(type_id);
                        } else {
                            // Datatype can be determined by the H5 item itself
                            // Get datatype for H5CompoundDS or H5Scalar
                            datatype = getDatatype();
                        }
                    } else {
                        if (array.getStorage() instanceof String[]) {
                            datatype = new H5Datatype(Datatype.CLASS_STRING, -1, -1, -1);
                        } else {
                            int type_id = HdfObjectUtils.getNativeHdfDataTypeForClass(array.getElementType(), unsigned);
                            datatype = new H5Datatype(type_id);
                        }
                    }
                    H5ScalarDS ds = null;
                    if (!saveInDifferentFile) {
                        ds = (H5ScalarDS) fileToWrite.get(getName());

                        if ((ds != null) && (h5Item != null)) {
                            ds.init();
                        }
                    }

                    if (ds == null) {
                        try {
                            ds = (H5ScalarDS) fileToWrite.createScalarDS(getName(), parentInFile, datatype, shape, null,
                                    null, 0, null);
                        } catch (Exception hdf5te) {
                            if (!hdf5te.getMessage().contains("Object already exists")) {
                                Factory.getLogger().error("Unable to create HDF5 dataset!", hdf5te);

                            }
                        }
                    }
                    if ((ds != null) && (array != null) && (shape != null)) {
                        try {
                            if ((array.getStorage() != null) && partialWriteMode) {
                                long[] startDims = ds.getStartDims();
                                long[] selectedDims = ds.getSelectedDims();

                                Arrays.fill(selectedDims, 1);
                                Arrays.fill(startDims, 0);

                                IIndex index = array.getIndex();
                                long[] origin = HdfObjectUtils.convertIntToLong(index.getOrigin());
                                System.arraycopy(shape, 0, selectedDims, selectedDims.length - shape.length,
                                        shape.length);
                                System.arraycopy(origin, 0, startDims, 0, origin.length);
                                ds.write(array.getStorage());
                            } else {
                                ds.write(array.getStorage());
                            }
                        } catch (HDF5Exception h5e) {
                            Factory.getLogger().error("Unable to write data!", h5e);
                            throw HdfObjectUtils.toDataAccessException(h5e);
                        }
                    }

                    Collection<HdfAttribute> attributes = this.attributes;
                    for (HdfAttribute attribute : attributes) {
                        attribute.save(ds, saveInDifferentFile);
                    }

                    if ((lastPath == null) || (!saveInDifferentFile)) {
                        this.h5Item = ds;
                        this.lastPath = getPath(fileToWrite);
                    }
                    this.dirty = false;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    HObject link = fileToWrite.createLink(parentInFile, getShortName(), linkedItem.h5Item,
                            HDF5Constants.H5L_TYPE_SOFT);
                    Collection<HdfAttribute> attribute = this.attributes;
                    for (HdfAttribute attr : attribute) {
                        attr.save(link, true);
                    }
                } catch (Exception e) {
                    throw HdfObjectUtils.toDataAccessException(e);
                }
            }
        } else {
            Collection<HdfAttribute> attributes = this.attributes;
            for (IAttribute attribute : attributes) {
                HdfAttribute attr = (HdfAttribute) attribute;
                attr.save(h5Item, saveInDifferentFile);
            }
        }
    }

}

package org.cdma.plugin.ansto;

import java.io.File;
import java.io.IOException;
import java.net.URI;

import org.cdma.Factory;
import org.cdma.dictionary.LogicalGroup;
import org.cdma.engine.netcdf.navigation.NcDataset;

public class AnstoDataset extends NcDataset {

    public static AnstoDataset instantiate(URI uri) {
        AnstoDataset result = null;
        if (uri != null) {
            String path = uri.getPath();
            File file = new File(path);
            if (file.exists() && !file.isDirectory()) {
                try {
                    result = new AnstoDataset(path);
                } catch (IOException e) {
                    Factory.getLogger().error("Unable to instantiate dataset:\n" + e.getMessage());
                }
            }
        }

        return result;
    }

    public AnstoDataset(String path) throws IOException {
        super(path, AnstoFactory.NAME);
    }

    @Override
    public LogicalGroup getLogicalRoot(String view) {
        return new LogicalGroup(view, null, this);
    }

}

/*******************************************************************************
 * Copyright (c) 2008 - ANSTO/Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * Norman Xiong (nxi@Bragg Institute) - initial API and implementation
 * Tony Lam (nxi@Bragg Institute) - initial API and implementation
 * Majid Ounsy (SOLEIL Synchrotron) - API v2 design and conception
 * Stephane Poirier (SOLEIL Synchrotron) - API v2 design and conception
 * Clement Rodriguez (ALTEN for SOLEIL Synchrotron) - API evolution
 * Gregory VIGUIER (SOLEIL Synchrotron) - API evolution
 * Raphael GIRARDOT (SOLEIL Synchrotron) - API evolution
 ******************************************************************************/
package org.cdma.plugin.soleil.nexus.utils;

import org.cdma.engine.hdf.utils.HdfNode;
import org.cdma.interfaces.IAttribute;
import org.cdma.interfaces.IContainer;
import org.cdma.interfaces.INode;

import fr.soleil.lib.project.ObjectUtils;

public class NxsNode extends HdfNode {

    private final String clazz;

    public NxsNode(final String name, final String className) {
        super(name);
        this.clazz = className;
    }

    public NxsNode(final IContainer container) {
        super(container);
        IAttribute attribute = container.getAttribute(NxsConstant.NX_CLASS);
        if (attribute == null) {
            this.clazz = ObjectUtils.EMPTY_STRING;
        } else {
            this.clazz = attribute.getStringValue();
        }
    }

    public String getClassName() {
        return clazz;
    }

    @Override
    public String toString() {
        return getName() + ATTRIBUTE_SEPARATOR_START2 + this.clazz + CLASS_END2;
    }

    @Override
    public boolean matchesNode(final INode node) {
        boolean classMatch, nameMatch;
        NxsNode nxsNode = (NxsNode) node;
        classMatch = nxsNode.getClassName().isEmpty() || nxsNode.getClassName().equalsIgnoreCase(this.clazz);
        nameMatch = node.getNodeName().isEmpty() || this.getNodeName().equals(node.getNodeName());

        return (classMatch && nameMatch);
    }

    protected boolean matches(String str1, String str2) {
        return str1.matches(str2) || str1.equals(str2);
    }

    @Override
    public boolean matchesPartNode(final INode node) {
        boolean classMatch = false, nameMatch = false;
        NxsNode nxsNode = (NxsNode) node;

        if (node != null) {
            classMatch = nxsNode.getClassName().isEmpty() || nxsNode.getClassName().equalsIgnoreCase(this.clazz);
            nameMatch = node.getNodeName().isEmpty()
                    || matches(this.getNodeName().replace("*", ".*"), node.getNodeName().replace("*", ".*"));
        }
        return (classMatch && nameMatch);
    }

    public static NxsNode createNode(String fullName) {
        String className = extractClass(fullName);
        String name;
        if (isValidClassName(className)) {
            name = extractName(fullName);
        } else {
            name = fullName;
            className = ObjectUtils.EMPTY_STRING;
        }
        return new NxsNode(name, className);
    }

}

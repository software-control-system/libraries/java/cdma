/*******************************************************************************
 * Copyright (c) 2008 - ANSTO/Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * Norman Xiong (nxi@Bragg Institute) - initial API and implementation
 * Tony Lam (nxi@Bragg Institute) - initial API and implementation
 * Majid Ounsy (SOLEIL Synchrotron) - API v2 design and conception
 * Stephane Poirier (SOLEIL Synchrotron) - API v2 design and conception
 * Clement Rodriguez (ALTEN for SOLEIL Synchrotron) - API evolution
 * Gregory VIGUIER (SOLEIL Synchrotron) - API evolution
 * Raphael GIRARDOT (SOLEIL Synchrotron) - API evolution
 ******************************************************************************/
package org.cdma.plugin.soleil.nexus.array;

import java.util.concurrent.atomic.AtomicLong;

import org.cdma.Factory;
import org.cdma.arrays.DefaultArrayIterator;
import org.cdma.arrays.DefaultSliceIterator;
import org.cdma.engine.hdf.array.HdfArray;
import org.cdma.engine.hdf.array.HdfIndex;
import org.cdma.exception.BackupException;
import org.cdma.exception.InvalidArrayTypeException;
import org.cdma.exception.InvalidRangeException;
import org.cdma.exception.NotImplementedException;
import org.cdma.exception.ShapeNotMatchException;
import org.cdma.interfaces.IArray;
import org.cdma.interfaces.IArrayIterator;
import org.cdma.interfaces.IIndex;
import org.cdma.interfaces.ISliceIterator;
import org.cdma.math.IArrayMath;
import org.cdma.plugin.soleil.nexus.NxsFactory;
import org.cdma.plugin.soleil.nexus.utils.NxsArrayUtils;
import org.cdma.utils.ArrayTools;
import org.slf4j.LoggerFactory;

import fr.soleil.lib.project.date.DateUtil;
import fr.soleil.lib.project.math.MathConst;

public final class NxsArray implements IArray {

    private static final AtomicLong INDEX_RECOVERING_TIME = new AtomicLong(0);
    private static final AtomicLong INDEX_SIZE_TIME = new AtomicLong(0);
    private static final AtomicLong FILL_ARRAY_TIME = new AtomicLong(0);
    private static final AtomicLong ELEMENT_TIME = new AtomicLong(0);
    private static final AtomicLong GET_STORAGE_TIME = new AtomicLong(0);

    public static void resetMetrics() {
        INDEX_RECOVERING_TIME.set(0);
        INDEX_SIZE_TIME.set(0);
        FILL_ARRAY_TIME.set(0);
        ELEMENT_TIME.set(0);
        GET_STORAGE_TIME.set(0);
        HdfArray.resetMetrics();
    }

    public static void traceMetrics(String applicationId) {
        StringBuilder builder = new StringBuilder(NxsArray.class.getSimpleName());
        DateUtil.elapsedTimeToStringBuilder("\nNxsArray Cumulative index recovering time: ", INDEX_RECOVERING_TIME,
                builder);
        DateUtil.elapsedTimeToStringBuilder("\nNxsArray Cumulative index size recovering time: ", INDEX_SIZE_TIME,
                builder);
        DateUtil.elapsedTimeToStringBuilder("\nNxsArray Cumulative storage access time: ", GET_STORAGE_TIME, builder);
        DateUtil.elapsedTimeToStringBuilder("\nNxsArray Cumulative array filling time: ", FILL_ARRAY_TIME, builder);
        DateUtil.elapsedTimeToStringBuilder("\nNxsArray Cumulative element access time: ", ELEMENT_TIME, builder);
        LoggerFactory.getLogger(applicationId).trace(builder.toString());
        HdfArray.traceMetrics(applicationId);
    }

    private Object data; // It's an array of values
    private NxsIndex index; // IIndex corresponding to mArray shape
    private final HdfArray[] arrays; // array of IArray

    public String shortName;

    private volatile boolean profile;

    public NxsArray(HdfArray... arrays) {
        this.arrays = arrays == null ? null : arrays.clone();
        data = null;
        profile = false;
        initDimSize();
        if ((index != null) && (this.arrays != null)) {
            // Define the same viewable part for all sub-IArray
            HdfIndex hdfIndex = index.getIndexStorage();
            for (HdfArray array : this.arrays) {
                if (array != null) {
                    array.setIndex(hdfIndex.clone());
                }
            }
        }
    }

    public NxsArray(NxsArray array) {
        profile = false;
        if (array == null) {
            index = null;
            data = null;
            arrays = null;
            Factory.getLogger().warn("Null NxsArray");
        } else {
            index = (array.index == null ? null : array.index.clone());
            data = array.data;
            if (index == null) {
                arrays = null;
                Factory.getLogger().warn("Null index");
            } else {
                IIndex storedIndex = index.getIndexStorage();
                if (array.arrays == null) {
                    arrays = null;
                } else {
                    arrays = new HdfArray[array.arrays.length];
                    for (int i = 0; i < array.arrays.length; i++) {
                        if (array.arrays[i] != null) {
                            arrays[i] = array.arrays[i].copy(false);
                            arrays[i].setIndex(storedIndex);
                        }
                    }
                }
            }
        }
    }

    public NxsArray(Object oArray, int[] iShape) throws InvalidArrayTypeException {
        this(new HdfArray[] { new HdfArray(NxsFactory.NAME, oArray, iShape) });
    }

    public void setProfile(boolean trace) {
        this.profile = trace;
        if (arrays != null) {
            for (HdfArray array : arrays) {
                // XXX array should not be null
                if (array != null) {
                    array.setProfile(trace);
                }
            }
        }
    }

    public boolean isProfile() {
        return profile;
    }

    @Override
    public Object getObject(IIndex index) {
        return this.get(index);
    }

    @Override
    public IArray copy() {
        return copy(true);
    }

    @Override
    public NxsArray copy(boolean dataToo) {
        NxsArray result = new NxsArray(this);
        if (dataToo) {
            result.data = ArrayTools.copyJavaArray(data);
        }
        return result;
    }

    @Override
    public IArrayMath getArrayMath() {
        return NxsFactory.createArrayMath(this);
    }

    @Override
    public NxsArrayUtils getArrayUtils() {
        NxsArrayUtils utils = new NxsArrayUtils(this);
        utils.setProfile(profile);
        return utils;
    }

    @Override
    public boolean getBoolean(IIndex ima) {
        IndexNode ind = getIndexNode(ima);
        IIndex itemIdx = ind.getIndex();
        int nodeIndex = ind.getNode();
        IArray array = ((arrays == null) || (nodeIndex < 0) || (nodeIndex >= arrays.length)) ? null : arrays[nodeIndex];
        return array == null ? false : array.getBoolean(itemIdx);
    }

    @Override
    public byte getByte(IIndex ima) {
        IndexNode ind = getIndexNode(ima);
        IIndex itemIdx = ind.getIndex();
        int nodeIndex = ind.getNode();
        IArray array = ((arrays == null) || (nodeIndex < 0) || (nodeIndex >= arrays.length)) ? null : arrays[nodeIndex];
        return array == null ? 0 : array.getByte(itemIdx);
    }

    @Override
    public char getChar(IIndex ima) {
        IndexNode ind = getIndexNode(ima);
        IIndex itemIdx = ind.getIndex();
        int nodeIndex = ind.getNode();
        IArray array = ((arrays == null) || (nodeIndex < 0) || (nodeIndex >= arrays.length)) ? null : arrays[nodeIndex];
        return array == null ? 0 : array.getChar(itemIdx);
    }

    @Override
    public double getDouble(IIndex ima) {
        IndexNode ind = getIndexNode(ima);
        IIndex itemIdx = ind.getIndex();
        int nodeIndex = ind.getNode();
        IArray array = ((arrays == null) || (nodeIndex < 0) || (nodeIndex >= arrays.length)) ? null : arrays[nodeIndex];
        return array == null ? MathConst.NAN_FOR_NULL : array.getDouble(itemIdx);
    }

    @Override
    public float getFloat(IIndex ima) {
        IndexNode ind = getIndexNode(ima);
        IIndex itemIdx = ind.getIndex();
        int nodeIndex = ind.getNode();
        IArray array = ((arrays == null) || (nodeIndex < 0) || (nodeIndex >= arrays.length)) ? null : arrays[nodeIndex];
        return array == null ? Float.NaN : array.getFloat(itemIdx);
    }

    @Override
    public int getInt(IIndex ima) {
        IndexNode ind = getIndexNode(ima);
        IIndex itemIdx = ind.getIndex();
        int nodeIndex = ind.getNode();
        IArray array = ((arrays == null) || (nodeIndex < 0) || (nodeIndex >= arrays.length)) ? null : arrays[nodeIndex];
        return array == null ? 0 : array.getInt(itemIdx);
    }

    @Override
    public long getLong(IIndex ima) {
        IndexNode ind = getIndexNode(ima);
        IIndex itemIdx = ind.getIndex();
        int nodeIndex = ind.getNode();
        IArray array = ((arrays == null) || (nodeIndex < 0) || (nodeIndex >= arrays.length)) ? null : arrays[nodeIndex];
        return array == null ? 0L : array.getLong(itemIdx);
    }

    @Override
    public short getShort(IIndex ima) {
        IndexNode ind = getIndexNode(ima);
        IIndex itemIdx = ind.getIndex();
        int nodeIndex = ind.getNode();
        IArray array = ((arrays == null) || (nodeIndex < 0) || (nodeIndex >= arrays.length)) ? null : arrays[nodeIndex];
        return array == null ? 0 : array.getShort(itemIdx);
    }

    @Override
    public Class<?> getElementType() {
        Class<?> result = null;
        if ((arrays != null) && (arrays.length > 0) && (arrays[0] != null)) {
            result = arrays[0].getElementType();
        }
        return result;
    }

    @Override
    public NxsIndex getIndex() {
        return index == null ? null : index.clone();
    }

    @Override
    public IArrayIterator getIterator() {
        NxsIndex index = getIndex();
        IArrayIterator iterator;
        if (index == null) {
            iterator = null;
            Factory.getLogger().warn("Can't get iterator: Null index");
        } else {
            index.set(new int[index.getRank()]);
            iterator = new DefaultArrayIterator(this, index);
        }
        return iterator;
    }

    @Override
    public int getRank() {
        return index == null ? -1 : index.getRank();
    }

    @Override
    public IArrayIterator getRegionIterator(int[] reference, int[] range) throws InvalidRangeException {
        IArrayIterator iterator;
        if (index == null) {
            iterator = null;
            Factory.getLogger().warn("Null iterator");
        } else {
            int[] shape = index.getShape();
            if (shape == null) {
                iterator = null;
                Factory.getLogger().warn("Null index shape");
            } else {
                IIndex hdfIndex = new HdfIndex(NxsFactory.NAME, shape, reference, range);
                iterator = new DefaultArrayIterator(this, hdfIndex);
            }
        }
        return iterator;
    }

    @Override
    public int[] getShape() {
        int[] result = index == null ? null : index.getShape();
        return result;
    }

    @Override
    public long getSize() {
        return index == null ? -1L : index.getSize();
    }

    @Override
    public Object getStorage() {
        Object result = data;
        if (index != null) {
            long time = -1, indexRecoveringTime = -1, tmpTime = -1, sizeTime = -1;
            if (profile) {
                time = System.currentTimeMillis();
            }
            IIndex matrixIndex = index.getIndexMatrix().clone();
            if (profile) {
                indexRecoveringTime = System.currentTimeMillis() - time;
                time = System.currentTimeMillis();
            }
            int rank = matrixIndex.getRank();
            if ((this.data == null) && (this.arrays != null)) {
                Object[] array;
                int offset;
                if (rank != 0) {
                    // Case of several NexusArray in the visible part of the matrix
                    int[] pos = new int[1];
                    int size = (int) matrixIndex.getSize();
                    if (profile) {
                        sizeTime = System.currentTimeMillis() - time;
                        time = System.currentTimeMillis();
                    }
                    array = new Object[size];
                    for (int i = 0; i < size; i++) {
                        pos[0] = i;
                        matrixIndex.set(pos);
                        if (profile) {
                            tmpTime = System.currentTimeMillis();
                        }
                        offset = (int) matrixIndex.currentElement();
                        if (profile) {
                            long elementTime = System.currentTimeMillis() - tmpTime;
                            ELEMENT_TIME.addAndGet(elementTime);
                        }
                        if ((offset < 0) || (offset >= arrays.length)) {
                            array = null;
                            break;
                        }
                        if (arrays[offset] != null) {
                            if (profile) {
                                tmpTime = System.currentTimeMillis();
                            }
                            array[i] = arrays[offset].getStorage();
                            if (profile) {
                                long storageTime = System.currentTimeMillis() - tmpTime;
                                GET_STORAGE_TIME.addAndGet(storageTime);
                            }
                        }
                    }
                    if (profile) {
                        long fillTime = System.currentTimeMillis() - time;
                        INDEX_SIZE_TIME.addAndGet(sizeTime);
                        FILL_ARRAY_TIME.addAndGet(fillTime);
                    }
                } else {
                    // Case of one NexusArray in the visible part of the matrix
                    if (profile) {
                        time = System.currentTimeMillis();
                    }
                    offset = (int) matrixIndex.currentElement();
                    if (profile) {
                        long elementTime = System.currentTimeMillis() - time;
                        ELEMENT_TIME.addAndGet(elementTime);
                    }
                    if ((offset < 0) || (arrays == null) || (offset >= arrays.length)) {
                        array = null;
                    } else {
                        array = new Object[1];
                        if (arrays[offset] != null) {
                            if (profile) {
                                time = System.currentTimeMillis();
                            }
                            array[0] = arrays[offset].getStorage();
                            if (profile) {
                                long storageTime = System.currentTimeMillis() - time;
                                GET_STORAGE_TIME.addAndGet(storageTime);
                            }
                        }
                    }
                }
                result = array;
            }
            if (profile) {
                INDEX_RECOVERING_TIME.addAndGet(indexRecoveringTime);
            }
        }
        return result;
    }

    @Override
    public void setBoolean(IIndex ima, boolean value) {
        set(ima, value);
    }

    @Override
    public void setByte(IIndex ima, byte value) {
        set(ima, value);
    }

    @Override
    public void setChar(IIndex ima, char value) {
        set(ima, value);
    }

    @Override
    public void setDouble(IIndex ima, double value) {
        set(ima, value);
    }

    @Override
    public void setFloat(IIndex ima, float value) {
        set(ima, value);
    }

    @Override
    public void setInt(IIndex ima, int value) {
        set(ima, value);
    }

    @Override
    public void setLong(IIndex ima, long value) {
        set(ima, value);
    }

    @Override
    public void setObject(IIndex ima, Object value) {
        set(ima, value);
    }

    @Override
    public void setShort(IIndex ima, short value) {
        set(ima, value);
    }

    @Override
    public String shapeToString() {
        int[] shape = getShape();
        StringBuilder sb = new StringBuilder();
        if (shape.length != 0) {
            sb.append('(');
            for (int i = 0; i < shape.length; i++) {
                int s = shape[i];
                if (i > 0) {
                    sb.append(",");
                }
                sb.append(s);
            }
            sb.append(')');
        }
        return sb.toString();
    }

    @Override
    public void setIndex(IIndex indexToSet) {
        if (indexToSet instanceof NxsIndex) {
            index = (NxsIndex) indexToSet;
        } else if (index == null) {
            Factory.getLogger().warn("Can't convert index to NxsIndex due to null previous index");
        } else {
            index = new NxsIndex(index.getIndexMatrix().getRank(), indexToSet.getShape(), indexToSet.getOrigin(),
                    indexToSet.getShape());
            index.set(indexToSet.getCurrentCounter());
        }
        if ((arrays != null) && (index != null)) {
            for (IArray array : arrays) {
                if (array != null) {
                    array.setIndex(index.getIndexStorage());
                }
            }
        }
    }

    @Override
    public ISliceIterator getSliceIterator(int rank) throws ShapeNotMatchException, InvalidRangeException {
        return new DefaultSliceIterator(this, rank);
    }

    @Override
    public void releaseStorage() throws BackupException {
        throw new NotImplementedException();
    }

    @Override
    public long getRegisterId() {
        throw new NotImplementedException();
    }

    @Override
    public void lock() {
        throw new NotImplementedException();
    }

    @Override
    public void unlock() {
        throw new NotImplementedException();
    }

    @Override
    public boolean isDirty() {
        throw new NotImplementedException();
    }

    @Override
    public IArray setDouble(double value) {
        throw new NotImplementedException();
    }

    @Override
    public String getFactoryName() {
        return NxsFactory.NAME;
    }

    @Override
    public String toString() {
        return index == null ? super.toString() : index.toString();
    }

    @Override
    public void setDirty(boolean dirty) {
        throw new NotImplementedException();
    }

    /**
     * @return this array sub-parts if any
     */
    public IArray[] getArrayParts() {
        return arrays;
    }

    // ---------------------------------------------------------
    // / Private methods
    // ---------------------------------------------------------
    /**
     * InitDimSize
     * Initialize member dimension sizes 'm_iDimS' according to defined member data 'm_oData'
     */
    private void initDimSize() {
        // Check data existence
        if ((arrays != null) && (arrays.length > 0) && (arrays[0] != null)) {
            // Set dimension rank
            int matrixRank = arrays.length > 1 ? 1 : 0;
            int[] shape = new int[matrixRank + arrays[0].getRank()];
            // Fill dimension size array
            if (matrixRank > 0) {
                shape[0] = arrays.length;
            }
            int i = 0;
            for (int size : arrays[0].getShape()) {
                shape[i + matrixRank] = size;
                i++;
            }
            index = new NxsIndex(matrixRank, shape, new int[shape.length], shape);
        }
    }

    /**
     * Get the object targeted by given index and return it (eventually using auto-boxing).
     * It's the central data access method that all other methods rely on.
     * 
     * @param index targeting a cell
     * @return the content of cell designed by the index
     * @throws InvalidRangeException if one of the index is bigger than the corresponding dimension shape
     */
    private Object get(IIndex index) {
        Object result = null;
        IndexNode ind = getIndexNode(index);
        IIndex itemIdx = ind.getIndex();

        int nodeIndex = ind.getNode();
        IArray slab = ((arrays == null) || (nodeIndex < 0) || (nodeIndex >= arrays.length)) ? null : arrays[nodeIndex];
        if (slab != null) {
            result = slab.getObject(itemIdx);
        }
        return result;
    }

    private IndexNode getIndexNode(IIndex refIndex) {
        IndexNode node;
        if (index == null) {
            Factory.getLogger().warn("Can't get indexNode due to null index");
            node = null;
        } else {
            int nodeIndex;
            IIndex itemIdx;
            if ((arrays != null) && (arrays.length > 1)) {
                NxsIndex idx;
                if (refIndex instanceof NxsIndex) {
                    idx = (NxsIndex) refIndex;
                } else {
                    int rank = index.getIndexMatrix().getRank();
                    idx = new NxsIndex(rank, index.getShape(), refIndex.getOrigin(), refIndex.getShape());
                    idx.set(refIndex.getCurrentCounter());
                }
                nodeIndex = (int) idx.currentElementMatrix();
                itemIdx = index.getIndexStorage().clone();
                itemIdx.set(idx.getIndexStorage().getCurrentCounter());
            } else {
                nodeIndex = 0;
                itemIdx = index.getIndexStorage().clone();
                itemIdx.set(refIndex.getCurrentCounter());
            }
            node = new IndexNode(itemIdx, nodeIndex);
        }
        return node;
    }

    private void set(IIndex indexToSet, Object value) {
        NxsIndex idx = null;
        if (indexToSet instanceof NxsIndex) {
            idx = (NxsIndex) indexToSet;
        } else if (index == null) {
            Factory.getLogger().warn("Can't adapt index to NxsIndex due to null previous index");
        } else {
            idx = new NxsIndex(index.getIndexMatrix().getRank(), indexToSet);
        }
        if ((idx != null) && (arrays != null)) {
            HdfIndex itemIdx = idx.getIndexStorage();
            long nodeIndex = idx.currentElementMatrix();
            if ((nodeIndex > -1) && (nodeIndex < arrays.length)) {
                IArray slab = arrays[(int) nodeIndex];
                if (slab != null) {
                    slab.setObject(itemIdx, value);
                }
            }
        }
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    private class IndexNode {
        private final IIndex index;
        private final int node;

        public IndexNode(IIndex index, int node) {
            this.index = index;
            this.node = node;
        }

        public IIndex getIndex() {
            return index;
        }

        public int getNode() {
            return node;
        }
    }

}

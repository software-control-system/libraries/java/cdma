/*******************************************************************************
 * Copyright (c) 2008 - ANSTO/Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * Norman Xiong (nxi@Bragg Institute) - initial API and implementation
 * Tony Lam (nxi@Bragg Institute) - initial API and implementation
 * Majid Ounsy (SOLEIL Synchrotron) - API v2 design and conception
 * Stephane Poirier (SOLEIL Synchrotron) - API v2 design and conception
 * Clement Rodriguez (ALTEN for SOLEIL Synchrotron) - API evolution
 * Gregory VIGUIER (SOLEIL Synchrotron) - API evolution
 * Raphael GIRARDOT (SOLEIL Synchrotron) - API evolution
 ******************************************************************************/
package org.cdma.plugin.soleil.nexus.navigation;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import org.cdma.Factory;
import org.cdma.engine.hdf.array.HdfArray;
import org.cdma.engine.hdf.navigation.HdfDataItem;
import org.cdma.exception.CDMAException;
import org.cdma.exception.DataAccessException;
import org.cdma.exception.InvalidArrayTypeException;
import org.cdma.exception.InvalidRangeException;
import org.cdma.exception.NotImplementedException;
import org.cdma.interfaces.IArray;
import org.cdma.interfaces.IAttribute;
import org.cdma.interfaces.IDataItem;
import org.cdma.interfaces.IDataset;
import org.cdma.interfaces.IDimension;
import org.cdma.interfaces.IGroup;
import org.cdma.interfaces.IRange;
import org.cdma.plugin.soleil.nexus.NxsFactory;
import org.cdma.plugin.soleil.nexus.array.NxsArray;
import org.cdma.plugin.soleil.nexus.array.NxsIndex;
import org.cdma.plugin.soleil.nexus.utils.NxsPath;
import org.cdma.utils.MetricsUtils;
import org.cdma.utils.Utilities.ModelType;
import org.slf4j.LoggerFactory;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.date.DateUtil;
import fr.soleil.lib.project.math.ArrayUtils;

public final class NxsDataItem extends ANxsContainer<HdfDataItem> implements IDataItem {

    private static final String NEW_LINE = "\n";
    private static final String SHAPE = "shape: ";
    private static final String ATTRIBUTES = "\nAttributes:\n";
    private static final String TOKEN = "- ";
    private static final String SEPARATOR = ": ";
    private static final String VS = " VS ";
    private static final String SHAPE_MUST_BE_OF_SAME_RANK_AS_THE_ARRAY = "Shape must be of same rank as the array!";
    private static final String ORIGIN_MUST_BE_OF_SAME_RANK_AS_THE_ARRAY = "Origin must be of same rank as the array!";
    private static final String DIM_START = "(";
    private static final String DIM_SEPARATOR = ", ";
    private static final String DIM_END = ")";
    private static final String SIGNAL = "signal";
    private static final String UNIT = "unit";
    private static final String SPACE = " ";
    private static final String AXIS = "axis";
    private static final String ANY = "*";
    private static final String REGEX_ANY = ".*";
    private static final String EQUALS = "=";

    private static final AtomicLong DATASET_CHECK_TIME = new AtomicLong(0);
    private static final AtomicLong GET_COPY_TIME = new AtomicLong(0);
    private static final AtomicLong PREPARE_DATA_TIME = new AtomicLong(0);

    public static void resetMetrics() {
        DATASET_CHECK_TIME.set(0);
        GET_COPY_TIME.set(0);
        PREPARE_DATA_TIME.set(0);
    }

    public static void traceMetrics(String applicationId) {
        StringBuilder builder = new StringBuilder(NxsDataItem.class.getSimpleName());
        DateUtil.elapsedTimeToStringBuilder("\nNxsDataItem Cumulative array copy recovering time: ", GET_COPY_TIME,
                builder);
        DateUtil.elapsedTimeToStringBuilder("\nNxsDataItem Cumulative array preparation time: ", PREPARE_DATA_TIME,
                builder);
        DateUtil.elapsedTimeToStringBuilder("\nNxsDataItem Cumulative dataset check time: ", DATASET_CHECK_TIME,
                builder);
        LoggerFactory.getLogger(applicationId).trace(builder.toString());
    }

    // / Members
    private NxsArray array; // CDMA IArray supporting a view of the data
    private List<DimOrder> dimensions; // list of dimensions

    // / Constructors
    public NxsDataItem(final String name, final NxsDataset handler, Boolean unsigned) {
        super(handler, null);
        hdfContainers = new HdfDataItem[] { new HdfDataItem(NxsFactory.NAME, name, unsigned) };
        dimensions = new ArrayList<DimOrder>();
        array = null;
        nxsPath = null;
    }

    public NxsDataItem(final String name, final NxsDataset handler) {
        this(name, handler, null);
    }

    public NxsDataItem(final NxsDataItem dataItem) {
        super(dataItem.dataset, dataItem.getParentGroup());
        hdfContainers = dataItem.hdfContainers.clone();
        dimensions = new ArrayList<DimOrder>(dataItem.dimensions);
        nxsPath = dataItem.nxsPath;
        try {
            if (hdfContainers.length == 1) {
                array = new NxsArray(dataItem.getData());
            } else {
                array = new NxsArray(dataItem.getData());
            }
            if (MetricsUtils.shouldProfile(getShortName())) {
                array.setProfile(true);
            }
        } catch (DataAccessException e) {
            array = null;
        }
    }

    public NxsDataItem(final HdfDataItem[] data, final IGroup parent, final NxsDataset handler) {
        super(handler, parent);
        if (data != null) {
            hdfContainers = data.clone();
        }
        dimensions = new ArrayList<DimOrder>();
        array = null;
        nxsPath = new NxsPath(this);
    }

    public NxsDataItem(final HdfDataItem item, final IGroup parent, final NxsDataset dataset) {
        this(new HdfDataItem[] { item }, parent, dataset);
    }

    public NxsDataItem(final NxsDataItem[] items, final IGroup parent, final NxsDataset dataset) {
        super(dataset, parent);
        ArrayList<HdfDataItem> list = new ArrayList<HdfDataItem>();
        for (NxsDataItem cur : items) {
            for (HdfDataItem item : cur.hdfContainers) {
                list.add(item);
            }
        }
        nxsPath = items[0].getNxsPath();
        hdfContainers = list.toArray(new HdfDataItem[list.size()]);

        dimensions = new ArrayList<DimOrder>();
        array = null;
    }

    // / Methods

    @Override
    public ModelType getModelType() {
        return ModelType.DataItem;
    }

    @Override
    public Collection<IAttribute> getAttributeList() {
        return hdfContainers[0].getAttributeList();
    }

    @Override
    public NxsArray getData() throws DataAccessException {
        // XXX checkDataset() seems mandatory to avoid a bug in which data was'nt loaded any more.
        // Tested with Foxtrot, multi threads, on a NANOSCOPIUM file, doing image integration on a ROI image,
        // with global result activated. After a while during treatment, all loaded images are filled with 0.
        // Going back to previously loaded image gives a 0 filled one too.
        // This bug seems very similar to DATAREDUC-793.
        long time = 0;
        boolean profile = MetricsUtils.shouldProfile(getShortName());
        if (profile) {
            time = System.currentTimeMillis();
        }
        checkDataset(true);
        if (profile) {
            time = System.currentTimeMillis() - time;
            DATASET_CHECK_TIME.addAndGet(time);
        }
        if ((array == null) && (hdfContainers.length > 0)) {
            HdfArray[] arrays = new HdfArray[hdfContainers.length];
            int i = 0;
            for (HdfDataItem item : hdfContainers) {
                arrays[i++] = item.getData();
            }
            array = new NxsArray(arrays);
        }
        if (array != null) {
            array.setProfile(profile);
        }
        return array;
    }

    @Override
    public NxsArray getData(final int[] origin, final int[] shape) throws DataAccessException, InvalidRangeException {
        NxsArray array;
        boolean profile = MetricsUtils.shouldProfile(getShortName());
        long time = 0;
        if (profile) {
            time = System.currentTimeMillis();
        }
        array = getData().copy(false);
        array.setProfile(profile);
        if (profile) {
            GET_COPY_TIME.addAndGet(System.currentTimeMillis() - time);
            time = System.currentTimeMillis();
        }
        NxsIndex index = array.getIndex();

        if ((shape == null) || (shape.length != array.getRank())) {
            throw new InvalidRangeException(SHAPE_MUST_BE_OF_SAME_RANK_AS_THE_ARRAY + SEPARATOR + shape.length + VS
                    + array.getRank() + SEPARATOR + getName());
        }
        if ((origin == null) || (origin.length != array.getRank())) {
            throw new InvalidRangeException(ORIGIN_MUST_BE_OF_SAME_RANK_AS_THE_ARRAY);
        }

        int str = 1;
        long[] stride = new long[array.getRank()];
        for (int i = array.getRank() - 1; i >= 0; i--) {
            stride[i] = str;
            str *= shape[i];
        }
        index.setStride(stride);
        index.setShape(shape);
        index.setOrigin(origin);
        array.setIndex(index);
        if (profile) {
            PREPARE_DATA_TIME.addAndGet(System.currentTimeMillis() - time);
        }
        return array;
    }

    @Override
    public NxsDataItem clone() {
        NxsDataItem clone = (NxsDataItem) super.clone();
        clone.hdfContainers = hdfContainers.clone();
        for (int i = 0; i < clone.hdfContainers.length; i++) {
            if (clone.hdfContainers[i] != null) {
                clone.hdfContainers[i] = clone.hdfContainers[i].clone();
            }
        }
        clone.dimensions = new ArrayList<DimOrder>(dimensions);
        clone.parent = getParentGroup();
        clone.array = null;
        clone.nxsPath = nxsPath;
        try {
            clone.array = new NxsArray(getData());
        } catch (DataAccessException e) {
            Factory.getLogger().error("Failed to clone NxsDataItem", e);
        }
        return clone;
    }

    public NxsDataItem getCopyWithFakeDimensionInsertedAt(int index) {
        NxsDataItem reshaped = clone();
        if (reshaped.hdfContainers.length > 0) {
            HdfDataItem item = reshaped.hdfContainers[0];
            if (item != null) {
                reshaped.hdfContainers[0] = item.getCopyWithFakeDimensionInsertedAt(index);
                reshaped.array = null;
            }
        }
        return reshaped;
    }

    @Override
    public void addOneAttribute(final IAttribute att) {
        hdfContainers[0].addOneAttribute(att);
    }

    @Override
    public void addStringAttribute(final String name, final String value) {
        hdfContainers[0].addStringAttribute(name, value);
    }

    @Override
    public IAttribute findAttributeIgnoreCase(final String name) {
        IAttribute result = null;
        for (IDataItem item : hdfContainers) {
            result = item.findAttributeIgnoreCase(name);
            if (result != null) {
                break;
            }
        }
        return result;
    }

    @Override
    public int findDimensionIndex(final String name) {
        int result = -1;
        for (DimOrder dimord : dimensions) {
            if (dimord.mDimension.getName().equals(name)) {
                result = dimord.order();
                break;
            }
        }
        return result;
    }

    @Override
    public String getDescription() {
        String result = null;
        for (IDataItem item : hdfContainers) {
            result = item.getDescription();
            if (result != null) {
                break;
            }
        }
        return result;
    }

    @Override
    public List<IDimension> getDimensions(final int i) {
        ArrayList<IDimension> list = null;
        if (i <= getRank()) {
            list = new ArrayList<IDimension>();
            for (DimOrder dim : dimensions) {
                if (dim.order() == i) {
                    list.add(dim.dimension());
                }
            }
        }
        return list;
    }

    @Override
    public List<IDimension> getDimensionList() {
        ArrayList<IDimension> list = new ArrayList<IDimension>();
        for (DimOrder dimOrder : dimensions) {
            list.add(dimOrder.dimension());
        }
        return list;
    }

    @Override
    public String getDimensionsString() {
        StringBuilder dimList = new StringBuilder();
        boolean first = true;
        for (DimOrder dim : dimensions) {
            if (first) {
                first = false;
            } else {
                dimList.append(SPACE);
            }
            dimList.append(dim.dimension().getName());
        }
        return dimList.toString();
    }

    @Override
    public int getElementSize() {
        return hdfContainers[0].getElementSize();
    }

    @Override
    public String getNameAndDimensions() {
        StringBuilder buf = new StringBuilder();
        getNameAndDimensions(buf, true, false);
        return buf.toString();
    }

    @Override
    public void getNameAndDimensions(final StringBuilder buf, boolean useFullName, final boolean showDimLength) {
        useFullName = useFullName && !showDimLength;
        String name = useFullName ? getName() : getShortName();
        buf.append(name);

        if (getRank() > 0) {
            buf.append(DIM_START);
        }
        for (int i = 0; i < dimensions.size(); i++) {
            DimOrder dim = dimensions.get(i);
            IDimension myd = dim.dimension();
            String dimName = myd.getName();
            if ((dimName == null) || !showDimLength) {
                dimName = ObjectUtils.EMPTY_STRING;
            }

            if (i != 0) {
                buf.append(DIM_SEPARATOR);
            }

            if (myd.isVariableLength()) {
                buf.append(ANY);
            } else if (myd.isShared()) {
                if (!showDimLength) {
                    buf.append(dimName);
                    buf.append(EQUALS);
                    buf.append(myd.getLength());
                } else {
                    buf.append(dimName);
                }
            } else {
                if (dimName != null) {
                    buf.append(dimName);
                }
                buf.append(myd.getLength());
            }
        }
        if (getRank() > 0) {
            buf.append(DIM_END);
        }
    }

    @Override
    public IGroup getParentGroup() {
        return parent;
    }

    @Override
    public List<IRange> getRangeList() {
        List<IRange> list = null;
        try {
            list = new NxsIndex(getData().getShape()).getRangeList();
        } catch (DataAccessException e) {
        }
        return list;
    }

    @Override
    public List<IRange> getSectionRanges() {
        List<IRange> list = null;
        try {
            list = getData().getIndex().getRangeList();
        } catch (DataAccessException e) {
        }
        return list;
    }

    @Override
    public int getRank() {
        int result;
        int[] shape = getShape();

        if ((shape == null) || (shape.length == 1 && shape[0] == 1)) {
            result = 0;
        } else {
            result = shape.length;
        }

        return result;
    }

    @Override
    public IDataItem getSection(final List<IRange> section) throws InvalidRangeException {
        NxsDataItem item = null;
        try {
            item = new NxsDataItem(this);
            array = (NxsArray) item.getData().getArrayUtils().sectionNoReduce(section).getArray();
        } catch (DataAccessException e) {
        }
        return item;
    }

    @Override
    public int[] getShape() {
        int[] shape;
        if (hdfContainers.length == 1) {
            shape = hdfContainers[0].getShape();
        } else {
            try {
                shape = getData().getShape();
            } catch (DataAccessException e) {
                shape = ArrayUtils.EMPTY_SHAPE;
            }
        }
        return shape;
    }

    @Override
    public long getSize() {
        int[] shape = getShape();
        long total;
        if (shape == null) {
            total = 0;
        } else {
            total = 1;
            for (int size : shape) {
                total *= size;
            }
        }
        return total;
    }

    @Override
    public int getSizeToCache() {
        throw new NotImplementedException();
    }

    @Override
    public IDataItem getSlice(final int dim, final int value) throws InvalidRangeException {
        NxsDataItem item = new NxsDataItem(this);
        try {
            item.array = (NxsArray) item.getData().getArrayUtils().slice(dim, value).getArray();
        } catch (Exception e) {
            item = null;
        }
        return item;
    }

//    @Override
//    public IDataItem getASlice(final int dimension, final int value) throws InvalidRangeException {
//        return getSlice(dimension, value);
//    }

    @Override
    public Class<?> getType() {
        return hdfContainers[0].getType();
    }

    @Override
    public String getUnitsString() {
        String value = null;
        IAttribute attr = getAttribute(UNIT);
        if (attr != null) {
            value = attr.getStringValue();
        }
        return value;
    }

    @Override
    public boolean hasAttribute(final String name, final String value) {
        boolean result = false;
        IAttribute attr = getAttribute(name);
        if (attr != null) {
            String test = attr.getStringValue();
            if (value.indexOf('*') > -1) {
                // Regular expression compatibility
                String regex = value.replace(ANY, REGEX_ANY);
                result = test.matches(regex);
            } else {
                result = test.equals(value);
            }
        }
        return result;
    }

    @Override
    public boolean hasCachedData() {
        return hdfContainers[0].hasCachedData();
    }

    @Override
    public void invalidateCache() {
        hdfContainers[0].invalidateCache();
    }

    @Override
    public boolean isCaching() {
        return hdfContainers[0].isCaching();
    }

    @Override
    public boolean isMemberOfStructure() {
        return hdfContainers[0].isMemberOfStructure();
    }

    @Override
    public boolean isMetadata() {
        return (getAttribute(SIGNAL) == null);
    }

    @Override
    public boolean isScalar() {
        return (getRank() == 0);
    }

    @Override
    public boolean isUnlimited() {
        return false;
    }

    @Override
    public boolean isUnsigned() {
        return hdfContainers[0].isUnsigned();
    }

    @Override
    public byte readScalarByte() throws DataAccessException {
        return hdfContainers[0].readScalarByte();
    }

    @Override
    public double readScalarDouble() throws DataAccessException {
        return hdfContainers[0].readScalarDouble();
    }

    @Override
    public float readScalarFloat() throws DataAccessException {
        return hdfContainers[0].readScalarFloat();
    }

    @Override
    public int readScalarInt() throws DataAccessException {
        return hdfContainers[0].readScalarInt();
    }

    @Override
    public long readScalarLong() throws DataAccessException {
        return hdfContainers[0].readScalarLong();
    }

    @Override
    public short readScalarShort() throws DataAccessException {
        return hdfContainers[0].readScalarShort();
    }

    @Override
    public String readScalarString() throws DataAccessException {
        return hdfContainers[0].readScalarString();
    }

    @Override
    public int prepareForReading(int rank) throws DataAccessException {
        int dataPerBlock = 1;
        for (HdfDataItem item : hdfContainers) {
            dataPerBlock = item.prepareForReading(rank);
        }
        return dataPerBlock;
    }

    @Override
    public boolean isUnsafeReadingEnabled() {
        boolean unsafeReadingEnabled = false;
        for (HdfDataItem item : hdfContainers) {
            unsafeReadingEnabled = item.isUnsafeReadingEnabled();
        }
        return unsafeReadingEnabled;
    }

    @Override
    public void setUnsafeReadingEnabled(boolean unsafeReadingEnabled) {
        for (HdfDataItem item : hdfContainers) {
            item.setUnsafeReadingEnabled(unsafeReadingEnabled);
        }
    }

    @Override
    public void finalizeReading() throws DataAccessException {
        for (IDataItem item : hdfContainers) {
            item.finalizeReading();
        }
    }

    @Override
    public boolean removeAttribute(final IAttribute attr) {
        boolean result = false;
        for (IDataItem item : hdfContainers) {
            item.removeAttribute(attr);
        }
        result = true;
        return result;
    }

    @Override
    public void setCachedData(final IArray cacheData, final boolean isMetadata) throws InvalidArrayTypeException {
        if (cacheData instanceof NxsArray) {
            array = (NxsArray) cacheData;
            IArray[] parts = array.getArrayParts();
            for (int i = 0; i < parts.length && i < hdfContainers.length; i++) {
                hdfContainers[i].setCachedData(parts[i], false);
            }
        } else if (hdfContainers.length == 1) {
            hdfContainers[0].setCachedData(cacheData, isMetadata);
        } else {
            throw new InvalidArrayTypeException("Unable to set data: NxsArray is expected!");
        }
    }

    @Override
    public void setCaching(final boolean caching) {
        for (IDataItem item : hdfContainers) {
            item.setCaching(caching);
        }
    }

    @Override
    public void setDataType(final Class<?> dataType) {
        for (IDataItem item : hdfContainers) {
            item.setDataType(dataType);
        }
    }

    @Override
    public void setDimensions(final String dimString) {
        parent = getParentGroup();

        List<String> dimNames = java.util.Arrays.asList(dimString.split(SPACE));
        Collection<IDataItem> items = parent.getDataItemList();

        for (IDataItem item : items) {
            IAttribute attr = item.getAttribute(AXIS);
            if (attr != null) {
                if (ANY.equals(dimString)) {
                    setDimension(new NxsDimension(NxsFactory.NAME, item), attr.getNumericValue().intValue());
                } else if (dimNames.contains(attr.getName())) {
                    setDimension(new NxsDimension(NxsFactory.NAME, item), attr.getNumericValue().intValue());
                }
            }
        }
    }

    @Override
    public void setDimension(final IDimension dim, final int ind) {
        dimensions.add(new DimOrder(ind, dim));
    }

    @Override
    public void setElementSize(final int elementSize) {
        for (IDataItem item : hdfContainers) {
            item.setElementSize(elementSize);
        }
    }

    @Override
    public void setParent(final IGroup group) {
        if (parent == null || !parent.equals(group)) {
            parent = group;
            for (HdfDataItem dataItem : hdfContainers) {
                dataItem.setParent(((NxsGroup) group).getHdfGroup());
            }
        }
    }

    @Override
    public void setSizeToCache(final int sizeToCache) {
        for (IDataItem item : hdfContainers) {
            item.setSizeToCache(sizeToCache);
        }
    }

    @Override
    public String toString() {
        return getName();
    }

    @Override
    public String toStringDebug() {
        StringBuilder strDebug = new StringBuilder();
        strDebug.append(getName());
        if (strDebug.length() > 0) {
            strDebug.append(NEW_LINE);
        }
        try {
            strDebug.append(SHAPE).append(getData().shapeToString()).append(NEW_LINE);
        } catch (DataAccessException e) {
        }
        List<IDimension> dimensions = getDimensionList();
        for (IDimension dim : dimensions) {
            strDebug.append(dim.getCoordinateVariable().toString());
        }

        Collection<IAttribute> list = getAttributeList();
        if (list.size() > 0) {
            strDebug.append(ATTRIBUTES);
        }
        for (IAttribute a : list) {
            strDebug.append(TOKEN).append(a).append(NEW_LINE);
        }

        return strDebug.toString();
    }

//    @Override
//    public String writeCDL(final String indent, final boolean useFullName, final boolean strict) {
//        throw new NotImplementedException();
//    }

    @Override
    public void setUnitsString(final String units) {
        throw new NotImplementedException();
    }

    @Override
    public IDataset getDataset() {
        return dataset;
    }

    public void setDataset(final IDataset dataset) {
        if (dataset instanceof NxsDataset) {
            this.dataset = (NxsDataset) dataset;
        } else {
            try {
                this.dataset = NxsDataset.instanciate(new File(dataset.getLocation()).toURI());
            } catch (CDMAException e) {
                Factory.getLogger().warn(e.getMessage());
            }
        }
    }

    @Override
    public String getLocation() {
        String result = null;
        if (hdfContainers != null && hdfContainers.length > 0) {
            result = hdfContainers[0].getLocation();
        }
        return result;
    }

    @Override
    public IGroup getRootGroup() {
        IGroup root;
        if (parent == null) {
            checkDataset(true);
            root = dataset.getRootGroup();
        } else {
            root = parent.getRootGroup();
        }
        return root;
    }

    @Override
    public String getFactoryName() {
        return NxsFactory.NAME;
    }

    @Override
    public long getLastModificationDate() {
        checkDataset(true);
        return dataset.getLastModificationDate();
    }

    // specific methods
    public HdfDataItem[] getHdfDataItems() {
        HdfDataItem[] result = new HdfDataItem[hdfContainers.length];
        int i = 0;
        for (HdfDataItem item : hdfContainers) {
            result[i++] = item;
        }
        return result;
    }

    public void setNxsPath(final NxsPath path) {
        nxsPath = path;
    }

    public void linkTo(NxsDataItem item) {
        hdfContainers[0].linkTo(item.getHdfDataItems()[0]);
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    // Inner class
    // Associate an IDimension to an order of the array
    private static class DimOrder {
        // Members
        private final int mOrder; // order of the corresponding dimension in the NxsDataItem
        private final IDimension mDimension; // dimension object

        public DimOrder(final int order, final IDimension dim) {
            mOrder = order;
            mDimension = dim;
        }

        public int order() {
            return mOrder;
        }

        public IDimension dimension() {
            return mDimension;
        }
    }

}

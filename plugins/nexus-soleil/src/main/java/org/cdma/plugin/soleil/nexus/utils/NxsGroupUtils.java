package org.cdma.plugin.soleil.nexus.utils;

import org.cdma.interfaces.IAttribute;
import org.cdma.plugin.soleil.nexus.navigation.NxsGroup;

public class NxsGroupUtils {

    protected static boolean isClassOk(IAttribute classAtribute, String className) {
        boolean classOk;
        if (className.isEmpty()) {
            // RG: In some cases, especially virtual data items, it is impossible to previously load class name.
            classOk = true;
        } else if (classAtribute == null) {
            classOk = false;
        } else {
            try {
                classOk = className.equals(classAtribute.getStringValue());
            } catch (Exception e) {
                classOk = false;
            }
        }
        return classOk;
    }

    public static NxsGroup getGroup(NxsGroup group, NxsNode container) {
        NxsGroup result;
        String path = container.getName();
        if (path.equals(group.getShortName())
                && isClassOk(group.getAttribute(NxsConstant.NX_CLASS), container.getClassName())) {
            result = group;
        } else {
            result = (NxsGroup) group.getGroup(container.getNodeName());
        }
        return result;
    }

}

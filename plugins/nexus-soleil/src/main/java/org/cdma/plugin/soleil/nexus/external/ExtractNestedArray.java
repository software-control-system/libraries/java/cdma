package org.cdma.plugin.soleil.nexus.external;

import java.util.ArrayList;
import java.util.List;

import org.cdma.dictionary.Context;
import org.cdma.dictionary.IPluginMethod;
import org.cdma.exception.CDMAException;
import org.cdma.exception.DataAccessException;
import org.cdma.interfaces.IArray;
import org.cdma.interfaces.IContainer;
import org.cdma.plugin.soleil.nexus.NxsFactory;
import org.cdma.plugin.soleil.nexus.navigation.NxsDataItem;
import org.cdma.utils.Utilities.ModelType;

public class ExtractNestedArray implements IPluginMethod {

    @Override
    public String getFactoryName() {
        return NxsFactory.NAME;
    }

    @Override
    public void execute(Context context) throws CDMAException {
        List<IContainer> result = new ArrayList<>();
        List<IContainer> inList = context.getContainers();
        NxsDataItem item = null;
        for (IContainer container : inList) {
            if (container.getModelType().equals(ModelType.DataItem)) {
                item = (NxsDataItem) container;
                try {
                    IArray array = item.getData(new int[] { 0, 0, 0 },
                            new int[] { 1, 1, item.getShape()[item.getRank() - 1] });
                    item.setCachedData(array, false);
                } catch (DataAccessException dae) {
                    dae.printStackTrace();
                }
                result.add(item);
            }
        }
        context.setContainers(result);
    }

}

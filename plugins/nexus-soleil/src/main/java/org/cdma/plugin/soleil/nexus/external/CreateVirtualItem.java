/*******************************************************************************
 * Copyright (c) 2008 - ANSTO/Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * Norman Xiong (nxi@Bragg Institute) - initial API and implementation
 * Tony Lam (nxi@Bragg Institute) - initial API and implementation
 * Majid Ounsy (SOLEIL Synchrotron) - API v2 design and conception
 * Stephane Poirier (SOLEIL Synchrotron) - API v2 design and conception
 * Clement Rodriguez (ALTEN for SOLEIL Synchrotron) - API evolution
 * Gregory VIGUIER (SOLEIL Synchrotron) - API evolution
 * Raphael GIRARDOT (SOLEIL Synchrotron) - API evolution
 ******************************************************************************/
package org.cdma.plugin.soleil.nexus.external;

import java.util.ArrayList;
import java.util.List;

import org.cdma.dictionary.Context;
import org.cdma.dictionary.IPluginMethod;
import org.cdma.exception.CDMAException;
import org.cdma.interfaces.IAttribute;
import org.cdma.interfaces.IContainer;
import org.cdma.interfaces.IGroup;
import org.cdma.plugin.soleil.nexus.NxsFactory;
import org.cdma.plugin.soleil.nexus.array.NxsArray;
import org.cdma.plugin.soleil.nexus.navigation.NxsDataItem;
import org.cdma.plugin.soleil.nexus.navigation.NxsDataset;
import org.cdma.plugin.soleil.nexus.utils.NxsPath;

/**
 * Create a list of IDataItem that are empty from a IGroup list. Created items have no data linked.
 * 
 * @param context
 * @throws CDMAException
 */

public class CreateVirtualItem implements IPluginMethod {

    private static final String SHORT_NAME_IDENTIFIER = "shortName:";
    private static final String TRUE = "true";

    @Override
    public String getFactoryName() {
        return NxsFactory.NAME;
    }

    @Override
    public void execute(final Context context) throws CDMAException {
        List<IContainer> inList = context.getContainers();
        List<IContainer> outList = new ArrayList<>();

        NxsDataItem item;
        NxsPath path;
        NxsArray array;
        String name;
        String expectedValue = null;
        Object[] params = context.getParams();
        boolean useShortName = false;
        if (params != null) {
            for (Object param : params) {
                if (param instanceof String) {
                    String value = (String) param;
                    if (value.startsWith(SHORT_NAME_IDENTIFIER)) {
                        useShortName = TRUE.equalsIgnoreCase(value.substring(SHORT_NAME_IDENTIFIER.length()).trim());
                    } else {
                        expectedValue = value;
                    }
                }
            }
        }
        for (IContainer container : inList) {
//            if (container.getModelType().equals(ModelType.Group)) {
            name = container.getName();

            item = new NxsDataItem(name, (NxsDataset) container.getDataset());
            if (expectedValue == null) {
                expectedValue = useShortName ? container.getShortName() : name;
            }
            array = new NxsArray(new String[] { expectedValue }, new int[] { 1 });
            item.setShortName(container.getShortName());
            item.setDataset(container.getDataset());
//            if (container instanceof IGroup) {
//                item.setParent((IGroup) container);
//            } else {
            item.setParent((IGroup) container.getParentGroup());
//            }
            // DO NOT REMOVE THE 2 FOLLOWING LINES
            path = new NxsPath(item);
            item.setNxsPath(path);
            item.setCachedData(array, false);
            for (IAttribute attr : container.getAttributeList()) {
                item.addOneAttribute(attr);
            }
            outList.add(item);
//            } else {
//                outList.add(container);
//            }
        }

        // Update context
        context.setContainers(outList);
    }
}

/*******************************************************************************
 * Copyright (c) 2008 - ANSTO/Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * Norman Xiong (nxi@Bragg Institute) - initial API and implementation
 * Tony Lam (nxi@Bragg Institute) - initial API and implementation
 * Majid Ounsy (SOLEIL Synchrotron) - API v2 design and conception
 * Stephane Poirier (SOLEIL Synchrotron) - API v2 design and conception
 * Clement Rodriguez (ALTEN for SOLEIL Synchrotron) - API evolution
 * Gregory VIGUIER (SOLEIL Synchrotron) - API evolution
 * Raphael GIRARDOT (SOLEIL Synchrotron) - API evolution
 ******************************************************************************/
package org.cdma.plugin.soleil.nexus.internal;

import java.io.File;
import java.io.FilenameFilter;
import java.net.URI;

import org.cdma.Factory;
import org.cdma.exception.CDMAException;
import org.cdma.exception.DataAccessException;
import org.cdma.interfaces.IContainer;
import org.cdma.interfaces.IDataItem;
import org.cdma.interfaces.IDataset;
import org.cdma.interfaces.IGroup;
import org.cdma.plugin.soleil.nexus.NxsDatasource;
import org.cdma.plugin.soleil.nexus.navigation.NxsDataset;
import org.cdma.utilities.configuration.ConfigDataset;

public class DetectedSource {

    private static final String EXTENSIONS[] = new String[] { ".nxs", ".hdf", ".h4", ".hdf4", ".he4", ".h5", ".hdf5",
            ".he5" };

    private boolean dataset;
    private boolean experiment;
    private boolean browsable;
    private boolean producer;
    private boolean readable;
    private boolean folder;
    private boolean initialized;
    private final URI uri;
    private long timestamp;

    public DetectedSource(final URI uri, final boolean browsable, final boolean readable, final boolean producer,
            final boolean experiment, final boolean datasetFolder) {
        this.readable = readable;
        this.producer = producer;
        this.browsable = browsable;
        this.experiment = experiment;
        this.dataset = datasetFolder;
        this.uri = uri;
        this.initialized = true;
        this.timestamp = getTimestamp(uri);
    }

    public DetectedSource(final URI uri) {
        this.uri = uri;
        init(uri);
    }

    private long getTimestamp(final URI uri) {
        long result = Integer.MIN_VALUE;
        String path = uri.getPath();
        if (path != null) {
            File file = new File(path);
            result = file.lastModified();
        }
        return result;
    }

    public URI getURI() {
        return uri;
    }

    public boolean isDatasetFolder() {
        dataset = isDatasetFolder(new File(uri.getPath()));
        return dataset;
    }

    public boolean isExperiment() {
        // For NeXus SOLEIL: experience implies a fragment in URI
        if (uri.getFragment() != null) {
            fullInit();
        }
        return experiment;
    }

    public boolean isBrowsable() {
        fullInit();
        return browsable;
    }

    public boolean isProducer() {
        return true;
        // fullInit();
        // return mIsProducer;
    }

    public boolean isReadable() {
        if (!readable) {
            readable = initReadable(uri);
        }
        return readable;
    }

    public boolean isFolder() {
        return folder;
    }

    public void setInitialized(final boolean initialized) {
        this.initialized = initialized;
    }

    /**
     * Return true if the source hasn't been modified since a while and is
     * considered as stable.
     */
    public boolean hasChanged(final URI uri) {
        boolean result = false;
        long currentTimestamp = getTimestamp(uri);
        if (currentTimestamp != timestamp) {
            result = true;
        }
        return result;
    }

    // ---------------------------------------------------------
    // private methods
    // ---------------------------------------------------------
    private void init(final URI uri) {
        if (uri != null) {
            // Check if the uri is a folder
            String path = uri.getPath();
            if (path == null) {
                readable = false;
                producer = false;
                browsable = false;
                experiment = false;
                initialized = true;
            } else {
                File file = new File(path);
                timestamp = file.lastModified();
                folder = file.isDirectory();
                // Check it is a NeXus file
                readable = initReadable(uri);
            }
        }
    }

    private synchronized void fullInit() {
        if ((!initialized) || hasChanged(uri)) {
            // Check if we are producer of the source
            producer = initProducer(uri);
            // Check if the uri corresponds to dataset experiment
            experiment = initExperiment(uri);
            // Check if the URI is considered as browsable
            browsable = initBrowsable(uri);
            initialized = true;
        }
    }

    private boolean initReadable(final URI uri) {
        boolean result = false;
        File file = new File(uri.getPath());
        String name = file.getName();
        // Check if the URI is a NeXus file
        if (DetectedSource.accept(name)) {
            result = true;
        }
        return result;
    }

    private boolean initProducer(final URI uri) {
        boolean result = false;
        if (readable) {
            result = true;
        }
        return result;
    }

    private boolean initExperiment(final URI uri) {
        boolean result = false;
        // Check if the URI is a NeXus file
        if (producer) {
            // Instantiate the dataset and detect its configuration
            NxsDataset dataset;
            try {
                dataset = NxsDataset.instanciate(uri);
            } catch (CDMAException e) {
                dataset = null;
            }
            if (dataset != null) {
                boolean close = false;
                ConfigDataset conf;
                try {
                    if (dataset.isOpen()) {
                        close = false;
                    } else {
                        close = true;
                        dataset.open();
                    }
                    // Interrogate the config to know the experiment path
                    conf = dataset.getConfiguration();
                } catch (CDMAException e) {
                    conf = null;
                } finally {
                    if (close && (dataset != null)) {
                        try {
                            dataset.close();
                        } catch (DataAccessException e) {
                            Factory.getLogger()
                                    .error("Failted to close dataset " + uri + " while checking for expirement URI", e);
                        }
                    }
                }
                if (conf != null) {
                    result = true;
                }
            }
        }
        return result;
    }

    private boolean initBrowsable(final URI uri) {
        boolean result = false;
        // If experiment not browsable
        if (!experiment) {
            // If it is a folder containing split NeXus file (quick_exaf)
            if (folder || producer) {
                result = true;
            }
        }
        return result;
    }

    /**
     * Return true if the given is a folder dataset.
     *
     * @note the given file must be a folder
     */
    private boolean isDatasetFolder(final File file) {
        boolean result = false;
        NeXusFilter filter = new NeXusFilter();
        File[] files = file.listFiles(filter);
        if ((files != null) && (files.length > 0)) {
            try {
                NxsDatasource source = NxsDatasource.getInstance();
                DetectedSource detect = source.getSource(files[0].toURI());
                if (detect.isProducer()) {
                    try (IDataset dataset = new NexusDatasetImpl(files[0], false);) {
                        IGroup group = dataset.getRootGroup();
                        IContainer groups = group.findContainerByPath("/<NXentry>/<NXdata>");
                        if (groups instanceof IGroup) {
                            for (IDataItem item : ((IGroup) groups).getDataItemList()) {
                                if (item.getAttribute("dataset_part") != null) {
                                    result = true;
                                    break;
                                }
                            }
                        }
                    }
                }
            } catch (Exception e) {
            }
        }
        return result;
    }

    private static boolean accept(final String fileName) {
        boolean result = false;
        if (fileName != null) {
            String name = fileName.toLowerCase();
            int length = name.length();
            for (String extension : EXTENSIONS) {
                if (length >= extension.length() && name.endsWith(extension)) {
                    result = true;
                    break;
                }
            }
        }
        return result;
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    public static class NeXusFilter implements FilenameFilter {
        @Override
        public boolean accept(final File dir, final String name) {
            return DetectedSource.accept(name);
        }
    }

}

/*******************************************************************************
 * Copyright (c) 2008 - ANSTO/Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * Norman Xiong (nxi@Bragg Institute) - initial API and implementation
 * Tony Lam (nxi@Bragg Institute) - initial API and implementation
 * Majid Ounsy (SOLEIL Synchrotron) - API v2 design and conception
 * Stephane Poirier (SOLEIL Synchrotron) - API v2 design and conception
 * Clement Rodriguez (ALTEN for SOLEIL Synchrotron) - API evolution
 * Gregory VIGUIER (SOLEIL Synchrotron) - API evolution
 * Raphael GIRARDOT (SOLEIL Synchrotron) - API evolution
 ******************************************************************************/
package org.cdma.plugin.soleil.nexus.navigation;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.lang.ref.SoftReference;
import java.net.URI;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.cdma.Factory;
import org.cdma.dictionary.LogicalGroup;
import org.cdma.engine.hdf.navigation.HdfDataset;
import org.cdma.engine.hdf.navigation.HdfGroup;
import org.cdma.exception.CDMAException;
import org.cdma.exception.DataAccessException;
import org.cdma.exception.NoResultException;
import org.cdma.exception.WriterException;
import org.cdma.interfaces.IAttribute;
import org.cdma.interfaces.IContainer;
import org.cdma.interfaces.IDataset;
import org.cdma.interfaces.IGroup;
import org.cdma.plugin.soleil.nexus.NxsFactory;
import org.cdma.plugin.soleil.nexus.dictionary.NxsLogicalGroup;
import org.cdma.plugin.soleil.nexus.internal.DetectedSource.NeXusFilter;
import org.cdma.plugin.soleil.nexus.internal.NexusDatasetImpl;
import org.cdma.utilities.configuration.ConfigDataset;
import org.cdma.utilities.configuration.ConfigManager;
import org.cdma.utils.Utilities.ModelType;

import fr.soleil.lib.project.ObjectUtils;

public final class NxsDataset implements IDataset {

    private boolean open; // is the dataset open
    private URI path; // URI of this dataset
    private ConfigDataset config; // Configuration associated to this dataset
    private final List<HdfDataset> hdfDatasets; // HdfDataset compounding this NxsDataset
    private IGroup physicalRoot; // Physical root of the document
    private final ConcurrentMap<String, LogicalGroup> logicalRootMap;
    private final boolean withWriteAccess;

    // SoftReference of datasets associated to their URI
    private static Map<String, SoftReference<NxsDataset>> datasets = new HashMap<String, SoftReference<NxsDataset>>();

    static {
        Runtime.getRuntime().addShutdownHook(new Thread("Close known NxsDatasets at JVM shutdown") {
            @Override
            public void run() {
                synchronized (datasets) {
                    for (SoftReference<NxsDataset> ref : datasets.values()) {
                        NxsDataset dataset = ObjectUtils.recoverObject(ref);
                        if (dataset != null) {
                            try {
                                dataset.close();
                            } catch (DataAccessException e) {
                                Factory.getLogger().error(e.getMessage());
                            }
                        }
                    }
                    datasets.clear();
                }
            }
        });

    }

    public static NxsDataset instanciate(final URI destination) throws CDMAException {
        return instanciate(destination, false);
    }

    protected static final void cleanDatasetNoCheck(String uri, NxsDataset dataset) throws DataAccessException {
        datasets.remove(uri);
        dataset.close();
    }

    public static NxsDataset instanciate(final URI destination, final boolean withWriteAccess) throws CDMAException {
        NxsDataset dataset = null;
        if (destination != null) {
            String uri = destination.toString();
            synchronized (datasets) {
                SoftReference<NxsDataset> ref = datasets.get(uri);
                if (ref != null) {
                    dataset = ref.get();
                    try {
                        if (dataset != null) {
                            // if a forced close happened, it is better to reinitialize everything
                            if (withWriteAccess || (withWriteAccess != dataset.withWriteAccess) || (dataset.isDead())) {
                                cleanDatasetNoCheck(uri, dataset);
                                dataset = null;
                            }
                        }
                    } catch (DataAccessException e) {
                        Factory.getLogger().error(e.getMessage());
                    }
                }

                if (dataset == null) {
                    String filePath = destination.getPath();
                    if (filePath != null) {
                        try {
                            dataset = new NxsDataset(new File(filePath), withWriteAccess);
                            String fragment = destination.getFragment();

                            if ((fragment != null) && (!fragment.isEmpty())) {
                                dataset.open();
                                try {
                                    IGroup group = dataset.getRootGroup();
                                    try {
                                        String path = URLDecoder.decode(fragment, "UTF-8");
                                        for (IContainer container : group.findAllContainerByPath(path)) {
                                            if (container.getModelType().equals(ModelType.Group)) {
                                                dataset.physicalRoot = (IGroup) container;
                                                break;
                                            }
                                        }
                                    } catch (UnsupportedEncodingException e) {
                                        Factory.getLogger().warn(e.getMessage());
                                    }
                                } finally {
                                    dataset.close();
                                }
                            }
                            if (!withWriteAccess) {
                                datasets.put(uri, new SoftReference<NxsDataset>(dataset));
                            }
                        } catch (Exception e) {
                            if (e instanceof CDMAException) {
                                throw (CDMAException) e;
                            } else {
                                throw new NoResultException(e);
                            }
                        }
                    }
                }
            } // end synchronized (datasets)
        }
        return dataset;
    }

    // ---------------------------------------------------------
    // / Private methods
    // ---------------------------------------------------------

    private NxsDataset(final File destination, final boolean withWriteAccess) throws DataAccessException {
        logicalRootMap = new ConcurrentHashMap<>();
        path = destination.toURI();
        hdfDatasets = new ArrayList<>();
        this.withWriteAccess = withWriteAccess;
        try {
            connect(destination);
        } catch (Exception e) {
            if (e instanceof DataAccessException) {
                throw (DataAccessException) e;
            } else {
                throw new DataAccessException(
                        "Failed to open " + (destination == null ? null : destination.getAbsolutePath()), e);
            }
        }
    }

    private void connect(File destination) throws Exception {
        HdfDataset datafile;
        if (destination.isDirectory()) {
            NeXusFilter filter = new NeXusFilter();
            File[] files = destination.listFiles(filter);
            if (files != null && files.length > 0) {
                for (File file : files) {
                    datafile = new NexusDatasetImpl(file, withWriteAccess);
                    hdfDatasets.add(datafile);
                }
            }
        } else {
            datafile = new NexusDatasetImpl(destination, withWriteAccess);
            hdfDatasets.add(datafile);
        }
        open = false;
    }

    // ---------------------------------------------------------
    // / Other methods
    // ---------------------------------------------------------

    @Override
    public String getFactoryName() {
        return NxsFactory.NAME;
    }

    @Override
    public LogicalGroup getLogicalRoot(String view) {
        LogicalGroup root;
        if (view == null) {
            root = null;
        } else {
            root = logicalRootMap.get(view);
            if (root == null) {
                try {
                    String param = getConfiguration().getParameter(NxsFactory.DEBUG_INF);
                    boolean debug = Boolean.parseBoolean(param);
                    root = new NxsLogicalGroup(view, null, null, this, debug);
                    LogicalGroup tmp = logicalRootMap.putIfAbsent(view, root);
                    if (tmp != null) {
                        root = tmp;
                    }
                } catch (NoResultException e) {
                    Factory.getLogger().warn(e.getMessage());
                }
            }
        }
        return root;
    }

    @Override
    public IGroup getRootGroup() {
        if ((physicalRoot == null) && (hdfDatasets.size() > 0)) {
            HdfGroup[] groups = new HdfGroup[hdfDatasets.size()];
            int i = 0;
            for (IDataset dataset : hdfDatasets) {
                groups[i++] = (HdfGroup) dataset.getRootGroup();
            }
            physicalRoot = new NxsGroup(groups, null, this);
        }
        return physicalRoot;
    }

    @Override
    public void saveTo(final String location) throws WriterException {
        for (IDataset dataset : hdfDatasets) {
            dataset.saveTo(location);
        }
    }

    @Override
    public void save(final IContainer container) throws WriterException {
        for (IDataset dataset : hdfDatasets) {
            dataset.save(container);
        }
    }

    @Override
    public void save(final String parentPath, final IAttribute attribute) throws WriterException {
        for (IDataset dataset : hdfDatasets) {
            dataset.save(parentPath, attribute);
        }
    }

    @Override
    public boolean sync() throws DataAccessException {
        boolean result = true;
        for (IDataset dataset : hdfDatasets) {
            if (!dataset.sync()) {
                result = false;
            }
        }
        return result;
    }

    @Override
    public void close() throws DataAccessException {
        open = false;
        // JAVAAPI-374 clear lock
        if (hdfDatasets != null) {
            closeHdfDatasets();
        }
    }

    protected void closeHdfDatasets() throws DataAccessException {
        for (HdfDataset dataset : hdfDatasets) {
            dataset.close();
        }
    }

    @Override
    public String getLocation() {
        return path.toString();
    }

    @Override
    public String getTitle() {
        String title = getRootGroup().getShortName();
        if (title.isEmpty()) {
            try {
                title = hdfDatasets.get(0).getTitle();
            } catch (NoSuchElementException e) {
            }
        }
        return title;
    }

    @Override
    public void setLocation(final String location) {
        if ((location != null) && (!location.equals(path.toString()))) {
            try {
                closeHdfDatasets();
            } catch (DataAccessException e) {
                Factory.getLogger().warn(e.getMessage());
                hdfDatasets.clear();
            }
            try {
                path = new URI(location);
                connect(new File(path));
            } catch (Exception e) {
                Factory.getLogger().warn(e.getMessage());
            }
        }
    }

    @Override
    public void setTitle(final String title) {
        try {
            hdfDatasets.get(0).setTitle(title);
        } catch (NoSuchElementException e) {
        }
    }

    @Override
    public void open() throws DataAccessException {
        open = true;
        if (hdfDatasets != null) {
            for (IDataset dataset : hdfDatasets) {
                if ((dataset != null) && (!dataset.isOpen())) {
                    dataset.open();
                }
            }
        }
    }

    @Override
    public void save() throws WriterException {
        for (IDataset dataset : hdfDatasets) {
            dataset.save();
        }
    }

    @Override
    public boolean isOpen() {
        return open;
    }

    @Override
    public boolean isDead() {
        boolean dead = false;
        if (hdfDatasets != null) {
            for (IDataset dataset : hdfDatasets) {
                if ((dataset != null) && (dataset.isDead())) {
                    dead = true;
                    break;
                }
            }
        }
        return dead;
    }

    public ConfigDataset getConfiguration() throws NoResultException {
        if (config == null) {
            if (hdfDatasets.size() > 0) {
                ConfigDataset conf;
                conf = ConfigManager.getInstance(NxsFactory.getInstance(), NxsFactory.CONFIG_FILE).getConfig(this);
                config = conf;
            }
        }
        return config;
    }

    @Override
    public long getLastModificationDate() {
        long last = 0;
        long temp = 0;

        File pathFile = new File(path.getPath());
        if (pathFile.exists() && pathFile.isDirectory()) {
            last = pathFile.lastModified();
        }

        for (HdfDataset dataset : hdfDatasets) {
            temp = dataset.getLastModificationDate();
            if (temp > last) {
                last = temp;
            }
        }

        return last;
    }

    public HdfDataset getHdfDataset() {
        return hdfDatasets.get(0);
    }

//    @Override
//    public int hashCode() {
//        int code = 0xDA7A;
//        int mult = 0x60131;
//        code = code * mult + new File(mPath.getPath()).hashCode();
//        return code;
//    }

    @Override
    public String toString() {
        String result = "Dataset with path = " + path.toString();
        return result;
    }

    @Override
    protected void finalize() throws Throwable {
        try {
            logicalRootMap.clear();
            close();
        } catch (Exception e) {
            // just ignore: we only try to clean
        }
        super.finalize();
    }

}

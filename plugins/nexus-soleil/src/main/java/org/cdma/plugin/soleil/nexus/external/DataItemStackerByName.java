/*******************************************************************************
 * Copyright (c) 2008 - ANSTO/Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * Norman Xiong (nxi@Bragg Institute) - initial API and implementation
 * Tony Lam (nxi@Bragg Institute) - initial API and implementation
 * Majid Ounsy (SOLEIL Synchrotron) - API v2 design and conception
 * Stephane Poirier (SOLEIL Synchrotron) - API v2 design and conception
 * Clement Rodriguez (ALTEN for SOLEIL Synchrotron) - API evolution
 * Gregory VIGUIER (SOLEIL Synchrotron) - API evolution
 * Raphael GIRARDOT (SOLEIL Synchrotron) - API evolution
 ******************************************************************************/
package org.cdma.plugin.soleil.nexus.external;

import java.util.List;

import org.cdma.dictionary.ADataItemMultiStacker;
import org.cdma.dictionary.Context;
import org.cdma.interfaces.IDataItem;
import org.cdma.plugin.soleil.nexus.NxsFactory;
import org.cdma.plugin.soleil.nexus.navigation.NxsDataItem;
import org.cdma.plugin.soleil.nexus.navigation.NxsDataset;

/**
 * Stack all found data items that have the same short name to construct a list of aggregated NxsDataItem
 */
public final class DataItemStackerByName extends ADataItemMultiStacker<NxsDataItem, String> {

    @Override
    public String getFactoryName() {
        return NxsFactory.NAME;
    }

    @Override
    protected String extractKey(IDataItem item) {
        return item == null ? null : item.getShortName();
    }

    @Override
    protected NxsDataItem buildDataItem(List<IDataItem> items, Context context) {
        NxsDataset dataset = (NxsDataset) context.getDataset();
        return new NxsDataItem(items.toArray(new NxsDataItem[items.size()]), dataset.getRootGroup(), dataset);
    }
}

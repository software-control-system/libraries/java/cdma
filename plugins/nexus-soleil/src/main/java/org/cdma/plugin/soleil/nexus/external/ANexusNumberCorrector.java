package org.cdma.plugin.soleil.nexus.external;

import org.cdma.dictionary.ANumberCorrector;
import org.cdma.exception.CDMAException;
import org.cdma.interfaces.IDataItem;
import org.cdma.plugin.soleil.nexus.NxsFactory;
import org.cdma.plugin.soleil.nexus.array.NxsArray;
import org.cdma.plugin.soleil.nexus.navigation.NxsDataItem;
import org.cdma.plugin.soleil.nexus.navigation.NxsDataset;

public abstract class ANexusNumberCorrector<D> extends ANumberCorrector<NxsDataItem, NxsArray, D> {

    @Override
    public String getFactoryName() {
        return NxsFactory.NAME;
    }

    @Override
    protected NxsDataItem prepareItemCopy(IDataItem originalItem) throws CDMAException {
        NxsDataItem item;
        if (originalItem instanceof NxsDataItem) {
            NxsDataItem nxsItem = (NxsDataItem) originalItem;
            item = new NxsDataItem(nxsItem.getName(), (NxsDataset) nxsItem.getDataset());
            item.setShortName(nxsItem.getShortName());
            item.setDataset(nxsItem.getDataset());
            item.setParent(nxsItem.getParentGroup());
            item.setNxsPath(nxsItem.getNxsPath());
        } else {
            item = null;
        }
        return item;
    }

    @Override
    protected NxsArray generateArray(double[] value, int[] shape) throws CDMAException {
        return new NxsArray(value, shape);
    }

}

/*******************************************************************************
 * Copyright (c) 2008 - ANSTO/Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * Norman Xiong (nxi@Bragg Institute) - initial API and implementation
 * Tony Lam (nxi@Bragg Institute) - initial API and implementation
 * Majid Ounsy (SOLEIL Synchrotron) - API v2 design and conception
 * Stephane Poirier (SOLEIL Synchrotron) - API v2 design and conception
 * Clement Rodriguez (ALTEN for SOLEIL Synchrotron) - API evolution
 * Gregory VIGUIER (SOLEIL Synchrotron) - API evolution
 * Raphael GIRARDOT (SOLEIL Synchrotron) - API evolution
 ******************************************************************************/
package org.cdma.plugin.soleil.nexus.external;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.cdma.dictionary.Context;
import org.cdma.dictionary.IPluginMethod;
import org.cdma.engine.hdf.utils.HdfPath;
import org.cdma.exception.CDMAException;
import org.cdma.interfaces.IContainer;
import org.cdma.interfaces.IGroup;
import org.cdma.interfaces.IKey;
import org.cdma.plugin.soleil.nexus.NxsFactory;
import org.cdma.plugin.soleil.nexus.array.NxsArray;
import org.cdma.plugin.soleil.nexus.navigation.ANxsContainer;
import org.cdma.plugin.soleil.nexus.navigation.NxsDataItem;
import org.cdma.plugin.soleil.nexus.navigation.NxsGroup;
import org.cdma.plugin.soleil.nexus.utils.NxsConstant;
import org.cdma.plugin.soleil.nexus.utils.NxsGroupUtils;
import org.cdma.plugin.soleil.nexus.utils.NxsNode;
import org.cdma.utils.Utilities.ModelType;

public class HarvestEquipmentAttributes implements IPluginMethod {

    private static final String XIA = "xia";
    private static final String SCIENTA_REGEX = ".*scienta.*";
    private static final String XIA_REGEX = ".*xia.*";
    private static final String REGEX = ".*([0-9]+)$";
    private static final String CAMERA_REGEX = ".*[0-9]+";
    private static final String REPLACEMENT = "$1";
    private static final String CAMERA = "camera";
    private static final String SPLITTER = "_";
    private static final String SCIENTA_ATT = "ScientaAtt";

    @Override
    public String getFactoryName() {
        return NxsFactory.NAME;
    }

    /**
     * For each container in the context, this method will try to find its meta-data:
     * - acquisition_sequence (based on NXentry)
     * - region (of the detector it belongs to, based on number at end of the node's name)
     * - equipment (based on the name of NX... direct son of the NXinstrument)
     * 
     * @param context
     * @throws CDMAException
     */
    @Override
    public void execute(final Context context) throws CDMAException {
        List<IContainer> inList = context.getContainers();
        List<IContainer> outList = new ArrayList<IContainer>();

        for (IContainer container : inList) {
            ModelType type = container.getModelType();
            outList.add(container);
            switch (type) {
                case Group:
                case DataItem:
                    ANxsContainer<?> nxsContainer = (ANxsContainer<?>) container;
                    NxsNode[] nodes = nxsContainer.getNxsPath().getNodes();
                    setAttributeAcquisitionSequence(container, nodes);
                    setAttributeEquipment(container, nodes, outList, context.getKey());
                    break;
                case LogicalGroup:
                    break;
                default:
                    break;
            }
        }
        context.setContainers(outList);
    }

    private void setAttributeAcquisitionSequence(final IContainer container, final NxsNode[] nodes) {
        // Scan attribute
        if (nodes.length > 0) {
            NxsGroup root = (NxsGroup) container.getRootGroup();
            // Check the root group is the real root of the file
            NxsNode current = (NxsNode) root.getNxsPath().getCurrentNode();
            if (current == null || !current.getClassName().equals(NxsConstant.NX_ENTRY)) {
                // LogicalMode: root = NXentry = acquisition_sequence
                root = (NxsGroup) root.getGroup(NxsConstant.ESCAPED_NX_ENTRY);
            }
            String attrName = NxsConstant.ATTR_SCAN;
            String attrValue = root.getShortName();
            container.addStringAttribute(attrName, attrValue);
        }
    }

    private void updateEquipment(String nodeName, String nodeNameNoLowerCase, IContainer container) {
        if (nodeName.matches(CAMERA_REGEX)) {
            String equipment = nodeNameNoLowerCase;
            container.addStringAttribute(NxsConstant.ATTR_EQUIPMENT, equipment);
        }
    }

    private void checkVirtualDataItem(IContainer container, String checkedValue, String value) {
        if (container instanceof NxsDataItem) {
            NxsDataItem nxsItem = (NxsDataItem) container;
            try {
                if (nxsItem.readScalarString().toLowerCase().matches(checkedValue)) {
                    nxsItem.setCachedData(new NxsArray(new String[] { value }, new int[] { 1 }), false);
                }
            } catch (Exception e) {
                // not managed
                e.printStackTrace();
            }
        }
    }

    private boolean setXiaEquipment(IContainer container, IGroup xia, String nodeName, String nodeNameNoLowerCase,
            List<IContainer> outList) {
        boolean equipmentSet = false;
        if (xia != null) {
            Collection<? extends IContainer> list = xia.getDataItemList();
            if (list.isEmpty()) {
                list = xia.getGroupList();
            }
            String region;
            List<String> regions = new ArrayList<String>();
            boolean first = true;
            for (IContainer child : list) {
                region = child.getName().replaceAll(REGEX, REPLACEMENT);
                String xiaEquipment = nodeNameNoLowerCase + region;
                if (!regions.contains(region)) {
                    regions.add(region);
                    if (first) {
                        first = false;
                        container.addStringAttribute(NxsConstant.ATTR_EQUIPMENT, xiaEquipment);
                        checkVirtualDataItem(container, XIA_REGEX, xiaEquipment/*, builder*/);
                        equipmentSet = true;
                    } else {
                        IContainer clone = container.clone();
                        clone.addStringAttribute(NxsConstant.ATTR_EQUIPMENT, xiaEquipment);
                        checkVirtualDataItem(clone, XIA_REGEX, xiaEquipment/*, builder*/);
                        equipmentSet = true;
                        outList.add(clone);
                    }
                }
            }
        }
        return equipmentSet;
    }

    private void setAttributeEquipment(final IContainer container, final NxsNode[] nodes,
            final List<IContainer> outList, final IKey key) {
        // Set the root group at the NXentry position
        NxsGroup root = (NxsGroup) container.getRootGroup();
        NxsNode[] rootNodes = root.getNxsPath().getNodes();

        // Try to set NxEntry as root, only if not already on NxEntry.
        if (HdfPath.PATH_SEPARATOR.equals(root.getName())
                && (rootNodes.length == 0 || !rootNodes[0].getClassName().equals(NxsConstant.NX_ENTRY))) {
            root = NxsGroupUtils.getGroup(root, nodes[1]);
        }

        int detectorIndex = -1, instrumentIndex = -1, dataIndex = -1;
        if (container.getAttribute(NxsConstant.ATTR_EQUIPMENT) == null) {
            int index = -1;
            for (NxsNode node : nodes) {
                index++;
                if (NxsConstant.NX_INSTRUMENT.equals(node.getClassName())) {
                    instrumentIndex = index;
                } else if (NxsConstant.NX_DETECTOR.equals(node.getClassName())) {
                    detectorIndex = index; // NXdetector is child of NXinstrument
                    break;
                } else if (NxsConstant.NX_DATA.equals(node.getClassName())) {
                    dataIndex = index; // NXdata is at same level as NXinstrument
                    break;
                }
            }
        }

        if (container.getAttribute(NxsConstant.ATTR_EQUIPMENT) == null) {
            if (detectorIndex > -1) {
                // Set equipment for detector children
                NxsNode node = nodes[detectorIndex];
                String nodeNameNoLowerCase = node.getNodeName();
                String nodeName = nodeNameNoLowerCase.toLowerCase();
                if (nodeName.matches(SCIENTA_REGEX)) {
                    // If Scienta
                    container.addStringAttribute(NxsConstant.ATTR_EQUIPMENT, nodeNameNoLowerCase);
                } else if (nodeName.matches(XIA)) {
                    // If Xia
                    if (nodes.length > detectorIndex + 1) {
                        // We are on sub-node of the XIA. The region is contained in the name.
                        node = nodes[detectorIndex + 1];
                        String xia = nodeNameNoLowerCase;
                        String region = node.getNodeName().replaceAll(REGEX, REPLACEMENT);
                        container.addStringAttribute(NxsConstant.ATTR_EQUIPMENT, xia + region);
                    } else {
                        // We are on the XIA. The region is contained in the children name
                        IGroup xia = NxsGroupUtils.getGroup(NxsGroupUtils.getGroup(root, nodes[detectorIndex - 1]),
                                nodes[detectorIndex]);
                        setXiaEquipment(container, xia, nodeName, nodeNameNoLowerCase, outList);
                    }
                } else if ((key != null) && CAMERA.equals(key.getName())) {
                    // set equipment for camera key
                    updateEquipment(nodeName, nodeNameNoLowerCase, container);

                }
            } else if ((nodes.length > 0) && (key != null) && CAMERA.equals(key.getName())) {
                // set equipment for camera key
                NxsNode node = nodes[nodes.length - 1];
                String nodeNameNoLowerCase = node.getNodeName();
                String nodeName = nodeNameNoLowerCase.toLowerCase();
                IGroup xia = NxsGroupUtils.getGroup(root, node);
                if (!setXiaEquipment(container, xia, nodeName, nodeNameNoLowerCase, outList)) {
                    updateEquipment(nodeName, nodeNameNoLowerCase, container);
                }
            }
        } // end if (container.getAttribute(NxsConstant.ATTR_EQUIPMENT) == null)

        if ((container.getAttribute(NxsConstant.ATTR_EQUIPMENT) == null) && (instrumentIndex > -1)
                && (nodes.length > instrumentIndex + 1)) {
            // Equipment attribute (NXdetector, NXmono...)
            NxsGroup instrument = NxsGroupUtils.getGroup(root, nodes[instrumentIndex]);
            IGroup equipmentGroup = NxsGroupUtils.getGroup(instrument, nodes[instrumentIndex + 1]);
            if (equipmentGroup != null) {
                String attrValue = equipmentGroup.getShortName();
                container.addStringAttribute(NxsConstant.ATTR_EQUIPMENT, attrValue);
            }
        } // if ((container.getAttribute(NxsConstant.ATTR_EQUIPMENT) == null) && (instrumentIndex > -1)
          // && (nodes.length > instrumentIndex + 1))

        if (dataIndex > -1) {
            // Set equipment attribute for NxData children
            // By convention @SOLEIL, the NXData child node name is the equipment name
            NxsNode node = nodes[dataIndex];
            String nodeNameNoLowerCase = node.getNodeName();
            String nodeName = nodeNameNoLowerCase.toLowerCase();
            String[] splitName = nodeNameNoLowerCase.split(SPLITTER);
            if (nodeName.matches(SCIENTA_REGEX) && (splitName.length > 1)) {
                int region;
                try {
                    region = Integer.parseInt(splitName[1]);
                } catch (Exception e) {
                    region = 0;
                }
                container.addStringAttribute(NxsConstant.ATTR_EQUIPMENT, SCIENTA_ATT + region);
            } else if (splitName.length > 2) {
                int region;
                try {
                    region = Integer.parseInt(splitName[2]);
                } catch (Exception e) {
                    region = 0;
                }
                container.addStringAttribute(NxsConstant.ATTR_EQUIPMENT, splitName[0] + region);
            } else {
                String region = nodes[nodes.length - 1].getNodeName().replaceAll(".*([0-9]+)$", "$1");
                container.addStringAttribute(NxsConstant.ATTR_EQUIPMENT, container.getShortName() + region);
            }
        } // end if (dataIndex > -1)
    }
}

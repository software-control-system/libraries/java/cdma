package org.cdma.plugin.soleil.nexus.external;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.cdma.dictionary.Context;
import org.cdma.dictionary.IPluginMethod;
import org.cdma.exception.CDMAException;
import org.cdma.interfaces.IAttribute;
import org.cdma.interfaces.IContainer;
import org.cdma.plugin.soleil.nexus.NxsFactory;
import org.cdma.plugin.soleil.nexus.navigation.NxsDataItem;
import org.cdma.utils.Utilities.ModelType;

import fr.soleil.lib.project.ObjectUtils;

public class OneMoreDimension implements IPluginMethod {

    private static final String ATTRIBUTE = "attribute";
    private static final String POSITION = "position";
    private static final String LAST = "last";
    private static final String SECOND_LAST = "second last";

    @Override
    public String getFactoryName() {
        return NxsFactory.NAME;
    }

    @Override
    public void execute(Context context) throws CDMAException {
        List<IContainer> inList = context.getContainers();
        List<IContainer> outList = new ArrayList<IContainer>();
        Object[] parameters = context.getParams();
        if ((parameters instanceof String[]) && (parameters.length == 2) && (parameters[0] != null)
                && (parameters[1] != null)) {
            String[] strParameters = (String[]) parameters;
            String[] paramNames = strParameters[0].split(",");
            String[] paramValues = strParameters[1].split(",");
            NxsDataItem item;
            if ((paramNames.length == paramValues.length) && (paramNames.length > 0)) {
                for (IContainer container : inList) {
                    if (container.getModelType().equals(ModelType.DataItem)) {
                        item = (NxsDataItem) container;
                        Map<String, String> attributes = new HashMap<>();
                        boolean canAdd = true;
                        int[] shape = item.getShape();
                        int position = -1;
                        for (int i = 0; i < paramNames.length; i++) {
                            String[] nameAndValue = getNameAndValue(paramNames[i]);
                            String value = paramValues[i];
                            if ((nameAndValue != null) && (value != null)) {
                                if (POSITION.equalsIgnoreCase(nameAndValue[0])) {
                                    value = value.trim();
                                    if (LAST.equalsIgnoreCase(value)) {
                                        position = shape.length;
                                    } else if (SECOND_LAST.equalsIgnoreCase(value)) {
                                        position = shape.length - 1;
                                    } else {
                                        try {
                                            position = Integer.parseInt(value);
                                        } catch (Exception e) {
                                            position = -1;
                                        }
                                    }
                                } else if (ATTRIBUTE.equalsIgnoreCase(nameAndValue[0])) {
                                    String attributeName = nameAndValue[1];
                                    if (attributeName != null) {
                                        attributeName = attributeName.trim();
                                        if (!attributeName.isEmpty()) {
                                            attributes.put(attributeName, value);
                                        }
                                    }
                                }
                            }
                        }
                        if (position < 0) {
                            canAdd = false;
                        } else if (!attributes.isEmpty()) {
                            for (Entry<String, String> entry : attributes.entrySet()) {
                                IAttribute attribute = item.getAttribute(entry.getKey());
                                if (attribute == null) {
                                    canAdd = false;
                                    break;
                                } else {
                                    try {
                                        if (!ObjectUtils.sameObject(entry.getValue(), attribute.getStringValue())) {
                                            canAdd = false;
                                            break;
                                        }
                                    } catch (Exception e) {
                                        canAdd = false;
                                        break;
                                    }
                                }
                            }
                        }
                        if (canAdd) {
                            outList.add(item.getCopyWithFakeDimensionInsertedAt(position));
                        }
                    }
                }
            }
        }
        if (outList.isEmpty()) {
            outList.addAll(inList);
        }
        context.setContainers(outList);
    }

    protected String[] getNameAndValue(String arg) {
        String[] result;
        if (arg == null) {
            result = null;
        } else {
            result = new String[2];
            int index = arg.indexOf(':');
            if (index > -1) {
                result[0] = arg.substring(0, index).trim();
                result[1] = arg.substring(index + 1, arg.length()).trim();
            } else {
                result[0] = arg.trim();
                result[1] = null;
            }
        }
        return result;
    }
}

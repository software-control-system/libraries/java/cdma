package org.cdma.plugin.soleil.nexus.navigation;

import java.util.ArrayList;
import java.util.Collection;

import org.cdma.engine.hdf.navigation.AHdfContainer;
import org.cdma.exception.DataAccessException;
import org.cdma.interfaces.IAttribute;
import org.cdma.interfaces.IContainer;
import org.cdma.interfaces.IGroup;
import org.cdma.plugin.soleil.nexus.utils.NxsPath;

import fr.soleil.lib.project.ObjectUtils;

public abstract class ANxsContainer<T extends AHdfContainer> implements IContainer {

    protected NxsPath nxsPath;
    protected NxsDataset dataset; // CDMA IDataset i.e. file handler
    protected IGroup parent; // parent group
    protected T[] hdfContainers;

    public ANxsContainer(NxsDataset dataset, IGroup parent) {
        super();
        this.nxsPath = null;
        this.dataset = dataset;
        this.parent = parent;
        this.hdfContainers = null;
    }

    public NxsPath getNxsPath() {
        return nxsPath;
    }

    protected void checkDataset(boolean open) {
        if ((open) && (dataset != null) && (!dataset.isOpen())) {
            try {
                dataset.open();
            } catch (DataAccessException e) {
                // TODO maybe log error
                e.printStackTrace();
            }
        }
    }

    @Override
    public IAttribute getAttribute(final String name) {
        IAttribute attr = null;
        if ((name != null) && (hdfContainers != null)) {
            for (IContainer container : hdfContainers) {
                if (container != null) {
                    attr = container.getAttribute(name);
                    if (attr != null) {
                        break;
                    }
                }
            }
        }
        return attr;
    }

    @Override
    public Collection<IAttribute> getAttributeList() {
        Collection<IAttribute> result = new ArrayList<>();
        if (hdfContainers != null) {
            for (IContainer container : hdfContainers) {
                if (container != null) {
                    result.addAll(container.getAttributeList());
                }
            }
        }
        return result;
    }

    @Override
    public String getName() {
        String name = ObjectUtils.EMPTY_STRING;
        if ((hdfContainers != null) && (hdfContainers.length > 0) && (hdfContainers[0] != null)) {
            name = hdfContainers[0].getName();
        }
        return name;
    }

    @Override
    public void setName(final String name) {
        if (hdfContainers != null) {
            for (IContainer container : hdfContainers) {
                if (container != null) {
                    container.setName(name);
                }
            }
        }
    }

    @Override
    public String getShortName() {
        String name = ObjectUtils.EMPTY_STRING;
        if ((hdfContainers != null) && (hdfContainers.length > 0) && (hdfContainers[0] != null)) {
            name = hdfContainers[0].getShortName();
        }
        return name;
    }

    @Override
    public void setShortName(String name) {
        if (hdfContainers != null) {
            for (IContainer container : hdfContainers) {
                if (container != null) {
                    container.setShortName(name);
                }
            }
        }
    }

    @Override
    public ANxsContainer<T> clone() {
        ANxsContainer<T> clone;
        try {
            @SuppressWarnings("unchecked")
            ANxsContainer<T> tmp = (ANxsContainer<T>) super.clone();
            if (parent != null) {
                tmp.parent = parent.clone();
            }
            tmp.dataset = dataset;
            clone = tmp;
        } catch (CloneNotSupportedException e) {
            // should not happen as implementing IContainer, which extends Cloneable
            clone = this;
        }
        return clone;
    }

}

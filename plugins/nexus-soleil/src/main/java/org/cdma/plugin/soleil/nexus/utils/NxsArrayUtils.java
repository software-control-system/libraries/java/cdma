/*******************************************************************************
 * Copyright (c) 2008 - ANSTO/Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * Norman Xiong (nxi@Bragg Institute) - initial API and implementation
 * Tony Lam (nxi@Bragg Institute) - initial API and implementation
 * Majid Ounsy (SOLEIL Synchrotron) - API v2 design and conception
 * Stephane Poirier (SOLEIL Synchrotron) - API v2 design and conception
 * Clement Rodriguez (ALTEN for SOLEIL Synchrotron) - API evolution
 * Gregory VIGUIER (SOLEIL Synchrotron) - API evolution
 * Raphael GIRARDOT (SOLEIL Synchrotron) - API evolution
 ******************************************************************************/
package org.cdma.plugin.soleil.nexus.utils;

import java.lang.reflect.Array;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import org.cdma.arrays.DefaultIndex;
import org.cdma.engine.hdf.array.HdfIndex;
import org.cdma.exception.InvalidRangeException;
import org.cdma.exception.ShapeNotMatchException;
import org.cdma.interfaces.IArray;
import org.cdma.interfaces.IRange;
import org.cdma.interfaces.ISliceIterator;
import org.cdma.plugin.soleil.nexus.array.NxsArray;
import org.cdma.plugin.soleil.nexus.array.NxsIndex;
import org.cdma.utils.IArrayUtils;
import org.slf4j.LoggerFactory;

import fr.soleil.lib.project.date.DateUtil;
import fr.soleil.lib.project.math.ArrayUtils;

public final class NxsArrayUtils implements IArrayUtils {

    private static final AtomicLong GET_DIM_AND_TYPE_TIME = new AtomicLong(0);
    private static final AtomicLong ARRAY_CREATION_TIME = new AtomicLong(0);
    private static final AtomicLong PREPARATION_TIME = new AtomicLong(0);
    private static final AtomicLong GET_STORAGE_TIME = new AtomicLong(0);
    private static final AtomicLong COPY_TIME = new AtomicLong(0);

    public static void resetMetrics() {
        GET_DIM_AND_TYPE_TIME.set(0);
        ARRAY_CREATION_TIME.set(0);
        PREPARATION_TIME.set(0);
        GET_STORAGE_TIME.set(0);
        COPY_TIME.set(0);
        NxsArray.resetMetrics();
    }

    public static void traceMetrics(String applicationId) {
        StringBuilder builder = new StringBuilder(NxsArrayUtils.class.getSimpleName());
        DateUtil.elapsedTimeToStringBuilder("\nNxsArrayUtils Cumulative array dimension and type recovering time: ",
                GET_DIM_AND_TYPE_TIME, builder);
        DateUtil.elapsedTimeToStringBuilder("\nNxsArrayUtils Cumulative new array creation time: ", ARRAY_CREATION_TIME,
                builder);
        DateUtil.elapsedTimeToStringBuilder("\nNxsArrayUtils Cumulative preparation time: ", PREPARATION_TIME, builder);
        DateUtil.elapsedTimeToStringBuilder("\nNxsArrayUtils Cumulative storage access time: ", GET_STORAGE_TIME,
                builder);
        DateUtil.elapsedTimeToStringBuilder("\nNxsArrayUtils Cumulative array filling time: ", COPY_TIME, builder);
        LoggerFactory.getLogger(applicationId).trace(builder.toString());
        NxsArray.traceMetrics(applicationId);
    }

    private final IArrayUtils mUtils;

    private volatile boolean profile;

    public NxsArrayUtils(NxsArray array) {
        mUtils = new NexusArrayUtils(array);
    }

    public boolean isProfile() {
        return profile;
    }

    public void setProfile(boolean trace) {
        this.profile = trace;
        IArray array = getArray();
        if (array instanceof NxsArray) {
            ((NxsArray) array).setProfile(trace);
        }
    }

    @Override
    public Object copyTo1DJavaArray() {
        // Instantiate a new convenient array for storage
        long time = -1, getArrayDimAndTypeTime = -1, arrayCreationTime = -1, prepareTime = -1, storageTime = -1;
        if (profile) {
            time = System.currentTimeMillis();
        }
        int length = (int) getArray().getSize();
        Class<?> type = getArray().getElementType();
        if (profile) {
            getArrayDimAndTypeTime = System.currentTimeMillis() - time;
            time = System.currentTimeMillis();
        }
        Object array;
        if (type == null) {
            array = null;
        } else {
            array = ArrayUtils.newInstance(type, length);
            if (profile) {
                arrayCreationTime = System.currentTimeMillis() - time;
                time = System.currentTimeMillis();
            }

            HdfIndex storageIndex = ((NxsIndex) getArray().getIndex()).getIndexStorage();
            DefaultIndex matrixIndex = ((NxsIndex) getArray().getIndex()).getIndexMatrix();

            // If the storing array is a stack of DataItem
            long size = matrixIndex.getSize();
            long nbMatrixCells = (size == 0 ? 1 : size);
            int nbStorageCells = (int) storageIndex.getSize();

            int startPos = (int) storageIndex.elementOffset(new int[storageIndex.getRank()]);
            if (profile) {
                prepareTime = System.currentTimeMillis() - time;
                time = System.currentTimeMillis();
            }
            Object fullArray = getArray().getStorage();
            if (profile) {
                storageTime = System.currentTimeMillis() - time;
                time = System.currentTimeMillis();
            }
            if (fullArray instanceof Object[]) {
                Object[] objArray = (Object[]) fullArray;
                Object partArray = null;
                for (int cell = 0; cell < nbMatrixCells; cell++) {
                    partArray = objArray[cell];
                    if (partArray != null) {
                        System.arraycopy(partArray, startPos, array, cell * nbStorageCells, nbStorageCells);
                    }
                }
            }
            if (profile) {
                long copyTime = System.currentTimeMillis() - time;
                GET_DIM_AND_TYPE_TIME.addAndGet(getArrayDimAndTypeTime);
                ARRAY_CREATION_TIME.addAndGet(arrayCreationTime);
                PREPARATION_TIME.addAndGet(prepareTime);
                GET_STORAGE_TIME.addAndGet(storageTime);
                COPY_TIME.addAndGet(copyTime);
            }
        }
        return array;
    }

    @Override
    public Object[] copyBlockTo1DJavaArrays(int blockSize) {
        // Instantiate a new convenient array for storage

        long time = -1, getArrayDimAndTypeTime = -1, arrayCreationTime = -1, prepareTime = -1, storageTime = -1;
        if (profile) {
            time = System.currentTimeMillis();
        }
        int length = (int) getArray().getSize();
        Class<?> type = getArray().getElementType();
        if (profile) {
            getArrayDimAndTypeTime = System.currentTimeMillis() - time;
            time = System.currentTimeMillis();
        }
        Object[] array;
        if (type == null) {
            array = null;
        } else {
            // XXX length already got the good value, we have to divide by the blockSize for minimum memory usage
            array = (Object[]) ArrayUtils.newInstance(type, blockSize, length / blockSize);
            if (profile) {
                arrayCreationTime = System.currentTimeMillis() - time;
                time = System.currentTimeMillis();
            }

            HdfIndex storageIndex = ((NxsIndex) getArray().getIndex()).getIndexStorage();
            DefaultIndex matrixIndex = ((NxsIndex) getArray().getIndex()).getIndexMatrix();

            // If the storing array is a stack of DataItem
            long size = matrixIndex.getSize();
            long nbMatrixCells = (size == 0 ? 1 : size);
            int nbStorageCells = (int) storageIndex.getSize();
            // Normally, the extracted data is at the right dimension.
            // This means that the dimension contains the block size.
            int[] shape = getArray().getShape();
            if (shape != null) {
                for (int dim : shape) {
                    if (dim == blockSize) {
                        nbStorageCells /= dim;
                        break;
                    } else if (dim != 1) {
                        break;
                    }
                }
            }
            int subArrayLength = (int) (nbStorageCells * nbMatrixCells);

            int startPos = (int) storageIndex.elementOffset(new int[storageIndex.getRank()]);
            if (profile) {
                prepareTime = System.currentTimeMillis() - time;
                time = System.currentTimeMillis();
            }
            Object fullArray = getArray().getStorage();
            if (profile) {
                storageTime = System.currentTimeMillis() - time;
                time = System.currentTimeMillis();
            }

            if (fullArray instanceof Object[]) {
                Object[] objArray = (Object[]) fullArray;
                Object partArray = null;
                for (int elementIndex = 0; elementIndex < blockSize; elementIndex++) {
                    partArray = objArray[(int) size];
                    Object subArray = new int[nbStorageCells];
                    System.arraycopy(partArray, startPos + elementIndex * subArrayLength, subArray, 0, subArrayLength);
                    array[elementIndex] = subArray;
                }

            }
            if (profile) {
                long copyTime = System.currentTimeMillis() - time;
                GET_DIM_AND_TYPE_TIME.addAndGet(getArrayDimAndTypeTime);
                ARRAY_CREATION_TIME.addAndGet(arrayCreationTime);
                PREPARATION_TIME.addAndGet(prepareTime);
                GET_STORAGE_TIME.addAndGet(storageTime);
                COPY_TIME.addAndGet(copyTime);
            }
        }
        return array;
    }

    @Override
    public Object copyToNDJavaArray() {
        return copyMatrixItemsToMultiDim();
    }

    // --------------------------------------------------
    // tools methods
    // --------------------------------------------------
    public static Object copyJavaArray(Object array) {
        Object result = array;
        if (result == null) {
            return null;
        }

        // Determine rank of array (by parsing data array class name)
        String sClassName = array.getClass().getName();
        int iRank = 0;
        int iIndex = 0;
        char cChar;
        while (iIndex < sClassName.length()) {
            cChar = sClassName.charAt(iIndex);
            iIndex++;
            if (cChar == '[') {
                iRank++;
            }
        }

        // Set dimension rank
        int[] shape = new int[iRank];

        // Fill dimension size array
        for (int i = 0; i < iRank; i++) {
            shape[i] = Array.getLength(result);
            result = Array.get(result, 0);
        }

        // Define a convenient array (shape and type)
        result = ArrayUtils.newInstance(array.getClass().getComponentType(), shape);

        return copyJavaArray(array, result);
    }

    public static Object copyJavaArray(Object source, Object target) {
        Object item = Array.get(source, 0);
        int length = Array.getLength(source);

        if (item.getClass().isArray()) {
            Object tmpSrc;
            Object tmpTar;
            for (int i = 0; i < length; i++) {
                tmpSrc = Array.get(source, i);
                tmpTar = Array.get(target, i);
                copyJavaArray(tmpSrc, tmpTar);
            }
        } else {
            System.arraycopy(source, 0, target, 0, length);
        }

        return target;
    }

    @Override
    public IArray getArray() {
        return mUtils.getArray();
    }

    @Override
    public void copyTo(IArray newArray) throws ShapeNotMatchException {
        mUtils.copyTo(newArray);
    }

    @Override
    public Object get1DJavaArray(Class<?> wantType) {
        return mUtils.get1DJavaArray(wantType);
    }

    @Override
    public void checkShape(IArray newArray) throws ShapeNotMatchException {
        mUtils.checkShape(newArray);
    }

//    @Override
//    public IArrayUtils concatenate(IArray array) throws ShapeNotMatchException {
//        return mUtils.concatenate(array);
//    }

    @Override
    public IArrayUtils reduce() {
        return mUtils.reduce();
    }

    @Override
    public IArrayUtils reduce(int dim) {
        return mUtils.reduce(dim);
    }

    @Override
    public IArrayUtils reduceTo(int rank) {
        return mUtils.reduceTo(rank);
    }

    @Override
    public IArrayUtils reshape(int[] shape) throws ShapeNotMatchException {
        return mUtils.reshape(shape);
    }

    @Override
    public IArrayUtils section(int[] origin, int[] shape) throws InvalidRangeException {
        return mUtils.section(origin, shape);
    }

    @Override
    public IArrayUtils section(int[] origin, int[] shape, long[] stride) throws InvalidRangeException {
        return mUtils.section(origin, shape, stride);
    }

    @Override
    public IArrayUtils sectionNoReduce(int[] origin, int[] shape, long[] stride) throws InvalidRangeException {
        return mUtils.sectionNoReduce(origin, shape, stride);
    }

    @Override
    public IArrayUtils sectionNoReduce(List<IRange> ranges) throws InvalidRangeException {
        return mUtils.sectionNoReduce(ranges);
    }

    @Override
    public IArrayUtils slice(int dim, int value) {
        return mUtils.slice(dim, value);
    }

    @Override
    public IArrayUtils transpose(int dim1, int dim2) {
        return mUtils.transpose(dim1, dim2);
    }

    @Override
    public boolean isConformable(IArray array) {
        return mUtils.isConformable(array);
    }

    @Override
    public IArrayUtils eltAnd(IArray booleanMap) throws ShapeNotMatchException {
        return mUtils.eltAnd(booleanMap);
    }

    @Override
    public IArrayUtils integrateDimension(int dimension, boolean isVariance) throws ShapeNotMatchException {
        return mUtils.integrateDimension(dimension, isVariance);
    }

    @Override
    public IArrayUtils enclosedIntegrateDimension(int dimension, boolean isVariance) throws ShapeNotMatchException {
        return mUtils.enclosedIntegrateDimension(dimension, isVariance);
    }

    @Override
    public IArrayUtils flip(int dim) {
        return mUtils.flip(dim);
    }

    @Override
    public IArrayUtils permute(int[] dims) {
        return mUtils.permute(dims);
    }

    // --------------------------------------------------
    // private methods
    // --------------------------------------------------
    /**
     * Copy the backing storage of this NxsArray into multidimensional
     * corresponding Java primitive array
     */
    private Object copyMatrixItemsToMultiDim() {
        // Create an array corresponding to the shape
        NxsArray array = (NxsArray) getArray();
        int[] shape = array.getShape();
        Object result = ArrayUtils.newInstance(array.getElementType(), shape);

        // Get the array's storage
        Object values = array.getStorage();

        int[] current;
        int length;
        Long startCell;

        try {

            ISliceIterator iter = array.getSliceIterator(1);
            NxsIndex startIdx = array.getIndex();
            HdfIndex storage = startIdx.getIndexStorage();
            DefaultIndex items = startIdx.getIndexMatrix();
            startIdx.setOrigin(new int[startIdx.getRank()]);

            length = startIdx.getShape()[startIdx.getRank() - 1];

            storage.set(new int[storage.getRank()]);

            // Turning buffers
            Object slab = null;
            Object dataset = null;

            // Copy each slice
            int last = -1;
            int cell = (int) (items.currentElement() - items.firstElement());
            while (iter.hasNext()) {
                iter.next();
                slab = result;

                // Getting the right slab in the multidimensional resulting array
                current = iter.getSlicePosition();
                startIdx.set(current);
                for (int pos = 0; pos < current.length - 1; pos++) {
                    slab = Array.get(slab, current[pos]);
                }

                cell = (int) (items.currentElement() - items.firstElement());
                if (last != cell) {
                    dataset = Array.get(values, cell);
                    last = cell;
                }
                startCell = storage.currentElement();
                System.arraycopy(dataset, startCell.intValue(), slab, 0, length);
            }
        } catch (ShapeNotMatchException e) {
        } catch (InvalidRangeException e) {
        }
        return result;
    }
}

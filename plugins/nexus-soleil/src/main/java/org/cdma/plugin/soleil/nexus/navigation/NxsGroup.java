/*******************************************************************************
 * Copyright (c) 2008 - ANSTO/Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * Norman Xiong (nxi@Bragg Institute) - initial API and implementation
 * Tony Lam (nxi@Bragg Institute) - initial API and implementation
 * Majid Ounsy (SOLEIL Synchrotron) - API v2 design and conception
 * Stephane Poirier (SOLEIL Synchrotron) - API v2 design and conception
 * Clement Rodriguez (ALTEN for SOLEIL Synchrotron) - API evolution
 * Gregory VIGUIER (SOLEIL Synchrotron) - API evolution
 * Raphael GIRARDOT (SOLEIL Synchrotron) - API evolution
 ******************************************************************************/
package org.cdma.plugin.soleil.nexus.navigation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.cdma.dictionary.Path;
import org.cdma.engine.hdf.navigation.HdfDataItem;
import org.cdma.engine.hdf.navigation.HdfGroup;
import org.cdma.exception.DataAccessException;
import org.cdma.exception.NoResultException;
import org.cdma.exception.NotImplementedException;
import org.cdma.exception.SignalNotAvailableException;
import org.cdma.interfaces.IAttribute;
import org.cdma.interfaces.IContainer;
import org.cdma.interfaces.IDataItem;
import org.cdma.interfaces.IDataset;
import org.cdma.interfaces.IDimension;
import org.cdma.interfaces.IGroup;
import org.cdma.interfaces.INode;
import org.cdma.plugin.soleil.nexus.NxsFactory;
import org.cdma.plugin.soleil.nexus.utils.NxsConstant;
import org.cdma.plugin.soleil.nexus.utils.NxsNode;
import org.cdma.plugin.soleil.nexus.utils.NxsPath;
import org.cdma.utils.Utilities.ModelType;

import fr.soleil.lib.project.ObjectUtils;

public final class NxsGroup extends ANxsContainer<HdfGroup> implements IGroup {

    // ****************************************************
    // Members
    // ****************************************************

    private Map<String, IDataItem> childrenItems;
    private Map<String, IGroup> childrenGroups;
    private List<IDimension> dimensions; // list of dimension
    private volatile boolean isChildUpdate; // is the children list up to date
    private final boolean isMultigroup; // is this group managing aggregation of group
    private NxsNode node;
    private Object childrenListingLock;

    // ****************************************************
    // Constructors
    // ****************************************************

    public NxsGroup(final NxsDataset dataset, final String name, final String path, final NxsGroup parent) {
        this(dataset, name, path, parent.getHdfGroup(), parent);
    }

    private NxsGroup(final NxsDataset dataset, final String name, final String path, final HdfGroup hdfParent,
            final NxsGroup parent) {
        super(dataset, parent);
        // Groups having a similar path from different files
        this.hdfContainers = new HdfGroup[1];
        hdfContainers[0] = new HdfGroup(NxsFactory.NAME, name, hdfParent, dataset.getHdfDataset());
//        hdfContainers[0] = new HdfGroup(NxsFactory.NAME, name, path, hdfParent, dataset.getHdfDataset());
        this.childrenItems = new LinkedHashMap<>();
        this.childrenGroups = new LinkedHashMap<>();
        this.dimensions = null;
        this.isChildUpdate = false;
        this.isMultigroup = false;
        this.node = null;
        // DATAREDUC-897: don't analyze children at group creation, and use a lock when listing.
        this.childrenListingLock = new Object();
//        listChildren();
    }

    public NxsGroup(final HdfGroup[] groups, final IGroup parent, final NxsDataset dataset) {
        super(dataset, parent);
        this.hdfContainers = groups.clone();
        this.childrenItems = new LinkedHashMap<>();
        this.childrenGroups = new LinkedHashMap<>();
        this.dimensions = null;
        this.isChildUpdate = false;
        this.isMultigroup = false;
        this.childrenListingLock = new Object();
        // DATAREDUC-897 and DATAREDUC-907: Analyze children at group creation only if engine needs it.
        boolean listChildren = false;
        if (groups != null) {
            for (HdfGroup group : groups) {
                if ((group != null) && group.shouldAlwaysCheckForChildren()) {
                    listChildren = true;
                }
                break;
            }
        }
        if (listChildren) {
            listChildren();
        }
    }

    public NxsGroup(final NxsGroup original) {
        super(original.dataset, original.parent);
        this.hdfContainers = new HdfGroup[original.hdfContainers.length];
        int i = 0;
        for (HdfGroup group : original.hdfContainers) {
            hdfContainers[i++] = group;
        }
        this.childrenItems = new LinkedHashMap<>();
        this.childrenGroups = new LinkedHashMap<>();
        this.dimensions = null;
        this.isChildUpdate = false;
        this.isMultigroup = (hdfContainers.length > 1);
        this.node = original.node;
        // DATAREDUC-897: don't analyze children at group creation, and use a lock when listing.
        this.childrenListingLock = new Object();
//        listChildren();
    }

    public NxsGroup(final IGroup parent, final NxsPath path, final NxsDataset dataset) {
        super(dataset, parent);
        try {
            List<IContainer> list = dataset.getRootGroup().findAllContainerByPath(path.getValue());
            List<IGroup> groups = new ArrayList<>();
            for (IContainer container : list) {
                if (container.getModelType() == ModelType.Group) {
                    groups.add((IGroup) container);
                }
            }
            HdfGroup[] array = new HdfGroup[groups.size()];
            this.hdfContainers = groups.toArray(array);
        } catch (NoResultException e) {
        }
        this.childrenItems = new LinkedHashMap<>();
        this.childrenGroups = new LinkedHashMap<>();
        this.dimensions = null;
        this.isChildUpdate = false;
        this.isMultigroup = false;
        // DATAREDUC-897: don't analyze children at group creation, and use a lock when listing.
        this.childrenListingLock = new Object();
//        listChildren();
    }

    // ****************************************************
    // Methods from interfaces
    // ****************************************************
    /**
     * Return a clone of this IGroup object.
     * 
     * @return new IGroup
     */
    @Override
    public NxsGroup clone() {
        NxsGroup clone = (NxsGroup) super.clone();
        // create a new lock object for the clone, to avoid 2 different references using the same lock.
        clone.childrenListingLock = new Object();
        clone.hdfContainers = new HdfGroup[hdfContainers.length];
        int i = 0;
        for (HdfGroup group : hdfContainers) {
            hdfContainers[i++] = group.clone();
        }
        clone.isChildUpdate = false;
        return clone;
    }

    @Override
    public ModelType getModelType() {
        return ModelType.Group;
    }

    @Override
    public String getLocation() {
        // return mParent.getLocation() + "/" + getShortName();
        NxsPath path = getNxsPath();
        return String.valueOf(path == null ? path : path.toString());
    }

    @Override
    public boolean hasAttribute(final String name, final String value) {
        for (IGroup group : hdfContainers) {
            if (group.hasAttribute(name, value)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void setParent(final IGroup group) {
        parent = group;
    }

    @Override
    public String getFactoryName() {
        return NxsFactory.NAME;
    }

    @Override
    public Map<String, String> harvestMetadata(final String mdStandard) throws DataAccessException {
        return null;
    }

    @Override
    public IGroup getParentGroup() {
        return parent;
    }

    @Override
    public IGroup getRootGroup() {
        return dataset.getRootGroup();
    }

    @Override
    public IDataItem getDataItem(final String shortName) {
//        List<IDataItem> list = getDataItemList();
//        IDataItem result = null;
//        INode nodeName = NxsPath.splitStringToNode(shortName)[0];
//        INode groupName;
//        INode[] nodes;
//        for (IDataItem item : list) {
//            nodes = NxsPath.splitStringToNode(item.getName());
//            groupName = nodes[nodes.length - 1];
//            if (groupName.matchesNode(nodeName)) {
//                result = item;
//                break;
//            }
//        }
//        return result;
        listChildren();
        return ((childrenItems == null) || (shortName == null) ? null : childrenItems.get(shortName));
    }

//    @Override
//    public IDataItem findDataItem(final IKey key) {
//        IDataItem item = null;
//        List<IContainer> list = new ArrayList<IContainer>();
//        try {
//            list = findAllOccurrences(key);
//        } catch (NoResultException e) {
//        }
//
//        for (IContainer object : list) {
//            if (object.getModelType().equals(ModelType.DataItem)) {
//                item = (IDataItem) object;
//                break;
//            }
//        }
//
//        return item;
//    }
//
//    @Override
//    public IDataItem findDataItem(final String keyName) {
//        IKey key = NxsFactory.getInstance().createKey(keyName);
//
//        return findDataItem(key);
//    }

    @Override
    public IDataItem getDataItemWithAttribute(final String name, final String value) {
        Collection<IDataItem> list = getDataItemList();
        IDataItem result = null;
        for (IDataItem item : list) {
            if (item.hasAttribute(name, value)) {
                result = item;
                break;
            }
        }

        return result;
    }

//    @Override
//    public IDataItem findDataItemWithAttribute(final IKey key, final String name, final String attribute)
//            throws NoResultException {
//        List<IContainer> list = findAllContainers(key);
//        IDataItem result = null;
//        for (IContainer item : list) {
//            if (item.getModelType() == ModelType.DataItem && item.hasAttribute(name, attribute)) {
//                result = (IDataItem) item;
//                break;
//            }
//        }
//
//        return result;
//    }
//
//    @Override
//    public IGroup findGroupWithAttribute(final IKey key, final String name, final String value) {
//        List<IContainer> list;
//        try {
//            list = findAllContainers(key);
//        } catch (NoResultException e) {
//            list = new ArrayList<IContainer>();
//        }
//        IGroup result = null;
//        for (IContainer item : list) {
//            if (item.getModelType() == ModelType.Group && item.hasAttribute(name, value)) {
//                result = (IGroup) item;
//                break;
//            }
//        }
//
//        return result;
//    }

    @Override
    public IContainer getContainer(final String shortName) {
//        List<IContainer> list = listChildren();
//        IContainer result = null;
//
//        for (IContainer container : list) {
//            if (container.getShortName().equals(shortName)) {
//                result = container;
//                break;
//            }
//        }
//
//        return result;
        IContainer result = null;
        if (shortName != null) {
            result = getDataItem(shortName);
            if (result == null) {
                result = getGroup(shortName);
            }
        }
        return result;
    }

    @Override
    public IGroup getGroup(final String shortName) {
        IGroup result = null;
        if (shortName != null) {
//            // XXX why this test?
//            if (shortName.equals(this.getShortName())) {
//                result = this;
//            } else {
//                // NxsNode node = NxsNode.createNode(shortName);
//                // List<IContainer> containers = listContainer(node);
//                // if (!containers.isEmpty()) {
//                //     IContainer cnt = containers.get(0);
//                //     if (cnt.getModelType().equals(ModelType.Group)) {
//                //         result = (IGroup) cnt;
//                //     }
//                // }
            listChildren();
            result = childrenGroups.get(shortName);
//            }
        }
        return result;

    }

    @Override
    public IGroup getGroupWithAttribute(final String attributeName, final String attributeValue) {
        Collection<IGroup> list = getGroupList();
        IGroup result = null;
        for (IGroup group : list) {
            if (group.hasAttribute(attributeName, attributeValue)) {
                result = group;
                break;
            }
        }
        return result;
    }

    @Override
    public Collection<IDataItem> getDataItemList() {
        listChildren();
//        ArrayList<IDataItem> list = new ArrayList<IDataItem>();
//        for (IContainer container : children.values()) {
//            if (container.getModelType() == ModelType.DataItem) {
//                list.add((IDataItem) container);
//            }
//        }
//        return list;
        return childrenItems.values();
    }

    @Override
    public int getDataItemCount() {
        listChildren();
//        int count = 0;
//        for (IContainer container : children) {
//            if (container.getModelType() == ModelType.DataItem) {
//                count++;
//            }
//        }
//        return count;
        return childrenItems.size();
    }

    @Override
    public IDataset getDataset() {
        return dataset;
    }

//    @Override
//    public IGroup findGroup(final IKey key) {
//        IGroup item = null;
//        List<IContainer> list = new ArrayList<IContainer>();
//        try {
//            list = findAllOccurrences(key);
//        } catch (NoResultException e) {
//        }
//
//        for (IContainer object : list) {
//            if (object.getModelType().equals(ModelType.Group)) {
//                item = (IGroup) object;
//                break;
//            }
//        }
//
//        return item;
//    }

//    @Override
//    public IGroup findGroup(final String keyName) {
//        IKey key = NxsFactory.getInstance().createKey(keyName);
//
//        return findGroup(key);
//    }

    @Override
    public Collection<IGroup> getGroupList() {
        listChildren();
//        ArrayList<IGroup> list = new ArrayList<IGroup>();
//        for (IContainer container : children) {
//            if (container.getModelType() == ModelType.Group) {
//                list.add((IGroup) container);
//            }
//        }
        return childrenGroups.values();
    }

    @Override
    public int getGroupCount() {
        listChildren();
//        int count = 0;
//        for (IContainer container : children) {
//            if (container.getModelType() == ModelType.Group) {
//                count++;
//            }
//        }
//        return count;
        return childrenGroups.size();
    }

//    @Override
//    public IContainer findContainer(final String shortName) {
//        IKey key = NxsFactory.getInstance().createKey(shortName);
//        IContainer result;
//        try {
//            List<IContainer> list = findAllOccurrences(key);
//            if (list.size() > 0) {
//                result = list.get(0);
//            } else {
//                result = null;
//            }
//        } catch (NoResultException e) {
//            result = null;
//        }
//
//        return result;
//    }

    @Override
    public IContainer findContainerByPath(final String path) throws NoResultException {
        List<IContainer> containers = findAllContainerByPath(path);
        IContainer result = null;

        if (containers.size() > 0) {
            result = containers.get(0);
        }

        return result;
    }

    @Override
    public List<IContainer> findAllContainerByPath(final String path) throws NoResultException {
        List<IContainer> result = new ArrayList<>();
        IGroup root = getRootGroup();
//        if (root.getParentGroup() != null) {
//            root = root.getParentGroup();
//        }

        // Try to list all nodes matching the path
        // Transform path into a NexusNode array
        if (path != null) {
            NxsNode[] nodes = NxsPath.splitStringToNode(path);
            if ((nodes != null) && (nodes.length == 0)) {
                result.add(root);
            } else {
                // Call recursive method
                int level = 0;
                result = findAllContainers(root, nodes, level);
            }
        }
        return result;
    }

    @Override
    public boolean removeDataItem(final IDataItem item) {
        return removeDataItem(item.getShortName());
    }

    @Override
    public boolean removeDataItem(final String varName) {
        boolean succeed = false;
        for (IGroup group : hdfContainers) {
            if (group.removeDataItem(varName)) {
                succeed = true;
            }
        }
        return succeed;
    }

    @Override
    public boolean removeGroup(final IGroup group) {
        return removeGroup(group.getShortName());
    }

    @Override
    public boolean removeGroup(final String shortName) {
        boolean succeed = false;
        for (IGroup group : hdfContainers) {
            if (group.removeGroup(shortName)) {
                succeed = true;
            }
        }
        return succeed;
    }

//    @Deprecated
//    @Override
//    public void setDictionary(final org.cdma.interfaces.IDictionary dictionary) {
//        if (groups.length > 0) {
//            groups[0].setDictionary(dictionary);
//        }
//    }

//    @Deprecated
//    @Override
//    public org.cdma.interfaces.IDictionary findDictionary() {
//        org.cdma.interfaces.IDictionary dictionary = null;
//        if (groups.length > 0) {
//            IFactory factory = NxsFactory.getInstance();
//            dictionary = new org.cdma.plugin.soleil.nexus.dictionary.NxsDictionary();
//            try {
//                dictionary.readEntries(Factory.getPathMappingDictionaryFolder(factory)
//                        + NxsLogicalGroup.detectDictionaryFile((NxsDataset) getDataset()));
//            } catch (UriAccessException e) {
//                dictionary = null;
//                Factory.getLogger().log(Level.SEVERE, e.getMessage());
//            }
//        }
//        return dictionary;
//    }

    @Override
    public boolean isRoot() {
        return (hdfContainers.length > 0 && hdfContainers[0].isRoot());
    }

    @Override
    public boolean isEntry() {
        boolean result = false;
        IGroup parent = getParentGroup();
        if (parent != null) {
            result = getParentGroup().isRoot();
        }
        return result;
    }

//    @Override
//    public List<IContainer> findAllContainers(final IKey key) throws NoResultException {
//        return findAllOccurrences(key);
//    }
//
//    @Override
//    public List<IContainer> findAllOccurrences(final IKey key) throws NoResultException {
//        String pathStr = findDictionary().getPath(key).toString();
//        Path path = new Path(NxsFactory.getInstance(), pathStr);
//        return findAllContainerByPath(path.getValue());
//    }

    @Override
    public IContainer findObjectByPath(final Path path) {
        IContainer result = null;
        try {
            result = findContainerByPath(path.getValue());
        } catch (NoResultException e) {
            result = null;
        }
        return result;
    }

    @Override
    public void addOneAttribute(final IAttribute attribute) {
        if ((hdfContainers != null) && (hdfContainers.length > 0)) {
            hdfContainers[0].addOneAttribute(attribute);
        }
    }

    @Override
    public void addStringAttribute(final String name, final String value) {
        if ((hdfContainers != null) && (hdfContainers.length > 0) && (hdfContainers[0] != null)) {
            hdfContainers[0].addStringAttribute(name, value);
        }
    }

    @Override
    public boolean removeDimension(final IDimension dimension) {
        throw new NotImplementedException();
    }

    @Override
    public boolean removeDimension(final String dimName) {
        throw new NotImplementedException();
    }

    @Override
    public List<IDimension> getDimensionList() {
        listChildren();
        return dimensions;
    }

    @Override
    public IDimension getDimension(final String name) {
        IDimension result = null;
        if (name != null) {
            for (IDimension dimension : dimensions) {
                if (name.equals(dimension.getName())) {
                    result = dimension;
                    break;
                }
            }
        }
        return result;
    }

    @Override
    public void addDataItem(final IDataItem item) {
        if (item instanceof NxsDataItem) {
            NxsDataItem nxsDataItem = (NxsDataItem) item;
            HdfDataItem[] itemsToAdd = nxsDataItem.getHdfDataItems();
            for (HdfDataItem hdfDataItem : itemsToAdd) {
                hdfContainers[0].addDataItem(hdfDataItem);
            }
            item.setParent(this);
        }
//        children.add(item);
        if (item != null) {
            childrenItems.put(item.getShortName(), item);
        }
    }

    @Override
    public boolean removeAttribute(final IAttribute attribute) {
        throw new NotImplementedException();
    }

    @Override
    public void addOneDimension(final IDimension dimension) {
        throw new NotImplementedException();
    }

    @Override
    public void addSubgroup(final IGroup group) {
        if (group instanceof NxsGroup) {
            NxsGroup nxsGroup = (NxsGroup) group;
            hdfContainers[0].addSubgroup(nxsGroup.getHdfGroup());
        }
//        children.add(group);
        if (group != null) {
            childrenGroups.put(group.getShortName(), group);
        }
    }

    @Override
    public void updateDataItem(final String key, final IDataItem dataItem) throws SignalNotAvailableException {
        throw new NotImplementedException();
    }

    @Override
    public long getLastModificationDate() {
        checkDataset(true);
        return dataset.getLastModificationDate();
    }

    // ------------------------------------------------------------------------
    // Protected methods
    // ------------------------------------------------------------------------
    protected void setChild(final IContainer node) {
//        if (!children.contains(node)) {
//            children.add(node);
//        }
        if (node instanceof IDataItem) {
            IDataItem item = (IDataItem) node;
            childrenItems.put(item.getShortName(), item);
        } else if (node instanceof IGroup) {
            IGroup group = (IGroup) node;
            childrenGroups.put(group.getShortName(), group);
        }
    }

    // ------------------------------------------------------------------------
    // Private methods
    // ------------------------------------------------------------------------
    private void listChildren() {
        if (isMultigroup) {
            listChildrenMultiGroup();
        } else {
            listChildrenMonoGroup();
        }
    }

    private void listChildrenMultiGroup() {
        if (!isChildUpdate) {
            // DATAREDUC-897: lock to avoid over analyzing
            synchronized (childrenListingLock) {
                if (!isChildUpdate) {
                    List<IContainer> tmp = null;
                    childrenItems = new LinkedHashMap<>();
                    childrenGroups = new LinkedHashMap<>();
                    dimensions = new ArrayList<>();
                    // Store in a map all different containers from all m_groups
                    String tmpName;
                    Map<String, ArrayList<IContainer>> items = new HashMap<>();
                    for (IGroup group : hdfContainers) {
                        tmp = new ArrayList<>();
                        tmp.addAll(group.getDataItemList());
                        tmp.addAll(group.getGroupList());
                        for (IContainer item : tmp) {
                            tmpName = item.getShortName();
                            if (items.containsKey(tmpName)) {
                                items.get(tmpName).add(item);
                            } else {
                                ArrayList<IContainer> tmpList = new ArrayList<>();
                                tmpList.add(item);
                                items.put(tmpName, tmpList);
                            }
                        } // end for (IContainer item : tmp)
                    } // end for (IGroup group : hdfContainers)
                    checkDataset(true);
                    // Construct what were found
                    for (Entry<String, ArrayList<IContainer>> entry : items.entrySet()) {
                        tmp = entry.getValue();
                        // If a Group list then construct a new Group folder
                        if (tmp.get(0).getModelType() == ModelType.Group) {
                            NxsGroup group = new NxsGroup(tmp.toArray(new HdfGroup[tmp.size()]), this, dataset);
                            childrenGroups.put(group.getShortName(), group);
                        }
                        // If a IDataItem list then construct a new compound NxsDataItem
                        else {
                            ArrayList<HdfDataItem> nxsDataItems = new ArrayList<>();
                            for (IContainer item : tmp) {
                                if (item.getModelType() == ModelType.DataItem) {
                                    nxsDataItems.add((HdfDataItem) item);
                                }
                            }
                            HdfDataItem[] array = new HdfDataItem[nxsDataItems.size()];
                            nxsDataItems.toArray(array);
                            NxsDataItem item = new NxsDataItem(array, this, dataset);
                            childrenItems.put(item.getShortName(), item);
                        }
                    } // end for (Entry<String, ArrayList<IContainer>> entry : items.entrySet())
                    isChildUpdate = true;
                } // end if (!isChildUpdate)
            } // end synchronized (childrenListingLock)
        } // end if (!isChildUpdate)
    }

    private void listChildrenMonoGroup() {
        if (!isChildUpdate) {
            // DATAREDUC-897: lock to avoid over analyzing
            synchronized (childrenListingLock) {
                if (!isChildUpdate) {
                    childrenItems = new LinkedHashMap<>();
                    childrenGroups = new LinkedHashMap<>();
                    dimensions = new ArrayList<>();

                    checkDataset(true);
                    // Store in a list all different containers from all m_groups
                    for (IDataItem item : hdfContainers[0].getDataItemList()) {
                        NxsDataItem child = new NxsDataItem((HdfDataItem) item, this, dataset);
                        childrenItems.put(child.getShortName(), child);
                    }

                    for (IGroup group : hdfContainers[0].getGroupList()) {
                        // HACK GV mGroups[0]
                        NxsGroup child = new NxsGroup(new HdfGroup[] { (HdfGroup) group }, this, dataset);
                        childrenGroups.put(child.getShortName(), child);
                    }
                    isChildUpdate = true;
                } // end if (!isChildUpdate)
            } // end synchronized (childrenListingLock)
        } // end if (!isChildUpdate)
    }

    private List<IContainer> findAllContainers(final IContainer container, final NxsNode[] nodes, final int level) {
        List<IContainer> result = new ArrayList<>();
        if (container != null) {
            if (container instanceof NxsGroup) {
                NxsGroup group = (NxsGroup) container;
                if (nodes.length > level) {
                    // List current node children
                    List<IContainer> children;

                    NxsNode current = nodes[level];
                    children = group.listContainer(current);

                    for (IContainer node : children) {
                        if (level < nodes.length - 1) {
                            result.addAll(findAllContainers(group.getContainer(node.getShortName()), nodes, level + 1));
                        }
                        // Create IContainer and add it to result list
                        else {
                            result.add(group.getContainer(node.getShortName()));
                        }
                    }
                }
            } else {
                result.add(container);
            }
        }
        return result;
    }

    // ****************************************************
    // Specific methods
    // ****************************************************
    @Override
    public NxsPath getNxsPath() {
        if (nxsPath == null) {
            nxsPath = new NxsPath(this);
        }
        return nxsPath;
    }

    @Override
    public String toString() {
        StringBuilder res = new StringBuilder();
        boolean added = false;
        for (IGroup group : hdfContainers) {
            res.append(group.getLocation()).append("\n");
            added = true;
        }
        if (added) {
            res.deleteCharAt(res.length() - 1);
        }
        return res.toString();
    }

    public List<IContainer> listContainer(final NxsNode node) {
        List<IContainer> result = new ArrayList<IContainer>();
        if (node != null) {
            for (IGroup group : getGroupList()) {
                NxsNode nxNode = ((NxsGroup) group).getNxsNode();
                if (nxNode != null && nxNode.matchesPartNode(node)) {
                    result.add(group);
                }
            }
            for (IDataItem item : getDataItemList()) {
                INode nxNode = new NxsNode(item);
                if (nxNode != null && nxNode.matchesPartNode(node)) {
                    result.add(item);
                }
            }
        }
        return result;
    }

    private NxsNode getNxsNode() {
        if (node == null) {
            String clazz = ObjectUtils.EMPTY_STRING;
            IAttribute attribute = getAttribute(NxsConstant.NX_CLASS);
            if (attribute != null) {
                clazz = attribute.getStringValue();
            }
            node = new NxsNode(getShortName(), clazz);
        }
        return node;
    }

    public HdfGroup getHdfGroup() {
        return hdfContainers[0];
    }
}

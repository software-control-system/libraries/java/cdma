package org.cdma.plugin.soleil.nexus.external;

import org.cdma.dictionary.Context;
import org.cdma.utilities.expression.ExpressionDelegate;

public class ApplyExpression extends ANexusNumberCorrector<ExpressionDelegate> {

    @Override
    protected ExpressionDelegate extractParameters(Context context) {
        return new ExpressionDelegate(context);
    }

    @Override
    protected double getCorrectedValue(double value, ExpressionDelegate parameters) {
        return parameters.getCorrectedValue(value);
    }

}

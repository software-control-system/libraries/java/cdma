package org.cdma.plugin.soleil.nexus;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;

import org.cdma.exception.CDMAException;
import org.cdma.exception.DataAccessException;
import org.cdma.interfaces.IDataItem;
import org.cdma.interfaces.IDataset;
import org.cdma.interfaces.IGroup;
import org.cdma.plugin.soleil.nexus.navigation.NxsDataset;

public class DictionaryBuilder {
    public static void generateMappingDictionnary(String inputFileName) throws CDMAException {
        File inputFile = new File(inputFileName);
        try (FileWriter mappingWriter = new FileWriter(
                inputFile.getParentFile().getPath() + File.separator + "mapping.xml");
                FileWriter viewWriter = new FileWriter(
                        inputFile.getParentFile().getPath() + File.separator + "view.xml");
                IDataset dataset = NxsDataset.instanciate(new URI(inputFileName));) {
            dataset.open();
            IGroup root = dataset.getRootGroup();
            writeMappingAndViewForGroup(mappingWriter, viewWriter, root);
        } catch (IOException e) {
            throw new DataAccessException(e);
        } catch (URISyntaxException e) {
            throw new CDMAException(e);
        }
    }

    public static void exploreFile(String inputFileName) throws Exception {
        IDataset dataset = NxsDataset.instanciate(new URI(inputFileName));
        dataset.open();
        IGroup root = dataset.getRootGroup();
        Collection<IGroup> groupList = root.getGroupList();
        for (IGroup iGroup : groupList) {
            // XXX DEBUG
            System.out.println(iGroup.getName());
        }
    }

/*
 * 
<item key="image_C05">
<path>/{NXentry}/NANOSCOPIUM/I13-LN-C05__DT__IMG-ANALYZER/image</path>
<call>DataItemStacker</call>
<call>HarvestEquipmentAttributes</call>
<attribute name="interpretation" value="image"/>
</item>

and

<item key ="image_C05"/>
*/
    public static void writeMappingAndViewForGroup(FileWriter mappingWriter, FileWriter viewWriter,
            IGroup groupToExplore) throws IOException {
        Collection<IGroup> groups = groupToExplore.getGroupList();
        Collection<IDataItem> dataitems = groupToExplore.getDataItemList();

        for (IDataItem iDataitem : dataitems) {
            if (iDataitem.getType() != String.class) {

                String name = iDataitem.getShortName();
                String key = "<item key=\"" + iDataitem.getParentGroup().getShortName() + "_" + name + "\"";
                mappingWriter.write(key + ">\n");
                mappingWriter.write("<path>" + iDataitem.getName() + "</path>\n");
                mappingWriter.write("<call>DataItemStacker</call>\n");
                mappingWriter.write("<call>HarvestEquipmentAttributes</call>\n");

                if ("image".equals(name)) {
                    mappingWriter.write("<attribute name=\"interpretation\" value=\"image\"/>\n");
                }
                mappingWriter.write("</item>\n");
                viewWriter.write(key + "/>\n");
            }
        }

        for (IGroup iGroup : groups) {
            writeMappingAndViewForGroup(mappingWriter, viewWriter, iGroup);
        }
    }

    public static void main(String args[]) throws Exception {
        // DictionaryBuilder.generateMappingDictionnary("/home/gregory/Work/Samples/NeXuS/diag_00070.nxs");
        DictionaryBuilder.exploreFile("/home/gregory/Work/Samples/NeXuS/diag_00080.nxs");

    }
}

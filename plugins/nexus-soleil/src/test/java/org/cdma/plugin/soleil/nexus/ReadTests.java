/*******************************************************************************
 * Copyright (c) 2008 - ANSTO/Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * Norman Xiong (nxi@Bragg Institute) - initial API and implementation
 * Tony Lam (nxi@Bragg Institute) - initial API and implementation
 * Majid Ounsy (SOLEIL Synchrotron) - API v2 design and conception
 * Stephane Poirier (SOLEIL Synchrotron) - API v2 design and conception
 * Clement Rodriguez (ALTEN for SOLEIL Synchrotron) - API evolution
 * Gregory VIGUIER (SOLEIL Synchrotron) - API evolution
 ******************************************************************************/
package org.cdma.plugin.soleil.nexus;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.cdma.engine.hdf.utils.HdfObjectUtils;
import org.cdma.exception.CDMAException;
import org.cdma.interfaces.IArray;
import org.cdma.interfaces.IContainer;
import org.cdma.interfaces.IDataItem;
import org.cdma.interfaces.IDataset;
import org.cdma.interfaces.IGroup;
import org.cdma.interfaces.ISliceIterator;
import org.cdma.plugin.soleil.nexus.navigation.NxsDataset;
import org.junit.Test;

public class ReadTests {

    @Test
    public void aReadFile() throws CDMAException {
        IDataset dataSet = NxsDataset.instanciate(WriteTests.FIRST_FILE_TO_WRITE.toURI());
        IGroup root = dataSet.getRootGroup();
        if (root != null) {

            // Test ROOT
            Collection<IGroup> groupList = root.getGroupList();
            assertEquals(2, groupList.size());
            assertNull(root.getParentGroup());

            // Test IGroup
            IGroup group1 = root.getGroup("group1");
            assertNotNull(group1);
            assertEquals(2, group1.getAttributeList().size());
            assertEquals("Attribute List size", 2, group1.getAttributeList().size());
            assertEquals("group1", group1.getShortName());
            assertEquals("/group1", group1.getName());
            assertEquals(root, group1.getRootGroup());
            assertEquals(dataSet, group1.getDataset());
            assertTrue(group1.isEntry());
            assertFalse(group1.isRoot());

            // Test IDataItem
            IDataItem data1 = group1.getDataItem("data1");
            assertNotNull(data1);
            IArray iArray = data1.getData();
            Object storage = iArray.getStorage();

            Object[] arrays = (Object[]) storage;
            int[] array = (int[]) arrays[0];
            int[] expected = { 0, 1, 2, 3 };
            assertArrayEquals(expected, array);
            assertEquals(int.class, iArray.getElementType());
            assertEquals(root, data1.getRootGroup());
            assertEquals(group1, data1.getParentGroup());
            assertEquals(dataSet, data1.getDataset());
            assertEquals("/group1/data1", data1.getName());
            assertEquals("data1", data1.getShortName());

            // Test String
            IDataItem stringData = group1.getDataItem("stringData");
            assertNotNull(stringData);
            IArray stringArray = stringData.getData();
            /*Object messageArray = */stringArray.getStorage();
            String message = stringData.readScalarString();
            System.out.println(message);
            // Test Navigation
            IContainer container = root.findContainerByPath("/group1/data1");
            assertNotNull(container);

            List<IContainer> containers = root.findAllContainerByPath("/group1/data1");
            assertNotNull(containers);
            assertTrue(containers.size() == 1);

            containers = root.findAllContainerByPath("/group*Modified/data*");
            assertNotNull(containers);
            assertTrue(containers.size() == 1);

            containers = root.findAllContainerByPath("/group*/data*");
            assertNotNull(containers);
            assertTrue(containers.size() == 2);

        }
    }

    @Test
    public void bReadCompressedFile() {
        String fileName = "D:/Samples/Nanoscopium/series_269_master.h5";
        File fileToRead = new File(fileName);
        String datasetName = "DS1";
        int DIM_X = 32;
        int DIM_Y = 64;
//        int CHUNK_X = 4;
//        int CHUNK_Y = 8;
//        int RANK = 2;
//        int NDIMS = 2;

        long fileId = -1;
        long datasetId = -1;
        long dcplId = -1;
        int[][] dsetData = new int[DIM_X][DIM_Y];
        IDataset dataSet = null;

        // Open an existing file.
        try {
            dataSet = NxsDataset.instanciate(fileToRead.toURI());
            if (dataSet != null) {
                IGroup root = dataSet.getRootGroup();
                IGroup entry = root.getGroup("entry");
                if (entry != null) {
                    IDataItem data = entry.getDataItem("data_000000");
                    IArray array = data.getData();
                    for (ISliceIterator it = array.getSliceIterator(2); it.hasNext();) {
                        IArray slideArray = it.getArrayNext();
                        // TODO DEBUG
                        System.out.println(Arrays.toString(slideArray.getShape()));
                        /*Object dataArray = */slideArray.getStorage();
                    }
                    // TODO DEBUG
                    System.out.println("STOP");
                }

            }
            // file_id = H5.H5Fopen(FILENAME, HDF5Constants.H5F_ACC_RDONLY, HDF5Constants.H5P_DEFAULT);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Open an existing dataset.
        try {
            if (fileId >= 0) {
                datasetId = HdfObjectUtils.openH5Dataset(fileId, datasetName);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Retrieve the dataset creation property list.
        try {
            if (datasetId >= 0) {
                dcplId = HdfObjectUtils.getCreatePlist(datasetId);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Retrieve and print the filter type. Here we only retrieve the
        // first filter because we know that we only added one filter.
        try {
            if (dcplId >= 0) {
                String filterType = HdfObjectUtils.getFilterType(dcplId);
                System.out.print("Filter type is: ");
                System.out.println(filterType);
                System.out.println();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Read the data using the default properties.
        try {
            if (datasetId >= 0) {
                HdfObjectUtils.readDatasetForTests(datasetId, dsetData);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

package org.cdma.plugin.soleil.edf.external;

import org.cdma.dictionary.ANumberCorrector;
import org.cdma.exception.CDMAException;
import org.cdma.interfaces.IArray;
import org.cdma.interfaces.IDataItem;
import org.cdma.plugin.soleil.edf.EdfFactory;
import org.cdma.plugin.soleil.edf.array.InlineArray;
import org.cdma.plugin.soleil.edf.navigation.EdfDataItem;

public abstract class AEdfNumberCorrector<D> extends ANumberCorrector<EdfDataItem, InlineArray, D> {

    @Override
    public String getFactoryName() {
        return EdfFactory.NAME;
    }

    @Override
    protected EdfDataItem prepareItemCopy(IDataItem originalItem) throws CDMAException {
        EdfDataItem item;
        if (originalItem instanceof EdfDataItem) {
            EdfDataItem edfItem = (EdfDataItem) originalItem;
            IArray data = edfItem.getData();
            item = new EdfDataItem(edfItem.getName(), generateArray(new double[0], data.getShape()));
            item.setParent(edfItem.getParentGroup());
        } else {
            item = null;
        }
        return item;
    }

    @Override
    protected InlineArray generateArray(double[] value, int[] shape) throws CDMAException {
        return new InlineArray(EdfFactory.NAME, value, shape);
    }

}

/*******************************************************************************
 * Copyright (c) 2008 - ANSTO/Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * Norman Xiong (nxi@Bragg Institute) - initial API and implementation
 * Tony Lam (nxi@Bragg Institute) - initial API and implementation
 * Majid Ounsy (SOLEIL Synchrotron) - API v2 design and conception
 * Stephane Poirier (SOLEIL Synchrotron) - API v2 design and conception
 * Clement Rodriguez (ALTEN for SOLEIL Synchrotron) - API evolution
 * Gregory VIGUIER (SOLEIL Synchrotron) - API evolution
 * Raphael GIRARDOT (SOLEIL Synchrotron) - API evolution
 ******************************************************************************/
package org.cdma.plugin.soleil.edf.dictionary;

import org.cdma.Factory;
import org.cdma.IFactory;
import org.cdma.dictionary.ExtendedDictionary;
import org.cdma.dictionary.LogicalGroup;
import org.cdma.exception.DataAccessException;
import org.cdma.exception.UriAccessException;
import org.cdma.interfaces.IDataItem;
import org.cdma.interfaces.IDataset;
import org.cdma.interfaces.IGroup;
import org.cdma.interfaces.IKey;
import org.cdma.plugin.soleil.edf.EdfFactory;
import org.cdma.plugin.soleil.edf.navigation.EdfDataset;
import org.cdma.plugin.soleil.edf.navigation.EdfGroup;

public class EdfLogicalGroup extends LogicalGroup {

    private static final String DEFAULT_Dictionnary = "default.xml";
    private static final String FACILITY = "Facility";

    public EdfLogicalGroup(String view, IKey key, IDataset dataset, boolean exception) {
        super(view, key, dataset, exception);
    }

    public EdfLogicalGroup(String view, IKey key, IDataset dataset) {
        super(view, key, dataset);
    }

    public EdfLogicalGroup(String view, LogicalGroup parent, IKey key, IDataset dataset, boolean exception) {
        super(view, parent, key, dataset, exception);
    }

    public EdfLogicalGroup(String view, LogicalGroup parent, IKey key, IDataset dataset) {
        super(view, parent, key, dataset);
    }

    /**
     * According to the current corresponding dataset, this method will try to guess which XML
     * dictionary mapping file should be used
     *
     * @return
     * @throws UriAccessException
     */
    public static String detectDictionaryFile(EdfDataset dataset) {
        String dictionary = DEFAULT_Dictionnary;
        if (dataset.getRootGroup() != null) {
            EdfGroup root = dataset.getRootGroup();
            if (root != null) {
                IDataItem facilityItem = root.getDataItem(FACILITY);
                if (facilityItem == null) {
                    if (!root.getGroupList().isEmpty()) {
                        IGroup firstGroup = root.getGroupList().get(0);
                        facilityItem = firstGroup.getDataItem(FACILITY);
                    }
                }
                if (facilityItem != null) {
                    try {
                        dictionary = facilityItem.readScalarString() + ".xml";
                    } catch (DataAccessException e) {
                        Factory.getLogger().error(e.getMessage(), e);
                    }
                }
            }
        }
        return dictionary;
    }

    @Override
    public ExtendedDictionary findAndReadDictionary() {
        IFactory factory = EdfFactory.getInstance();

        // Detect the key dictionary file and mapping dictionary file
        String keyFile = Factory.getPathKeyDictionary(view);
        String mapFile = Factory.getPathMappingDictionaryFolder(factory)
                + EdfLogicalGroup.detectDictionaryFile((EdfDataset) getDataset());
        ExtendedDictionary dictionary = new ExtendedDictionary(factory, keyFile, mapFile);
        try {
            dictionary.readEntries();
        } catch (UriAccessException e) {
            Factory.getLogger().error(e.getMessage());
            dictionary = null;
        }

        return dictionary;
    }
}

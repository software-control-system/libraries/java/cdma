/*******************************************************************************
 * Copyright (c) 2008 - ANSTO/Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * Norman Xiong (nxi@Bragg Institute) - initial API and implementation
 * Tony Lam (nxi@Bragg Institute) - initial API and implementation
 * Majid Ounsy (SOLEIL Synchrotron) - API v2 design and conception
 * Stephane Poirier (SOLEIL Synchrotron) - API v2 design and conception
 * Clement Rodriguez (ALTEN for SOLEIL Synchrotron) - API evolution
 * Gregory VIGUIER (SOLEIL Synchrotron) - API evolution
 * Raphael GIRARDOT (SOLEIL Synchrotron) - API evolution
 ******************************************************************************/
package org.cdma.plugin.soleil.edf.navigation;

import java.lang.reflect.Array;
import java.util.Arrays;

import org.cdma.Factory;
import org.cdma.arrays.DefaultArrayMatrix;
import org.cdma.exception.InvalidArrayTypeException;
import org.cdma.interfaces.IArray;
import org.cdma.interfaces.IAttribute;
import org.cdma.plugin.soleil.edf.EdfFactory;

import fr.soleil.lib.project.math.ArrayUtils;

public class EdfAttribute implements IAttribute {

    private final String factoryName; // factory name that attribute depends on
    private final String name;
    private IArray value;

    public EdfAttribute(String name, Object value) {
        this.factoryName = EdfFactory.NAME;
        this.name = name;

        Object data = value;
        if (value != null) {
            if (value.getClass().isArray()) {
                if (data instanceof char[]) {
                    data = new String[] { new String((char[]) value) };
                }
            } else {
                data = ArrayUtils.newInstance(value.getClass(), 1);
                Array.set(data, 0, value);
            }

            try {
                this.value = new DefaultArrayMatrix(factoryName, data);
                this.value.lock();

            } catch (InvalidArrayTypeException e) {
                Factory.getLogger().error("Unable to initialize data!", e);
            }
        }
    }

    @Override
    public String getFactoryName() {
        return factoryName;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Class<?> getType() {
        Class<?> result = null;
        if (value != null) {
            result = value.getElementType();
        }
        return result;
    }

    @Override
    public boolean isString() {
        boolean result = false;
        if (value != null) {
            Class<?> container = value.getElementType();
            result = (container.equals(Character.TYPE) || container.equals(String.class));
        }
        return result;
    }

    @Override
    public boolean isArray() {
        return value.getSize() > 1;
    }

    @Override
    public int getLength() {
        Long length = value.getSize();
        return length.intValue();
    }

    @Override
    public IArray getValue() {
        return value;
    }

    @Override
    public String getStringValue() {
        String result;
        if (isString()) {
            if (Character.TYPE.equals(getType())) {
                result = new String((char[]) value.getStorage());
            } else {
                result = getStringValue(0);
            }
        } else {
            result = getNumericValue().toString();
        }
        return result;
    }

    @Override
    public String getStringValue(int index) {
        if (isString()) {
            Object data = value.getStorage();
            return ((String[]) data)[0];
            // return ((String) Array.get(value.getStorage(), index));
        } else {
            return null;
        }
    }

    @Override
    public Number getNumericValue() {
        Number result = null;
        if (!isString()) {
            result = getNumericValue(0);
        }
        return result;
    }

    @Override
    public Number getNumericValue(int index) {
        Object localValue;
        if (isArray()) {
            localValue = Array.get(value.getArrayUtils().copyTo1DJavaArray(), index);
        } else {
            localValue = Array.get(value.getStorage(), index);
        }

        if (isString()) {
            localValue = Double.parseDouble((String) localValue);
        }

        return (Number) localValue;
    }

    @Override
    public void setStringValue(String val) {
        try {
            value = new DefaultArrayMatrix(factoryName, new String[] { val });
        } catch (InvalidArrayTypeException e) {
            Factory.getLogger().error("Unable to initialize data!", e);
        }
    }

    @Override
    public void setValue(IArray value) {
        this.value = value;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(getClass().getName()).append('{').append(name).append(": ");
        if (value == null) {
            builder.append(String.valueOf(value));
        } else {
            Object storage = value.getStorage();
            if (storage == null) {
                builder.append(String.valueOf(storage));
            } else if (storage.getClass().isArray()) {
                if (storage.getClass().getComponentType().isArray()) {
                    builder.append(Arrays.deepToString((Object[]) storage));
                } else if (storage instanceof boolean[]) {
                    builder.append(Arrays.toString((boolean[]) storage));
                } else if (storage instanceof char[]) {
                    builder.append(Arrays.toString((char[]) storage));
                } else if (storage instanceof byte[]) {
                    builder.append(Arrays.toString((byte[]) storage));
                } else if (storage instanceof short[]) {
                    builder.append(Arrays.toString((short[]) storage));
                } else if (storage instanceof int[]) {
                    builder.append(Arrays.toString((int[]) storage));
                } else if (storage instanceof long[]) {
                    builder.append(Arrays.toString((long[]) storage));
                } else if (storage instanceof float[]) {
                    builder.append(Arrays.toString((float[]) storage));
                } else if (storage instanceof double[]) {
                    builder.append(Arrays.toString((double[]) storage));
                } else {
                    builder.append(Arrays.toString((Object[]) storage));
                }
            } else {
                builder.append(storage);
            }
        }
        builder.append('}');
        return builder.toString();
    }
}

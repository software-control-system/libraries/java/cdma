package org.cdma.plugin.soleil.edf.external;

import org.cdma.dictionary.Context;

public class InverseValue extends AEdfNumberCorrector<Object> {

    @Override
    protected Object extractParameters(Context context) {
        // no parameter to extract
        return null;
    }

    @Override
    protected double getCorrectedValue(double value, Object parameters) {
        return 1 / value;
    }

}

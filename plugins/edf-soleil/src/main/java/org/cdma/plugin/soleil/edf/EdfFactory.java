/*******************************************************************************
 * Copyright (c) 2008 - ANSTO/Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * Norman Xiong (nxi@Bragg Institute) - initial API and implementation
 * Tony Lam (nxi@Bragg Institute) - initial API and implementation
 * Majid Ounsy (SOLEIL Synchrotron) - API v2 design and conception
 * Stephane Poirier (SOLEIL Synchrotron) - API v2 design and conception
 * Clement Rodriguez (ALTEN for SOLEIL Synchrotron) - API evolution
 * Gregory VIGUIER (SOLEIL Synchrotron) - API evolution
 * Raphael GIRARDOT (SOLEIL Synchrotron) - API evolution
 ******************************************************************************/
package org.cdma.plugin.soleil.edf;

import java.io.File;
import java.net.URI;

import org.cdma.AbstractFactory;
import org.cdma.Factory;
import org.cdma.arrays.DefaultArrayMatrix;
import org.cdma.dictionary.Key;
import org.cdma.dictionary.LogicalGroup;
import org.cdma.dictionary.Path;
import org.cdma.exception.CDMAException;
import org.cdma.exception.DataAccessException;
import org.cdma.exception.InvalidArrayTypeException;
import org.cdma.exception.NotImplementedException;
import org.cdma.exception.UriAccessException;
import org.cdma.interfaces.IArray;
import org.cdma.interfaces.IAttribute;
import org.cdma.interfaces.IDataItem;
import org.cdma.interfaces.IDataset;
import org.cdma.interfaces.IDatasource;
import org.cdma.interfaces.IGroup;
import org.cdma.interfaces.IKey;
import org.cdma.plugin.soleil.edf.navigation.EdfAttribute;
import org.cdma.plugin.soleil.edf.navigation.EdfDataItem;
import org.cdma.plugin.soleil.edf.navigation.EdfDataset;
import org.cdma.plugin.soleil.edf.navigation.EdfGroup;

import fr.soleil.lib.project.math.ArrayUtils;

public class EdfFactory extends AbstractFactory {

    public static final String NAME = "SoleilEDF";
    public static final String LABEL = "SOLEIL's EDF plug-in";
    public static final String DEBUG_INF = "CDMA_DEBUG";
    private static final String PLUG_VERSION = "1.0.0";
    private static final String DESC = "Manages EDF data files";
    private static EdfFactory factory;
    private EdfDatasource detector;

    public EdfFactory() {
    }

    public static EdfFactory getInstance() {
        synchronized (EdfFactory.class) {
            if (factory == null) {
                factory = new EdfFactory();
            }
        }
        return factory;
    }

    @Override
    public IDataset openDataset(URI uri) throws UriAccessException {
        EdfDataset dataset = new EdfDataset(uri.getPath());
        try {
            dataset.open();
        } catch (DataAccessException e) {
            throw new UriAccessException(e.getMessage(), e);
        }
        return dataset;
    }

    @Override
    public IArray createArray(Class<?> clazz, int[] shape) {
        IArray result = null;
        Object o = ArrayUtils.newInstance(clazz, shape);
        try {
            result = new DefaultArrayMatrix(EdfFactory.NAME, o);
        } catch (InvalidArrayTypeException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public IArray createArray(Class<?> clazz, int[] shape, Object storage) {
        IArray result = null;
        try {
            result = new DefaultArrayMatrix(EdfFactory.NAME, storage);
        } catch (InvalidArrayTypeException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public IArray createArray(Object javaArray) {
        IArray result = null;
        if (javaArray != null && javaArray.getClass().isArray()) {
            try {
                result = new DefaultArrayMatrix(EdfFactory.NAME, javaArray);
            } catch (InvalidArrayTypeException e) {
                Factory.getLogger().error("Unable to initialize data!", e);
            }
        }

        return result;
    }

    @Override
    public IArray createStringArray(String string) {
        throw new NotImplementedException();
    }

    @Override
    public IArray createDoubleArray(double[] javaArray) {
        IArray result = null;
        try {
            result = new DefaultArrayMatrix(EdfFactory.NAME, javaArray);
        } catch (InvalidArrayTypeException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public IArray createDoubleArray(double[] javaArray, int[] shape) {
        IArray result = null;
        try {
            result = new DefaultArrayMatrix(EdfFactory.NAME, javaArray);
        } catch (InvalidArrayTypeException e) {

            e.printStackTrace();
        }
        return result;
    }

    @Override
    public IArray createArrayNoCopy(Object javaArray) {
        throw new NotImplementedException();
    }

    @Override
    public IDataItem createDataItem(IGroup parent, String shortName, IArray array) throws InvalidArrayTypeException {
        EdfDataItem dataitem = new EdfDataItem(shortName, array);
        dataitem.setParent(parent);
        return dataitem;
//        EdfDataItem dataitem = new EdfDataItem(shortName);
//        dataitem.setCachedData(array, false);
//        dataitem.setParent(parent);
//        return dataitem;
    }

    @Override
    public IGroup createGroup(String shortName) throws DataAccessException {
        EdfGroup grp = new EdfGroup(new File(shortName));
        return grp;
    }

    @Override
    public IAttribute createAttribute(String name, Object value) {
        return new EdfAttribute(name, value);
    }

    @Override
    public IDataset createDatasetInstance(URI uri) throws CDMAException {
        return new EdfDataset(uri.getPath());
    }

    @Override
    public IDataset createEmptyDatasetInstance() throws DataAccessException {
        throw new NotImplementedException();
    }

    @Override
    public IKey createKey(String keyName) {
        return new Key(EdfFactory.getInstance(), keyName);
    }

    @Override
    public String getName() {
        return NAME;
    }

//    @Override
//    @Deprecated
//    public org.cdma.interfaces.IDictionary openDictionary(URI uri) throws UriAccessException {
//        throw new NotImplementedException();
//    }
//
//    @Override
//    @Deprecated
//    public org.cdma.interfaces.IDictionary openDictionary(String filepath) throws UriAccessException {
//        throw new NotImplementedException();
//    }

    @Override
    public IGroup createGroup(IGroup parent, String shortName) {
        throw new NotImplementedException();
    }

    @Override
    public LogicalGroup createLogicalGroup(String view, IDataset dataset, IKey key) {
        throw new NotImplementedException();
    }

    @Override
    public Path createPath(String path) {
        return new Path(this, path);
    }

    @Override
    public String getPluginLabel() {
        return LABEL;
    }

    @Override
    public IDatasource getPluginURIDetector() {
        synchronized (EdfDatasource.class) {
            if (detector == null) {
                detector = EdfDatasource.getInstance();
            }
        }
        return detector;
    }

//    @Override
//    @Deprecated
//    public org.cdma.interfaces.IDictionary createDictionary() {
//        throw new NotImplementedException();
//    }

    @Override
    public void processPostRecording() {
        // NOTHING TO DO
    }

    @Override
    public boolean isLogicalModeAvailable() {
        return true;
    }

    @Override
    public String getPluginVersion() {
        return PLUG_VERSION;
    }

    @Override
    public String getCDMAVersion() {
        return CDMA_VERSION;
    }

    @Override
    public String getPluginDescription() {
        return EdfFactory.DESC;
    }

}

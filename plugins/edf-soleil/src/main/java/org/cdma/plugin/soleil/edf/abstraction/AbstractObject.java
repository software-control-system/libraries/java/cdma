/*******************************************************************************
 * Copyright (c) 2008 - ANSTO/Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * Norman Xiong (nxi@Bragg Institute) - initial API and implementation
 * Tony Lam (nxi@Bragg Institute) - initial API and implementation
 * Majid Ounsy (SOLEIL Synchrotron) - API v2 design and conception
 * Stephane Poirier (SOLEIL Synchrotron) - API v2 design and conception
 * Clement Rodriguez (ALTEN for SOLEIL Synchrotron) - API evolution
 * Gregory VIGUIER (SOLEIL Synchrotron) - API evolution
 * Raphael GIRARDOT (SOLEIL Synchrotron) - API evolution
 ******************************************************************************/
package org.cdma.plugin.soleil.edf.abstraction;

import java.util.ArrayList;
import java.util.Collection;

import org.cdma.interfaces.IAttribute;
import org.cdma.interfaces.IContainer;
import org.cdma.interfaces.IGroup;
import org.cdma.plugin.soleil.edf.utils.StringUtils;

public abstract class AbstractObject implements IContainer {

    protected String name;
    protected IGroup parentGroup;
    protected Collection<IAttribute> attributes;

    public AbstractObject() {
        super();
        name = null;
        parentGroup = null;
        this.attributes = new ArrayList<IAttribute>();
    }

    @Override
    public AbstractObject clone() {
        try {
            AbstractObject clone = (AbstractObject) super.clone();
            if (attributes != null) {
                clone.attributes = new ArrayList<>(attributes);
            }
            return clone;
        } catch (CloneNotSupportedException e) {
            // Should never happen, because this class implements Cloneable
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void addOneAttribute(IAttribute attribute) {
        if ((attributes != null) && (attribute != null) && (!attributes.contains(attribute))) {
            attributes.add(attribute);
        }
    }

    @Override
    public Collection<IAttribute> getAttributeList() {
        if (attributes == null) {
            return null;
        }
        ArrayList<IAttribute> clone = new ArrayList<IAttribute>();
        clone.addAll(attributes);
        return clone;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean hasAttribute(String name, String value) {
        if (attributes != null) {
            for (IAttribute attribute : attributes) {
                if ((attribute != null)) {
                    if (StringUtils.isSameString(name, attribute.getName())
                            && StringUtils.isSameString(value, attribute.getStringValue())) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @Override
    public IAttribute getAttribute(String name) {
        if (attributes != null) {
            for (IAttribute attribute : attributes) {
                if ((attribute != null)) {
                    if (StringUtils.isSameString(name, attribute.getName())) {
                        return attribute;
                    }
                }
            }
        }
        return null;
    }

    @Override
    public boolean removeAttribute(IAttribute attribute) {
        if (attributes != null) {
            return attributes.remove(attribute);
        }
        return false;
    }

    @Override
    public IGroup getRootGroup() {
        IGroup group = parentGroup;
        while (true) {
            if ((group == null) || group.isRoot()) {
                break;
            }
            group = group.getParentGroup();
        }
        return group;
    }

    @Override
    public IGroup getParentGroup() {
        return parentGroup;
    }

    @Override
    public void setParent(IGroup group) {
        if ((parentGroup == null) || (!parentGroup.equals(group))) {
            this.parentGroup = group;
        }
    }

}

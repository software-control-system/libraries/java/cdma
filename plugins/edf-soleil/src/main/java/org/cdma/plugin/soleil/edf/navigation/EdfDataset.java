/*******************************************************************************
 * Copyright (c) 2008 - ANSTO/Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * Norman Xiong (nxi@Bragg Institute) - initial API and implementation
 * Tony Lam (nxi@Bragg Institute) - initial API and implementation
 * Majid Ounsy (SOLEIL Synchrotron) - API v2 design and conception
 * Stephane Poirier (SOLEIL Synchrotron) - API v2 design and conception
 * Clement Rodriguez (ALTEN for SOLEIL Synchrotron) - API evolution
 * Gregory VIGUIER (SOLEIL Synchrotron) - API evolution
 * Raphael GIRARDOT (SOLEIL Synchrotron) - API evolution
 ******************************************************************************/
package org.cdma.plugin.soleil.edf.navigation;

import java.io.File;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.cdma.dictionary.LogicalGroup;
import org.cdma.exception.DataAccessException;
import org.cdma.exception.NotImplementedException;
import org.cdma.exception.WriterException;
import org.cdma.interfaces.IAttribute;
import org.cdma.interfaces.IContainer;
import org.cdma.interfaces.IDataset;
import org.cdma.plugin.soleil.edf.EdfFactory;
import org.cdma.plugin.soleil.edf.dictionary.EdfLogicalGroup;

public class EdfDataset implements IDataset {

    private String filePath;
    private File file;
    private EdfGroup rootGroup;
    private final ConcurrentMap<String, LogicalGroup> logicalRootMap;
    private boolean open;
    private String title;

    public EdfDataset(String filePath) {
        logicalRootMap = new ConcurrentHashMap<>();
        open = false;
        setLocation(filePath);
        try {
            open();
        } catch (DataAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public synchronized void close() throws DataAccessException {
        open = false;
    }

    @Override
    public EdfGroup getRootGroup() {
        return rootGroup;
    }

    @Override
    public String getLocation() {
        return filePath;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public synchronized void setLocation(String location) {
        this.filePath = location;
        this.file = null;
        this.rootGroup = null;
    }

    @Override
    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public boolean sync() throws DataAccessException {
        return false;
    }

    @Override
    public void open() throws DataAccessException {
        if (!open) {
            logicalRootMap.clear();
            if ((filePath == null) || (filePath.trim().isEmpty())) {
                file = null;
                rootGroup = null;
                throw new DataAccessException("No root directory defined");
            } else {
                if ((file == null) || (rootGroup == null)) {
                    file = new File(filePath);
                    rootGroup = new EdfGroup(this, file);
                    // rootGroup.addSubgroup(new EdfGroup(file));
                    this.open = true;
                }
            }
        }
    }

    @Override
    public void save() {
        throw new NotImplementedException();
    }

    @Override
    public synchronized boolean isOpen() {
        return open;
    }

    @Override
    public boolean isDead() {
        return (file != null) && (!file.exists());
    }

    @Override
    public LogicalGroup getLogicalRoot(String view) {
        LogicalGroup logicalRoot;
        if (view == null) {
            logicalRoot = null;
        } else {
            logicalRoot = logicalRootMap.get(view);
            if (logicalRoot == null) {
                logicalRoot = new EdfLogicalGroup(view, null, null, this);
                LogicalGroup tmp = logicalRootMap.putIfAbsent(view, logicalRoot);
                if (tmp != null) {
                    // TODO maybe close logicalRoot
                    logicalRoot = tmp;
                }
            }
        }
        return logicalRoot;
    }

    @Override
    public String getFactoryName() {
        return EdfFactory.NAME;
    }

    @Override
    public void saveTo(String location) throws WriterException {
        throw new NotImplementedException();
    }

    @Override
    public void save(IContainer container) throws WriterException {
        throw new NotImplementedException();
    }

    @Override
    public void save(String parentPath, IAttribute attribute) throws WriterException {
        throw new NotImplementedException();
    }

    @Override
    public long getLastModificationDate() {
        return file.lastModified();
    }

    @Override
    protected void finalize() throws Throwable {
        try {
            logicalRootMap.clear();
        } catch (Exception e) {
            // just ignore: we only try to clean
        }
        super.finalize();
    }
}

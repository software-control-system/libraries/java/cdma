/*******************************************************************************
 * Copyright (c) 2008 - ANSTO/Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * Norman Xiong (nxi@Bragg Institute) - initial API and implementation
 * Tony Lam (nxi@Bragg Institute) - initial API and implementation
 * Majid Ounsy (SOLEIL Synchrotron) - API v2 design and conception
 * Stephane Poirier (SOLEIL Synchrotron) - API v2 design and conception
 * Clement Rodriguez (ALTEN for SOLEIL Synchrotron) - API evolution
 * Gregory VIGUIER (SOLEIL Synchrotron) - API evolution
 * Raphael GIRARDOT (SOLEIL Synchrotron) - API evolution
 ******************************************************************************/
package org.cdma.utilities.configuration.internal;

import java.util.Collection;
import java.util.List;

import org.cdma.exception.DataAccessException;
import org.cdma.exception.NoResultException;
import org.cdma.interfaces.IArray;
import org.cdma.interfaces.IAttribute;
import org.cdma.interfaces.IContainer;
import org.cdma.interfaces.IDataItem;
import org.cdma.interfaces.IDataset;
import org.cdma.interfaces.IGroup;
import org.jdom2.Attribute;
import org.jdom2.Element;

import fr.soleil.lib.project.ObjectUtils;

/**
 * <b>ConfigParameterCriterion implements ConfigParameter</b><br>
 * 
 * The aim of that class is to perform boolean test according an IDataset.<br/>
 * Two type can be done:<br/>
 * - CriterionType.EXIST will try to find a specific path in the IDataset <br/>
 * - CriterionType.EQUAL will compare values that targeted by a path to a referent one
 * <p>
 * This class is used when the ConfigManager will try to <b>determine compatibility between an IDataset and a
 * ConfigDataset</b>. It corresponds to "if" DOM element in the "criteria" section of the plugin configuration file.
 * 
 * @see ConfigParameter
 * @see CriterionType
 * 
 * @author rodriguez
 */
public class ConfigParameterCriterion implements ConfigParameter {
    private String mPath; // Path to seek in
    private CriterionType mType; // Type of operation to do
    private CriterionValue mTest; // Expected value of the test
    private String[] names, values; // attribute names and values
    private String mValue; // Compare value with the expected property

    public ConfigParameterCriterion(Element dom) {
        Attribute attribute;
        String value;
        String name;

        // Checking each attribute of the current node
        List<?> attributes = dom.getAttributes();
        for (Object att : attributes) {
            attribute = (Attribute) att;
            name = attribute.getName();
            value = attribute.getValue();

            // Case of a path
            if (TARGET_STRING.equalsIgnoreCase(name)) {
                mPath = value;
            }
            // Case of "exist" attribute
            else if (EXIST_STRING.equalsIgnoreCase(name)) {
                mType = CriterionType.EXIST;
                mTest = CriterionValue.valueOf(value.toUpperCase());
                mValue = ObjectUtils.EMPTY_STRING;
            }
            // Case of "equal" attribute
            else if (EQUAL_STRING.equalsIgnoreCase(name)) {
                mType = CriterionType.EQUAL;
                mTest = CriterionValue.TRUE;
                mValue = value;
            }
            // Case of "not equal" attribute
            else if (NOT_EQUAL_STRING.equalsIgnoreCase(name)) {
                mType = CriterionType.EQUAL;
                mTest = CriterionValue.FALSE;
                mValue = value;
            }
            // Case of "attribute" attribute
            else if (ATTRIBUTE_STRING.equalsIgnoreCase(name)) {
                mType = CriterionType.ATTRIBUTE;
                mTest = CriterionValue.TRUE;
                if (value == null) {
                    names = null;
                    values = null;
                } else {
                    String[] split = value.split(",");
                    names = new String[split.length];
                    values = new String[split.length];
                    int attrIndex = 0;
                    for (String val : split) {
                        int index = val.indexOf(':');
                        // at least 1 character for name
                        if (index > 0) {
                            names[attrIndex] = val.substring(0, index);
                            values[attrIndex++] = val.substring(index + 1).replace("*", ".*");
                        } else {
                            names[attrIndex] = value;
                            values[attrIndex++] = null;
                        }
                    }
                }
            } else {
                mTest = CriterionValue.NONE;
                mType = CriterionType.NONE;
            }
        }
    }

    @Override
    public CriterionType getType() {
        return mType;
    }

    @Override
    public String getValue(IDataset dataset) {
        CriterionValue result;
        switch (mType) {
            case EXIST:
                result = existPath(dataset);
                break;
            case EQUAL:
                result = getValuePath(dataset);
                break;
            case ATTRIBUTE:
                result = getAttributeValue(dataset);
                break;
            default:
                result = CriterionValue.NONE;
                break;
        }

        if (result.equals(mTest)) {
            result = CriterionValue.TRUE;
        } else {
            result = CriterionValue.FALSE;
        }

        return result.toString();
    }

    @Override
    public String getName() {
        return ObjectUtils.EMPTY_STRING;
    }

    @Override
    public String toString() {
        String result = "Type: " + mType + " Path: " + mPath + " Test: " + mTest + " Value: " + mValue;
        return result;
    }

    // ---------------------------------------------------------
    // / Private methods
    // ---------------------------------------------------------
    private CriterionValue existPath(IDataset dataset) {
        IGroup root = dataset.getRootGroup();
        IContainer result = null;
        try {
            result = root.findContainerByPath(mPath);
        } catch (NoResultException e) {
        }
        return result == null ? CriterionValue.FALSE : CriterionValue.TRUE;
    }

    private CriterionValue getValuePath(IDataset dataset) {
        IGroup root = dataset.getRootGroup();
        IContainer item;
        String value = null;
        CriterionValue result;
        try {
            item = root.findContainerByPath(mPath);
            if (item instanceof IDataItem) {
                IArray data = ((IDataItem) item).getData();
                if ((data != null) && (data.getElementType() == String.class)) {
                    value = (String) data.getObject(data.getIndex());
                }
            }
        } catch (NoResultException e) {
        } catch (DataAccessException e) {
        }

        if ((mValue != null) && mValue.equals(value)) {
            result = CriterionValue.TRUE;
        } else {
            result = CriterionValue.FALSE;
        }

        return result;
    }

    private CriterionValue getAttributeValue(IDataset dataset) {
        CriterionValue result = CriterionValue.FALSE;
        if ((names != null) && (values != null) && (names.length > 0) && (values.length == names.length)) {
            result = CriterionValue.TRUE;
            for (int i = 0; (i < names.length) && (result == CriterionValue.TRUE); i++) {
                result = getAttributeValue(dataset, names[i], values[i]);
            }
        } else {
            result = CriterionValue.FALSE;
        }
        return result;
    }

    private CriterionValue getAttributeValue(IDataset dataset, String attributeName, String attributeValue) {
        CriterionValue result = CriterionValue.FALSE;
        if (attributeName != null) {
            IGroup root = dataset.getRootGroup();
            IAttribute attribute;
            try {
                Collection<IContainer> containers;
                try {
                    containers = root.findAllContainerByPath(mPath);
                } catch (Exception e) {
                    containers = null;
                }
                if (containers != null) {
                    for (IContainer item : containers) {
                        if (item == null) {
                            attribute = null;
                        } else {
                            attribute = item.getAttribute(attributeName);
                        }
                        if (attribute != null) {
                            if (attributeValue == null) {
                                result = CriterionValue.TRUE;
                                break;
                            } else {
                                String value = attribute.getStringValue();
                                if ((value != null) && value.matches(attributeValue)) {
                                    result = CriterionValue.TRUE;
                                    break;
                                }
                            }
                        }
                    }
                }
            } catch (Exception e) {
                attribute = null;
                result = CriterionValue.FALSE;
            }
        }
        return result;
    }
}

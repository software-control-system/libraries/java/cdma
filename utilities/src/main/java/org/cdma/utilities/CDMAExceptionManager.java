package org.cdma.utilities;

import java.util.ArrayList;
import java.util.List;

import org.cdma.exception.CDMAException;

public class CDMAExceptionManager {

    private static final List<ICDMAExceptionHandler> LISTENERS = new ArrayList<ICDMAExceptionHandler>();

    // private static final Logger LOGGER = LoggerFactory.getLogger(CDMAExceptionManager.class);

    public static void addCDMAExceptionHandler(ICDMAExceptionHandler handler) {
        if (!LISTENERS.contains(handler)) {
            LISTENERS.add(handler);
        }
    }

    public static void removeCDMAExceptionHandler(ICDMAExceptionHandler handler) {
        if (LISTENERS.contains(handler)) {
            synchronized (LISTENERS) {
                LISTENERS.remove(handler);
            }
        }
    }

    public static void notifyHandler(Object source, CDMAException error) {
        // TODO Logging centralization LOGGER
        for (ICDMAExceptionHandler handler : LISTENERS) {
            handler.handleCDMAException(source, error);
        }
    }

}

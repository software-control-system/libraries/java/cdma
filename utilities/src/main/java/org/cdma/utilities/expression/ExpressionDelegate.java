package org.cdma.utilities.expression;

import org.cdma.Factory;
import org.cdma.dictionary.Context;

import fr.soleil.lib.project.expression.IExpressionParser;
import fr.soleil.lib.project.expression.exception.ExpressionEvaluationException;
import fr.soleil.lib.project.expression.jeval.JEvalExpressionParser;

public class ExpressionDelegate {

    public static final String DEFAULT_VARIABLE = "x";

    protected final String expression;
    protected final IExpressionParser parser;

    public ExpressionDelegate(Context context) {
        expression = (String) context.getParams()[0];
        parser = new JEvalExpressionParser();
    }

    public double getCorrectedValue(double value) {
        parser.removeVariable(DEFAULT_VARIABLE);
        parser.addVariable(DEFAULT_VARIABLE, Double.valueOf(value));
        double result;
        try {
            result = parser.parseExpression(expression);
        } catch (ExpressionEvaluationException e) {
            Factory.getLogger().error(e.getMessage(), e);
            result = Double.NaN;
        }
        return result;
    }
}
